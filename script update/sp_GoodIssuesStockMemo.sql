USE [DBKMS_08_01_2020]
GO
/****** Object:  StoredProcedure [dbo].[sp_GoodIssuesStockMemo]    Script Date: 1/15/2020 9:48:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROC [dbo].[sp_GoodIssuesStockMemo]
@PurchaseMemoID int,
@InvoiceAP nvarchar(50)
AS 
BEGIN
   --Declare varaible
   DECLARE @Id INT
   DECLARE @NewQty float=0
   --DECLARE @OldExpireDate date='2019-09-09'
   DECLARE @NewExpireDate date
   DECLARE @NewCost float=0
   DECLARE @ItemID int
   DECLARE @UomID int
   DECLARE @ExchangRate float
   DECLARE @CheckItem int=0
   DECLARE @PriceCurrencyID int
   DECLARE @PriceListID int
   DECLARE @LocalcurrencyID int
   DEClARE @WarehouseID int
     -- insert outgoing payment
   IF(@InvoiceAP='AP')
      begin
	       Insert into tbOutgoingPaymentVendor(BalanceDue,[Date],DocumentNo,DocumentType,OverdueDays,Total,TotalPayment,BranchID,CurrencyID,VendorID,WarehouseID,[Status],CashDiscount,TotalDiscount,Applied_Amount,ExchangeRate,Cash,SysCurrency)
           select Balance_Due*(-1),DueDate,InvoiceNo,'PC',CONVERT(float,0),Balance_Due*(-1),Balance_Due*(-1),BranchID,LocalCurrencyID,VendorID,WarehouseID,'open',CONVERT(float,0),CONVERT(float,0),CONVERT(float,0),ExchangeRate, Balance_Due*(-1),SysCurrencyID from tbPurchaseCreditMemo where PurchaseMemoID=@PurchaseMemoID
	  end 
   --Insert to temporery table
   select @WarehouseID=WarehouseID from tbPurchaseCreditMemo where PurchaseMemoID=@PurchaseMemoID
   INSERT INTO tpGoodReciptStock(ItemID,Cost,[ExpireDate],Qty,UomID,LocalcurrencyID,WarehouseID)
   SELECT pm.ItemID,pm.PurchasPrice,pm.[ExpireDate],pm.Qty,pm.UomID ,pm.LocalCurrencyID,@WarehouseID
          FROM tbPurchaseCreditMemoDetail pm
		  join tbItemMasterData item on item.ID=pm.ItemID
          where pm.PurchaseCreditMemoID=@PurchaseMemoID and item.Process!='Standard'
    --Transaction stock
   Select @ExchangRate= ExchangeRate from tbPurchaseCreditMemo where PurchaseMemoID=@PurchaseMemoID
   while(SELECT COUNT(*) FROM tpGoodReciptStock)>0
     BEGIN
	      --Transaction update stock in warehouse detail
	      Select Top 1 @Id = ID,@NewCost=Cost*@ExchangRate,@NewQty=Qty*(-1),@NewExpireDate=[ExpireDate],@ItemID=ItemID ,@UomID=UomID,@LocalcurrencyID=LocalcurrencyID From tpGoodReciptStock		  
		 --  Select @OldExpireDate=[ExpireDate] from tbWarehouseDetail where ItemID=@ItemID AND UomID=@UomID 
		  --uom factor
		declare @Factor float=0
		declare @GUomId int=0
		select @GUomId=GroupUomID from tbItemMasterData where ID=@ItemID
		select @Factor=isnull(Factor,0) from tbGroupDefindUoM where GroupUoMID=@GUomId and AltUOM=@UomID
		 --update stock item master
		Declare @Instock_master float 
		Select @Instock_master=isnull(StockIn,0) from tbItemMasterData where ID=@ItemID
		UPDATE tbItemMasterData 
		SET 
			StockIn=@Instock_master+@NewQty*@Factor
			where ID=@ItemID
	     
		--update stock in warehouse summary
		Declare @Instock_Whs float
		Select @Instock_Whs=isnull(InStock,0) from tbWarehouseSummary where ItemID=@ItemID and WarehouseID=@WarehouseID
		UPDATE tbWarehouseSummary 
			SET InStock=@Instock_Whs+@NewQty*@Factor
			where ItemID=@ItemID AND WarehouseID=@WarehouseID
		 
		 --update stock
	      UPDATE tbWarehouseDetail 
		      SET Cost=@NewCost,
		      InStock=InStock+@NewQty,
		       [ExpireDate]=@NewExpireDate
		      where ItemID=@ItemID	AND	Cost=@NewCost AND UomID=@UomID AND [ExpireDate]=@NewExpireDate and WarehouseID=@WarehouseID
   		  DELETE tpGoodReciptStock WHERE ID = @Id
		  --insert tbinevtoryAudit
			DECLARE @Process nvarchar(20)
			Declare @Tran_Value float=0
		    Declare @CumulativeQty float=0 
			 Declare @CumulativeValue float=0 
			Declare @AvgCost float=0
			select @Process=item.Process  from tbItemMasterData item where  item.ID=@ItemID
			IF(@Process='FIFO')
			  begin    
			     Select  @Tran_Value=ISNULL((@NewCost*@NewQty),0),@CumulativeQty=ISNULL(sum(Qty),0)+@NewQty,@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+(@NewQty*@NewCost) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID                                                   
		     	 INSERT INTO tbInventoryAudit(WarehouseID,BranchID,CurrencyID,ItemID,UomID,UserID,InvoiceNo,Cost,Qty,Process,SystemDate,[ExpireDate],TimeIn,Trans_Type,Trans_Valuse,CumulativeQty,CumulativeValue,Price)
		      	 select pur.WarehouseID,pur.BranchID,pur.SysCurrencyID,@ItemID,@UomID,pur.UserID,pur.InvoiceNo,@NewCost,@NewQty,@Process,pur.DocumentDate,@NewExpireDate,RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),'PC',@Tran_Value,@CumulativeQty,@CumulativeValue,CONVERT(float,0) from tbPurchaseCreditMemo pur 
			     where pur.PurchaseMemoID=@PurchaseMemoID
			  end
			ELSE
			  begin
			       Select  @Tran_Value=ISNULL((@NewCost*@NewQty),0),@CumulativeQty=ISNULL(sum(Qty),0)+@NewQty,@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+(@NewQty*@NewCost) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID   and WarehouseID=@WarehouseID
				   set @AvgCost=ISNULL(@CumulativeValue/NullIF(@CumulativeQty,0),0);                                                  
		     	   INSERT INTO tbInventoryAudit(WarehouseID,BranchID,CurrencyID,ItemID,UomID,UserID,InvoiceNo,Cost,Qty,Process,SystemDate,[ExpireDate],TimeIn,Trans_Type,Trans_Valuse,CumulativeQty,CumulativeValue,Price)
		      	   select pur.WarehouseID,pur.BranchID,pur.SysCurrencyID,@ItemID,@UomID,pur.UserID,pur.InvoiceNo,ROUND(@AvgCost,6),@NewQty,@Process,pur.DocumentDate,@NewExpireDate,RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),'PC',@Tran_Value,@CumulativeQty,@CumulativeValue,CONVERT(float,0) from tbPurchaseCreditMemo pur 
			       where pur.PurchaseMemoID=@PurchaseMemoID
			  end          
		 --Transaction update cost in price list detail
		  IF(@Process='FIFO' )
		  Begin
			 INSERT INTO tpPriceList(PirceListID,currencyID)
			  SELECT pl.ID,pl.CurrencyID  from tbPriceList pl
			  Declare @LastCost float=0
			  Declare @maxid int
			  select @maxid=max(ID) from tbInventoryAudit
			  where ItemID=@ItemID and UomID=@UomID
			  select @LastCost=ISNULL(inve.Cost,0) from tbInventoryAudit inve
			  where @ItemID=inve.ItemID and @UomID=inve.UomID and inve.ID=@maxid
			  While(select COUNT(*) from tpPriceList)>0
				begin
				
				   Select top 1 @PriceListID=PirceListID , @PriceCurrencyID=currencyID from tpPriceList
				   update tbPriceListDetail
				   set Cost=@LastCost
				   where ItemID=@ItemID and UomID=@UomID and PriceListID=@PriceListID
				   Delete tpPriceList where PirceListID=@PriceListID
				end
		  End
		  ELSE
		  Begin
				INSERT INTO tpPriceList(PirceListID,currencyID)
			   SELECT pl.ID,pl.CurrencyID  from tbPriceList pl
			   While(select COUNT(*) from tpPriceList)>0
				begin 
				   Select top 1 @PriceListID=PirceListID , @PriceCurrencyID=currencyID from tpPriceList
				   update tbPriceListDetail
				   set Cost=@AvgCost
				   where ItemID=@ItemID and UomID=@UomID and PriceListID=@PriceListID
				   Delete tpPriceList where PirceListID=@PriceListID
		    end
		  End

	 END
END












