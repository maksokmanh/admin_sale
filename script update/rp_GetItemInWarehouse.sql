USE [DBKMS_08_01_2020]
GO
/****** Object:  StoredProcedure [dbo].[rp_GetItemInWarehouse]    Script Date: 1/15/2020 9:30:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[rp_GetItemInWarehouse](
@Process char,
@WarehouseID int=0,
@ItemID int =0
)
AS
Begin
    IF(@WarehouseID>0)
	  Begin
	      IF @Process='M'
			Begin
				select 
				max(item.ID) as ID,
				max(item.Code) as Code,
				max(item.KhmerName) as KhmerName,
				max(item.EnglishName) as EnglishName,
				sum(wd.InStock) as InStock,
				max(wd.Committed) as [Committed],
				sum(wd.Ordered) as [Ordered],
				max(uom.Name) as Uom,
				max(item.[Image]) as [Image],
				max(item.Barcode) as Barcode,
				Case when max(wd.[ExpireDate])='2019-09-09' then ''
				     else Convert(nvarchar(10),max(wd.[ExpireDate]))
				End as [ExpireDate],
				max(wd.ItemID) as ItemID
				from tbItemMasterData item
				inner join tbWarehouseSummary wd on wd.ItemID= item.ID
				inner join tbUnitofMeasure uom on item.InventoryUoMID=uom.ID
				where wd.WarehouseID=@WarehouseID
				group by item.ID
			End
			ELSE
			Begin
			select
			wd.ID as ItemID,
			cur.[Description]+' '+ convert(varchar(max),wd.Cost) as Cost, 
			wd.InStock as InStock,
			wd.Committed as [Committed],
			wd.Ordered as [Ordered],
			uom.Name as Uom,
			wd.UomID as UomID,
			Case when wd.ExpireDate='2019-09-09' then ''
				     else Convert(nvarchar(10),wd.[ExpireDate])
				End as [ExpireDate],
			w.Name  as Warehouse,
			wd.ItemID as ID
			from tbItemMasterData item
				inner join tbWarehouseDetail wd on wd.ItemID= item.ID
				inner join tbUnitofMeasure uom on wd.UomID=uom.ID
				inner join tbCurrency cur on wd.CurrencyID=cur.ID
				inner join tbWarhouse w on wd.WarehouseID=w.ID
				where wd.WarehouseID=@WarehouseID and item.ID=@ItemID and wd.InStock>0
			End
	  End
	ELSE 
	   Begin
	       IF @Process='M'
			  Begin
				 select 
				 max(item.ID) as ID,
				 max(item.Code) as Code,
				 max(item.KhmerName) as KhmerName,
				 max(item.EnglishName) as EnglishName,
				 sum(wd.InStock) as InStock,
				 max(wd.Committed) as [Committed],
				 sum(wd.Ordered) as [Ordered],
				 max(uom.Name) as Uom,
				 max(item.[Image]) as [Image],
				 max(item.Barcode) as Barcode,
				 Case when max(wd.[ExpireDate])='2019-09-09' then ''
				     else Convert(nvarchar(10),max(wd.[ExpireDate]))
				End as [ExpireDate],
				 max(wd.ItemID) as ItemID
				 from tbItemMasterData item
					inner join tbWarehouseSummary wd on wd.ItemID= item.ID
					inner join tbUnitofMeasure uom on item.InventoryUoMID=uom.ID
					group by item.ID
			  End
			 ELSE
			  Begin
				select
				wd.ID as ItemID,
				cur.[Description]+' '+ convert(varchar(max),wd.Cost) as Cost, 
				wd.InStock as InStock,
				wd.Committed as [Committed],
				wd.Ordered as [Ordered],
				uom.Name as Uom,
				wd.UomID as UomID,
				Case when wd.[ExpireDate]='2019-09-09' then ''
				     else Convert(nvarchar(10),wd.[ExpireDate])
				End as [ExpireDate],
				w.Name as Warehouse,
				wd.ItemID  as ID
				from tbItemMasterData item
					inner join tbWarehouseDetail wd on wd.ItemID= item.ID
					inner join tbUnitofMeasure uom on wd.UomID=uom.ID
					inner join tbCurrency cur on wd.CurrencyID=cur.ID
					inner join tbWarhouse w on wd.WarehouseID=w.ID
					where item.ID=@ItemID and wd.InStock>0
            
			  End
	   End
End





