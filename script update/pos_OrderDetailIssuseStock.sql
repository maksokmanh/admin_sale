USE [DBKMS_08_01_2020]
GO
/****** Object:  StoredProcedure [dbo].[pos_OrderDetailIssuseStock]    Script Date: 1/16/2020 3:04:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[pos_OrderDetailIssuseStock](@OrderID int)
AS
BEGIN
--Insert data to tmp table
insert into tmpOrderDetail(ItemID,Qty,UomID,Cost,UnitPrice,CurrencyID,Process,WarehouseID,BranchID,UserID,InvoiceNo,ExchageRate)
select ItemID,isnull(Qty,0),od.UomID,od.Cost*o.ExchangeRate,od.UnitPrice*o.ExchangeRate,o.LocalCurrencyID,item.Process,o.WarehouseID,BranchID,UserOrderID,ReceiptNo,ExchangeRate
                                    from tbOrderDetail od 
                                    inner join tbOrder o on od.OrderID=o.OrderID
									inner join tbItemMasterData item on od.ItemID=item.ID
									where Qty!=0 and o.OrderID=@OrderID
--Declare goble vaiable
Declare @LineID int 
Declare @ItemID int
Declare @Qty float
Declare @UomID int
Declare @Cost money
Declare @UnitPrice money
Declare @CurrencyID int 
Declare @Process nvarchar(max)
Declare @WarehouseID int
Declare @BranchID int 
Declare @UserID int
Declare @InvoiceNo nvarchar(max)


While(select count(*) from tmpOrderDetail)>0
Begin
	declare @Instock float
	declare @Check_Stock float
	declare @CommitQty float
	declare @Remain float
	declare @IssusQty float
	declare @Order_Qty float
	declare @FIFOQty float
	declare @FIFOCost money
	declare @AvgCost money
	declare @Tran_Value money
	declare @CumulativeQty money
	declare @CumulativeValue money
	declare @ExpireDate date
	declare @StockMoveLineID int
	Declare @ExchangRate float

	select top 1 @LineID=ID,@ItemID=ItemID,@Qty=Qty,@UomID=UomID,@Cost=Cost,@UnitPrice=UnitPrice,@CurrencyID=CurrencyID,@Process=Process,@WarehouseID=WarehouseID,@BranchID=BranchID,@UserID=UserID,@InvoiceNo=InvoiceNo,@ExchangRate=ExchageRate from tmpOrderDetail
	
	IF(@Process!='Standard')
		Begin
		    INSERT INTO tbStockMoving(WarehouseID,UomID,UserID,SyetemDate,TimeIn,InStock,[Committed],Ordered,Available,CurrencyID,[ExpireDate],ItemID,Cost)
			SELECT WarehouseID,UomID,UserID,SyetemDate,TimeIn,InStock,[Committed],Ordered,Available,CurrencyID,[ExpireDate],ItemID,Cost from tbWarehouseDetail 
			                                                                                                                            where ItemID=@ItemID and UomID=@UomID
																																		order by [ExpireDate] ASC 
			
			while(select count(*) from tbStockMoving where InStock>0)>0
				begin
					declare @ItemID_SM int
					declare @UomID_SM int
					select top 1 @InStock=Instock,@FIFOCost=Cost,@ExpireDate=[ExpireDate],@StockMoveLineID=ID,@ItemID_SM=ItemID,@UomID_SM=UomID from tbStockMoving
					
					--uom factor
					declare @Factor float=0
					declare @GUomId int=0
					select @GUomId=GroupUomID from tbItemMasterData where ID=@ItemID
					select @Factor=isnull(Factor,0) from tbGroupDefindUoM where GroupUoMID=@GUomId and AltUOM=@UomID
					--update stock item master
						Declare @Order_master float 
						Declare @Instock_master_po float
						Select @Order_master=isnull(StockOnHand,0),@Instock_master_po=ISNULL(StockIn,0) from tbItemMasterData where ID=@ItemID
						UPDATE tbItemMasterData 
						SET 
							--StockCommit=@Order_master-@Qty*@Factor,
							StockIn=@Instock_master_po-@Qty*@Factor
							where ID=@ItemID
	     
					--update stock in warehouse summary
					Declare @Order_Whs float
					Declare @Instock_Whs_po float
					Select @Order_Whs=isnull([Committed],0),@Instock_Whs_po=isnull(InStock,0) from tbWarehouseSummary where ItemID=@ItemID and WarehouseID=@WarehouseID
		
					UPDATE tbWarehouseSummary 
						SET [Committed]=@Order_Whs-@Qty*@Factor,
							InStock=@Instock_Whs_po-@Qty*@Factor
						where ItemID=@ItemID AND WarehouseID=@WarehouseID
				   --End

					set @Check_Stock=@Instock-@Qty
					if(@Check_Stock<0)
						begin

						    set @Remain=(@Instock-@Qty)*(-1)
							set @IssusQty = @Qty-@Remain
							if(@Remain<=0)
								begin
									set @Qty=0
								end
							else
								begin
									set @Qty=@Remain--1
								end
							
							if(@Process='FIFO')
								begin
									
									update tbWarehouseDetail
									set InStock=isnull(@Instock-@IssusQty,0)  where WarehouseID=@WarehouseID and ItemID=@ItemID_SM and UomID=@UomID_SM and Cost=@FIFOCost and [ExpireDate]=@ExpireDate
									update tbStockMoving
									set InStock=isnull(@Instock-@IssusQty,0) where ID=@StockMoveLineID
									delete tbStockMoving where ID=@StockMoveLineID

									 --insert to inventory audit report
									select @Tran_Value=ISNULL((@IssusQty*@FIFOCost)*-1,0),@CumulativeQty=ISNULL(sum(Qty),0)+(@IssusQty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@IssusQty*@FIFOCost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
									insert into tbInventoryAudit(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
									values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@IssusQty*-1,@FIFOCost,@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,@ExpireDate)
									 --insert to revenues audit
									select @Tran_Value=ISNULL((@IssusQty*@FIFOCost),0),@CumulativeQty=ISNULL(sum(Qty),0)+(@IssusQty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@IssusQty*@FIFOCost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
									insert into tbRevenueItem(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
									values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@IssusQty,@FIFOCost,@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,@ExpireDate)
								end
						    else
								begin
									update tbWarehouseDetail
									set InStock=isnull(@Instock-@IssusQty,0)  where WarehouseID=@WarehouseID and ItemID=@ItemID_SM and UomID=@UomID_SM and [ExpireDate]=@ExpireDate
									update tbStockMoving
									set InStock=isnull(@Instock-@IssusQty,0) where ID=@StockMoveLineID
									delete tbStockMoving where ID=@StockMoveLineID

									 --insert to inventory audit report 
									select @FIFOCost=isnull(Cost,0) from tbPriceListDetail where ItemID=@ItemID_SM and UomID=@UomID_SM
									select @Tran_Value=ISNULL((@IssusQty*@FIFOCost)*-1,0),@CumulativeQty=ISNULL(sum(Qty),0)+(@IssusQty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@IssusQty*@FIFOCost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
									set @AvgCost=nullif(@CumulativeValue,0)/nullif(@CumulativeQty,0)
									insert into tbInventoryAudit(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
									values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@IssusQty*-1,@AvgCost,@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,@ExpireDate)
									 --insert to revenues audit
									select @FIFOCost=isnull(Cost,0) from tbPriceListDetail where ItemID=@ItemID_SM and UomID=@UomID_SM
									select @Tran_Value=ISNULL((@IssusQty*@FIFOCost),0),@CumulativeQty=ISNULL(sum(Qty),0)+(@IssusQty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@IssusQty*@FIFOCost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
									set @AvgCost=nullif(@CumulativeValue,0)/nullif(@CumulativeQty,0)
									insert into tbRevenueItem(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
									values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@IssusQty,isnull(@AvgCost,0),@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,@ExpireDate)
								
								end

						end
					else
						begin
							set @FIFOQty=@Instock-@Qty
							set @IssusQty=@Instock-@FIFOQty
							
						    --insert to inventory audit report
							if(@Process='FIFO')
								begin
									update tbWarehouseDetail
									set InStock=isnull(@FIFOQty,0)  where WarehouseID=@WarehouseID and ItemID=@ItemID_SM and UomID=@UomID_SM and Cost=@FIFOCost and [ExpireDate]=@ExpireDate
									update tbStockMoving
									set InStock=@FIFOQty where ID=@StockMoveLineID
									--insert to inventory audit
									select @Tran_Value=ISNULL((@IssusQty*@FIFOCost)*-1,0),@CumulativeQty=ISNULL(sum(Qty),0)+(@IssusQty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@IssusQty*@FIFOCost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
									insert into tbInventoryAudit(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
									values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@IssusQty*-1,@FIFOCost,@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,@ExpireDate)
									--insert to revenues audit
									select @Tran_Value=ISNULL((@IssusQty*@FIFOCost),0),@CumulativeQty=ISNULL(sum(Qty),0)+(@IssusQty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@IssusQty*@FIFOCost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
									insert into tbRevenueItem(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
									values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@IssusQty,@FIFOCost,@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,@ExpireDate)
								end
							else
								begin
								    update tbWarehouseDetail
									set InStock=isnull(@FIFOQty,0)  where WarehouseID=@WarehouseID and ItemID=@ItemID_SM and UomID=@UomID_SM and [ExpireDate]=@ExpireDate
									update tbStockMoving
									set InStock=@FIFOQty where ID=@StockMoveLineID

								    select @FIFOCost=isnull(Cost,0) from tbPriceListDetail where ItemID=@ItemID_SM and UomID=@UomID_SM
									--insert into revenuse
									select @Tran_Value=ISNULL((@IssusQty*@FIFOCost)*-1,0),@CumulativeQty=ISNULL(sum(Qty),0)+(@IssusQty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@IssusQty*@FIFOCost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
									set @AvgCost=nullif(@CumulativeValue,0)/nullif(@CumulativeQty,0)
									insert into tbInventoryAudit(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
									values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@IssusQty*-1,isnull(@AvgCost,0),@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,@ExpireDate)
									--insert into revenues audit
									select @Tran_Value=ISNULL((@IssusQty*@FIFOCost),0),@CumulativeQty=ISNULL(sum(Qty),0)+(@IssusQty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@IssusQty*@FIFOCost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
									set @AvgCost=nullif(@CumulativeValue,0)/nullif(@CumulativeQty,0)
									insert into tbRevenueItem(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
									values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@IssusQty,isnull(@AvgCost,0),@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,@ExpireDate)
									
									--Transaction update cost in price list detail
									  declare @PriceListID int
									  INSERT INTO tpPriceList(PirceListID,currencyID)
									  SELECT pl.ID,pl.CurrencyID  from tbPriceList pl
  
									  While(select COUNT(*) from tpPriceList)>0
										begin 
				
											Select top 1 @PriceListID=PirceListID from tpPriceList
											--select @Rate_p1=ex.Rate from tbExchangeRate ex where ex.CurrencyID=@PriceCurrencyID 
											update tbPriceListDetail
											set Cost=isnull(@AvgCost,0)
											where ItemID=@ItemID and UomID=@UomID and PriceListID=@PriceListID
											Delete tpPriceList where PirceListID=@PriceListID
										end		
								end
							
							delete tbStockMoving
							break;
						end
				end
				--update committed stock
				declare @CommitIssuse float
				select @CommitQty=max([Committed]) from tbWarehouseDetail where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
				select @CommitIssuse=sum([Qty]) from tbOrderDetail where ItemID=@ItemID and UomID=@UomID and OrderID=@OrderID
				update tbWarehouseDetail 
				set [Committed]=@CommitQty-@CommitIssuse
				where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID

				delete tmpOrderDetail where ID=@LineID
				
		End
	ELSE
		Begin
			----insert to inventory audit
			--select @Tran_Value=ISNULL((@Qty*@Cost)*-1,0),@CumulativeQty=ISNULL(sum(Qty),0)+(@Qty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@Qty*@Cost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
			--insert into tbInventoryAudit(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
			--values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@Qty*-1,@Cost,@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,GETDATE())
			--delete tmpOrderDetail where ID=@LineID
			--insert to reveues audit
			select @Tran_Value=ISNULL((@Qty*@Cost),0),@CumulativeQty=ISNULL(sum(Qty),0)+(@Qty*-1),@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+((@Qty*@Cost)*-1) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID
			insert into tbRevenueItem(WarehouseID,BranchID,UserID,ItemID,CurrencyID,UomID,InvoiceNo,Trans_Type,Process,SystemDate,TimeIn,Qty,Cost,Price,CumulativeQty,CumulativeValue,Trans_Valuse,[ExpireDate])
			values(@WarehouseID,@BranchID,@UserID,@ItemID,@CurrencyID,@UomID,@InvoiceNo,'SO',@Process,Getdate(),RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),@Qty,@Cost,@UnitPrice,@CumulativeQty,@CumulativeValue,@Tran_Value,GETDATE())
			delete tmpOrderDetail where ID=@LineID
		End
	
End

END













