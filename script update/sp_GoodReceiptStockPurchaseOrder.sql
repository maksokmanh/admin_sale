USE [DBKMS_08_01_2020]
GO
/****** Object:  StoredProcedure [dbo].[sp_GoodReceiptStockPurchaseOrder]    Script Date: 1/15/2020 9:52:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[sp_GoodReceiptStockPurchaseOrder]
@purchaseID int
AS 
BEGIN
   --Declare varaible
   DECLARE @Id INT
   DECLARE @NewQty float=0
   DECLARE @ItemID int
   DECLARE @UomID int
   Declare @WarehouseID int
   select @WarehouseID=WarehouseID from tbPurchaseOrder where PurchaseOrderID=@purchaseID
   --Insert to temporery table
   INSERT INTO tpGoodReciptStock(ItemID,Cost,[ExpireDate],Qty,UomID,LocalcurrencyID,WarehouseID)
   SELECT od.ItemID,od.PurchasPrice,od.[ExpireDate],od.Qty,od.UomID ,od.LocalCurrencyID,@WarehouseID FROM tbPurchaseOrderDetail od 
   where od.PurchaseOrderID=@purchaseID 
    --Transaction stock
   while(SELECT COUNT(*) FROM tpGoodReciptStock)>0
     BEGIN
	   Select Top 1 @Id = ID,@NewQty=Qty,@ItemID=ItemID,@UomID=UomID From tpGoodReciptStock	
			--uom factor
				declare @Factor float=0
				declare @GUomId int=0
				select @GUomId=GroupUomID from tbItemMasterData where ID=@ItemID
				select @Factor=isnull(Factor,0) from tbGroupDefindUoM where GroupUoMID=@GUomId and AltUOM=@UomID
		   --update stock item master
					Declare @Order_master float 
					Select @Order_master=isnull(StockOnHand,0) from tbItemMasterData where ID=@ItemID
					UPDATE tbItemMasterData 
					SET 
						StockOnHand=@Order_master+@NewQty*@Factor
						where ID=@ItemID
	     
	       --update stock in warehouse summary
		  Declare @Order_Whs float
		  Select @Order_Whs=isnull(Ordered,0) from tbWarehouseSummary where ItemID=@ItemID and WarehouseID=@WarehouseID
		  print @Order_Whs
		  UPDATE tbWarehouseSummary 
					SET Ordered=@Order_Whs+@NewQty*@Factor
					where ItemID=@ItemID AND WarehouseID=@WarehouseID
		 DELETE tpGoodReciptStock WHERE ID = @Id

	 END
END












