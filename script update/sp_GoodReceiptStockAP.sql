USE [DBKMS_08_01_2020]
GO
/****** Object:  StoredProcedure [dbo].[sp_GoodReceiptStockAP]    Script Date: 1/15/2020 8:23:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[sp_GoodReceiptStockAP]
@purchaseID int,
@type nvarchar(50)
AS 
BEGIN
   IF(@type='AP' or @type='PO')
     Begin
	     --Declare varaible
		DECLARE @Id INT
		DECLARE @NewQty float=0
		DECLARE @OldExpireDate date='2019-09-09'
		DECLARE @NewExpireDate date
		DECLARE @NewCost float=0
		DECLARE @ItemID int
		DECLARE @UomID int
		DECLARE @OldStock float
		DECLARE @ExchangRate float
		DECLARE @CheckItem int=0
		DECLARE @PriceCurrencyID int
		DECLARE @PriceListID int
		DECLARE @LocalcurrencyID int
		DECLARE @WarehouseID int
		DECLARE @Process nvarchar(20)
		select @WarehouseID=WarehouseID from tbPurchase_AP where PurchaseAPID=@purchaseID
		-- insert outgoing payment 
		Insert into tbOutgoingPaymentVendor(BalanceDue,[Date],DocumentNo,DocumentType,OverdueDays,Total,TotalPayment,BranchID,CurrencyID,VendorID,WarehouseID,[Status],CashDiscount,TotalDiscount,Applied_Amount,ExchangeRate,Cash,SysCurrency)
		select Balance_Due,DueDate,InvoiceNo,'PU',CONVERT(float,0),Balance_Due,Balance_Due,BranchID,LocalCurrencyID,VendorID,WarehouseID,'open',CONVERT(float,0),CONVERT(float,0),CONVERT(float,0),ExchangeRate,Balance_Due,SysCurrencyID from tbPurchase_AP where PurchaseAPID=@purchaseID
		--Insert to temporery table
		INSERT INTO tpGoodReciptStock(ItemID,Cost,[ExpireDate],Qty,UomID,LocalcurrencyID,WarehouseID)
		SELECT ap.ItemID,ap.PurchasPrice,ap.[ExpireDate],ap.Qty,ap.UomID ,ap.LocalCurrencyID,@WarehouseID FROM tbPurchaseAPDetail ap 
		where ap.Purchase_APID=@purchaseID  
		--Transaction stock
		Select @ExchangRate= ExchangeRate from tbPurchase_AP where PurchaseAPID=@purchaseID
		while(SELECT COUNT(*) FROM tpGoodReciptStock)>0
			BEGIN
				--Transaction update stock in warehouse detail
				Select Top 1 @Id = ID,@NewCost=Cost*@ExchangRate,@NewQty=Qty,@NewExpireDate=[ExpireDate],@ItemID=ItemID ,@UomID=UomID,@LocalcurrencyID=LocalcurrencyID From tpGoodReciptStock		  
				select @Process=item.Process  from tbItemMasterData item where  item.ID=@ItemID
				Select @OldExpireDate=[ExpireDate] from tbWarehouseDetail where ItemID=@ItemID AND UomID=@UomID and WarehouseID=@WarehouseID
				 
				  --uom factor
				declare @Factor float=0
				declare @GUomId int=0
				select @GUomId=GroupUomID from tbItemMasterData where ID=@ItemID
				select @Factor=isnull(Factor,0) from tbGroupDefindUoM where GroupUoMID=@GUomId and AltUOM=@UomID

				--update stock item master
				Declare @Order_master float 
				Declare @Instock_master_po float
				Select @Order_master=isnull(StockOnHand,0),@Instock_master_po=ISNULL(StockIn,0) from tbItemMasterData where ID=@ItemID
				IF(@type='PO')
					Begin
						UPDATE tbItemMasterData 
						SET
							StockOnHand=@Order_master-@NewQty*@Factor,
							StockIn=@Instock_master_po+@NewQty*@Factor
							where ID=@ItemID
					End
				Else
					Begin
						UPDATE tbItemMasterData 
						SET
							StockIn=@Instock_master_po+@NewQty*@Factor
							where ID=@ItemID
					End
				--update stock in warehouse summary
				Declare @Order_Whs float
				Declare @Instock_Whs_po float
				Select @Order_Whs=isnull(Ordered,0),@Instock_Whs_po=isnull(InStock,0) from tbWarehouseSummary where ItemID=@ItemID and WarehouseID=@WarehouseID
		
				IF(@type='PO')
					Begin
						UPDATE tbWarehouseSummary 
						SET Ordered=@Order_Whs-@NewQty*@Factor,
							InStock=@Instock_Whs_po+@NewQty*@Factor
						where ItemID=@ItemID AND WarehouseID=@WarehouseID
					End
				ELSE
					Begin
						UPDATE tbWarehouseSummary 
						SET
							InStock=@Instock_Whs_po+@NewQty*@Factor
						where ItemID=@ItemID AND WarehouseID=@WarehouseID
					End

				--Warehouse detail					  
				IF(@OldExpireDate='2019-09-09' or @OldExpireDate='0001-01-01') 
					Begin
					UPDATE tbWarehouseDetail 
						SET Cost=@NewCost,
						InStock=@NewQty,
						[ExpireDate]=@NewExpireDate,
						Factor=@Factor
						where ItemID=@ItemID AND UomID=@UomID and WarehouseID=@WarehouseID
						DELETE tpGoodReciptStock WHERE ID = @Id
					
					End
				ELSE
					Begin
					IF(@Process='FIFO')
						Begin
								Select @CheckItem=Count(*),@OldStock=max(InStock) from tbWarehouseDetail where ItemID=@ItemID AND Cost=@NewCost AND UomID=@UomID AND [ExpireDate]=@NewExpireDate and WarehouseID=@WarehouseID
								IF(@CheckItem!=0)
								--update stock
								Begin
									
									UPDATE tbWarehouseDetail 
									SET Cost=@NewCost,
									InStock=@OldStock+@NewQty,
									[ExpireDate]=@NewExpireDate,
									Factor=@Factor
									where ItemID=@ItemID	AND	Cost=@NewCost AND UomID=@UomID AND [ExpireDate]=@NewExpireDate and WarehouseID=@WarehouseID
									
									DELETE tpGoodReciptStock WHERE ID = @Id
								End
								ELSE
								--add new stock
								Begin
										INSERT INTO tbWarehouseDetail(WarehouseID,UomID,UserID,SyetemDate,TimeIn,InStock,[Committed],Ordered,Available,CurrencyID,[ExpireDate],ItemID,Cost,Factor)
										Select pur.WarehouseID,@UomID,pur.UserID,pur.PostingDate,CONVERT(time(0),getdate()),@NewQty,0,0,0,pur.SysCurrencyID,@NewExpireDate,@ItemID,@NewCost,@Factor from tbPurchase_AP pur where PurchaseAPID=@purchaseID 
										DELETE tpGoodReciptStock WHERE ID = @Id
								End
							
						End
					ELSE
						Begin
								Select @CheckItem=Count(*),@OldStock=max(InStock) from tbWarehouseDetail where ItemID=@ItemID AND UomID=@UomID AND [ExpireDate]=@NewExpireDate and WarehouseID=@WarehouseID
								IF(@CheckItem!=0)
								--update stock
								Begin
									UPDATE tbWarehouseDetail 
									SET Cost=@NewCost,
									InStock=@OldStock+@NewQty,
									[ExpireDate]=@NewExpireDate,
									Factor=@Factor
									where ItemID=@ItemID AND UomID=@UomID AND [ExpireDate]=@NewExpireDate and WarehouseID=@WarehouseID
									DELETE tpGoodReciptStock WHERE ID = @Id
								End
								ELSE
								--add new stock
								Begin
										INSERT INTO tbWarehouseDetail(WarehouseID,UomID,UserID,SyetemDate,TimeIn,InStock,[Committed],Ordered,Available,CurrencyID,[ExpireDate],ItemID,Cost,Factor)
										Select pur.WarehouseID,@UomID,pur.UserID,pur.PostingDate,CONVERT(time(0),getdate()),@NewQty,0,0,0,pur.SysCurrencyID,@NewExpireDate,@ItemID,@NewCost,@Factor from tbPurchase_AP pur where PurchaseAPID=@purchaseID 
										DELETE tpGoodReciptStock WHERE ID = @Id
								End
						End

					End
				--insert tbinevtoryAudit
			
				Declare @Tran_Value float=0
				Declare @CumulativeQty float=0 
				Declare @CumulativeValue float=0 
				Declare @AvgCost float=0
			
				IF(@Process='FIFO')
					begin    
						Select @Tran_Value=ISNULL((@NewCost*@NewQty),0),@CumulativeQty=ISNULL(sum(Qty),0)+@NewQty,@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+(@NewQty*@NewCost) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID and WarehouseID=@WarehouseID                                                   
		     			INSERT INTO tbInventoryAudit(WarehouseID,BranchID,CurrencyID,ItemID,UomID,UserID,InvoiceNo,Cost,Qty,Process,SystemDate,[ExpireDate],TimeIn,Trans_Type,Trans_Valuse,CumulativeQty,CumulativeValue,Price)
		      			select pur.WarehouseID,pur.BranchID,pur.SysCurrencyID,@ItemID,@UomID,pur.UserID,pur.InvoiceNo,@NewCost,@NewQty,@Process,pur.DocumentDate,@NewExpireDate,RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),'PU',@Tran_Value,@CumulativeQty,@CumulativeValue,CONVERT(float,0) from tbPurchase_AP pur 
						where pur.PurchaseAPID=@purchaseID
					end
				ELSE
					begin
						Select @Tran_Value=ISNULL((@NewCost*@NewQty),0),@CumulativeQty=ISNULL(sum(Qty),0)+@NewQty,@CumulativeValue=ISNULL(sum(Trans_Valuse),0)+(@NewQty*@NewCost) from tbInventoryAudit where ItemID=@ItemID and UomID=@UomID   and WarehouseID=@WarehouseID
						set @AvgCost=ISNULL(@CumulativeValue/NullIF(@CumulativeQty,0),0);                                                  
		     			INSERT INTO tbInventoryAudit(WarehouseID,BranchID,CurrencyID,ItemID,UomID,UserID,InvoiceNo,Cost,Qty,Process,SystemDate,[ExpireDate],TimeIn,Trans_Type,Trans_Valuse,CumulativeQty,CumulativeValue,Price)
		      			select pur.WarehouseID,pur.BranchID,pur.SysCurrencyID,@ItemID,@UomID,pur.UserID,pur.InvoiceNo,ROUND(@AvgCost,6),@NewQty,@Process,pur.DocumentDate,@NewExpireDate,RIGHT(CONVERT(VARCHAR, GETDATE(), 100),7),'PU',@Tran_Value,@CumulativeQty,@CumulativeValue,CONVERT(float,0) from tbPurchase_AP pur 
						where pur.PurchaseAPID=@purchaseID
					end      
				--Transaction update cost in price list detail
				INSERT INTO tpPriceList(PirceListID,currencyID)
				SELECT pl.ID,pl.CurrencyID  from tbPriceList pl
  
				While(select COUNT(*) from tpPriceList)>0
				begin 
					IF(@Process='FIFO')
					Begin
			  
						Select top 1 @PriceListID=PirceListID , @PriceCurrencyID=currencyID from tpPriceList				
						update tbPriceListDetail
						set Cost=@NewCost
						where ItemID=@ItemID and UomID=@UomID and PriceListID=@PriceListID
						Delete tpPriceList where PirceListID=@PriceListID
					End
					ELSE
					Begin
			   
						Select top 1 @PriceListID=PirceListID , @PriceCurrencyID=currencyID from tpPriceList			   
						update tbPriceListDetail
						set Cost=@AvgCost
						where ItemID=@ItemID and UomID=@UomID and PriceListID=@PriceListID
						Delete tpPriceList where PirceListID=@PriceListID
					End
				end		  		   
		 
			END
	 End
   ELSE
	    Begin
		   -- insert outgoing payment 
		      Insert into tbOutgoingPaymentVendor(BalanceDue,[Date],DocumentNo,DocumentType,OverdueDays,Total,TotalPayment,BranchID,CurrencyID,VendorID,WarehouseID,[Status],CashDiscount,TotalDiscount,Applied_Amount,ExchangeRate,Cash,SysCurrency)
		      select Balance_Due,DueDate,InvoiceNo,'PU',CONVERT(float,0),Balance_Due,Balance_Due,BranchID,LocalCurrencyID,VendorID,WarehouseID,'open',CONVERT(float,0),CONVERT(float,0),CONVERT(float,0),ExchangeRate,Balance_Due,SysCurrencyID from tbPurchase_AP where PurchaseAPID=@purchaseID
		 End
END












