#pragma checksum "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ae8546309fc939dea669015d0508961bdad4285b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Print_PrintTopSaleQuantity), @"mvc.1.0.view", @"/Views/Print/PrintTopSaleQuantity.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Print/PrintTopSaleQuantity.cshtml", typeof(AspNetCore.Views_Print_PrintTopSaleQuantity))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB;

#line default
#line hidden
#line 2 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models;

#line default
#line hidden
#line 5 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
#line 6 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Localization;

#line default
#line hidden
#line 7 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Account;

#line default
#line hidden
#line 8 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory;

#line default
#line hidden
#line 9 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.General;

#line default
#line hidden
#line 10 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Inventory;

#line default
#line hidden
#line 11 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Tables;

#line default
#line hidden
#line 12 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Banking;

#line default
#line hidden
#line 13 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.HumanResources;

#line default
#line hidden
#line 14 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.Category;

#line default
#line hidden
#line 15 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.PriceList;

#line default
#line hidden
#line 16 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Pagination;

#line default
#line hidden
#line 18 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#line 19 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ServicesClass;

#line default
#line hidden
#line 20 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Promotions;

#line default
#line hidden
#line 21 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Purchase;

#line default
#line hidden
#line 22 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ReportClass;

#line default
#line hidden
#line 23 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.POS;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ae8546309fc939dea669015d0508961bdad4285b", @"/Views/Print/PrintTopSaleQuantity.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"11cef691bd6d33588ed15b6e9923ab4a65a7850b", @"/Views/_ViewImports.cshtml")]
    public class Views_Print_PrintTopSaleQuantity : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<POS_WEB.Models.Services.ReportSale.TopSaleQuantity>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
  
    ViewData["Title"] = "PrintTopSaleQuantity";
    Layout = "_Layout_print";
    var num = 0;
    double totalqty = 0;
    
    var datefrom = Convert.ToDateTime(Model.First().DateFrom);
    var dateto = Convert.ToDateTime(Model.First().DateTo);

#line default
#line hidden
            BeginContext(333, 566, true);
            WriteLiteral(@"
<style>
    body {
        font-family: 'Khmer OS Battambang';
        font-size: 14px;
    }
    .tbfontsize {
        font-size: 16px;
    }
</style>
<div class=""text-center"" style=""margin-top: 20px; padding:10px;"">
    <h3>របាយការណ៍ទំនិញលក់ដាច់ជាងគេ</h3>
    <h3>Top Sale Quantity Report</h3>
</div>
<div class=""row"" style=""margin: 30px -1px 20px -10px"">
    <div class=""col-xs-5"">
        <table class=""tbfontsize"">
            <tr>
                <th>Branch</th>
                <td style=""padding: 0 5px 0 5px"">:</td>
                <td>");
            EndContext();
            BeginContext(900, 20, false);
#line 31 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
               Write(Model.First().Branch);

#line default
#line hidden
            EndContext();
            BeginContext(920, 157, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Date From</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(1078, 31, false);
#line 36 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
               Write(datefrom.ToString("dd-MM-yyyy"));

#line default
#line hidden
            EndContext();
            BeginContext(1109, 155, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Date To</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(1265, 29, false);
#line 41 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
               Write(dateto.ToString("dd-MM-yyyy"));

#line default
#line hidden
            EndContext();
            BeginContext(1294, 680, true);
            WriteLiteral(@"</td>
            </tr>
        </table>
    </div>
    <div class=""col-xs-2""></div>
    <div class=""col-xs-5"">
        <table>
            
        </table>
    </div>
</div>

<div style=""min-height:300px; border: 1px outset grey"">
    <table class=""table"">
        <thead>
            <tr style=""background:#5F758B;color:white;"">
                <th width=""30"">No.</th>
                <th>Code</th>
                <th>Khmer Name</th>
                <th>English Name</th>
                <th>Date Out</th>
                <th>Time Out</th>
                <th>Qty</th>
                <th>Uom</th>
            </tr>
        </thead>
        <tbody>
");
            EndContext();
#line 68 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
             foreach (var item in Model)
            {
                num++;
                totalqty += item.Qty;
                var dateout = Convert.ToDateTime(item.DateOut);

#line default
#line hidden
            BeginContext(2159, 46, true);
            WriteLiteral("                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(2206, 3, false);
#line 74 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
                   Write(num);

#line default
#line hidden
            EndContext();
            BeginContext(2209, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2241, 9, false);
#line 75 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
                   Write(item.Code);

#line default
#line hidden
            EndContext();
            BeginContext(2250, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2282, 14, false);
#line 76 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
                   Write(item.KhmerName);

#line default
#line hidden
            EndContext();
            BeginContext(2296, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2328, 16, false);
#line 77 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
                   Write(item.EnglishName);

#line default
#line hidden
            EndContext();
            BeginContext(2344, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2376, 30, false);
#line 78 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
                   Write(dateout.ToString("dd-MM-yyyy"));

#line default
#line hidden
            EndContext();
            BeginContext(2406, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2438, 12, false);
#line 79 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
                   Write(item.TimeOut);

#line default
#line hidden
            EndContext();
            BeginContext(2450, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2482, 8, false);
#line 80 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
                   Write(item.Qty);

#line default
#line hidden
            EndContext();
            BeginContext(2490, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(2522, 8, false);
#line 81 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
                   Write(item.Uom);

#line default
#line hidden
            EndContext();
            BeginContext(2530, 30, true);
            WriteLiteral("</td>\r\n                </tr>\r\n");
            EndContext();
#line 83 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
            }

#line default
#line hidden
            BeginContext(2575, 288, true);
            WriteLiteral(@"        </tbody>
    </table>
</div>
<div style=""float:right; border:1px outset grey; border-top:0px !important; padding:10px 64px 10px 40px; width:35%"">
    <table>
        <tr>
            <th>Sub Total</th>
            <td style=""padding: 0 40px 0 25px"">:</td>
            <td>");
            EndContext();
            BeginContext(2864, 8, false);
#line 92 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\Print\PrintTopSaleQuantity.cshtml"
           Write(totalqty);

#line default
#line hidden
            EndContext();
            BeginContext(2872, 44, true);
            WriteLiteral("</td>\r\n        </tr>\r\n    </table>\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<POS_WEB.Models.Services.ReportSale.TopSaleQuantity>> Html { get; private set; }
    }
}
#pragma warning restore 1591
