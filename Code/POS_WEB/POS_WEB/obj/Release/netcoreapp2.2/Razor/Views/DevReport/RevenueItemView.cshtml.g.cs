#pragma checksum "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\DevReport\RevenueItemView.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c7988febc644e3e2878ee9e96db24803ee6a3c1d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_DevReport_RevenueItemView), @"mvc.1.0.view", @"/Views/DevReport/RevenueItemView.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/DevReport/RevenueItemView.cshtml", typeof(AspNetCore.Views_DevReport_RevenueItemView))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB;

#line default
#line hidden
#line 2 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models;

#line default
#line hidden
#line 5 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
#line 6 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Localization;

#line default
#line hidden
#line 7 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Account;

#line default
#line hidden
#line 8 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory;

#line default
#line hidden
#line 9 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.General;

#line default
#line hidden
#line 10 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Inventory;

#line default
#line hidden
#line 11 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Tables;

#line default
#line hidden
#line 12 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Banking;

#line default
#line hidden
#line 13 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.HumanResources;

#line default
#line hidden
#line 14 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.Category;

#line default
#line hidden
#line 15 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.PriceList;

#line default
#line hidden
#line 16 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Pagination;

#line default
#line hidden
#line 18 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#line 19 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ServicesClass;

#line default
#line hidden
#line 20 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Promotions;

#line default
#line hidden
#line 21 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Purchase;

#line default
#line hidden
#line 22 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ReportClass;

#line default
#line hidden
#line 23 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.POS;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c7988febc644e3e2878ee9e96db24803ee6a3c1d", @"/Views/DevReport/RevenueItemView.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"11cef691bd6d33588ed15b6e9923ab4a65a7850b", @"/Views/_ViewImports.cshtml")]
    public class Views_DevReport_RevenueItemView : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "0", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("dx-viewport"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\DevReport\RevenueItemView.cshtml"
  
    ViewData["Title"] = "Revenue Item Report";

#line default
#line hidden
            BeginContext(57, 3272, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c7988febc644e3e2878ee9e96db24803ee6a3c1d8363", async() => {
                BeginContext(63, 3259, true);
                WriteLiteral(@"
  
    <style>
        .dx-checkbox-container {
            height: -webkit-max-content;
            height: -moz-max-content;
            height: max-content;
        }

        .options {
            padding: 20px;
            /*margin-top: 20px;*/
            background-color: rgba(191, 191, 191, 0.15);
        }

        .caption {
            font-size: 18px;
            font-weight: 500;
        }

        .option {
            margin-top: 10px;
        }

        /*#gridContainer {
            max-height: 550px;
        }*/
        /*#gridContainer .dx-datagrid-header-panel {
            padding: 0;
            background-color: rgba(85, 149, 222, 0.6);
        }*/

        #gridContainer .dx-datagrid-header-panel .dx-toolbar {
            margin: 0;
            padding-right: 20px;
            background-color: transparent;
        }

        #gridContainer .dx-datagrid-header-panel .dx-toolbar-items-container {
            height: 70px;
        }

        #g");
                WriteLiteral(@"ridContainer .dx-datagrid-header-panel .dx-toolbar-before .dx-toolbar-item:not(:first-child) {
            background-color: rgba(103, 171, 255, 0.6);
        }

        #gridContainer .dx-datagrid-header-panel .dx-toolbar-before .dx-toolbar-item:last-child {
            padding-right: 10px;
        }

        #gridContainer .dx-datagrid-header-panel .dx-selectbox {
            margin: 17px 10px;
        }

        #gridContainer .dx-datagrid-header-panel .dx-button {
            margin: 17px 0;
        }

        #gridContainer .informer {
            height: 70px;
            width: 130px;
            text-align: center;
            color: #fff;
        }

        #gridContainer .count {
            padding-top: 15px;
            line-height: 27px;
            margin: 0;
        }

        .input_datefrom, .input_dateto, .input_user, .input_branch {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transi");
                WriteLiteral(@"tion: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 1px solid #DDDDDD;
            width: 100%;
            min-width: 100%;
            height: 34px;
            font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
            font-size: 14px;
            border-radius: 3px;
        }

        .lable_branch, .lable_datefrom, .lable_dateto, .lable_user {
            font-family: Arial;
            font-size: 12px;
            font-weight: 700;
        }

        .btn-success, .btn-primary {
            margin-top: 25px;
            font-size: 12px;
            font-family: Arial;
            height: 28px;
        }

        table.dx-datagrid-table tr.dx-row.dx-column-lines.dx-header-row {
            background: #5F758B;
            color: #EEE;
            opacity: 1;
            line-height: 20px;
            tran");
                WriteLiteral("sition: all .2s ease-out;\r\n        }\r\n\r\n        table tr {\r\n            opacity: 1;\r\n            line-height: 20px;\r\n            transition: all .2s ease-out;\r\n        }\r\n    </style>\r\n\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3329, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(3331, 1754, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c7988febc644e3e2878ee9e96db24803ee6a3c1d12932", async() => {
                BeginContext(3357, 188, true);
                WriteLiteral("\r\n    <div class=\"demo-container\">\r\n        <div class=\"options\">\r\n            <div class=\"row\">\r\n                <div class=\"col-md-2\">\r\n                    <label class=\"lable_datefrom\">");
                EndContext();
                BeginContext(3546, 22, false);
#line 126 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\DevReport\RevenueItemView.cshtml"
                                             Write(Localizer["Date From"]);

#line default
#line hidden
                EndContext();
                BeginContext(3568, 202, true);
                WriteLiteral("</label>\r\n                    <input type=\"date\" class=\"input_datefrom\" id=\"datefrom\" />\r\n                </div>\r\n                <div class=\"col-md-2\">\r\n                    <label class=\"lable_dateto\">");
                EndContext();
                BeginContext(3771, 20, false);
#line 130 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\DevReport\RevenueItemView.cshtml"
                                           Write(Localizer["Date To"]);

#line default
#line hidden
                EndContext();
                BeginContext(3791, 198, true);
                WriteLiteral("</label>\r\n                    <input type=\"date\" class=\"input_dateto\" id=\"dateto\" />\r\n                </div>\r\n                <div class=\"col-md-2\">\r\n                    <label class=\"lable_branch\">");
                EndContext();
                BeginContext(3990, 22, false);
#line 134 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\DevReport\RevenueItemView.cshtml"
                                           Write(Localizer["By Branch"]);

#line default
#line hidden
                EndContext();
                BeginContext(4012, 111, true);
                WriteLiteral("</label>\r\n                    <select type=\"text\" class=\"input_branch\" id=\"bybranch\">\r\n                        ");
                EndContext();
                BeginContext(4123, 27, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c7988febc644e3e2878ee9e96db24803ee6a3c1d15429", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4150, 143, true);
                WriteLiteral("\r\n                    </select>\r\n                </div>\r\n                <div class=\"col-md-2\">\r\n                    <label class=\"lable_user\">");
                EndContext();
                BeginContext(4294, 22, false);
#line 140 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\DevReport\RevenueItemView.cshtml"
                                         Write(Localizer["User Name"]);

#line default
#line hidden
                EndContext();
                BeginContext(4316, 108, true);
                WriteLiteral(" </label>\r\n                    <select type=\"text\" class=\"input_user\" id=\"byuser\">\r\n                        ");
                EndContext();
                BeginContext(4424, 27, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "c7988febc644e3e2878ee9e96db24803ee6a3c1d17483", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(4451, 224, true);
                WriteLiteral("\r\n                    </select>\r\n                </div>\r\n                <div class=\"col-md-4 \">\r\n                    <button class=\"btn btn-xs btn-success\" id=\"filter\"><i class=\"fas fa-search\" style=\"margin-right:5px;\"></i>");
                EndContext();
                BeginContext(4676, 19, false);
#line 146 "D:\KERNEL COMPANY\WEB DEVELOPMENT\WEB RELEASE\pos-project\Code\Kernel POS_5\master\POS_WEB\POS_WEB\Views\DevReport\RevenueItemView.cshtml"
                                                                                                                         Write(Localizer["Filter"]);

#line default
#line hidden
                EndContext();
                BeginContext(4695, 11, true);
                WriteLiteral("</button>\r\n");
                EndContext();
                BeginContext(4814, 264, true);
                WriteLiteral(@"                </div>

            </div>
        </div>
        <div id=""gridContainer""></div>
        <div class=""options"">
            <div class=""option"">
                <div id=""autoExpand""></div>
            </div>
        </div>
        </div>
");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5085, 14372, true);
            WriteLiteral(@"
<script>
    $(function () {
        filterData();
        //Get branch
        $.ajax({
            url: ""/DevReport/GetBranch"",
            type: ""Get"",
            dataType: ""Json"",
            success: function (respones) {
                var data = '';
                $.each(respones, function (i, item) {
                    data +=
                        '<option value=""' + item.ID + '"">' + item.Name + '</option>';
                });
                $(""#bybranch"").append(data);
            }
        });
        $(""#bybranch"").on('change', function () {
            var branchid = $(this).val();
            // Get Employee
            $.ajax({
                url: ""/DevReport/GetEmployee"",
                type: ""Get"",
                dataType: ""Json"",
                data: { BranchID: branchid },
                success: function (respones) {
                    var data = '';
                    $(""#byuser option:not(:first-child)"").remove();
                    $.each(re");
            WriteLiteral(@"spones, function (i, item) {
                        data +=
                            '<option value=""' + item.ID + '"">' + item.Name + '</option>';
                    });
                    $(""#byuser"").append(data);
                }
            })
        });
        $('#filter').click(function () {
            filterData(this);
        });
        function filterData(button) {
            let btn_filter = $(button).children(""i.fa-search"");
            var date_from = $(""#datefrom"").val();
            var date_to = $(""#dateto"").val();
            var branch_id = $(""#bybranch"").val();
            var user_id = $(""#byuser"").val();
            $.ajax({
                url: ""/DevReport/RevenueItemReport"",
                type: 'GET',
                dataType: 'JSON',
                data: {
                    DateFrom: date_from,
                    DateTo: date_to,
                    BranchID: branch_id,
                    UserID: user_id
                },
                be");
            WriteLiteral(@"foreSend: function () {
                    btn_filter.addClass(""fa-spinner fa-spin"");
                },
                success: function (reponse) {
                    bindGrid(reponse);
                    btn_filter.removeClass(""fa-spinner fa-spin"");
                }
            });
        }
        function bindGrid(reponse) {
            var dataGrid = $(""#gridContainer"").dxDataGrid({
                dataSource: reponse,
                allowColumnReordering: true,
                showBorders: true,
                hoverStateEnabled: true,
                allowColumnResizing: true,
                columnResizingMode: ""nextColumn"",
                columnMinWidth: 50,
                columnAutoWidth: true,
                scrolling: {
                    columnRenderingMode: ""virtual""
                },
                grouping: {
                    autoExpandAll: false,
                },
                searchPanel: {
                    visible: true
                },
");
            WriteLiteral(@"                filterRow: {
                    visible: false,
                    applyFilter: ""auto""
                },
                paging: {
                    pageSize: 10
                },
                pager: {
                    showPageSizeSelector: true,
                    allowedPageSizes: [5, 10, 20, 50, 100],
                    showInfo: true
                },
                loadPanel: {
                    enabled: true
                },
                groupPanel: {
                    visible: false
                },

                columns: [
                    
                    {
                        caption: ""WH.Code"",
                        dataField: ""WarehouseCode"",
                        groupIndex:0
                    },
                    {
                        caption: ""Receipt No"",
                        dataField: ""InvoiceNo""
                    },
                    {
                        dataField: ""Barcode"",
   ");
            WriteLiteral(@"                     //width: 350,
                        caption: ""Barcode"",
                    },
                    {
                        dataField: ""ItemCode"",
                        //width: 250,
                        caption: ""Item Code"",

                    },
                    {
                        dataField: ""ItemName"",
                        //width: 250,
                        caption: ""Item Name"",

                    },
                    {
                        caption: ""Qty"",
                        dataField: ""Qty"",
                        alignment: ""right"",
                    },
                    {
                        caption: ""Uom"",
                        dataField: ""Uom"",
                        alignment: ""right"",
                    },
                    {
                        caption: ""Cost"",
                        dataField: ""Cost"",
                        alignment: ""right"",
                        format: {
            ");
            WriteLiteral(@"                type: ""fixedPoint"",
                            precision: 2
                        }
                    },
                    {
                        caption: ""Total Cost"",
                        dataField: ""TotalCost"",
                        alignment: ""right"",
                        format: {
                            type: ""fixedPoint"",
                            precision: 2
                        }
                    }
                ],

                summary: {
                    groupItems: [
                        {
                            showInColumn: ""TotalCost"",
                            column: ""TotalCost"",
                            summaryType: ""sum"",
                            displayFormat: ""{0}"",
                            showInGroupFooter: false,
                            alignByColumn: true
                        }
                       
                    ],
                    totalItems: [
                    ");
            WriteLiteral(@"    {
                            showInColumn: ""InvoiceNo"",
                            column: ""DateFrom"",
                            summaryType: ""max"",
                            displayFormat: ""Date From : {0}"",
                            alignment: ""left""
                        },
                        {
                            showInColumn: ""Barcode"",
                            column: ""DateTo"",
                            summaryType: ""max"",
                            displayFormat: ""Date To : {0}"",
                            alignment: ""left""
                        },
                        {
                            showInColumn: ""Cost"",
                            column: ""SSoldAmount"",
                            summaryType: ""max"",
                            displayFormat: ""Sold Amount :"",
                            alignment: ""left""
                        },
                        {
                            showInColumn: ""Cost"",
                   ");
            WriteLiteral(@"         column: ""SDiscountItem"",
                            summaryType: ""max"",
                            displayFormat: ""Discount Item   :"",
                            alignment: ""left""
                        },
                        {
                            showInColumn: ""Cost"",
                            column: ""SDiscountTotal"",
                            summaryType: ""max"",
                            displayFormat: ""Discount Total  :"",
                            alignment: ""left""
                        },
                        {
                            showInColumn: ""Cost"",
                            column: ""SVat"",
                            summaryType: ""max"",
                            displayFormat: ""Vat. Included   :"",
                            alignment: ""left""
                        },
                        {
                            showInColumn: ""Cost"",
                            column: ""SGrandTotal"",
                            summaryT");
            WriteLiteral(@"ype: ""max"",
                            displayFormat: ""Grand Total :"",
                            alignment: ""left""
                        },
                        {
                            showInColumn: ""Cost"",
                            column: ""STotalCost"",
                            summaryType: ""max"",
                            displayFormat: ""Total Cost :"",
                            alignment: ""left""
                        },
                        {
                            showInColumn: ""Cost"",
                            column: ""STotalProfit"",
                            summaryType: ""max"",
                            displayFormat: ""Total Profit :"",
                            alignment: ""left""
                        },
                        //value
                        {
                            showInColumn: ""TotalCost"",
                            column: ""SSoldAmount"",
                            summaryType: ""max"",
                            ");
            WriteLiteral(@"displayFormat: ""{0}"",
                            alignment: ""right""
                        },
                        {
                            showInColumn: ""TotalCost"",
                            column: ""SDiscountItem"",
                            summaryType: ""max"",
                            displayFormat: ""{0}"",
                            alignment: ""right""
                        },
                        {
                            showInColumn: ""TotalCost"",
                            column: ""SDiscountTotal"",
                            summaryType: ""max"",
                            displayFormat: ""{0}"",
                            alignment: ""right""
                        },
                        {
                            showInColumn: ""TotalCost"",
                            column: ""SVat"",
                            summaryType: ""max"",
                            displayFormat: ""{0}"",
                            alignment: ""right""
                      ");
            WriteLiteral(@"  },
                        {
                            showInColumn: ""TotalCost"",
                            column: ""SGrandTotal"",
                            summaryType: ""max"",
                            displayFormat: ""{0}"",
                            alignment: ""right""
                        },
                        {
                            showInColumn: ""TotalCost"",
                            column: ""STotalCost"",
                            summaryType: ""max"",
                            displayFormat: ""{0}"",
                            alignment: ""right""
                        },
                        {
                            showInColumn: ""TotalCost"",
                            column: ""STotalProfit"",
                            summaryType: ""max"",
                            displayFormat: ""{0}"",
                            alignment: ""right""
                        },

                    ]

                },
                export: {
           ");
            WriteLiteral(@"         enabled: true
                },
                onExporting: function (e) {
                    var workbook = new ExcelJS.Workbook();
                    var worksheet = workbook.addWorksheet('revenue-item-report');

                    DevExpress.excelExporter.exportDataGrid({
                        component: e.component,
                        worksheet: worksheet,
                        topLeftCell: { row: 4, column: 1 }
                    }).then(function (dataGridRange) {
                        // header
                        var headerRow = worksheet.getRow(2);
                        headerRow.height = 30;
                        worksheet.mergeCells(2, 1, 2, 9);
                        headerRow.getCell(1).value = 'Revenue Item Report';
                        headerRow.getCell(1).font = { name: 'Segoe UI Light', size: 22 };
                        headerRow.getCell(1).alignment = { horizontal: 'center' };

                        // footer
                      ");
            WriteLiteral(@"  //var footerRowIndex = dataGridRange.to.row + 2;
                        //var footerRow = worksheet.getRow(footerRowIndex);
                        //worksheet.mergeCells(footerRowIndex, 1, footerRowIndex, 8);
                        //footerRow.getCell(1).value = 'www.wikipedia.org';
                        //footerRow.getCell(1).font = { color: { argb: 'BFBFBF' }, italic: true };
                        //footerRow.getCell(1).alignment = { horizontal: 'right' };

                    }).then(function () {
                        // https://github.com/exceljs/exceljs#writing-xlsx
                        workbook.xlsx.writeBuffer().then(function (buffer) {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'revenue-item-report.xlsx');
                        });
                    });
                    e.cancel = true;
                },
                onToolbarPreparing: function (e) {
                    var dataGrid = e.component;
           ");
            WriteLiteral(@"         e.toolbarOptions.items.unshift({
                        location: ""before"",
                        template: function () {
                            return 
                        }
                    },
                        {
                            location: ""after"",
                            widget: ""dxButton"",
                            options: {
                                icon: ""refresh"",
                                onClick: function () {
                                    dataGrid.refresh();
                                }
                            }
                        }
                    );
                }

            }).dxDataGrid(""instance"");
            $(""#autoExpand"").dxCheckBox({
                value: false,
                text: ""Expand All Groups"",
                onValueChanged: function (data) {
                    dataGrid.option(""grouping.autoExpandAll"", data.value);
                }
            });
           
");
            WriteLiteral("        }\r\n\r\n    })\r\n\r\n</script>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
