﻿"use strict";
var connection = new signalR.HubConnectionBuilder()
    .withUrl("/realbox", { transport: signalR.HttpTransportType.LongPolling })
    .build();

//Initail connection
console.log('Trying to connect to hub!');
connection.onclose((e) => {
    console.log('Connection closed!', e);
});
connection.start().then(function () {
    console.log("time connected...");
    //connection.send('TimeWalker', 'csad');
}).catch(function (err) {
    return console.error('error start time->:'+err.toString());
});
var tableTimerGloble = [];
//Table timer
connection.on("TimeWalker", function (response) {
    console.log('run...')
    tableTimerGloble = response;
    $.each(tableTimerGloble, function (key, val) {
        
        $('#' + val.tableTime.id).html(val.tableTime.time);
        if (val.tableTime.status === 'A') {
            $('#' + val.tableTime.id).html("");
            $("#user" + val.tableTime.id).text("");
            let grids = $("#table-item-gridview .wrap-grid .grid");
            $.each(grids, function (i, grid) {
                let grid_id = $(grid).children().data('id');
                if (grid_id === val.tableTime.id) {
                    $(grid).css("background-color", "#CC9");
                    $(grid).find(".grid-image").find(".time").remove();
                }
            });
            $('#' + val.tableTime.id).hide();
        }
        else if (val.tableTime.status === 'B') {
            $('#' + val.tableTime.id).css("color", "white");
            let grids = $("#table-item-gridview .wrap-grid .grid");
            $.each(grids, function (i, grid) {
                let grid_id = $(grid).children().data('id');
                if (grid_id === val.tableTime.id) {
                    $(grid).css("background-color", "#e03454");
                    $(grid).find(".grid-image").find(".time").remove();
                    $(grid).find(".grid-image").append("<div class='time'>"+ val.tableTime.time +"</div>");
                }
            });
        }
        else if (val.tableTime.status === 'P') {
            $("#user" + val.tableTime.id).text("");
            $('#' + val.tableTime.id).css("color", "white");
            let grids = $("#table-item-gridview .wrap-grid .grid");
            $.each(grids, function (i, grid) {
                let grid_id = $(grid).children().data('id');
                if (grid_id === val.tableTime.id) {
                    $(grid).css("background-color", "#50A775");
                    $(grid).find(".grid-image").find(".time").remove();
                    $(grid).find(".grid-image").append("<div class='time'>" + val.tableTime.time + "</div>");
                }
            });
          
        }
    });

});

//real time push notification count order on receipt and user stay in table
connection.on("PushOrder", function (orders, tableid, user) {
    $("#user" + tableid).text(user + '...');
    let dropbox_order = $('#dropbox-order');
    if (orders.length !== 0) {

        $("#badge-order").text(orders.length);

        dropbox_order.children().remove();
        $.each(orders, function (i, order) {

            if (order.checkBill === 'Y') {
                dropbox_order.append('<a href="#" class="option " style="color:white;background:#50A775;" data-id=' + order.orderID + ' onclick=clickOnOrderNo(' + order.tableID + ',' + order.orderID + ')><i class="fas fa-receipt"></i> ' + order.orderNo + '/' + order.timeIn + ' </a>');
            }
            else {
                dropbox_order.append('<a href="#" class="option" data-id=' + order.orderID + ' onclick=clickOnOrderNo(' + order.tableID + ',' + order.orderID + ')><i class="fas fa-receipt"></i> ' + order.orderNo + '/' + order.timeIn + ' </a>');

            }
        });
    }
    else {

        $('#badge-order').text(orders.length);
        dropbox_order.children().remove();
    }

});
connection.on("ClearUserOrder", function (tableid) {
    $("#user" + tableid).text("");
});
//real time change status bill in client
connection.on("PushStatusBill", function (order_id) {

    let dropbox_order = $('#dropbox-order .option');
    $.each(dropbox_order, function (i, order) {
        let id = $(order).data("id");
        if (id === order_id) {

            $(order).css("background-color", "#50A775");
            $(order).css("color", "white");
        }
    });

});

//real time move table not work error
connection.on("GetTableAvailable", function (response) {

    let out = "";
    let tables = $.ajax({
        url: '/POS/GetTableAvailable',
        type: 'GET',
        async: false,
        data: { group_id: 0, tableid: table_info.id }
    }).responseJSON;
    $.each(tables, function (i, table) {

        out += "<label class='grid rc-container'>"
            + "<input data-id=" + table.ID + " name='table' type='radio'>"
            + "<span class='radiomark'></span>"
            + table.Name
            + "</label>";
    });
    $('#list-table-free').html(out);

});