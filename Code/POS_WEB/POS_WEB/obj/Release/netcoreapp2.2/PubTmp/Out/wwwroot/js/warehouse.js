 (function( $ ){
    let data_context = [    
        {
            "id": "001",
            "name_kh": "អាមេរិកេណូ ទឹកកក",
            "name_en": "Americano Ice",
            "price": 0.6,
            //"image": "",//pictures/americano Ice.jpg
            "discount": 0
        }, 
        {
            "id": "002",
            "name_kh": "កាពូស៊ីណូ ក្តៅ",
            "name_en": "Cappuccino Hot",
            "price": 1.3,
            "image": "pictures/cappuccino hot.jpg",
            "discount": 0
        },
        {
            "id": "003",
            "name_kh": "ខេមល ឡាតេ ក្តៅ",
            "name_en": "Caramel Latte Hot",
            "price": 4.5,
            "image": "pictures/caramel latte hot.jpg",
            "discount": 0
        },
        {
            "id": "004",
            "name_kh": "ខេមល ឡាតេ ទឹកកក",
            "name_en": "Caramel Latte Ice",
            "price": 1.1,
            "image": "pictures/Caramel Latte Ice.jpg",
            "discount": 0
        },
        {
            "id": "005",
            "name_kh": "តែបៃតង ឡាតេ ក្តៅ",
            "name_en": "Green Tea Latte Hot",
            "price": 3.2,
            "image": "pictures/green tea latte hot.jpg",
            "discount": 0.25
        },
        {
            "id": "006",
            "name_kh": "សូកូឡាខេមល ទឹកកក",
            "name_en": "Chocolate Caramel Ice",
            "price": 2.8,
            "image": "pictures/Chocolate Caramel Ice.jpg",
            "discount": 0.30
        },
        {
            "id": "007",
            "name_kh": "តែបៃតង ឡាតេ ទឹកកក",
            "name_en": "Green Tea Latte Ice",
            "price": 2.4,
            "image": "pictures/green tea latte Ice.jpg",
            "discount": 0.10
        },
        {
            "id": "008",
            "name_kh": "ម៉ូចា ទឹកកក",
            "name_en": "Mocha Ice",
            "price": 1.6,
            "image": "pictures/mocha Ice.jpg",
            "discount": 0
        },
        {
            "id": "009",
            "name_kh": "តែបៃតងទឹកឃ្មុំក្រូចឆ្មាទឹកកក",
            "name_en": "Honey Green Tea Lemon Ice",
            "price": 1.2,
            "image": "pictures/Honey Green Tea Lemon Ice.jpg",
            "discount": 0.15
        },
        {
            "id": "010",
            "name_kh": "តែប៉ាស្យុងរសជាតិទឹកឃ្មុំទឹកកក",
            "name_en": "Honey Passion Tea Ice",
            "price": 2.5,
            "image": "pictures/Honey Passion Tea Ice.jpg",
            "discount": 0
        },
        {
            "id": "011",
            "name_kh": "វេនីឡា ឡាតេ ក្តៅ",
            "name_en": "Vanilla Latte Hot",
            "price": 0.95,
            "image": "pictures/vanilla latte hot.jpg",
            "discount": 0
        },
            {
            "id": "012",
            "name_kh": "វេនីឡា ឡាតេ ទឹកកក",
            "name_en": "Vannilla latte Iced",
            "price": 0.99,
            "image": "pictures/Vannilla latte Iced.png",
            "discount": 0
        },
            {
            "id": "013",
            "name_kh": "ប៉ាស្យុងដាក់ទឹកឃ្មុំទឹកកក",
            "name_en": "Passion-with-honey Iced",
            "price": 1.3,
            "image": "pictures/passion-with-honey Iced.jpg",
            "discount": 0.13
        },
        {
            "id": "014",
            "name_kh": "កាហ្វេទឹកដោះគោខាប់ទឹកកក",
            "name_en": "Condense Milk Cafe Ice",
            "price": 0.97,
            "image": "pictures/condense milk cafe Ice.jpg",
            "discount": 0
        },
        {
            "id": "015",
            "name_kh": "តែក្រហមក្រហមទឹកកក",
            "name_en": "Red Tea Lemon Ice",
            "price": 0.95,
            "image": "pictures/Red Tea Lemon ice.jpg",
            "discount": 0
        },
         {
            "id": "016",
            "name_kh": "តែបៃតងក្រូចឆ្មាទឹកកក",
            "name_en": "Green Tea Lemon ice",
            "price": 0.89,
            "image": "pictures/Green Tea Lemon ice.jpg",
            "discount": 0
        }
    ];
    $.extend({
        getItems: function(){
            return data_context;
        },

    });
 })(jQuery);

class Warehouse extends Map
{
    constructor(option = 0){
        super();  
        if(_$_.isValidJSON(option)){
            this.addTable(option.table, option.jsons, option.key);
        } 
        
        Map.prototype.find = super.get;
        Warehouse.prototype.table = this.map;
        Warehouse.prototype.from = this.array;
        Warehouse.prototype.select = this.array;

        //Filter for objects by specified condition.
        Array.prototype.where = function(condition){
            return $.grep(this, function(json, i){
                return condition(json, i);
            });
        };

        //Find single object from Array by specified condition.
        Array.prototype.first = function(condition){
            return $.grep(this, function(json, i){
                return condition(json, i);
            })[0];
        };

        //Iterate through each of values in array.
        Array.prototype.each = function(callback){
            return $.each(this, function(i, v){
                return callback(i, v);
            });
        }
      
        //Let any object could be inserted into map as extension method.
        Map.prototype.insert = function(value, key){
            let _keys = [];
            if(key !== undefined){
                if(_$_.isValidJSON(value)){
                    if(this.size > 0){
                        _keys = [...this.keys()];
                        if(_$_.isUniform(value, this.get(_keys[0]))){
                            this.set(value[key], value); 
                        } else {
                            console.error("Object["+ key +" : "+ value[key] +"] has wrong data format.");
                        }

                    } else {
                        this.set(value[key], value);   
                    }
                }

                if(_$_.isValidArray(value)){
                    for(let item of value){
                        this.insert(item, key);
                    }
                }
            } else {
                if(_$_.isValidArray(value)){
                    this.insert(value[0], value[1]);
                }
            }   
        };

        //Extension method for modifying object.
        Map.prototype.update = function(value, key){
            if(key !== undefined){
                if(_$_.isValidJSON(value)){
                    if(this.has(value[key]) 
                    && _$_.isUniform(value, this.get(value[key]))){
                        this.set(value[key], value);  
                    } else {
                        console.error("Object["+ key +" : "+ value[key] +"] has not matched or wrong data format.");
                    }            
                }
                
                if(_$_.isValidArray(value)){      
                    for(let item of value){
                        this.update(item, key);               
                    }
                } 
            } else {
                if(_$_.isValidArray(value)){
                    this.update(value[0], value[1]);
                }
            }     
        }

        
    }

    //Using Map to add new table to warehouse
    addTable(table, jsons, key){
        if(Array.isArray(jsons) && key !== undefined){
            if(this.get(table) === undefined){
                this.set(table, new Map());
            }

            for(let json of jsons){
                if(_$_.isValidJSON(json)){
                    this.get(table).set(json[key], json);
                }        
            }
        }
    }

    //Use async promise as iteration param.
    async arrayAsync(table, promise){
        if(this.array(table).length > 0){
            return await Promise.all(this.array(table)).then(function(values){
                return promise(values);      
            });
        } else {
            console.error("Cannot iterate through empty array.");
        }
        
    }

    //Select all object in array value, if async param is defined then use the method 
    //as asynchronous iteration otherwise, as normal iteration.
    array(table, promise){
        if(promise!== undefined){
            return this.arrayAsync(table, promise);
        } else {
            let data = [];
            if(table !== undefined && this.has(table)){ 
                data = [...this.get(table).values()];
                if(data.length === 0){ return 0; }
                return data;
            }
            return 0;
        } 
    }

    map(table){
        if(this.get(table) === undefined){
            this.set(table, new Map());  
            setTimeout(() => {
                if(this.get(table).size === 0){
                    this.delete(table);
                } 
            }, 60000);     
        } 
       
        return this.get(table);
    }

    //Copy objects(s) from one table to another by specified key(s).
    async copy(from_table, to_table, data){
        if(this.get(from_table) === undefined){
            console.error("Source is invalid.");
            return;
        }

        if(this.get(to_table) === undefined){
            this.set(to_table, new Map());
        }

        let _keys = [];
        if(data === undefined){
            if(this.get(to_table).size > 0){
                _keys = [...this.get(to_table).keys()];
                if(_$_.isUniform(this.array(from_table)[0], this.get(to_table).get(_keys[0]))){
                    this.set(to_table, this.get(from_table)); 
                }
            } else {
                this.set(to_table, this.get(from_table));   
            }
            
        } 

        let item = {};
        if(!_$_.isValidArray(data)){
            if(!_$_.isValidJSON(data)){
                let key = data;
                if((item = this.table(from_table).get(key)) !== undefined){    
                    if(this.table(to_table).size > 0){
                        _keys = [...this.get(to_table).keys()];
                        if(_$_.isUniform(item, this.get(to_table).get(_keys[0]))){
                            await this.table(to_table).set(key, item); 
                        } else {
                            console.error("Object["+ key +"] has wrong data format.");
                        }

                    } else {
                        await this.table(to_table).set(key, item);   
                    }
                     
                } 
            }        
        } else {
            let value = data[0];
            let key_name = data[1];
            if(!_$_.isValidJSON(value)){
                for(let k of data){
                    this.copy(from_table, to_table, k);
                }
            } 
            
            if(data.length === 2){    
                if(_$_.isValidJSON(value)){
                    await this.copy(from_table, to_table, value[key_name]);
                }

                if(_$_.isValidArray(value)){
                    if(_$_.isValidJSON(value[0])){
                        for(let json of value){
                            this.copy(from_table, to_table, json[key_name]);
                        }
                    }
                    
                }

            }
    
        }

    }

    //Move objects(s) from one table to another by specified key(s).
    async cut(from_table, to_table, key){
        if(key === undefined){
            this.copy(from_table, to_table);
            let copied = false;
            for(let e of this.get(from_table).keys()){
                copied = this.get(to_table).has(e);      
            }

            if(copied){
               this.set(from_table, new Map());
            }
        } 

        if(!_$_.isValidArray(key)){
            this.copy(from_table, to_table, key);
            await this.get(from_table).delete(key);
        }

        if(_$_.isValidArray(key)){
            for(let k of key){
                this.cut(from_table, to_table, k);
            }
        } 
   
    }

    //Delete object from table by specified key(s).
    async remove(table, key){
        if(!_$_.isValidArray(key)){
            if(this.get(table).has(key)){
                await this.get(table).delete(key);
            }
        }

        if(_$_.isValidArray(key)){
            for(let k of key){
                this.remove(table, k);
            }
        }
    }

    //Insert an object into table.
    async insert(table, value, key){
        let _keys = [];
        if(key !== undefined){
            if(this.get(table) === undefined){
                this.set(table, new Map());                
            } 

            if(_$_.isValidJSON(value)){
                if(this.get(table).size > 0){
                    _keys = [...this.get(table).keys()];
                    if(_$_.isUniform(value, this.get(table).get(_keys[0]))){
                        await this.get(table).set(value[key], value); 
                    } else {
                        console.error("Object["+ key +" : "+ value[key] +"] has wrong data format.");
                    }

                } else {
                    await this.get(table).set(value[key], value);   
                }
            }

            if(_$_.isValidArray(value)){
                for(let item of value){
                    this.insert(table, item, key);
                }
            }
        } else {
            if(_$_.isValidArray(value)){
                await this.insert(table, value[0], value[1]);
            }
        }
    }

    //Modify existing object in table.
    async update(table, value, key){    
        if(key !== undefined){
            if(_$_.isValidJSON(value)){
                if(this.get(table).has(value[key]) 
                && _$_.isUniform(value, this.get(table).get(value[key]))){
                    await this.get(table).set(value[key], value);  
                } else {
                    console.error("Object["+ key +" : "+ value[key] +"] has not matched or wrong data format.");
                }            
            }
            
            if(_$_.isValidArray(value)){      
                for(let item of value){
                    this.update(table, item, key);               
                }
            } 
        } else {
            if(_$_.isValidArray(value)){
                await this.update(table, value[0], value[1]);
            }
        }     
    }
    
    //Search valid json objects by specified condition.
    async filter(table, condition){
        if(table !== undefined){
            if(typeof condition === "function"){
                return await $.grep(this.from(table), function(json, i){
                    return condition(json, i);
                });
            }
        }
    }

    searchText(table, column, value, equal = 0){
        return $.grep(this.from(table), function(json, i){
            if(typeof json[column] === "string" && typeof value === "string"){
                if(typeof equal !== undefined && option.equal){
                    return json[column].toLowerCase() === value.toLowerCase();
                } else {
                    return (json[column].toLowerCase()).includes(value.toLowerCase());
                }        
            }
        });
    }
    
    distinct(list, key){
       return list.filter((json, index) => {
           return list.map(json => json[key]).indexOf(json[key]) === index;
       });
    }
}







