#pragma checksum "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "322679a9a9a3e15d071abed1ee9b026fa1103e42"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_PriceList_Edit), @"mvc.1.0.view", @"/Views/PriceList/Edit.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/PriceList/Edit.cshtml", typeof(AspNetCore.Views_PriceList_Edit))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB;

#line default
#line hidden
#line 2 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models;

#line default
#line hidden
#line 5 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
#line 6 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Localization;

#line default
#line hidden
#line 7 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Account;

#line default
#line hidden
#line 8 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory;

#line default
#line hidden
#line 9 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.General;

#line default
#line hidden
#line 10 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Inventory;

#line default
#line hidden
#line 11 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Tables;

#line default
#line hidden
#line 12 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Banking;

#line default
#line hidden
#line 13 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.HumanResources;

#line default
#line hidden
#line 14 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.Category;

#line default
#line hidden
#line 15 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.PriceList;

#line default
#line hidden
#line 16 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Pagination;

#line default
#line hidden
#line 17 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Promotions;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"322679a9a9a3e15d071abed1ee9b026fa1103e42", @"/Views/PriceList/Edit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ad1c3559f4478d8ce7a5cacd53a7a35c6e4a29ab", @"/Views/_ViewImports.cshtml")]
    public class Views_PriceList_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<PriceLists>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("text-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-control"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("value", "0", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("txtcurrency"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-sm btn-danger"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ValidationSummaryTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ValidationMessageTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(19, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
  
    ViewData["Title"] = "Edit";

#line default
#line hidden
            BeginContext(61, 325, true);
            WriteLiteral(@"
<style>
    .fa {
        margin-right: 4px;
    }

    #stylei {
        color: blue;
    }

    .modal-header {
        background-color: blue;
    }

    .stylemodal {
        color: white;
    }

    b {
        color: white;
        font-family: Arial;
        margin-left: 10px;
    }
</style>
");
            EndContext();
            BeginContext(386, 1643, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "322679a9a9a3e15d071abed1ee9b026fa1103e429616", async() => {
                BeginContext(410, 70, true);
                WriteLiteral("\r\n    <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n            ");
                EndContext();
                BeginContext(480, 66, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("div", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "322679a9a9a3e15d071abed1ee9b026fa1103e4210074", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ValidationSummaryTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper);
#line 33 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper.ValidationSummary = global::Microsoft.AspNetCore.Mvc.Rendering.ValidationSummary.ModelOnly;

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-validation-summary", __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationSummaryTagHelper.ValidationSummary, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(546, 14, true);
                WriteLiteral("\r\n            ");
                EndContext();
                BeginContext(560, 35, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "322679a9a9a3e15d071abed1ee9b026fa1103e4211876", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
#line 34 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.ID);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(595, 85, true);
                WriteLiteral("\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label\">");
                EndContext();
                BeginContext(681, 17, false);
#line 36 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                        Write(Localizer["Name"]);

#line default
#line hidden
                EndContext();
                BeginContext(698, 28, true);
                WriteLiteral(" :</label>\r\n                ");
                EndContext();
                BeginContext(726, 45, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "322679a9a9a3e15d071abed1ee9b026fa1103e4214200", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
#line 37 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Name);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(771, 18, true);
                WriteLiteral("\r\n                ");
                EndContext();
                BeginContext(789, 59, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("span", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "322679a9a9a3e15d071abed1ee9b026fa1103e4215909", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ValidationMessageTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper);
#line 38 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.Name);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-validation-for", __Microsoft_AspNetCore_Mvc_TagHelpers_ValidationMessageTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(848, 107, true);
                WriteLiteral("\r\n            </div>\r\n\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label\">");
                EndContext();
                BeginContext(956, 21, false);
#line 42 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                        Write(Localizer["Currency"]);

#line default
#line hidden
                EndContext();
                BeginContext(977, 77, true);
                WriteLiteral(" :</label>\r\n                <div class=\"input-group\">\r\n\r\n                    ");
                EndContext();
                BeginContext(1054, 182, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("select", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "322679a9a9a3e15d071abed1ee9b026fa1103e4218270", async() => {
                    BeginContext(1152, 26, true);
                    WriteLiteral("\r\n                        ");
                    EndContext();
                    BeginContext(1178, 27, false);
                    __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "322679a9a9a3e15d071abed1ee9b026fa1103e4218701", async() => {
                    }
                    );
                    __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                    __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                    __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = (string)__tagHelperAttribute_3.Value;
                    __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
                    await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                    if (!__tagHelperExecutionContext.Output.IsContentModified)
                    {
                        await __tagHelperExecutionContext.SetOutputContentAsync();
                    }
                    Write(__tagHelperExecutionContext.Output);
                    __tagHelperExecutionContext = __tagHelperScopeManager.End();
                    EndContext();
                    BeginContext(1205, 22, true);
                    WriteLiteral("\r\n                    ");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.SelectTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper);
#line 45 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.CurrencyID);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
#line 45 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items = ViewBag.CurrencyID;

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-items", __Microsoft_AspNetCore_Mvc_TagHelpers_SelectTagHelper.Items, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1236, 319, true);
                WriteLiteral(@"
                    <div class=""input-group-append"">
                        <button type=""button"" class=""btn btn-sm"" data-toggle=""modal"" data-target=""#ModalCurrency""><i class=""fa fa-plus-circle"" id=""stylei""></i></button>
                    </div>
                </div>
                <div class=""text-danger"">");
                EndContext();
                BeginContext(1556, 13, false);
#line 52 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                    Write(ViewBag.Error);

#line default
#line hidden
                EndContext();
                BeginContext(1569, 257, true);
                WriteLiteral(@"</div>
            </div>

        </div>
    </div>
    <div class=""row pull-left"">
        <div class=""col-md-12"">
            <div class=""form-group"">
                <button type=""submit"" class=""btn btn-primary btn-sm""><i class=""fa fa-save""></i>");
                EndContext();
                BeginContext(1827, 17, false);
#line 60 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                                                                          Write(Localizer["Save"]);

#line default
#line hidden
                EndContext();
                BeginContext(1844, 27, true);
                WriteLiteral("</button>\r\n                ");
                EndContext();
                BeginContext(1871, 101, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "322679a9a9a3e15d071abed1ee9b026fa1103e4223542", async() => {
                    BeginContext(1923, 27, true);
                    WriteLiteral("<i class=\"fa fa-reply\"></i>");
                    EndContext();
                    BeginContext(1951, 17, false);
#line 61 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                                                                          Write(Localizer["Back"]);

#line default
#line hidden
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_5.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(1972, 50, true);
                WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_7.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2029, 224, true);
            WriteLiteral("\r\n<div class=\"modal fade\" id=\"ModalCurrency\">\r\n    <div class=\"modal-dialog modal-md\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <i class=\"fa fa-plus-circle stylemodal\"><b>");
            EndContext();
            BeginContext(2254, 21, false);
#line 70 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                                      Write(Localizer["Currency"]);

#line default
#line hidden
            EndContext();
            BeginContext(2275, 268, true);
            WriteLiteral(@"</b></i>
            </div>
            <div class=""modal-body"">
                <div class=""row"">
                    <div class=""col-md-12"">
                        <div class=""form-group row"">
                            <label class=""col-sm-3 control-label"">");
            EndContext();
            BeginContext(2544, 19, false);
#line 76 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                                             Write(Localizer["Symbol"]);

#line default
#line hidden
            EndContext();
            BeginContext(2563, 260, true);
            WriteLiteral(@" :</label>
                            <input type=""text"" class=""col-sm-9 form-control"" id=""txtsymbol"" />
                        </div>
                        <div class=""form-group row"">
                            <label class=""col-sm-3 control-label"">");
            EndContext();
            BeginContext(2824, 24, false);
#line 80 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                                             Write(Localizer["description"]);

#line default
#line hidden
            EndContext();
            BeginContext(2848, 369, true);
            WriteLiteral(@" :</label>
                            <input type=""text"" class=""col-sm-9 form-control"" id=""txtdescription"" />
                        </div>
                    </div>
                </div>
            </div>
            <div class=""modal-footer"">
                <button class=""btn btn-sm btn-primary"" onclick=""AddCurrency()""><i class=""fa fa-plus-circle""></i>");
            EndContext();
            BeginContext(3218, 16, false);
#line 87 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                                                                                           Write(Localizer["Add"]);

#line default
#line hidden
            EndContext();
            BeginContext(3234, 111, true);
            WriteLiteral("</button>\r\n                <button class=\"btn btn-sm btn-danger\" data-dismiss=\"modal\"><i class=\"fa fa-ban\"></i>");
            EndContext();
            BeginContext(3346, 18, false);
#line 88 "D:\POS-KRMS\Kernel POS\Kernel POS\master\POS_WEB\POS_WEB\Views\PriceList\Edit.cshtml"
                                                                                               Write(Localizer["Close"]);

#line default
#line hidden
            EndContext();
            BeginContext(3364, 1738, true);
            WriteLiteral(@"</button>
            </div>
        </div>
    </div>
</div>
<script>
    function AddCurrency() {
        var name = $(""#txtsymbol"").val();
        var des = $(""#txtdescription"").val();
        var cout = """";
        if (name == 0) {
            cout++;
            $(""#txtsymbol"").css(""border-color"", ""red"");
        } else {
            cout = 0;
            $(""#txtsymbol"").css(""border-color"", ""lightgrey"");
        }
        if (des == 0) {
            cout++;
            $(""#txtdescription"").css(""border-color"", ""red"");
        }
        else {
            cout = 0;
            $(""#txtdescription"").css(""border-color"", ""lightgrey"");
        }
        if (cout > 0) {
            cout = 0;
            return;
        }
        $.ajax({
            url: ""/Currency/AddCurrency"",
            type: ""POST"",
            dataType: ""Json"",
            data: { Symbol: name, Description: des },
            complete: function (respones) {
                $(""#txtsymbol"").val("""");
      ");
            WriteLiteral(@"          $(""#txtdescription"").val("""");
                $(""#txtcurrency option"").remove();
                $.ajax({
                    url: ""/Currency/GetCurrency"",
                    type: ""GET"",
                    dataType: ""JSON"",
                    success: function (respones) {
                        var data = """";

                        $.each(respones, function (i, item) {
                            data +=
                                '<option value=""' + item.ID + '"">' + item.Symbol + '</option>';
                        });
                        $(""#txtcurrency"").append(data);
                    }
                });
            }
        });
    }
</script>


");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<PriceLists> Html { get; private set; }
    }
}
#pragma warning restore 1591
