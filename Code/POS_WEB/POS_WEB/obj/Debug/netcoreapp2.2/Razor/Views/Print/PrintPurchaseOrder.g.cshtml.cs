#pragma checksum "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c0ce20a0309f5ab347993d638f723511a6ad3a25"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Print_PrintPurchaseOrder), @"mvc.1.0.view", @"/Views/Print/PrintPurchaseOrder.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Print/PrintPurchaseOrder.cshtml", typeof(AspNetCore.Views_Print_PrintPurchaseOrder))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB;

#line default
#line hidden
#line 2 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models;

#line default
#line hidden
#line 5 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
#line 6 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Localization;

#line default
#line hidden
#line 7 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Account;

#line default
#line hidden
#line 8 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory;

#line default
#line hidden
#line 9 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.General;

#line default
#line hidden
#line 10 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Inventory;

#line default
#line hidden
#line 11 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Administrator.Tables;

#line default
#line hidden
#line 12 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Banking;

#line default
#line hidden
#line 13 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.HumanResources;

#line default
#line hidden
#line 14 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.Category;

#line default
#line hidden
#line 15 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Inventory.PriceList;

#line default
#line hidden
#line 16 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Pagination;

#line default
#line hidden
#line 18 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using System.Security.Claims;

#line default
#line hidden
#line 19 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ServicesClass;

#line default
#line hidden
#line 20 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Promotions;

#line default
#line hidden
#line 21 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.Purchase;

#line default
#line hidden
#line 22 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.ReportClass;

#line default
#line hidden
#line 23 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\_ViewImports.cshtml"
using POS_WEB.Models.Services.POS;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c0ce20a0309f5ab347993d638f723511a6ad3a25", @"/Views/Print/PrintPurchaseOrder.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"11cef691bd6d33588ed15b6e9923ab4a65a7850b", @"/Views/_ViewImports.cshtml")]
    public class Views_Print_PrintPurchaseOrder : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<POS_WEB.Models.Services.Purchase.Print.PrintPurchaseAP>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
  
    ViewData["Title"] = "PurchaseOrder";
    Layout = "_Layout_print";
    var num = 0;
    double profit = 0;

    //var datefrom = Convert.ToDateTime(Model.First().DateFrom);
    //var dateto = Convert.ToDateTime(Model.First().DateTo);

#line default
#line hidden
            BeginContext(328, 534, true);
            WriteLiteral(@"
<style>
    body {
        font-family: 'Khmer OS Battambang';
        font-size: 14px;
    }
    .tbfontsize {
        font-size: 16px;
    }
</style>

<div class=""row"" style=""margin: 30px -1px 20px -10px"">
    <div class=""col-xs-5"">
        <div style=""margin-top: 20px; padding:10px;"">           
            <h3>Information Vendor</h3>
        </div>
        <table class=""tbfontsize"">
            <tr>
                <th>Name</th>
                <td style=""padding: 0 5px 0 5px"">:</td>
                <td>");
            EndContext();
            BeginContext(863, 24, false);
#line 31 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().VendorName);

#line default
#line hidden
            EndContext();
            BeginContext(887, 153, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Phone</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(1041, 19, false);
#line 36 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().Phone);

#line default
#line hidden
            EndContext();
            BeginContext(1060, 155, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Reff No</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(1216, 22, false);
#line 41 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().VendorNo);

#line default
#line hidden
            EndContext();
            BeginContext(1238, 155, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Address</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(1394, 21, false);
#line 46 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().Address);

#line default
#line hidden
            EndContext();
            BeginContext(1415, 417, true);
            WriteLiteral(@"</td>
            </tr>
        </table>
    </div>
    <div class=""col-xs-3""></div>
    <div class=""col-xs-4"">
        <div style=""margin-top: 20px; padding:10px; color:orangered"">           
            <h3>PO INVOICE</h3>
        </div>
        <table class=""tbfontsize"">
            <tr>
                <th>Invoice No</th>
                <td style=""padding: 0 5px 0 5px"">:</td>
                <td>");
            EndContext();
            BeginContext(1833, 21, false);
#line 59 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().Invoice);

#line default
#line hidden
            EndContext();
            BeginContext(1854, 160, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Posting Date</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(2015, 25, false);
#line 64 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().PostingDate);

#line default
#line hidden
            EndContext();
            BeginContext(2040, 155, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>DueDate</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(2196, 21, false);
#line 69 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().DueDate);

#line default
#line hidden
            EndContext();
            BeginContext(2217, 161, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>Document Date</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(2379, 26, false);
#line 74 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().DocumentDate);

#line default
#line hidden
            EndContext();
            BeginContext(2405, 155, true);
            WriteLiteral("</td>\r\n            </tr>\r\n            <tr>\r\n                <th>By User</th>\r\n                <td style=\"padding: 0 5px 0 5px\">:</td>\r\n                <td>");
            EndContext();
            BeginContext(2561, 22, false);
#line 79 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
               Write(Model.First().UserName);

#line default
#line hidden
            EndContext();
            BeginContext(2583, 552, true);
            WriteLiteral(@"</td>
            </tr>
        </table>
    </div>
</div>

<div style=""min-height:1000px;border: 1px outset grey"">
    <table class=""table"">
        <thead>
            <tr style=""background:#5F758B;color:white;"">
                <th width=""30"">No.</th>
                <th>Code</th>
                <th>Name(KH)</th>  
                <th>Qty</th>
                <th>UomName</th>
                <th>Price</th>
                <th>Discount</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
");
            EndContext();
#line 100 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
             foreach (var item in Model)
            {
                num++;
                //profit += item.Profit;

#line default
#line hidden
            BeginContext(3258, 46, true);
            WriteLiteral("                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(3305, 3, false);
#line 105 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                   Write(num);

#line default
#line hidden
            EndContext();
            BeginContext(3308, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(3340, 9, false);
#line 106 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                   Write(item.Code);

#line default
#line hidden
            EndContext();
            BeginContext(3349, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(3381, 14, false);
#line 107 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                   Write(item.KhmerName);

#line default
#line hidden
            EndContext();
            BeginContext(3395, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(3427, 8, false);
#line 108 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                   Write(item.Qty);

#line default
#line hidden
            EndContext();
            BeginContext(3435, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(3467, 12, false);
#line 109 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                   Write(item.UomName);

#line default
#line hidden
            EndContext();
            BeginContext(3479, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(3511, 10, false);
#line 110 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                   Write(item.Price);

#line default
#line hidden
            EndContext();
            BeginContext(3521, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(3553, 25, false);
#line 111 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                   Write(item.DiscountValue_Detail);

#line default
#line hidden
            EndContext();
            BeginContext(3578, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(3610, 14, false);
#line 112 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                   Write(item.Total_Sys);

#line default
#line hidden
            EndContext();
            BeginContext(3624, 30, true);
            WriteLiteral("</td>\r\n                </tr>\r\n");
            EndContext();
#line 114 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
            }

#line default
#line hidden
            BeginContext(3669, 277, true);
            WriteLiteral(@"        </tbody>
    </table>
</div>
<div style=""float:right; border:1px outset grey; border-top:0px !important; padding:10px 22px 10px 10px"">
    <table>
        <tr>
            <th>Sub Total</th>
            <td style=""padding: 0 40px 0 15px"">:</td>
            <td>");
            EndContext();
            BeginContext(3948, 25, false);
#line 123 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
            Write(Model.First().SysCurrency);

#line default
#line hidden
            EndContext();
            BeginContext(3974, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(3977, 27, false);
#line 123 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                                         Write(Model.First().Sub_Total_Sys);

#line default
#line hidden
            EndContext();
            BeginContext(4005, 144, true);
            WriteLiteral("</td>\r\n        </tr>\r\n        <tr>\r\n            <th>Discount Value</th>\r\n            <td style=\"padding: 0 40px 0 15px\">:</td>\r\n            <td>");
            EndContext();
            BeginContext(4151, 25, false);
#line 128 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
            Write(Model.First().SysCurrency);

#line default
#line hidden
            EndContext();
            BeginContext(4177, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(4180, 27, false);
#line 128 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                                         Write(Model.First().DiscountValue);

#line default
#line hidden
            EndContext();
            BeginContext(4208, 133, true);
            WriteLiteral("</td>\r\n        </tr>\r\n        <tr>\r\n            <th>Vat</th>\r\n            <td style=\"padding: 0 40px 0 15px\">:</td>\r\n            <td>");
            EndContext();
            BeginContext(4343, 25, false);
#line 133 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
            Write(Model.First().SysCurrency);

#line default
#line hidden
            EndContext();
            BeginContext(4369, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(4372, 22, false);
#line 133 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                                         Write(Model.First().TaxValue);

#line default
#line hidden
            EndContext();
            BeginContext(4395, 144, true);
            WriteLiteral("</td>\r\n        </tr>\r\n        <tr>\r\n            <th>Applied Amount</th>\r\n            <td style=\"padding: 0 40px 0 15px\">:</td>\r\n            <td>");
            EndContext();
            BeginContext(4541, 25, false);
#line 138 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
            Write(Model.First().SysCurrency);

#line default
#line hidden
            EndContext();
            BeginContext(4567, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(4570, 28, false);
#line 138 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                                         Write(Model.First().Applied_Amount);

#line default
#line hidden
            EndContext();
            BeginContext(4599, 139, true);
            WriteLiteral("</td>\r\n        </tr>\r\n        <tr>\r\n            <th>Total Due</th>\r\n            <td style=\"padding: 0 40px 0 15px\">:</td>\r\n            <td>");
            EndContext();
            BeginContext(4740, 25, false);
#line 143 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
            Write(Model.First().SysCurrency);

#line default
#line hidden
            EndContext();
            BeginContext(4766, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(4769, 29, false);
#line 143 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                                         Write(Model.First().Balance_Due_Sys);

#line default
#line hidden
            EndContext();
            BeginContext(4799, 162, true);
            WriteLiteral("</td>\r\n        </tr>\r\n    </table>    \r\n</div>\r\n\r\n<footer style=\"margin-top: 100px;\">\r\n    <div style=\"margin-top: 90px; padding:10px\">\r\n        <h5>Print Date : ");
            EndContext();
            BeginContext(4962, 35, false);
#line 150 "D:\WEB APPLICATIONS\ASP.NET CORE VS2017\admin_sale\Code\POS_WEB\POS_WEB\Views\Print\PrintPurchaseOrder.cshtml"
                    Write(DateTime.Now.ToString("dd-MM-yyyy"));

#line default
#line hidden
            EndContext();
            BeginContext(4997, 32, true);
            WriteLiteral("</h5>\r\n    </div>\r\n</footer>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<POS_WEB.Models.Services.Purchase.Print.PrintPurchaseAP>> Html { get; private set; }
    }
}
#pragma warning restore 1591
