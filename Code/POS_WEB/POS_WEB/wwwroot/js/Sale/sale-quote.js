﻿"use strict"
var db = new Warehouse();
var quote_details = [];
var _currency = "";
var _curencyID = 0;

$.each($("[data-date]"), function (i, t) {
    setDate(t, moment(Date.now()).format("YYYY-MM-DD"));
});

function setDate(selector, date_value) {
    var _date = $(selector);
    _date[0].valueAsDate = new Date(date_value);
    _date[0].setAttribute(
        "data-date",
        moment(_date[0].value)
            .format(_date[0].getAttribute("data-date-format"))
    );
}

// tb master
var master = {
    SQID: 0,
    CusID: 0,
    BranchID: 0,
    WarehouseID: 0,
    UserID: 0,
    LocalCurrencyID: 0,
    RefNo: "",
    InvoiceNo: "",
    ExchangeRate: 0,
    PostingDate: "",
    ValidUntilDate: "",
    DocumentDate: "",
    IncludeVat: false,
    Status: "open",
    Remarks: "",
    SubTotal: 0,
    SubTotal_Sys: 0,
    DisRate: 0,
    DisValue: 0,
    TypeDis: "Percent",
    VatRate: 0,
    VatValue: 0,
    TotalAmount: 0,
    TotalAmount_Sys: 0,
    SaleQuoteDetails: new Array()
}

db.insert("tb_master", master, "SQID");

// get warehouse
setWarehouse(0);
function setWarehouse(id) {
    $.ajax({
        url: "/Sale/GetWarehouse",
        type: "Get",
        dataType: "Json",
        success: function (response) {
            $("#ware-id option:not(:first-child)").remove();
            $.each(response, function (i, item) {
                $("#ware-id").append("<option value=" + item.ID + ">" + item.Name + "</option>");
                if (!!id && item.ID === id) {
                    $("#ware-id").val(id);
                }
            });
        }
    });
}
// get Invoice
$.ajax({
    url: "/Sale/GetSaleQuoteInvoice",
    type: "Get",
    dataType: "text",
    data: { IncludeVat: false },
    success: function (res) {
        $("#invoice-no").val(res);
        db.from("tb_master").where(function (json) {
            json.InvoiceNo = res;
        })
    }
});
// get Default currency
$.ajax({
    url: "/Sale/GetDefaultCurrency",
    type: "Get",
    dataType: "Json",
    async: false,
    success: function (response) {
        _currency = response.Description;
        _curencyID = response.ID;
        $(".cur-class").text(_currency);
    }
});
// date format
$("input[type='date']").on("change", function () {
    this.setAttribute(
        "data-date",
        moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
    )
});
// get  currency option
$.ajax({
    url: "/Sale/GetCurrency",
    type: "Get",
    dataType: "Json",
    success: function (response) {
        $("#cur-id option:not(:first-child)").remove();
        $.each(response, function (i, item) {
            if (item.ID == _curencyID) {
                $("#cur-id").append("<option selected value=" + item.ID + ">" + item.Description + "</option>");
            } else {
                $("#cur-id").append("<option  value=" + item.ID + ">" + item.Description + "</option>");
            }
        });
    }
});
// GetExchagne
var ExChange = [];
$.ajax({
    url: "/Sale/GetExchange",
    type: "Get",
    dataType: "Json",
    async: false,
    success: function (res) {
        $.each(res, function (i, item) {
            if (item.CurID == _curencyID) {
                $("#ex-id").val(1 + " " + _currency + " = " + item.SetRate + " " + item.CurName);
                db.from("tb_master").where(function (json) {
                    json.ExchangeRate = item.Rate;
                });
            }
        });
        ExChange = res;
    }
});

db.insert("tb_exchange", ExChange, "ID");

// Get Vat
$.ajax({
    url: "/Sale/GetVat",
    type: "Get",
    dataType: "Json",
    async: false,
    success: function (res) {
        db.insert("tb_vat", res, "ID");
    }
});
// Get Group Defind Uom
$.ajax({
    url: "/Sale/GetGDUom",
    type: "Get",
    dataType: "Json",
    async: false,
    success: function (res) {
        db.insert("tb_uom", res, "ID");
    }

});


$(".modal-header").on("mousedown", function (mousedownEvt) {
    var $draggable = $(this);
    var x = mousedownEvt.pageX - $draggable.offset().left,
        y = mousedownEvt.pageY - $draggable.offset().top;
    $("body").on("mousemove.draggable", function (mousemoveEvt) {
        $draggable.closest(".modal-dialog").offset({
            "left": mousemoveEvt.pageX - x,
            "top": mousemoveEvt.pageY - y
        });
    });
    $("body").one("mouseup", function () {
        $("body").off("mousemove.draggable");
    });
    $draggable.closest(".modal").one("bs.modal.hide", function () {
        $("body").off("mousemove.draggable");
    });
});

// get customer
$("#show-list-cus").click(function () {
    $("#ModalCus").modal("show");
    $.ajax({
        url: "/Sale/GetCustomer",
        type: "Get",
        dataType: "Json",
        success: function (response) {
            bindCustomer(response);
            $(".modal-dialog #find-cus").on("keyup", function (e) {
                let __value = this.value.toLowerCase().replace(/\s+/, "");
                let rex = new RegExp(__value, "gi");
                let __customers = $.grep(response, function (person) {
                    return person.Code.match(rex) || person.Name.toLowerCase().replace(/\s+/, "").match(rex)
                        || person.Phone.toLowerCase().replace(/\s+/, "").match(rex)
                        || person.Type.match(rex)
                });
                bindCustomer(__customers);
            });
        }
    });
});

// bind customer
function bindCustomer(response) {
    const paging = new Kernel.DataPaging({
        pageSize: 10
    }).render(response, function (summary) {
        $(".modal-dialog #list-cus tr:not(:first-child)").remove();
        var _index = 1;
        $.each(summary.data, function (i, item) {
            var tr = $("<tr></tr>").on("dblclick", function () {
                dblCus(this);
            }).on("click", function () {
                clickCus(this);
            });
            tr.append("<td style='min-width:3px;'>" + _index + "</td>")
                .append("<td hidden>" + item.ID + "</td>")
                .append("<td>" + item.Code + "</td>")
                .append("<td>" + item.Name + "</td>")
                .append("<td>" + item.Type + "</td>")
                .append("<td>" + item.Phone + "</td>");
            $(".modal-dialog #list-cus").append(tr);
            _index++;
        });
        $(".modal-dialog .ck-data-loading").hide();
    });
    $("#data-paging-customer").html(paging.selfElement);
}


//  dbl cus
function dblCus(c) {
    var id = parseInt($(c).find("td:eq(1)").text());
    var name = $(c).find("td:eq(3)").text();
    $("#cus-id").val(name); 
    db.from("tb_master").where(function (json) {
        json.CusID = id;
    });
    $("#item-id").prop("disabled", false);
    $("#barcode-reading").prop("disabled", false).focus();
    $("#ModalCus").modal("hide");
}
// click Cus
var _nameCus = "";
var _idCus = 0;
function clickCus(c) {
    _idCus = parseInt($(c).find("td:eq(1)").text());
    _nameCus = $(c).find("td:eq(3)").text();
    $(c).addClass("Active").siblings().removeClass("Active");
}

$("#cus-choosed").click(function () {
    $("#cus-id").val(_nameCus);
    db.from("tb_master").where(function (json) {
        json.CusID = _idCus;
    });

    $("#barcode-reading").prop("disabled", false).focus();
    $("#item-id").prop("disabled", false);
    $("#ModalCus").modal("hide");
    
});

$("#find-cus").keyup(function () {
    var name = this.value;
    setTimeout(() => {
        $.ajax({
            url: "/Sale/FindCustomer",
            type: "Get",
            dataType: "Json",
            data: { name: name },
            success: function (res) {
                BindCustomer(res);
            }
        });
    }, 500)
});

// include vat 
$("#include-vat-id").change(function () {
    var check = $(this).prop("checked");
    if (check == true) {
        // get Invoice
        $.ajax({
            url: "/Sale/GetSaleQuoteInvoiceVAT",
            type: "Get",
            dataType: "text",
            data: { IncludeVat: false },
            success: function (res) {
                $("#invoice-no").val(res);
                db.from("tb_master").where(function (json) {
                    json.InvoiceNo = res;
                })
            }
        });

    } else {
        // get Invoice
        $.ajax({
            url: "/Sale/GetSaleQuoteInvoice",
            type: "Get",
            dataType: "text",
            data: { IncludeVat: false },
            success: function (res) {
                $("#invoice-no").val(res);
                db.from("tb_master").where(function (json) {
                    json.InvoiceNo = res;
                });
            }
        });
    }
    $("#vat-id").prop("disabled", !check);
    updateLineItem(check, $("#sub-dis-id").val());
    setSummary(check, $("#sub-dis-id").val());

});
// change currency 
$("#cur-id").change(function () {
    var $this = this;
    if (this.value != 0) {
        var ex = db.from("tb_exchange").where(function (item) {
            return $this.value == item.CurID;
        });
        if (db.table("tb_master").size > 0) {
            db.from("tb_master").where(function (item) {
                item.ExchangeRate = ex[0].Rate;
            });
        }

        $("#ex-id").val(1 + " " + _currency + " = " + ex[0].SetRate + " " + ex[0].CurName);
        $(".cur-class").text(ex[0].CurName);
    }
});

function chooseItem(event) {
    if (event.type === "dblclick") {
        dblItem(this);
    } else {
        clickItem(this);
    }
}

// dbl item
function dblItem(d) {
    let _item = db.table("tb_item").get(item_master_id);  
    createDetail(_item);
    $(".modal-dialog-container").modal("hide");
}

$(window).on("keypress", function (e) {
    if (e.which === 13) {
        if (document.activeElement === this.document.getElementById("barcode-reading")) {
            let activeElem = this.document.activeElement;
            var master = db.from("tb_master");
            if (master.CurID != 0) {
                $.ajax({
                    url: "/Sale/GetItem",
                    type: "Get",
                    dataType: "Json",
                    data: { CusID: master[0].CusID },
                    success: function (res) {
                        if (res.status == "T") {
                            bindItemBarcode(res.ls_item, activeElem);
                            $("#include-vat-id").prop("disabled", false);
                        }
                    }
                });
            }
            
        }
    }
});

function bindItemBarcode(items, input) { 
    db.table("tb_item").clear();
    db.insert("tb_item", items, "ID");
    $.each(items, function (i, item) {
        if (item.Barcode === input.value.trim()) {
            createDetail(item);
        }
    });
    input.value = "";
}

// click item
let item_master_id = 0, item_copy_type = 0;
function clickItem(c) {
    item_master_id = $(c).data("id");
    $(c).addClass("highlight").siblings().removeClass("highlight");
}

// choose item
$("#item-id").click(function () {
    $("#ModalItem").modal("show");
    var master = db.from("tb_master");
    if (master.CurID != 0) {
        $.ajax({
            url: "/Sale/GetItem",
            type: "Get",
            dataType: "Json",
            data: { CusID: master[0].CusID },
            success: function (res) {
                if (res.status == "T") {
                    if (res.ls_item.length > 0) {
                        bindItem(res.ls_item);
                        $(".modal-dialog #find-item").on("keyup", function (e) {
                            let __value = this.value.toLowerCase().replace(/\s+/, "");
                            let items = $.grep(res.ls_item, function (item) {
                                return item.Barcode === __value || item.KhmerName.toLowerCase().replace(/\s+/, "").includes(__value)
                                    || item.UnitPrice == __value || item.Code.toLowerCase().replace(/\s+/, "").includes(__value)
                                    || item.UoM.toLowerCase().replace(/\s+/, "").includes(__value);
                            });
                            bindItem(items);
                        });
                        db.table("tb_item").clear();
                        db.insert("tb_item", res.ls_item, "ID");
                    }

                    if (db.table("tb_vat").size > 0) {
                        $("#include-vat-id").prop("disabled", false);
                    }
                    setTimeout(() => {
                        $(".modal-dialog #find-item").focus();
                    }, 300);
                }
            }
        });
    }
});

function bindItem(items) {
    const paging = new Kernel.DataPaging({
        pageSize: 10
    }).render(items, function (summary) {
        $(".modal-dialog #list-item tr:not(:first-child)").remove();
        let _index = 1;
        if (items.length > 0) {
            $.each(summary.data, function (i, json) {
                var tr = $("<tr data-id=" + json.ID + "></tr>").on("dblclick click", chooseItem);
                tr.append("<td style='min-width:3px; width:3px;'>" + _index + "</td>")
                    .append("<td>" + json.Code + "</td>")
                    .append("<td>" + json.KhmerName + "</td>")
                    .append("<td>" + json.Currency + "  " + json.UnitPrice + "</td>")
                    .append("<td>" + json.UoM + "</td>")
                    .append("<td>" + json.Barcode + "</td>")
                $(".modal-dialog #list-item").append(tr);
                _index++;
            });
        }
        $(".ck-data-loading").hide();
    });
    $(".modal-dialog #data-paging-item").html(paging.selfElement);
}


$("#item-master-choosed").click(function () {
    let _item = db.table("tb_item").get(item_master_id);
    createDetail(_item);
    $(".modal-dialog-container").modal("hide");

});

$("#sub-dis-id").on("change", function () {
    updateLineItem($("#include-vat-id").prop("checked"), this.value);
    setSummary($("#include-vat-id").prop("checked"), this.value);
});


$(".summary").on("keyup", function () {
    setSummary($("#include-vat-id").prop("checked"), $("#sub-dis-id").find("option:selected").val());
});

function createDetail(master) {
    const _detail = {
        LineID: Date.now(),
        SQDID: 0,
        SQID: 0,
        ItemID: master.ItemID,
        ItemCode: master.Code,
        ItemNameKH: master.KhmerName,
        ItemNameEN: master.EnglishName,
        ItemType: master.ItemType,
        Qty: 1,
        GUomID: master.GroupUomID,
        UomID: master.UomID,
        UomName: master.UoM,
        OpenQty: 1,
        PrintQty: 0,
        Cost: master.Cost,
        UnitPrice: master.UnitPrice,
        DisRate: 0,
        DisValue: 0,
        TypeDis: "Percent",
        VatRate: 0,
        VatValue: 0,
        Total: 0,
        Total_Sys: 0,
        Process: master.Process,
        CurrencyID: master.CurrencyID,
        WarehouseID: 0,
        BranchID: 0,
        UserID: 0,
        ExpireDate: "2000-01-01",
        InvoiceNo: "",
        Factor: 0,
        Remarks: "",
        Delete: false
    };
    db.insert("tb_detail", _detail, "LineID");
    bindDetail("#list-detail", _detail, false);
}

function bindDetail(table_selector, detail, disabled = false) {  
    $(table_selector).updateRow(detail, "LineID", {
        start_index: detail.LineID,
        hidden_columns: ["LineID", "SQID", "SQDID", "SOID", "SODID", "SQID", "SQDID", "ItemID", "ItemNameEN", "Cost", "GUomID", "DisValue", "Total_Sys", "VatValue", "OpenQty", "PrintQty", "InvoiceNo",
            "Factor", "Delete", "CurrencyID", "Process", "WarehouseID", "BranchID", "UserID", "InvoiceNo", "ItemStatus", "ExpireDate", "Factor", "UomName", "ItemType"],
        html: [
            {
                insertion: "replace",
                column: "UomID",
                element: defineUom(detail, disabled)
            },
            {
                insertion: "replace",
                column: "ItemName",
                element: $("<input " + (disabled ? 'disabled' : '') + " value='" + detail.ItemName + "'>").on("keyup", function () {
                    let row = $(this).parent().parent();
                    let detail = db.table("tb_detail").get(row.data("lineid"));
                    detail.ItemName = this.value;
                })
            },
            {
                insertion: "replace",
                column: "Qty",
                element: $("<input " + (disabled ? 'disabled' : '') + " data-number type='number' value='" + detail.Qty + "'>").on("keyup", function () {
                    let row = $(this).parent().parent();
                    let __detail = db.table("tb_detail").get(row.data("lineid"));         
                    __detail.Qty = parseFloat(this.value);
                    __detail.OpenQty = __detail.Qty;
                    if (__detail.SQDID > 0) {
                        if (__detail.Qty > __detail.OpenQty) {
                            return;
                        } else {
                            __detail.Qty = parseFloat(this.value);
                            __detail.OpenQty = __detail.Qty;
                        }
                    }           
                    
                    setLineTotal(row, __detail, $("#sub-dis-id").val());
                    setSummary($("#include-vat-id").prop("checked"), $("#sub-dis-id").val());
                })
            },
            {
                insertion: "replace",
                column: "UnitPrice",
                element: $("<input " + (disabled ? 'disabled' : '') + " data-number type='number' value='" + detail.UnitPrice + "'>").on("keyup", function () {
                    let row = $(this).parent().parent();
                    let detail = db.table("tb_detail").get(row.data("lineid"));
                    detail.UnitPrice = this.value;
                    setLineTotal(row, detail, $("#sub-dis-id").val());
                    setSummary($("#include-vat-id").prop("checked"), $("#sub-dis-id").val());

                })
            },
            {
                insertion: "replace",
                column: "DisRate",
                element: $("<input " + (disabled ? 'disabled' : '') + " data-number type='number' value='" + detail.DisRate + "'>").on("keyup", function () {
                    let row = $(this).parent().parent();
                    let detail = db.table("tb_detail").get(row.data("lineid"));
                    detail.DisRate = parseFloat(this.value);
                    setLineTotal(row, detail, $("#sub-dis-id").val());
                    setSummary($("#include-vat-id").prop("checked"), $("#sub-dis-id").val());
                })
            },
            {
                insertion: "replace",
                column: "TypeDis",
                element: defineDiscountTypes(detail, disabled)
            },
            {
                insertion: "replace",
                column: "VatRate",
                element: "<span>" + ($("#include-vat-id").prop("checked") ? $("#vat-id").val() : 0) + " %</span>"
            },
            {
                insertion: "replace",
                column: "Remarks",
                element: $("<input " + (disabled ? 'disabled' : '') + " value='" + (!!detail.Remarks ? detail.Remarks : "") + "'>").on("keyup", function () {
                    let row = $(this).parent().parent();
                    let detail = db.table("tb_detail").get(row.data("lineid"));
                    detail.Remarks = this.value;
                })
            },

        ],
        prebuild: function (data) {
            setLineTotal(this, data, $("#sub-dis-id").val());
        },
        postbuild: function (data) {  
            if (!disabled) {
                $(this).find("td:last-child").after($("<td></td>")                  
                    .append($("<i title='Remove' class='fas fa-trash-alt fa-lg text-danger csr-pointer'></i>").on("click", removeDetail)));
            }
            else {
                $(this).find("td:last-child").after($("<td><i title='Remove' class='fas fa-trash-alt fa-lg text-danger'></i></td>"));
            }             
        }
    });
    setSummary($("#include-vat-id").prop("checked"), $("#sub-dis-id").val());

    function removeDetail(e) {
        let _row = $(this).parent().parent();
        const _dialog = new DialogBox({
            caption: "Sale Quotation Removal",
            icon: "warning",
            type: "yes-no",
            content: "Are you sure you want to remove this item?"
        });
        _dialog.confirm(function () {                   
            _dialog.shutdown("during", function () {
                let rowdata = db.table("tb_detail").get(_row.data("lineid"));
                if (rowdata.SQDID > 0) {
                    $.get("/Sale/RemoveQuoteDetail", { detailID: rowdata.SQDID }, function (model) {
                        console.log(model)
                        if (model.Action == 1) {                         
                            _row.remove();
                        }

                        new ViewMessage({
                            summary: {
                                selector: "#error-summary"
                            }
                        }, model);
                        $(window).scrollTop(0);
                    });
                } else {
                    _row.remove();
                }
                if ($(table_selector).children("tr:not(:first-child)").length === 0) {
                    $("#include-vat-id").prop("disabled", true);
                }
                
            });
            
            db.table("tb_detail").delete(_row.data("lineid"));
            setSummary($("#include-vat-id").prop("checked"), $("#sub-dis-id").val());
        });
    }
}

var defined_uoms = [];
function defineUom(detail, disabled = false) {
    defined_uoms = db.from("tb_uom").where(function (json) {
        return json.GroupUoMID == detail.GUomID;
    });
    let select = $("<select "+ (disabled? 'disabled' : '') +"></select>");
    $.each(defined_uoms, function (i, item) {
        if (item.UomID == detail.UomID) {
            $("<option selected value='" + item.UnitofMeasure.ID + "'>" + item.UnitofMeasure.AltUomName + "</option>").appendTo(select);
        } else {
            $("<option value='" + item.UnitofMeasure.ID + "'>" + item.UnitofMeasure.AltUomName + "</option>").appendTo(select);
        }
    });
    return select;
}

function defineDiscountTypes(data, disabled = false) {
    let select = $("<select " + (disabled ? 'disabled' : '') + "></select>");
    select.append("<option value='Percent'>Percent ( % )</option>")
        .append("<option value='Cash'>Cash ( $ )</option>");
    select.val(data.TypeDis);
    select.on("change", function () {
        setLineTotal($(this).parent().parent(), data, this.value);
        setSummary($("#include-vat-id").prop("checked"), $("#sub-dis-id").val());
    });
    return select;
}

function setLineTotal(row, detail, dis_type) {
    let line_total = 0;
    switch (dis_type.toLowerCase()) {
        case "percent":
            line_total = parseFloat(detail.Qty) * parseFloat(detail.UnitPrice) * (1 - parseFloat(detail.DisRate) / 100);
            detail.DisValue = parseFloat(detail.Qty) * parseFloat(detail.UnitPrice) * (parseFloat(detail.DisRate) / 100);
            break;
        case "cash":
            line_total = parseFloat(detail.Qty) * parseFloat(detail.UnitPrice) - parseFloat(detail.DisRate);
            detail.DisValue = parseFloat(detail.DisRate);
            break;
    }

    detail.TypeDis = dis_type;
    detail.Total = parseFloat(line_total).toFixed(2);
    detail.Total_Sys = detail.Total; 
    $(row).find("td[cell-total]").html(line_total.toFixed(2));
}

function updateLineItem(checked, dis_type) {
    let rows = $("#list-detail").find("tr:not(:first-child)");
    let vat_value = $("#vat-id").val();
    let vat_text = $("#vat-id option:selected").text();
    if (rows.length > 0) {
        $.each(rows, function (i, row) {
            let detail = db.table("tb_detail").get($(row).data("lineid"));
            if (checked) {
                detail.VatRate = vat_value;
                $(row).find("td[cell-vatrate]").html(vat_text);
            } else {
                detail.VatRate = 0;
                $(row).find("td[cell-vatrate]").html("0 %");
            }

            setLineTotal(row, detail, dis_type);
        });
    }
}

createVATList("#vat-id");
function createVATList(selector) {
    if (db.table("tb_vat").size > 0) {
        $("#include-vat-id").prop("disabled", false);
        for (let vt of db.from("tb_vat")) {
            $(selector).append("<option value='" + vt.Rate + "'>" + vt.Rate + " %" + "</option>");
        }
        $(selector).on("change", function () {
            updateLineItem($("#include-vat-id").prop("checked"), $("#sub-dis-id").val());
            setSummary($("#include-vat-id").prop("checked"), $("#sub-dis-id").val());
        });
    } 
}

function setSummary(include_vat, dis_type) {
    let subtotal = 0;
    let total = 0;
    let vat = 0;
    let vat_rate = $("#vat-id").find("option:selected").val();
    let discount = parseFloat($("#dis-id").val());
    if (db.table("tb_detail").size > 0) {
        db.from("tb_detail").where(function (item) {
            subtotal += parseFloat(item.Total);
            vat += parseFloat(item.Qty) * parseFloat(item.UnitPrice) * parseFloat(vat_rate) / 100;
        });
    }
    switch (dis_type.toLowerCase()) {
        case "percent":
            total = subtotal * (1 - discount / 100);
            break;
        case "cash":
            total = subtotal - discount;
            break;
    }

    if (!include_vat) {
        vat = 0;
    }

    if (db.table("tb_master").size > 0) {
        db.from("tb_master").where(function (item) {
            item.SubTotal = subtotal.toFixed(2);
            item.SubTotal_Sys = subtotal.toFixed(2);
            item.TotalAmount = total.toFixed(2);
            item.TotalAmount_Sys = total.toFixed(2);
            item.VatRate = parseFloat(vat_rate);
            item.VatValue = vat;
            item.TypeDis = dis_type;
        });
    }
    $("#vat-value").val(vat);
    $("#sub-id").val(subtotal.toFixed(2));
    $("#total-id").val(total.toFixed(2));
}

$.each($("input[data-number]"), function (i, input) {
    $(input).asNumber();
});

$("#submit-item").on("click", function (e) {
    db.from("tb_master").where(function (item_master) {       
        item_master.InvoiceNo = $("#invoice-no").val();
        item_master.WarehouseID = parseInt($("#ware-id").val());
        item_master.RefNo = $("#ref-id").val();
        item_master.LocalCurrencyID = parseInt($("#cur-id").find("option:selected").val());
        item_master.PostingDate = $("#post-date").val();
        item_master.ValidUntilDate = $("#valid-date").val();
        item_master.DocumentDate = $("#document-date").val();
        item_master.IncludeVat = $("#include-vat-id").prop("checked");
        item_master.Remarks = $("#remark-id").val();
        item_master.TypeDis = $("#sub-dis-id").val();
        item_master.DisRate = $("#dis-id").val();
        item_master.DisValue = parseFloat(item_master.SubTotal) * parseFloat(item_master.DisRate) / 100;
        item_master.VatRate = parseFloat($("#vat-id").val());
        item_master.VatValue = parseFloat($("#vat-value").val());
        item_master.SaleQuoteDetails = db.from("tb_detail") == 0 ? new Array() : db.from("tb_detail");
        item_master.FeeNote = $("#fee-note").val();
        item_master.FeeAmount = $("#fee-amount").val();
    });

    $.ajax({
        url: "/Sale/UpdateSaleQuote",
        type: "POST",
        data: $.antiForgeryToken({ data: JSON.stringify(db.from("tb_master")[0])}),
        success: function (model) {
            if (model.Action == 1) {
                new ViewMessage({
                    summary: {
                        selector: "#error-summary"
                    }
                }, model).refresh(1500);
            }
            new ViewMessage({
                summary: {
                    selector: "#error-summary"
                }
            }, model);
            $(window).scrollTop(0);
        }
    });
});

$("#btn-find").on("click", function () {
    $("#invoice-no").val("").prop("readonly", false).css({
        "border": "1.5px solid #87ceeb"
    }).focus();
    $("#btn-addnew").prop("hidden", false);
    $("#btn-find").prop("hidden", true);
});

$("#invoice-no").on("keypress", function (e) {
    if (e.which === 13 && !!$("#invoice-no").val().trim()) {
        $.ajax({
            url: "/Sale/FindSaleQuote",
            data: { number: $("#invoice-no").val().trim() },
            success: function (result) {
                getSaleItemMasters(result, "SQID", "SaleQuoteDetails", 0);
            }
        });
    }
});

function getSaleItemMasters(master, key, detailKey, copyType = 0) {
    if (!!master) {
        $("#item-id").prop("disabled", false);
        setRequested(master, key, detailKey, copyType);
    } else {
        const _dialog = new DialogBox({
            caption: "Searching",
            icon: "danger",
            content: "Sale Quotation Not found!",
            close_button: "none"
        });
        _dialog.confirm(function () {
            location.reload();
        });
    }
}

function setRequested(item, key, detailKey, copyType = 0) {
    $.get("/Sale/GetCustomer", { id: item.CusID }, function (cus) {
        $("#cus-id").val(cus.Name);
    });
    $("#include-vat-id").prop("disabled", true);
    $("#ware-id").val(item.WarehouseID);
    $("#ref-id").val(item.RefNo);
    $("#cur-id").val(item.LocalCurrencyID);
    $("#include-vat-id").prop("checked", item.IncludeVat);
    $("#sta-id").val(item.Status);
    $("#sub-id").val(item.SubTotal);
    $("#sub-dis-id").val(item.TypeDis);
    $("#dis-id").val(item.DisRate);
    $("#fee-note").val(item.FeeNote);
    setDate("#post-date", item.PostingDate.toString().split("T")[0]);
    setDate("#valid-date", item.ValidUntilDate.toString().split("T")[0]);
    setDate("#document-date", item.DocumentDate.toString().split("T")[0]);
    if (item.FeeAmount) {
        $("#fee-amount").val(item.FeeAmount);
    }
    $("#remark-id").val(item.Remarks);
    setVAT(item);
    $("#total-id").val(item.TotalAmount);

    db.table("tb_detail").clear();
    $("#list-detail").find("tr:not(:first-child)").remove();
    $.each(item[detailKey], function (i, detail) {
        if (!detail.Delete) {
            let _detail = { LineID: Date.now() };
            _detail = $.extend(_detail, detail);
            db.insert("tb_detail", _detail, "LineID");
            bindDetail("#list-detail", _detail, detail.Qty > detail.OpenQty);
        }
    });

    if (item[detailKey]) {
        if (item.VatValue > 0) {
            $("#vat-value").val(item.VatValue);
        }
        delete item[detailKey];
    }

    db.table("tb_master").clear();
    if (item.Status === "open") {     
        db.insert("tb_master", item, key);
        if (copyType > 0) {
            db.from("tb_master").where(function (json) {
                json.CopyType = copyType, // 1: Quotation, 2: Order, 3: Delivery, 4: AR  
                json.BasedCopyKey = item.InvoiceNo;
            });
        }
    } else {
        $("#submit-item").prop("disabled", true);
        $("#item-id").prop("disabled", true);
        $("#sub-dis-id").prop("disabled", true);
        $("#dis-id").prop("disabled", true);
        $("#remark-id").prop("disabled", true);
        $("#include-vat-id").prop("disabled", true);
        $("#cur-id").prop("disabled", true);
        $("#ref-id").prop("disabled", true);
        $("#ware-id").prop("disabled", true);
        $("#show-list-cus").addClass("csr-default").off();
        $.each($("[data-date]"), function (i, t) {
            $(t).prop("disabled", true);
        });
        
    }
    setSummary(item.IncludeVat, item.TypeDis);
    
}

function setVAT(item) {
    $("#vat-id").val(item.VatRate);
    $("#vat-value").val(item.VatValue);
}



