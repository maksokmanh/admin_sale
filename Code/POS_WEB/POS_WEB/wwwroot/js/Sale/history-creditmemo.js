﻿"use strict"

const __filterType = {
    Customer: 0,
    Warehouse: 0,
    DateFrom: $("#sale-date-from").val(),
    DateTo: $("#sale-date-to").val(),
    DateType: parseInt($("#sale-date-type").val()),
    SaleType: 5 //{ Quotation = 1, Order = 2, Delivery = 3, AR = 4, CreditMemo = 5 }
}

Object.defineProperty(__filterType, "SaleType", {
    writable: false,
    configurable: false
});

const __filterUrl = "/Sale/FilterSaleHistory",
    __searchUrl = "/Sale/SearchSaleHistory",
    __printUrl = "/Print/SaleCreditMemoPrintHistory";

window.onload = function () {
    bindSaleHistories("#sale-histories", JSON.parse($("#datasource").text()));
}
$("#sale-customer").on("change", function () {
    __filterType.Customer = parseInt(this.value);
    searchSaleHistory("#sale-histories", __filterUrl, __filterType);
    $("#input-search").val("");
});

$("#sale-warehouse").on("change", function () {
    __filterType.Warehouse = parseInt(this.value);
    searchSaleHistory("#sale-histories", __filterUrl, __filterType);
    $("#input-search").val("");
});

$("#sale-date-type").on("change", function () {
    __filterType.DateType = parseInt(this.value);
    if (this.value <= 0) {
        $("#sale-date-from").prop("disabled", true).val("");
        $("#sale-date-to").prop("disabled", true).val("");
    } else {
        $("#sale-date-from").prop("disabled", false).val(__filterType.DateFrom);
        $("#sale-date-to").prop("disabled", false).val(__filterType.DateTo);
    }
    searchSaleHistory("#sale-histories", __filterUrl, __filterType);
    $("#input-search").val("");
});

$("#sale-date-from").on("change", function () {
    __filterType.DateFrom = this.value;
    searchSaleHistory("#sale-histories", __filterUrl, __filterType);
    $("#input-search").val("");
});

$("#sale-date-to").on("change", function () {
    __filterType.DateTo = this.value;
    searchSaleHistory("#sale-histories", __filterUrl, __filterType);
    $("#input-search").val("");
});

$("#btn-search").click(function () {
    let __value = $(this).siblings("#input-search").val();
    searchSaleHistory("#sale-histories", __searchUrl, { saleType: __filterType.SaleType, word: __value });
});


$("#input-search").on("keyup focus", function (e) {
    clearFilter();
    $("#sale-histories").find("tr:not(:first-child)").remove();
    if (e.which === 13) {
        searchSaleHistory("#sale-histories", __searchUrl, { saleType: __filterType.SaleType, word: this.value });
    }
});

function clearFilter() {
    $("#sale-customer").val(0);
    $("#sale-warehouse").val(0);
    $("#sale-date-type").val(0);
    $("#sale-date-from").val("");
    $("#sale-date-to").val("");
}

function searchSaleHistory(selector, url, data) {
    const __option = {
        url: url,
        data: data,
        success: function (data) {
            bindSaleHistories("#sale-histories", data)
        }
    };
    $.ajax(__option);
}

function bindSaleHistories(selector, data) {
    $(selector).find("tr:not(:first-child)").remove();
    const paging = new Kernel.DataPaging({
        pageSize: 10
    });

    paging.render(data, function (brief) {
        $(selector).bindRows(brief.data, "ID", {
            hidden_columns: ["ID", "CustomerID", "WarehouseID"],
            postbuild: function (data) {
                let td = $("<td class='sh-print'></td>").append($("<a href='#' onclick='onPrintHistory(this);' class='fas fa-print fa-lg csr-pointer fn-green'></a>"));
                $(this).find("td:last-child").after(td);
            }
        });
        $("#data-loading").hide();
    })
    $("#data-paging-container").html(paging.selfElement);
}

function onPrintHistory(self) {
    const id = $(self).parent().parent().data("id");
    window.open(__printUrl, "_blank");
}

