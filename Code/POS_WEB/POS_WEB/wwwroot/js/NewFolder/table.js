(function( $ ){
    let event_types = ["click", "dblclick", "mouseenter", "mouseleave","mouseout",
                       "mousemove", "mouseover", "mousedown", "mouseup", "wheel"];

    let h = {};
    $.extend({
        bindRows: function(table_selector, jsons, key, option = 0){
            /**************************************************************************/
            /*  Create a header of table if array of column names specified than use, */
            /*  otherwise use default properties of json as column names.             */
            /**************************************************************************/
            $(table_selector).find("tr:not(:first-child)").remove();

            let header = $("<tr></tr>");
            if(validArray(option.columns)){
                for(let m of option.columns){
                    header.append(
                        "<th>" + m + "</th>"
                    );
                }
            } else {
                h = Object.assign({}, jsons[0]);
                for(let m of Object.getOwnPropertyNames(h)){
                    header.append(
                        "<th>" + m + "</th>"
                    );   
                }     
            }

            if($(table_selector).find("tr:first-child").length === 0){
                $(table_selector).append(header);
            }
            
            /*************************************************************************/
            /* Create rows of table depending on number of json object in the array. */
            /*************************************************************************/
            if(validArray(jsons)){
                for(let json of jsons){
                    let row = builtRow(json, key, option);
                    row.on("mouseover", resetHighlight);
                    $(table_selector).append(row);  
                    if(option.highlight_row !== undefined){
                        if(row.data(key.toLowerCase()) === option.highlight_row){
                            row.addClass("highlight");
                            row.siblings().removeClass("highlight");
                            row[0].scrollIntoView({
                                behavior: 'auto',
                                block: 'center'
                            });
                        }  
                    }
                    fitTable(table_selector); 
                } 
            }  

    
        },//End bindRows()  

        updateRow: function(table_selector, json, key, option = 0){ 

            let $row = $();
            $row = builtRow(json, key, option);
            $(table_selector).append($row);
            fitTable(table_selector);  
            let _key = option.highlight_row = json[key];
            $.each($(table_selector).find("tr"), function(i, tr){
                if($(tr).hasClass("summary")){ $(tr).remove(); }

                $(tr).on("mouseover", resetHighlight);  
                if($(tr).data(key.toLowerCase()) === _key){
                    tr.scrollIntoView({
                        behavior: 'auto',
                        block: 'center'
                    });
                    $(tr).replaceWith($row);  
                    $(tr).addClass("highlight");
                    $(tr).siblings().removeClass("highlight");
                }  
            });  

        }//End updateRow()
    });

    function buildSummaryDescription(row, option) {
        let description = $("<td>"+ option.summary.description +"</td>");
        row.append(description);
    }

    function buildSummaryResult(table_selector, json, option){
        let $summary_row = $("<tr></tr>").addClass("summary");
        $.each(json,function(c, d){
            let $td = $("<td></td>");
            for(let ct of option.summary.result){
                if(c === ct.column){
                    $td.html(ct.content);
                }     
            }                 
            $summary_row.append($td);
        });
        $(table_selector).append($summary_row);
    }

    function buildCollapser(table_selector, option){
        let $row = $("<tr><td class='fas fa-caret-down fa-lg'></td></tr>");
        $row.on("click", function(e){
            $(this).siblings(":not(:first-child)").toggleClass("collapse");
            $(this).find("td").toggleClass("fa-caret-right");
        });

        $(table_selector).append($row);
    }

    function resetHighlight(e){
        $(this).siblings().removeClass("highlight");
    }

    function builtRow(json, key, option){
        let row = $("<tr></tr>");
        let col = $();
        $.each(json, function(c, value){   
            let col_name = c;       
            let row_data = $("<tr></tr>");
            let hide_key = option.hide_key !== undefined && option.hide_key;
            let scale_col = "";
            if(validJSON(option.scalable)){
                if(typeof option.scalable.column == 'string'){
                    scale_col = option.scalable.column;
                }
            } 
            
            if(key !== undefined && key == c){ 
                row_data = $("<tr data-" + key + "="+ value +"></tr>");
                row = row_data;              
            }
   
            if(validJSON(option.scalable) && col_name == scale_col){
                col = scalableColumn(value, option.scalable);                                      
            } else {   
                col = $("<td>" + value + "</td>");  
            } 

            if(validArray(option.text_align)){
               for(let ta of option.text_align){
                   $.each(ta, function(cn, p){
                       if(col_name === cn){
                           col = $("<td>"+ value +"</td>");
                           col.css("text-align", p);
                       }
                   });
               }
            }

            //As default show primary key
            if(col_name === key){
                col = $("<td>" + value + "</td>");
                if(hide_key){
                    col = $("<td hidden>" + value + "</td>"); 
                }
            } 

            //Hide specified columns
            if(validArray(option.hidden_columns)){              
                for(let hc of option.hidden_columns){             
                    if(hc === col_name){
                        col = $("<td hidden>" + value + "</td>");  
                    }
                }
            }

            if(validArray(option.html)){
                for(let value of option.html){
                    if(value.column !== undefined && value.column === col_name){
                        injectHtml(col, value, col_name);
                    }
                }
            }     
            row.append(col);
        });

        //Inject html into column outside of json properties range.
        if(option.html !== undefined){
            for(let hc of option.html){
                if(hc.column === -1){   
                    col = $("<td></td>");       
                    injectHtml(col, hc);                              
                }
            }
            row.append(col);
        }
                    
        /* Note */
        /******************************************************************************************/
        /* Event listener callback could be 'function' or 'arrow function'.                       */
        /* Function is either 'constructible or callable' but arrow function is 'only callable'.  */
        /* 'Constructible' function can possess 'this' so lexical 'this' in the function refers   */
        /*      to current event binding element (here each row of the table).                    */
        /* 'Only callable' function cannot possess 'this' so 'this'                               */
        /*      in the arrow function always refers to 'DOM'.                                        */ 
        /******************************************************************************************/
        
        if(validJSON(option)){
            $.each(option, function(key, value){ 
                for(let et of event_types){  
                    if(typeof value == 'function'){
                        if(key == et){
                            row.on(key, value);     
                        } 
                    }                                        
                }                    
            })   
        } 
        return row;
    }

    function scalableColumn(value, scalable){
        $col = $("<td></td>");
        $wrap_scale = $("<div class='wrap-scale'>"
            + "<i class='scale-down'>-</i>"
            + "<label data-scale='" + value + "' class='scale'>" + value + "</label>" 
            + "<i class='scale-up'>+</i>"
        +"</div>");
        if(scalable.callback !== undefined){ 
            $wrap_scale.on(scalable.event, scalable.callback);
        }    
        $wrap_scale.appendTo($col);
        return $col;  
    }

    function injectHtml($col, value) {
        if(value.element !== undefined){                    
            let $_json = (value.listener === undefined)?
                        $(value.element) : $(value.element).on(value.listener.event, value.listener.callback);
            let $_arr = (value.listener === undefined)? 
                        $(value.element) : $(value.element).on(value.listener[0], value.listener[1]);    
            switch(value.insertion){
                case "prepend":
                    $col.prepend(Array.isArray(value.listener)? $_arr : $_json);                                                      
                break;

                case "append":
                    $col.append(Array.isArray(value.listener)? $_arr : $_json); 
                break;

                case "replace":
                    $col.html(Array.isArray(value.listener)? $_arr : $_json); 
                break;

                default: 
                    $col.append(Array.isArray(value.listener)? $_arr : $_json);  
            }

        }  
    }

    function fitTable(table_selector){
        if($(table_selector).height() > $(table_selector).parent().height()){
            $(table_selector).parent().addClass("fit-scroll-y");
        } else {
            $(table_selector).parent().removeClass("fit-scroll-y");
        }
    };

    //Check if json object is valid.
    function validJSON(json){
        return json !== undefined && json.constructor === Object && Object.keys(json).length > 0;
    }
    //Check if value is valid array.
    function validArray(array){
        return array !== undefined && Array.isArray(array) && array.length > 0;
    }
})(jQuery);
