﻿$("#FormFile").on("change", function (e) {
    $("#filename-list").prop("disabled", true);
    $("#submit-item-master-data").prop("disabled", true);
    $("#preview-item-master-data").prop("disabled", true);
    $("#filename-list").children().remove();
    $("#preview-motocycle-list").find("tr:not(:first-child)").remove();

    $.ajax({
        url: "/ExcelWorkbook/GetFileNames",
        type: "POST",
        enctype: "multipart/form-data",
        data: new FormData($("#item-master-formfile")[0]),
        processData: false,
        contentType: false,
        cache: false,
        success: function (result) {
            new ViewMessage(result);
            $("#filename-list").children().remove();
            if (Object.keys(result).length > 0) {
                $("#filename-list").prop("disabled", false);
                $("#submit-item-master-data").prop("disabled", false);
                $("#preview-item-master-data").prop("disabled", false);
                
                for (let i in result) {
                    $("#filename-list").append("<option value='" + i + "'>" + result[i] + "</option>");
                }

                $("#preview-item-master-data").on("click", function () {
                    let btn_preview = $(this).children("i.fa-search");
                    btn_preview.addClass("fa-spinner fa-spin");
                    $.ajax({
                        url: "/ExcelWorkbook/PreviewItemMasterData",
                        type: "POST",
                        enctype: "multipart/form-data",
                        data: new FormData($("#item-master-formfile")[0]),
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (result) {    
                            let _index = 1;
                            $("#preview-item-master-data-list").bindRows(result, "Code", {
                                postbuild: function (item) {
                                    $(this).find("td:first-child").before("<td>" + _index++ + "</td>");
                                }
                            });
                            btn_preview.removeClass("fa-spinner fa-spin");
                        }
                    });
                });

                $("#submit-item-master-data").on("click", function () {
                    let btn_upload = $(this).children("i.fa-file-upload");
                    btn_upload.addClass("fa-spinner fa-spin");
                    $.ajax({
                        url: "/ItemMasterData/Upload",
                        type: "POST",
                        enctype: "multipart/form-data",
                        data: new FormData($("#item-master-formfile")[0]),
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (result) {
                            if (result.Action == "1") {
                                new ViewMessage({
                                    summary: {
                                        attribute: "ck-message-summary"
                                    }
                                }, result);
                            }
                            new ViewMessage(result);
                            btn_upload.removeClass("fa-spinner fa-spin");
                        },
                        error: function (response) {
                            if (response.status === 500) {
                                new ViewMessage({
                                    model: {
                                        action: "reject"
                                    }
                                }, { data: { "itemMasterData.Barcode": "Insert Item Master Data failed." } });

                            }
                            btn_upload.removeClass("fa-spinner fa-spin");
                        }
                    });
                });
            }
        }
    });
});


//Tab content customer
$("ul#nav-import-list li").click(function () {
    $(this).addClass("active").siblings().removeClass("active");
    let content = $("#tab-content-customer");
    if ($(this).is($("#tab-new-upload"))) {
        $("#content-new-upload", content).prop("hidden", false).siblings().prop("hidden", true);
    }

    if ($(this).is($("#tab-preview"))) {
        $("#content-item-master-data", content).prop("hidden", false).siblings().prop("hidden", true);
        previewComponent("/ExcelWorkbook/PreviewComponent");

    }
});

 function previewComponent(url) {
    $("#uploaded-loader").addClass("show");
    let option = {
        url: url,
        dataType: "JSON",
        success: function (data) {          
            $("#uploaded-loader").removeClass("show");
            Promise.all([
                $("#im-table-pricelist").bindRows(data.PriceLists, "ID", {
                    hidden_columns: ["Currency", "CurrencyID", "Delete"],
                }),
                $("#im-table-printer").bindRows(data.PrinterNames, "ID", {
                    hidden_columns: ["MachineName", "Delete"],
                }),

                $("#im-table-group-uom").bindRows(data.GroupUOMs, "ID", {
                    hidden_columns: ["Delete"],
                }),
                $("#im-table-warehouse").bindRows(data.Warehouses, "ID", {
                    hidden_columns: ["Delete", "Address", "Branch", "Location", "StockIn"],
                }),
                $("#im-table-item-group1").bindRows(data.ItemGroup1, "ID", {
                    hidden_columns: ["BackID", "Background", "ColorID", "Colors", "Delete", "Visible", "Images"],
                }),
                $("#im-table-item-group2").bindRows(data.ItemGroup2, "ID", {
                    hidden_columns: ["BackID", "Background", "ColorID", "Colors", "Delete", "ItemGroup1", "Images"],
                }),
                $("#im-table-item-group3").bindRows(data.ItemGroup3, "ID", {
                    hidden_columns: ["BackID", "Background", "ColorID", "Colors", "Delete", "ItemGroup1", "ItemGroup2", "Images"],
                }),
                $("#im-table-group-defined-uom").bindRows(data.GroupDefinedUoM, "ID", {
                    hidden_columns: ["ID"]
                })

            ]);

        }
    };

    $.ajax(option);
}

$("#export-motocycle-timely-appointments").click(function () {
    let file_dialog = new DialogBox({
        caption: "Export Excel",
        content: {
            selector: "#dialog-export-timely-motocycle-appointments",
        },
        type: "ok-cancel"
    });

    file_dialog.confirm(function () {
        exportExcel("/Customer/ExportMotocycleTimelyAppointments", file_dialog);
    });

    file_dialog.reject(function () {
        this.meta.shutdown();
    });
});


function exportExcel(url, dialog) {
    let content = dialog.content;
    let file_type = content.find("select[name=FileType] option:selected").val();
    let file_name = content.find("input[name=FileName]").val();
    let _dir = {
        FileType: file_type,
        FileName: file_name
    };

    $.ajax({
        type: "POST",
        url: url,
        data: $.antiForgeryToken({ directory: _dir }),
        success: function (message) {
            console.log(message)
            new ViewMessage(message);
            if (message.Action == 1) {
                new ViewMessage({
                    summary: {
                        attribute: "model-validation-success"
                    }
                }, message);
            }
        }
    });
}

