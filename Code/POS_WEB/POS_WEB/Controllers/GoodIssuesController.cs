﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Inventory.Transaction;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class GoodIssuesController : Controller
    {
        private readonly DataContext _context;
        private readonly IGoodIssuse _issuse;
        public GoodIssuesController(DataContext context,IGoodIssuse issuse)
        {
            _context = context;
            _issuse = issuse;
        }
        public IActionResult GoodIssues()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Transaction";
            ViewBag.Subpage = "Goods Issue";
            ViewBag.InventoryMenu = "show";
            ViewBag.Transaction = "show";
            ViewBag.GoodsIssue = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A040");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
           
        }
        [HttpGet]
        public IActionResult GetInvoicenomber()
        {
            var count = _context.GoodIssues.Count() + 1;
            var list = "IS-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehousesFrom(int ID)
        {
            var list = _issuse.GetWarehouse_From(ID);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouseFrom(int ID)
        {
            var list = _issuse.GetItemByWarehouse_From(ID).ToList();
            return Ok(list);
        }
        public IActionResult SaveGoodIssues(GoodIssues issues)
        {
            List<ItemsReturnPC> list = new List<ItemsReturnPC>();
            List<ItemsReturnPC> list_group = new List<ItemsReturnPC>();
            foreach (var item in issues.GoodIssuesDetails.ToList())
            {
                var check = list_group.Find(w => w.ItemID == item.ItemID);
                var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom => uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                if (check == null)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == issues.WarehouseID && w.ItemID == item.ItemID);
                    var item_master = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == item.ItemID);
                    if (item_warehouse != null)
                    {
                        ItemsReturnPC item_group = new ItemsReturnPC
                        {
                            Line_ID = item.LineID,
                            Code = item_master.Code,
                            ItemID = item.ItemID,
                            KhmerName = item_group_uom.KhmerName + ' ' + item_group_uom.UnitofMeasureInv.Name,
                            InStock = (item_warehouse.InStock - item_warehouse.Committed).ToString(),
                            OrderQty = (item.Quantity * uom_defined.Factor).ToString(),
                            Committed = item_warehouse.Committed.ToString()
                        };
                        list_group.Add(item_group);
                    }

                }
                else
                {
                    check.OrderQty = (double.Parse(check.OrderQty) + item.Quantity * uom_defined.Factor).ToString();
                }
            }
            foreach (var item in list_group)
            {
                if (double.Parse(item.OrderQty) > double.Parse(item.InStock))
                {
                    ItemsReturnPC item_return = new ItemsReturnPC
                    {
                        Line_ID = item.Line_ID,
                        Code = item.Code,
                        ItemID = item.ItemID,
                        KhmerName = item.KhmerName,
                        InStock = item.InStock.ToString(),
                        OrderQty = item.OrderQty.ToString()
                    };
                    list.Add(item_return);
                }

            }
            //check instock
            if (list.Count > 0)
            {
                return Ok(list);//Count >0
            }
            else
            {
                foreach (var item in issues.GoodIssuesDetails.ToList())
                {
                    if (item.Quantity > 0)
                    {
                        object item_return;
                        var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom => uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                        var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                        var item_master = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == item.ItemID);
                        if (item.ManageExpire == "None")
                        {
                            item_return = _context.WarehouseDetails.FirstOrDefault(w => w.ItemID == item.ItemID && w.UomID == item.UomID && w.Cost == item.Cost / uom_defined.Factor  && w.InStock >= item.Quantity * uom_defined.Factor);
                        }
                        else
                        {
                            item_return = _context.WarehouseDetails.FirstOrDefault(w => w.ItemID == item.ItemID && w.UomID == item.UomID && w.Cost == item.Cost / uom_defined.Factor && w.ExpireDate == item.ExpireDate && w.InStock >= item.Quantity * uom_defined.Factor);
                        }
                        
                        if (item_return == null)
                        {
                           
                            var uom = _context.UnitofMeasures.FirstOrDefault(w => w.ID == item.UomID);
                            ItemsReturnPC item_add = new ItemsReturnPC
                            {
                                Line_ID = item.LineID,
                                Code = item_master.Code,
                                ItemID = item.ItemID,
                                KhmerName = item_master.KhmerName + ' ' + uom.Name,
                                InStock = "0",
                                OrderQty = "Have not exist."
                            };
                            list.Add(item_add);
                        }
                    }
                }
                if (list.Count > 0)
                {
                    return Ok(list);
                }
                else
                {
                    _issuse.SaveGoodIssues(issues);
                    return Ok();
                }
            }
           
        }
        [HttpPost]
        public IActionResult SaveGoodIssues1(GoodIssues issues)
        {
            List<GoodIssuesDetail> list = new List<GoodIssuesDetail>();
            foreach (var item in issues.GoodIssuesDetails.ToList())
            {
                var checkstock = _context.WarehouseDetails.FirstOrDefault(x => x.Cost == item.Cost &&
                                                                          x.ExpireDate == item.ExpireDate &&
                                                                          x.UomID == item.UomID &&
                                                                          x.WarehouseID == issues.WarehouseID &&
                                                                          x.ItemID == item.ItemID);
                if (checkstock != null)
                {
                    if (item.Quantity > checkstock.InStock)
                    {
                        item.Check = "Over stock";
                        list.Add(item);
                    }
                }
                else
                {
                    item.Check = "Not found in stock ";
                    list.Add(item);
                }
            }
            if (list.Count() > 0)
            {
                return Ok(list);
            }
            else
            {
                _issuse.SaveGoodIssues(issues);
                return Ok();
            }
           
        }
        [HttpGet]
        public IActionResult FindBarcode(int WarehouseID,string Barcode)
        {
            try
            {
                var list = _issuse.GetItemFindBarcode(WarehouseID, Barcode).ToList();
                return Ok(list);
            }
            catch (Exception)
            {

                return Ok();
            }
        }
    }
}
