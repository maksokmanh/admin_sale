﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Responsitory;

namespace POS_WEB.Controllers
{
    public class IncomingPaymentController : Controller
    {
        private readonly DataContext _context;
        public IncomingPaymentController (DataContext context)
        {
            _context = context;
        }
        //View
        public IActionResult IncomingPayment()
        {
            ViewBag.style = "fa fa-random";
            ViewBag.Main = "Banking";
            ViewBag.Page = "Incoming Payment";
            ViewBag.Subpage = "";
            ViewBag.Banking = "show";
            ViewBag.IncomingPaymentMenu = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A042");
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
        }

        private void ValidateSummary(IncomingPayment master, List<IncomingPaymentDetail> detail)
        {
            if (master.CustomerID == 0)
            {
                ModelState.AddModelError("CustomerID", "Please choose any customer.");
            }

            if (detail.Count() == 0)
            {
                ModelState.AddModelError("Details", "Please select at least one invoice.");
            }
        }

        //Save Data
        [HttpPost]
        public IActionResult SaveIncomingPayment(string incoming)
        {
            IncomingPayment incomingpay = JsonConvert.DeserializeObject<IncomingPayment>(incoming);
            ModelMessage msg = new ModelMessage();
            ValidateSummary(incomingpay, incomingpay.IncomingPaymentDetails);

            if (ModelState.IsValid) {

                _context.IncomingPayments.Add(incomingpay);
                _context.SaveChanges();
                foreach (var item in incomingpay.IncomingPaymentDetails.ToList())
                {
                    if (item.DocumentType == "SAR")
                    {
                        var balance = item.BalanceDue - item.Cash - item.TotalDiscount;
                        if (balance == 0)
                        {
                            var incomingcus = _context.IncomingPaymentCustomers.FirstOrDefault(x => x.DocumentNo == item.DocumentNo);
                            incomingcus.Status = "close";
                            _context.IncomingPaymentCustomers.Update(incomingcus);
                            var salear = _context.SaleARs.FirstOrDefault(x => x.InvoiceNo == item.DocumentNo);
                            salear.Status = "close";
                            salear.AppliedAmount = salear.AppliedAmount + item.BalanceDue;
                            _context.SaleARs.Update(salear);
                            _context.SaveChanges();
                        }
                        else
                        {
                            var incomingcus = _context.IncomingPaymentCustomers.FirstOrDefault(x => x.DocumentNo == item.DocumentNo);
                            incomingcus.Applied_Amount = item.Applied_Amount + item.Cash + item.TotalDiscount;
                            incomingcus.BalanceDue = item.BalanceDue - (item.Cash + item.TotalDiscount);
                            _context.IncomingPaymentCustomers.Update(incomingcus);
                            var salear = _context.SaleARs.FirstOrDefault(x => x.InvoiceNo == item.DocumentNo);
                            salear.AppliedAmount = salear.AppliedAmount + item.Cash + item.TotalDiscount;
                            _context.SaleARs.Update(salear);
                            _context.SaveChanges();
                        }
                    }
                    else
                    {
                        var balance = item.BalanceDue - item.Cash - item.TotalDiscount;
                        if (balance == 0)
                        {
                            var incomingcus = _context.IncomingPaymentCustomers.FirstOrDefault(x => x.DocumentNo == item.DocumentNo);
                            incomingcus.Status = "close";
                            _context.IncomingPaymentCustomers.Update(incomingcus);

                            var saleMemo = _context.SaleCreditMemos.FirstOrDefault(x => x.InvoiceNo == item.DocumentNo);
                            saleMemo.Status = "close";
                            saleMemo.AppliedAmount = saleMemo.AppliedAmount - item.BalanceDue;
                            _context.SaleCreditMemos.Update(saleMemo);
                            _context.SaveChanges();

                        }
                        else
                        {
                            var goingvender = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == item.DocumentNo);
                            goingvender.Applied_Amount = item.Applied_Amount + item.Cash + item.TotalDiscount;
                            goingvender.BalanceDue = item.BalanceDue - (item.Cash + item.TotalDiscount);
                            _context.OutgoingPaymentVendors.Update(goingvender);
                            var purchaseMemo = _context.PurchaseCreditMemos.FirstOrDefault(x => x.InvoiceNo == item.DocumentNo);
                            purchaseMemo.Applied_Amount = purchaseMemo.Applied_Amount - (item.Cash + item.TotalDiscount);
                            _context.SaveChanges();
                        }
                    }

                    msg.Action = ModelAction.Approve;
                    }
                }
            return Ok(new { Model = msg.Bind(ModelState)} );
        }

        //Get Paramater
        [HttpGet]
        public IActionResult GetIncomingPaymentCus(int CusID)
        {
            var list = from ipc in _context.IncomingPaymentCustomers
                       join bus in _context.BusinessPartners on ipc.CustomerID equals bus.ID
                       join b in _context.Branches on ipc.BranchID equals b.ID
                       join wh in _context.Warehouses on ipc.WarehouseID equals wh.ID
                       join cur in _context.Currency on ipc.CurrencyID equals cur.ID
                       join cur_s in _context.Currency on ipc.SysCurrency equals cur_s.ID
                       where ipc.CustomerID == CusID && ipc.Status == "open"
                       select new
                       {
                           IPCID = ipc.IncomingPaymentCustomerID,
                           CusID = bus.ID,
                           BranchID = b.ID,
                           CurrencyID = cur.ID,
                           ipc.DocumentNo,
                           ipc.DocumentType,
                           ipc.Date,
                           OverdueDays = ipc.Date.Day - DateTime.Now.Day,
                           ipc.Total,
                           ipc.BalanceDue,
                           TotalPayment = ipc.Total - ipc.Applied_Amount,
                           ipc.Applied_Amount,
                           CurrencyName = cur.Description,
                           SysName = cur_s.Description,
                           ipc.Status,
                           ipc.CashDiscount,
                           ipc.TotalDiscount,
                           ipc.Cash,
                           ipc.ExchangeRate,
                           ipc.SysCurrency
                       };
            return Ok(list);
        }
        public IActionResult GetNumberNo()
        {
            var count = _context.IncomingPayments.Count() + 1;
            var list = "IP-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        public IActionResult GetCustomer()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Customer").ToList();
            return Ok(list);
        }
    }
}
