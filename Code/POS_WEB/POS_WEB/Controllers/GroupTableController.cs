﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Tables;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class GroupTableController : Controller
    {
        private readonly DataContext _context;
        private readonly IGroupTable _groupTable;
        private readonly IHostingEnvironment _appEnvironment;
        public GroupTableController(DataContext context, IGroupTable groupTable, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _groupTable = groupTable;
            _appEnvironment = hostingEnvironment;
        }


        public async Task<IActionResult> Index(string minpage = "All", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Group Table";
            ViewBag.Administrator = "show";
            ViewBag.TableMenu = "show";
            ViewBag.TableGroup = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A010");
            var data = _groupTable.GroupTables().OrderBy(g => g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["TypeSortParm"] = sortOrder == "Type" ? "Type_desc" : "Type";
                ViewData["ImageSortParm"] = sortOrder == "Image" ? "Image_desc" : "Image";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                // var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString) || s.Types.Contains(searchString) || s.Image.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "Type":
                        Cate = Cate.OrderBy(s => s.Types);
                        break;
                    case "Type_desc":
                        Cate = Cate.OrderByDescending(s => s.Types);
                        break;
                    case "Image":
                        Cate = Cate.OrderBy(s => s.Image);
                        break;
                    case "Image_desc":
                        Cate = Cate.OrderByDescending(s => s.Image);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                //int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    if (d == 0) d = 1;
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<GroupTable>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["TypeSortParm"] = sortOrder == "Type" ? "Type_desc" : "Type";
                    ViewData["ImageSortParm"] = sortOrder == "Image" ? "Image_desc" : "Image";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    // var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString) || s.Types.Contains(searchString) || s.Image.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "Type":
                            Cate = Cate.OrderBy(s => s.Types);
                            break;
                        case "Type_desc":
                            Cate = Cate.OrderByDescending(s => s.Types);
                            break;
                        case "Image":
                            Cate = Cate.OrderBy(s => s.Image);
                            break;
                        case "Image_desc":
                            Cate = Cate.OrderByDescending(s => s.Image);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        if (d == 0) d = 1;
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<GroupTable>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }

        public IActionResult Create()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Group Table";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A010");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }          
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Types,Image,Delete")] GroupTable groupTable)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Group Table";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (groupTable.Types == "0" || groupTable.Types == null)
            {
                ViewBag.required = "Please select type !";
                return View(groupTable);
            }
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                        if (file.Length > 0)
                        {
                            var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                groupTable.Image = fileName;
                            }

                        }
                    }
                }
                await _groupTable.AddOrEdit(groupTable);
                return RedirectToAction(nameof(Index));
            }
            return View(groupTable);
        }

        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Group Table";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var groupTable = _groupTable.GetbyId(id);
                if (groupTable.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (groupTable == null)
                {
                    return NotFound();
                }
                return View(groupTable);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A010");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var groupTable = _groupTable.GetbyId(id);
                    if (groupTable.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (groupTable == null)
                    {
                        return NotFound();
                    }
                    return View(groupTable);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Types,Image,Delete")] GroupTable groupTable)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Group Table";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            if (groupTable.Types == "0" || groupTable.Types == null)
            {
                ViewBag.required = "Please select type !";
                return View(groupTable);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var files = HttpContext.Request.Form.Files;
                    foreach (var Image in files)
                    {
                        if (Image != null && Image.Length > 0)
                        {
                            var file = Image;
                            var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                            if (file.Length > 0)
                            {
                                var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                                {
                                    await file.CopyToAsync(fileStream);
                                    groupTable.Image = fileName;
                                }

                            }
                        }
                    }
                    await _groupTable.AddOrEdit(groupTable);
                }
                catch (Exception)
                {
                    
                }
                return RedirectToAction(nameof(Index));
            }
            return View(groupTable);
        }

       [HttpDelete]
       public async Task<IActionResult> DeleteGroupTable(int id)
        {
          await  _groupTable.Delete(id);
            return Ok();
        }
        [HttpPost]
        public IActionResult QuickAdd(GroupTable groupTable)
        {
           _groupTable.InsertGroupTable(groupTable);
            return Ok();
        }
        [HttpGet]
        public IActionResult GetDataGroupTable()
        {
            var list = _groupTable.GroupTables().OrderByDescending(s => s.ID);
            return Ok(list);
        }
    }
}
