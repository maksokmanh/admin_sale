﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    public class DiscountItemController : Controller
    {
        private readonly DataContext _context;
        public DiscountItemController(DataContext context)
        {
            _context = context;
        }
        [HttpGet]
        public IActionResult Index()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-percent";
                ViewBag.Main = "Promotion";
                ViewBag.Page = "Discount Item";
                ViewBag.Subpage = "List";
                ViewBag.PromotionMenu = "show";
                ViewBag.DiscountItem = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A036");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-percent";
                        ViewBag.Main = "Promotion";
                        ViewBag.Page = "Discount Item";
                        ViewBag.Subpage = "List";
                        ViewBag.PromotionMenu = "show";
                        ViewBag.DiscountItem = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
           
        }
        public IActionResult GetItems(int PriceListID,int Group1,int Group2,int Group3)
        {
            IEnumerable<DiscountItemDetail> items = _context.DiscountItemDetail.FromSql("sp_GetItemDiscount @PriceListID={0},@Group1={1},@Group2={2},@Group3={3}",
             parameters: new[] {
                    PriceListID.ToString(),
                    Group1.ToString(),
                    Group2.ToString(),
                    Group3.ToString()
             }).ToList();
            return Ok(items);
        }
        [HttpGet]
        public IActionResult GetPromotion()
        {
            var promotion = _context.Promotions;
            return Ok(promotion);
        }
        public IActionResult GetPriceList()
        {
            var pricelist = _context.PriceLists.Where(w => w.Delete == false);
            return Ok(pricelist);
        }
        public IActionResult GetGroup1()
        {
            var groups = _context.ItemGroup1.Where(w => w.Delete == false);
            return Ok(groups);
        }
        public IActionResult GetGroup2(int group1)
        {
            var groups = _context.ItemGroup2.Where(w => w.ItemG1ID == group1 && w.Delete == false && w.Name != "None");
            return Ok(groups);
        }
        public IActionResult GetGroup3(int group1, int group2)
        {
            var groups = _context.ItemGroup3.Where(w => w.ItemG1ID == group1 && w.ItemG2ID == group2 && w.Delete == false && w.Name != "None");
            return Ok(groups);
        }
       
        [HttpPost]
        public IActionResult SetPromotionDisountItem(DiscountItem DiscountItem)
        {
            foreach (var item in DiscountItem.DiscountItemDetails.ToList())
            {
                float discount = 0;
                float.TryParse(item.Discount.ToString(), out discount);
                var item_update = _context.PriceListDetails.Find(item.ID);
                item_update.PromotionID = DiscountItem.PromotionID;
                item_update.Discount = discount;
                item_update.TypeDis = item.TypeDis;
                _context.PriceListDetails.Update(item_update);
                _context.SaveChanges();
 
            }
            return Ok('Y');
        }

    }
}