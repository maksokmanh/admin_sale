﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class TaxController : Controller
    {
        private readonly DataContext _context;
        private readonly ITax _tax;
        private readonly IHostingEnvironment _appEnvironment;

        public TaxController(DataContext context, ITax tax, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _tax = tax;
            _appEnvironment = hostingEnvironment;
        }

        // GET: Tax
        public async Task<IActionResult> Index()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Banking";
            ViewBag.Page = "VAT";
            ViewBag.Subpage = "";
            ViewBag.Banking = "show";
            ViewBag.CreateTax = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var data = _tax.GetTax.OrderBy(t => t.ID);
                return View(await _context.Tax.Where(t => t.Delete == false).ToListAsync());
            }
            var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A021");
           
            if(permistion!=null)
            {
                if (permistion.Used == true)
                {
                    var data = _tax.GetTax.OrderBy(t => t.ID);
                    return View(await _context.Tax.Where(t => t.Delete == false).ToListAsync());
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }

        // GET: Tax/Create
        public IActionResult Create()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Banking";
            ViewBag.Page = "VAT";
            ViewBag.Subpage = "";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A021");

            if(permistion!=null)
            {
                if (permistion.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }       
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Rate,Type,Effective,Delete")] Tax tax)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Banking";
            ViewBag.Page = "VAT";
            ViewBag.Subpage = "";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (ModelState.IsValid)
            {
                _context.Add(tax);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tax);
        }

        // GET: Tax/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Banking";
            ViewBag.Page = "VAT";
            ViewBag.Subpage = "";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-plus-circle";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                if (id == null)
                {
                    return NotFound();
                }

                var tax = await _context.Tax.FindAsync(id);
                if (tax == null)
                {
                    return NotFound();
                }
                return View(tax);
            }
            var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A021");
            if(permistion!=null)
            {
                if (permistion.Used == true)
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var tax = await _context.Tax.FindAsync(id);
                    if (tax == null)
                    {
                        return NotFound();
                    }
                    return View(tax);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }  
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Rate,Type,Effective,Delete")] Tax tax)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Banking";
            ViewBag.Page = "VAT";
            ViewBag.Subpage = "";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (id != tax.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tax);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TaxExists(tax.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tax);
        }

        // GET: Tax/Delete/

        // POST: Tax/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tax = await _context.Tax.FindAsync(id);
            _context.Tax.Remove(tax);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TaxExists(int id)
        {
            return _context.Tax.Any(e => e.ID == id);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int? id)
        {
            await _tax.DeleteTax(id);
            return Ok();
        }
    }
}
