﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Promotions;
using POS_WEB.Models.Services.Responsitory;

namespace POS_WEB.Controllers
{   [Authorize]
    public class MemberCardController : Controller
    {
        private readonly IMemberCard _memberCard;
        private readonly DataContext _context;
        public MemberCardController(IMemberCard memberCard,DataContext dataContext)
        {
            _memberCard = memberCard;
            _context = dataContext;
        }

        public IActionResult MemberCard()
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Member";
            ViewBag.Subpage = "Create Card";
            ViewBag.PromotionMenu = "show";
            ViewBag.Member = "show";
            ViewBag.CreateCards = "highlight";

            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CardTypeID"] = new SelectList(_context.CardTypes.Where(x => x.Delete == false), "ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A035");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CardTypeID"] = new SelectList(_context.CardTypes.Where(x => x.Delete == false), "ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
           else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
        [HttpGet]
        public IActionResult GetMemberCard()
        {
            var list = _memberCard.GetMemberCards().ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult InsertMemberCard(MemberCard memberCard)
        {
            _memberCard.AddorEdit(memberCard);
            var list = _memberCard.GetMemberCards().ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult CheckReferentNumber(string Check)
       {
            var list = _context.MemberCards.Where(x => x.Ref_No == Check && x.Delete == false);
            return Ok(list);
        }
        [HttpPost]
        public IActionResult DeleteMemberCard(int ID)
        {
            _memberCard.DeleteMember(ID);
            var list = _memberCard.GetMemberCards().ToList();
            return Ok(list);
        }
        public IActionResult Register()
        {
            ViewBag.style = "fa fa-percent";
            ViewBag.Main = "Promotion";
            ViewBag.Page = "Member";
            ViewBag.Subpage = "Register";
            ViewBag.PromotionMenu = "show";
            ViewBag.Member = "show";
            ViewBag.Register = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CardTypeID"] = new SelectList(_context.CardTypes.Where(x => x.Delete == false), "ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A033");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CardTypeID"] = new SelectList(_context.CardTypes.Where(x => x.Delete == false), "ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
           else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
          
        }
    }
}
