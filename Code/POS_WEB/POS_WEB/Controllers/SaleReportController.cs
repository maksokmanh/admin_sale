﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Org.BouncyCastle.Math.EC.Rfc7748;
using POS_WEB.AppContext;
using POS_WEB.Models.Sale;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.ReportSaleAdmin;

namespace POS_WEB.Controllers
{
    public class SaleReportController : Controller
    {
        private readonly DataContext _context;
        public SaleReportController(DataContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        //Sale Quotation
        public IActionResult SaleQuotationView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale Admin";
                ViewBag.Subpage = "Sale Quotation Report";
                ViewBag.Report = "show";
                ViewBag.SaleAdmin = "show";
                ViewBag.SaleQuotation = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR001");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale Admin";
                        ViewBag.Subpage = "Sale Quotation Report";
                        ViewBag.Report = "show";
                        ViewBag.SaleAdmin = "show";
                        ViewBag.SaleQuotation = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }

        }
        //Sale Order
        public IActionResult SaleOrderView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale Admin";
                ViewBag.Subpage = "Sale Order Report";
                ViewBag.Report = "show";
                ViewBag.SaleAdmin = "show";
                ViewBag.SaleOrder = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR001");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale Admin";
                        ViewBag.Subpage = "Sale Order Report";
                        ViewBag.Report = "show";
                        ViewBag.SaleAdmin = "show";
                        ViewBag.SaleOrder = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }

        }
        //Sale Delivery
        public IActionResult SaleDeliveryView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale Admin";
                ViewBag.Subpage = "Sale Delivery Report";
                ViewBag.Report = "show";
                ViewBag.SaleAdmin = "show";
                ViewBag.SaleDelivery = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR001");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale Admin";
                        ViewBag.Subpage = "Sale Delivery Report";
                        ViewBag.Report = "show";
                        ViewBag.SaleAdmin = "show";
                        ViewBag.SaleDelivery = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }

        }
        //Sale AR
        public IActionResult SaleARView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale Admin";
                ViewBag.Subpage = "Sale A/R Report";
                ViewBag.Report = "show";
                ViewBag.SaleAdmin = "show";
                ViewBag.SaleAR = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR001");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale Admin";
                        ViewBag.Subpage = "Sale A/R Report";
                        ViewBag.Report = "show";
                        ViewBag.SaleAdmin = "show";
                        ViewBag.SaleAR = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }

        }
        //Sale Credit Memo
        public IActionResult SaleCreditMemoView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale Admin";
                ViewBag.Subpage = "Sale Credit Memo Report";
                ViewBag.Report = "show";
                ViewBag.SaleAdmin = "show";
                ViewBag.SaleCreditMemo = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR001");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale Admin";
                        ViewBag.Subpage = "Sale Credit Memo Report";
                        ViewBag.Report = "show";
                        ViewBag.SaleAdmin = "show";
                        ViewBag.SaleCreditMemo = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }

        //Filter Data
        [HttpGet]
        public IActionResult GetSaleQuotation(string DateFrom, string DateTo, int BranchID, int UserID, int CusID)
        {
            List<SaleQuote> saleQuotes = new List<SaleQuote>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID == 0)
            {
                saleQuotes = _context.SaleQuotes.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID == 0)
            {
                saleQuotes = _context.SaleQuotes.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID == 0)
            {
                saleQuotes = _context.SaleQuotes.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleQuotes = _context.SaleQuotes.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID != 0)
            {
                saleQuotes = _context.SaleQuotes.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID != 0)
            {
                saleQuotes = _context.SaleQuotes.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.CusID == CusID).ToList();
            }
            else
            {
                return Ok(new List<SaleQuote>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var summary = GetSummarySaleTotal(DateFrom, DateTo, BranchID, UserID, CusID, "SQ");
            if (summary != null)
            {
                var FiltersaleQuotes = saleQuotes;
                var list = from SQ in FiltersaleQuotes
                           join SQD in _context.SaleQuoteDetails on SQ.SQID equals SQD.SQID
                           join BP in _context.BusinessPartners on SQ.CusID equals BP.ID
                           join I in _context.ItemMasterDatas on SQD.ItemID equals I.ID
                           join CUR in _context.Currency on SQ.LocalCurrencyID equals CUR.ID
                           group new { SQ, SQD, BP, I, CUR } by new { SQ.SQID, SQD.SQDID } into g
                           let data = g.FirstOrDefault()
                           let master = data.SQ
                           let detail = data.SQD
                           let cu = data.CUR
                           select new
                           {
                               //Master
                               InvoiceNo = master.InvoiceNo,
                               CusName = g.First().BP.Name,
                               PostingDate = Convert.ToDateTime(master.PostingDate).ToString("dd-MM-yyyy"),
                               Discount = string.Format("{0:n3}", master.DisValue),
                               Sub_Total = string.Format("{0:n3}", master.SubTotal),
                               VatValue = string.Format("{0:n3}", master.VatValue),
                               TotalAmount = string.Format("{0:n3}", master.TotalAmount),
                               //Detail
                               ItemCode = detail.ItemCode,
                               ItemName = detail.ItemNameKH,
                               Qty = detail.Qty,
                               Uom = detail.UomName,
                               UnitPrice = detail.UnitPrice,
                               DisItem = string.Format("{0:n3}", detail.DisValue),
                               TotalItem = string.Format("{0:n3}", detail.Total),
                               //Summary
                               SumCount = summary.FirstOrDefault().CountInvoice,
                               SumSoldAmount = string.Format("{0:n3}", summary.FirstOrDefault().SoldAmount),
                               SumDisItem = string.Format("{0:n3}", summary.FirstOrDefault().DisCountItem),
                               SumDisTotal = string.Format("{0:n3}", summary.FirstOrDefault().DisCountTotal),
                               SumVat = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().TotalVatRate),
                               SumGrandTotal = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().Total)

                           };
                return Ok(list);
            }
            else
            {
                return Ok(new List<SaleQuote>());
            }
        }
        [HttpGet]
        public IActionResult GetSaleOrder(string DateFrom, string DateTo, int BranchID, int UserID, int CusID)
        {
            List<SaleOrder> saleOrders = new List<SaleOrder>();

            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID == 0)
            {
                saleOrders = _context.SaleOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID == 0)
            {
                saleOrders = _context.SaleOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID == 0)
            {
                saleOrders = _context.SaleOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID != 0)
            {
                saleOrders = _context.SaleOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleOrders = _context.SaleOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleOrders = _context.SaleOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID != 0)
            {
                saleOrders = _context.SaleOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.CusID == CusID).ToList();
            }
            else
            {
                return Ok(new List<SaleOrder>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var summary = GetSummarySaleTotal(DateFrom, DateTo, BranchID, UserID, CusID, "SO");
            if (summary != null)
            {
                var FilterSaleOrder = saleOrders;
                var list = from SO in FilterSaleOrder
                           join SOD in _context.SaleOrderDetails on SO.SOID equals SOD.SOID
                           join BP in _context.BusinessPartners on SO.CusID equals BP.ID
                           join I in _context.ItemMasterDatas on SOD.ItemID equals I.ID
                           join CUR in _context.Currency on SO.LocalCurrencyID equals CUR.ID
                           group new { SO, SOD, BP, I, CUR } by new { SO.SOID, SOD.SODID } into g
                           let data = g.FirstOrDefault()
                           let master = data.SO
                           let detail = data.SOD
                           let cu = data.CUR
                           select new
                           {
                               //Master
                               InvoiceNo = master.InvoiceNo,
                               CusName = g.First().BP.Name,
                               PostingDate = Convert.ToDateTime(master.PostingDate).ToString("dd-MM-yyyy"),
                               Discount = string.Format("{0:n3}", master.DisValue),
                               Sub_Total = string.Format("{0:n3}", master.SubTotal),
                               VatValue = string.Format("{0:n3}", master.VatValue),
                               TotalAmount = string.Format("{0:n3}", master.TotalAmount),
                               //Detail
                               ItemCode = detail.ItemCode,
                               ItemName = detail.ItemNameKH,
                               Qty = detail.Qty,
                               Uom = detail.UomName,
                               UnitPrice = detail.UnitPrice,
                               DisItem = string.Format("{0:n3}", detail.DisValue),
                               TotalItem = string.Format("{0:n3}", detail.Total),
                               //Summary
                               SumCount = summary.FirstOrDefault().CountInvoice,
                               SumSoldAmount = string.Format("{0:n3}", summary.FirstOrDefault().SoldAmount),
                               SumDisItem = string.Format("{0:n3}", summary.FirstOrDefault().DisCountItem),
                               SumDisTotal = string.Format("{0:n3}", summary.FirstOrDefault().DisCountTotal),
                               SumVat = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().TotalVatRate),
                               SumGrandTotal = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().Total)

                           };
                return Ok(list);
            }
            else
            {
                return Ok(new List<SaleOrder>());
            }
        }
        [HttpGet]
        public IActionResult GetSaleDelivery(string DateFrom, string DateTo, int BranchID, int UserID, int CusID)
        {
            List<SaleDelivery> saleDeliveries = new List<SaleDelivery>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID == 0)
            {
                saleDeliveries = _context.SaleDeliveries.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID == 0)
            {
                saleDeliveries = _context.SaleDeliveries.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID == 0)
            {
                saleDeliveries = _context.SaleDeliveries.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID != 0)
            {
                saleDeliveries = _context.SaleDeliveries.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleDeliveries = _context.SaleDeliveries.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleDeliveries = _context.SaleDeliveries.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID != 0)
            {
                saleDeliveries = _context.SaleDeliveries.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.CusID == CusID).ToList();
            }
            else
            {
                return Ok(new List<SaleDelivery>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var summary = GetSummarySaleTotal(DateFrom, DateTo, BranchID, UserID, CusID, "SD");
            if (summary != null)
            {
                var FilterSaleDelivery = saleDeliveries;
                var list = from SD in FilterSaleDelivery
                           join SDD in _context.SaleDeliveryDetails on SD.SDID equals SDD.SDID
                           join BP in _context.BusinessPartners on SD.CusID equals BP.ID
                           join I in _context.ItemMasterDatas on SDD.ItemID equals I.ID
                           join CUR in _context.Currency on SD.LocalCurrencyID equals CUR.ID
                           group new { SD, SDD, BP, I, CUR } by new { SD.SDID, SDD.SDDID } into g
                           let data = g.FirstOrDefault()
                           let master = data.SD
                           let detail = data.SDD
                           let cu = data.CUR
                           select new
                           {
                               //Master
                               InvoiceNo = master.InvoiceNo,
                               CusName = g.First().BP.Name,
                               PostingDate = Convert.ToDateTime(master.PostingDate).ToString("dd-MM-yyyy"),
                               Discount = string.Format("{0:n3}", master.DisValue),
                               Sub_Total = string.Format("{0:n3}", master.SubTotal),
                               VatValue = string.Format("{0:n3}", master.VatValue),
                               TotalAmount = string.Format("{0:n3}", master.TotalAmount),
                               //Detail
                               ItemCode = detail.ItemCode,
                               ItemName = detail.ItemNameKH,
                               Qty = detail.Qty,
                               Uom = detail.UomName,
                               UnitPrice = detail.UnitPrice,
                               DisItem = string.Format("{0:n3}", detail.DisValue),
                               TotalItem = string.Format("{0:n3}", detail.Total),
                               //Summary
                               SumCount = summary.FirstOrDefault().CountInvoice,
                               SumSoldAmount = string.Format("{0:n3}", summary.FirstOrDefault().SoldAmount),
                               SumDisItem = string.Format("{0:n3}", summary.FirstOrDefault().DisCountItem),
                               SumDisTotal = string.Format("{0:n3}", summary.FirstOrDefault().DisCountTotal),
                               SumVat = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().TotalVatRate),
                               SumGrandTotal = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().Total)

                           };
                return Ok(list);
            }
            else
            {
                return Ok(new List<SaleDelivery>());
            }
        }
        [HttpGet]
        public IActionResult GetSaleAR(string DateFrom, string DateTo, int BranchID, int UserID, int CusID)
        {
            List<SaleAR> saleARs = new List<SaleAR>();

            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID == 0)
            {
                saleARs = _context.SaleARs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID == 0)
            {
                saleARs = _context.SaleARs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID == 0)
            {
                saleARs = _context.SaleARs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID != 0)
            {
                saleARs = _context.SaleARs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleARs = _context.SaleARs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleARs = _context.SaleARs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID != 0)
            {
                saleARs = _context.SaleARs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.CusID == CusID).ToList();
            }
            else
            {
                return Ok(new List<SaleAR>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var summary = GetSummarySaleTotal(DateFrom, DateTo, BranchID, UserID, CusID, "SAR");
            if (summary != null)
            {
                var FilterSaleAR = saleARs;
                var list = from SAR in FilterSaleAR
                           join SARD in _context.SaleARDetails on SAR.SARID equals SARD.SARID
                           join BP in _context.BusinessPartners on SAR.CusID equals BP.ID
                           join I in _context.ItemMasterDatas on SARD.ItemID equals I.ID
                           join CUR in _context.Currency on SAR.LocalCurrencyID equals CUR.ID
                           group new { SAR, SARD, BP, I, CUR } by new { SAR.SARID, SARD.SARDID } into g
                           let data = g.FirstOrDefault()
                           let master = data.SAR
                           let detail = data.SARD
                           let cu = data.CUR
                           select new
                           {
                               //Master
                               InvoiceNo = master.InvoiceNo,
                               CusName = g.First().BP.Name,
                               PostingDate = Convert.ToDateTime(master.PostingDate).ToString("dd-MM-yyyy"),
                               Discount = string.Format("{0:n3}", master.DisValue),
                               Sub_Total = string.Format("{0:n3}", master.SubTotal),
                               VatValue = string.Format("{0:n3}", master.VatValue),
                               Applied_Amount = string.Format("{0:n3}", master.AppliedAmount),
                               TotalAmount = string.Format("{0:n3}", master.TotalAmount),
                               //Detail
                               ItemCode = detail.ItemCode,
                               ItemName = detail.ItemNameKH,
                               Qty = detail.Qty,
                               Uom = detail.UomName,
                               UnitPrice = detail.UnitPrice,
                               DisItem = string.Format("{0:n3}", detail.DisValue),
                               TotalItem = string.Format("{0:n3}", detail.Total),
                               //Summary
                               SumCount = summary.FirstOrDefault().CountInvoice,
                               SumSoldAmount = string.Format("{0:n3}",summary.FirstOrDefault().SoldAmount),
                               SumDisItem = string.Format("{0:n3}", summary.FirstOrDefault().DisCountItem),
                               SumDisTotal = string.Format("{0:n3}", summary.FirstOrDefault().DisCountTotal),
                               SumVat = string.Format("{0:n3}", summary.FirstOrDefault().TotalVatRate),
                               SumGrandTotal = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().Total),
                               SumAppliedAmount = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().AppliedAmount)

                           };
                return Ok(list);

            }
            else
            {
                return Ok(new List<SaleAR>());
            }
        }

        [HttpGet]
        public IActionResult GetSaleCreditMemo(string DateFrom, string DateTo, int BranchID, int UserID, int CusID)
        {
            List<SaleCreditMemo> saleCreditMemos = new List<SaleCreditMemo>();

            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID == 0)
            {
                saleCreditMemos = _context.SaleCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID == 0)
            {
                saleCreditMemos = _context.SaleCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID == 0)
            {
                saleCreditMemos = _context.SaleCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && CusID != 0)
            {
                saleCreditMemos = _context.SaleCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleCreditMemos = _context.SaleCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && CusID != 0)
            {
                saleCreditMemos = _context.SaleCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.CusID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && CusID != 0)
            {
                saleCreditMemos = _context.SaleCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.CusID == CusID).ToList();
            }
            else
            {
                return Ok(new List<SaleCreditMemo>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var summary = GetSummarySaleTotal(DateFrom, DateTo, BranchID, UserID, CusID, "SC");
            if (summary != null)
            {
                var FilterSaleCreditMemo = saleCreditMemos;
            var list = from SC in FilterSaleCreditMemo
                       join SCD in _context.SaleCreditMemoDetails on SC.SCMOID equals SCD.SCMOID
                       join BP in _context.BusinessPartners on SC.CusID equals BP.ID
                       join I in _context.ItemMasterDatas on SCD.ItemID equals I.ID
                       join CUR in _context.Currency on SC.LocalCurrencyID equals CUR.ID
                       group new { SC, SCD, BP, I, CUR } by new { SC.SCMOID, SCD.SCMODID } into g
                       let data = g.FirstOrDefault()
                       let master = data.SC
                       let detail = data.SCD
                       let cu =data.CUR
                       select new
                       {
                           //Master
                           InvoiceNo = master.InvoiceNo,
                           CusName = g.First().BP.Name,
                           PostingDate = Convert.ToDateTime(master.PostingDate).ToString("dd-MM-yyyy"),
                           Discount = string.Format("{0:n3}", master.DisValue),
                           Sub_Total = string.Format("{0:n3}", master.SubTotal),
                           VatValue = string.Format("{0:n3}", master.VatValue),
                           TotalAmount = string.Format("{0:n3}", master.TotalAmount),
                           BasedOn= master.BasedCopyKeys,
                           //Detail
                           ItemCode = detail.ItemCode,
                           ItemName = detail.ItemNameKH,
                           Qty = detail.Qty,
                           Uom = detail.UomName,
                           UnitPrice = detail.UnitPrice,
                           DisItem = string.Format("{0:n3}", detail.DisValue),
                           TotalItem = string.Format("{0:n3}", detail.Total),
                           //Summary
                           SumCount = summary.FirstOrDefault().CountInvoice,
                           SumSoldAmount = string.Format("{0:n3}", summary.FirstOrDefault().SoldAmount),
                           SumDisItem = string.Format("{0:n3}", summary.FirstOrDefault().DisCountItem),
                           SumDisTotal = string.Format("{0:n3}", summary.FirstOrDefault().DisCountTotal),
                           SumVat = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().TotalVatRate),
                           SumGrandTotal = cu.Description + " " + string.Format("{0:n3}", summary.FirstOrDefault().Total)

                       };
            return Ok(list);
            }
            else
            {
                return Ok(new List<SaleCreditMemo>());
            }
         }
       //Paramater
       [HttpGet]
        public IActionResult GetCustomer()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Customer").ToList();
            return Ok(list);
        }
        //public IActionResult GetVendor()
        //{
        //    var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Vendor").ToList();
        //    return Ok(list);
        //}
        public IActionResult GetBranch()
        {
            var list = _context.Branches.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        public IActionResult GetEmployee(int BranchID)
        {
            var list = from user in _context.UserAccounts.Where(x => x.Delete == false)
                       join emp in _context.Employees
                       on user.EmployeeID equals emp.ID
                       where user.BranchID == BranchID
                       select new UserAccount
                       {
                           ID = user.ID,
                           Employee = new Employee
                           {
                               Name = emp.Name
                           }
                       };
            return Ok(list);
        }
        //public IActionResult GetWarehouse(int BranchID)
        //{
        //    var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == BranchID).ToList();
        //    return Ok(list);
        //}

        public IEnumerable<SummarySaleAdmin> GetSummarySaleTotal(string DateFrom, string DateTo, int BranchID, int UserID, int CusID, string Type)
        {
            try
            {
                var data = _context.SummarySaleAdmin.FromSql("rp_GetSummarySaleAdminTotal @DateFrom={0},@DateTo={1}, @BranchID={2},@UserID={3},@CusID={4},@Type={5}",
                parameters: new[] {
                    DateFrom.ToString(),
                    DateTo.ToString(),
                    BranchID.ToString(),
                    UserID.ToString(),
                    CusID.ToString(),
                    Type.ToString()
                }).ToList();
                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
