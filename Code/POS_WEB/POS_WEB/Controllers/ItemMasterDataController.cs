﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Inventory;
using POS_WEB.Models.Services.Inventory.PriceList;
using POS_WEB.Models.Services.Page;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;
using CentricKernel;
using NPOI.SS.UserModel;
using Newtonsoft.Json;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class ItemMasterDataController : Controller
    {
        private readonly DataContext _context;
        private readonly IItemMasterData _itemMasterData;
        private readonly IHostingEnvironment _appEnvironment;
        private readonly WorkbookContext _workbook;
        public ItemMasterDataController(DataContext context,IItemMasterData itemMasterData, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _itemMasterData = itemMasterData;
            _appEnvironment = hostingEnvironment;
            _workbook = new WorkbookContext();
        }
        public IActionResult SetCurrentPage(string currPage)
        {
            ViewBag.PageRemeber = currPage;
            return Ok();
        }
        public IActionResult Index()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data ";
            ViewBag.Subpage = "List";
            ViewBag.InventoryMenu = "show";
            ViewBag.ItemMasterData = "highlight";
            if (Pagination.Page == "1/")
            {
                ViewBag.PageRemeber = "1/10/0";
            }
            else
            {
                ViewBag.PageRemeber = Pagination.Page;
            }
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A016");
            var data = _itemMasterData.GetItemMasterData(false).OrderBy(g => g.Code);
          
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View(data);

            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View(data);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
        public IActionResult IndexGrid(string Filergroup=null,string SearchString=null)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data ";
            ViewBag.Subpage = "Grid";
            ViewBag.Menu = "show";
            ViewData["CurrentFilter"] = SearchString;
           
            var data = _itemMasterData.GetMasterDatas();
            var Cate = from s in data select s;
            if (!String.IsNullOrEmpty(SearchString))
            {
               Cate = Cate.Where(s => s.Code.Contains(SearchString) || s.KhmerName.Contains(SearchString) || s.EnglishName.Contains(SearchString));
            }
            return View(Cate);
        }
        public IActionResult GetItems(bool InActive)
        {
            var items = _itemMasterData.GetItemMasterData(InActive);
            return Ok(items);
        }
        [HttpGet]
        public IActionResult GetMaster()
        {
           
            var list = _itemMasterData.GetMasterDatas().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemGroupI()
        {
            var list = _context.ItemGroup1.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemGroupII(int ID)
        {
            var list = _context.ItemGroup2.Where(x => x.Delete == false && x.ItemG1ID==ID && x.Name!="None").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemGroupIII(int ID ,int Group1ID)
        {
            var list = _context.ItemGroup3.Where(x => x.Delete == false && x.ItemG1ID==Group1ID && x.ItemG2ID==ID && x.Name!="None").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemMasterByGroup1(int ID)
        {            
            if (ID == 0)
            {
                var data = _itemMasterData.GetMasterDatas().Where(x => x.Delete == false).ToList();
                return Ok(data);
            }
            else
             {                
               var list=  _itemMasterData.GetMasterDatasByCategory(ID);
                return Ok(list);
            }      
        }
        [HttpGet]
        public IActionResult GetItemMasterByGroup2(int ID)
        {           
             var list = _itemMasterData.GetMasterDatas().Where(x=>x.ItemGroup2ID==ID);
             return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemMasterByGroup3(int ID)
        {
            var list = _itemMasterData.GetMasterDatas().Where(x => x.ItemGroup3ID == ID);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult DetailItemMasterData(int ID)
        {
            var list = _itemMasterData.GetMasterDatas().Where(x => x.ID == ID).ToList();
            return Ok(list);
        }
        public IActionResult Create()
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name != "None"), "ItemG2ID", "Name");
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false & d.Name != "None"), "ID", "Name");
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A016");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name" );
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name!="None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false & d.Name!="None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
           else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Code,KhmerName,EnglishName,StockIn,StockCommit,StockOnHand,Cost,UnitPrice,BaseUomID,PriceListID,GroupUomID,PurchaseUomID,SaleUomID,InventoryUoMID,Type,ItemGroup1ID,ItemGroup2ID,ItemGroup3ID,Inventory,Sale,Purchase,Barcode,PrintToID,Image,Process,Delete,WarehouseID,Description,ManageExpire")] ItemMasterData itemMasterData)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var count = 0;
            if (itemMasterData.PriceListID == 0)
            {
                ViewBag.Pricelist = "Please select pricelist !";
                count++;

            }
            if (itemMasterData.GroupUomID == 0)
            {
                ViewBag.GroupuomError = "Please select  group uom !";
                count++;

            }
            if (itemMasterData.ItemGroup1ID == 0)
            {
                ViewBag.itemgorup1 = "Please select item group ( 1 ) !";
                count++;

            }
            if (itemMasterData.Type == "0" || itemMasterData.Type == null)
            {
                ViewBag.typeError = "Please select type !";
                count++;
            }
            if (itemMasterData.PrintToID == 0 || itemMasterData.PrintToID == null)
            {
                ViewBag.printerError = "Please select printer name !";
                count++;
            }
            if (itemMasterData.Process == "0" || itemMasterData.Process == null)
            {
                ViewBag.processerror = "Please select process !";
                count++;

            }
            if (itemMasterData.Barcode == "0" || itemMasterData.Barcode == null)
            {
                ViewBag.barcodeerror = "Please input  barcode  !";
                count++;
            }
            else
            {
                var checkBarcode = _context.ItemMasterDatas.FirstOrDefault(w => w.Barcode == itemMasterData.Barcode);
                if (checkBarcode!=null)
                {
                    ViewBag.barcodeerror = "This barcode have exist !";
                    count++;
                }
               
            }
            if (itemMasterData.ManageExpire == "0" || itemMasterData.ManageExpire == null)
            {
                ViewBag.ManageExpire = "Please select manage expire !";
                count++;
            }
            if(itemMasterData.WarehouseID==0)
            {
                ViewBag.Warehouse = "Please select warehouse !";
                count++;
            }
            if (!string.IsNullOrWhiteSpace(itemMasterData.Image))
            {
                ViewBag.Image = itemMasterData.Image;
            }

            if (count > 0)
            {
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name", itemMasterData.GroupUomID);
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name", itemMasterData.ItemGroup1ID);
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name!="None"), "ItemG2ID", "Name", itemMasterData.ItemGroup2ID);
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.Name!="None"), "ID", "Name", itemMasterData.ItemGroup3ID);
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PriceListID);
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.InventoryUoMID);
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.SaleUomID);
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PurchaseUomID);
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PrintToID);
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                return View(itemMasterData);
            }
            else
            {
                await _itemMasterData.AddOrEdit(itemMasterData);
                UploadImg(itemMasterData);
                var currency = _context.PriceLists.FirstOrDefault(x => x.ID == itemMasterData.PriceListID && x.Delete == false);
                var companycur = from com in _context.Company.Where(x => x.Delete == false)
                                 join
                                pl in _context.PriceLists on com.PriceListID equals pl.ID
                                 select new Company
                                 {
                                     PriceList = new PriceLists
                                     {
                                         CurrencyID = pl.CurrencyID
                                     }
                                 };
                var defiendUom = _context.GroupDUoMs.Where(x => x.GroupUoMID == itemMasterData.GroupUomID && x.Delete == false).ToList();
                var SysCurrency = 0;
                foreach (var item in companycur)
                {
                    SysCurrency = item.PriceList.CurrencyID;
                }

                //Warenouse Summary
                if(itemMasterData.Process!="Standard")
                {
                    WarehouseSummary warehouseSummary = new WarehouseSummary
                    {
                        WarehouseID = itemMasterData.WarehouseID,
                        ItemID = itemMasterData.ID,
                        InStock = 0,
                        ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                        SyetemDate = Convert.ToDateTime(DateTime.Today),
                        UserID = int.Parse(User.FindFirst("UserID").Value),
                        TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                        CurrencyID = SysCurrency,
                        UomID = Convert.ToInt32(itemMasterData.InventoryUoMID),
                        Cost = 0,
                        Available = 0,
                        Committed = 0,
                        Ordered = 0

                    };
                    await _itemMasterData.AddWarehouseSummary(warehouseSummary);
                }
               
                foreach (var item in defiendUom)
                {
                    //Standard
                    if (itemMasterData.Process == "Standard")
                    {
                        if (item.AltUOM == itemMasterData.SaleUomID)
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = itemMasterData.SaleUomID,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = itemMasterData.Cost,
                                UnitPrice = itemMasterData.UnitPrice
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }
                        else
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = item.AltUOM,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = 0,
                                UnitPrice = 0
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }

                    }
                    //FIFO, Average
                    else
                    {

                        if (item.AltUOM == itemMasterData.SaleUomID)
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = itemMasterData.SaleUomID,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = itemMasterData.Cost,
                                UnitPrice = itemMasterData.UnitPrice
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);

                        }
                        else
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = item.AltUOM,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = 0,
                                UnitPrice = 0
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }
                        //Insert to warehoues detail

                        WarehouseDetail warehouseDetail = new WarehouseDetail
                        {
                            WarehouseID = itemMasterData.WarehouseID,
                            ItemID = itemMasterData.ID,
                            InStock = 0,
                            ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                            SyetemDate = Convert.ToDateTime(DateTime.Today),
                            UserID = int.Parse(User.FindFirst("UserID").Value),
                            TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                            CurrencyID = SysCurrency,
                            UomID = item.AltUOM,
                            Cost = 0,
                            Available = 0,
                            Committed = 0,
                            Ordered = 0

                        };
                        await _itemMasterData.AddWarehouseDeatail(warehouseDetail);
                    }

                }
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name", itemMasterData.GroupUomID);
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name", itemMasterData.ItemGroup1ID);
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name != "None"), "ItemG2ID", "Name", itemMasterData.ItemGroup2ID);
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.Name != "None"), "ID", "Name", itemMasterData.ItemGroup3ID);
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PriceListID);
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.InventoryUoMID);
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.SaleUomID);
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PurchaseUomID);
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PrintToID);
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                
                return RedirectToAction(nameof(Create));
            }  
        }
        public void UploadImg(ItemMasterData itemMasterData)
        {
            try
            {
                var Image = HttpContext.Request.Form.Files[0];
                if (Image != null && Image.Length > 0)
                {
                    var file = Image;
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                    if (file.Length > 0)
                    {
                        //Array.ForEach(Directory.GetFiles(uploads),
                        //    delegate (string path) {
                        //        string fileName = Path.Combine(Path.GetDirectoryName(path), itemMasterData.ID + Path.GetExtension(path));
                        //        System.IO.File.Delete(fileName);
                        //    });
                        //string image = string.Format("{0}{1}", itemMasterData.ID,
                        //    Path.GetExtension(itemMasterData.Image));

                        //itemMasterData.Image = itemMasterData.Image;
                        using (var fileStream = new FileStream(Path.Combine(uploads, itemMasterData.Image), FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }

                    }
                }
            }
            catch (Exception)
            {
            }
        }
        [HttpGet]
        public IActionResult Edit(int id, string currPage)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            Pagination.Page = currPage;
            ViewBag.PageRemeber = currPage;//page/recPage/itemid
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var itemMasterData = _itemMasterData.GetbyId(id);
               
                //ViewBag.Barcode = itemMasterData.Barcode;
                var check = _context.InventoryAudits.Where(x => x.ItemID == id).Count();
                if (check > 0)
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false && d.ID == itemMasterData.GroupUomID), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false && d.ID == itemMasterData.PriceListID), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.ID == itemMasterData.WarehouseID && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    ViewBag.Check = "Yes";
                    ViewBag.Type = itemMasterData.Type;
                    ViewBag.Process = itemMasterData.Process;
                    return View(itemMasterData);
                }
                else
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    return View(itemMasterData);
                }
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A016");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var itemMasterData = _itemMasterData.GetbyId(id);
                    
                    if (itemMasterData == null)
                    {
                        return NotFound();
                    }
                    ViewBag.Code = itemMasterData.Code;
                    ViewBag.Barcode = itemMasterData.Barcode;
                    var check = _context.InventoryAudits.Where(x => x.ItemID == id).Count();
                    if (check > 0)
                    {
                        ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false && d.ID == itemMasterData.GroupUomID), "ID", "Name");
                        ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                        ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                        ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                        ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false && d.ID == itemMasterData.PriceListID), "ID", "Name");
                        ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.ID == itemMasterData.WarehouseID && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                        ViewBag.Check = "Yes";
                        ViewBag.Type = itemMasterData.Type;
                        ViewBag.Process = itemMasterData.Process;
                        return View(itemMasterData);
                    }
                    else
                    {
                        ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                        ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID==itemMasterData.ItemGroup1ID && d.Name !="None"), "ItemG2ID", "Name");
                        ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID==itemMasterData.ItemGroup1ID && d.ItemG2ID==itemMasterData.ItemGroup2ID && d.Name!="None"), "ID", "Name");
                        ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                        return View(itemMasterData);
                    }

                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }   
        }
        [HttpGet]
        public IActionResult Copy(int id, string currPage)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "Copy";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            Pagination.Page = currPage;
            ViewBag.PageRemeber = currPage;//page/recPage/itemid
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var itemMasterData = _itemMasterData.GetbyId(id);
                if (itemMasterData.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (itemMasterData == null)
                {
                    return NotFound();
                }

                //ViewBag.Barcode = itemMasterData.Barcode;
                var check = _context.InventoryAudits.Where(x => x.ItemID == id).Count();
                if (check > 0)
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false && d.ID == itemMasterData.GroupUomID), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false && d.ID == itemMasterData.PriceListID), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.ID == itemMasterData.WarehouseID && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    ViewBag.Check = "Yes";
                    ViewBag.Type = itemMasterData.Type;
                    ViewBag.Process = itemMasterData.Process;
                    return View(itemMasterData);
                }
                else
                {
                    ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                    ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                    ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                    ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                    ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                    return View(itemMasterData);
                }
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A016");
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    var itemMasterData = _itemMasterData.GetbyId(id);

                    if (itemMasterData == null)
                    {
                        return NotFound();
                    }
                    ViewBag.Code = itemMasterData.Code;
                    ViewBag.Barcode = itemMasterData.Barcode;
                    var check = _context.InventoryAudits.Where(x => x.ItemID == id).Count();
                    if (check > 0)
                    {
                        ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false && d.ID == itemMasterData.GroupUomID), "ID", "Name");
                        ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                        ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                        ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                        ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false && d.ID == itemMasterData.PriceListID), "ID", "Name");
                        ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.ID == itemMasterData.WarehouseID && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                        ViewBag.Check = "Yes";
                        ViewBag.Type = itemMasterData.Type;
                        ViewBag.Process = itemMasterData.Process;
                        return View(itemMasterData);
                    }
                    else
                    {
                        ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                        ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                        ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                        ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name");
                        ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                        return View(itemMasterData);
                    }

                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Code,KhmerName,EnglishName,StockIn,StockCommit,StockOnHand,Cost,UnitPrice,BaseUomID,PriceListID,GroupUomID,PurchaseUomID,SaleUomID,InventoryUoMID,Type,ItemGroup1ID,ItemGroup2ID,ItemGroup3ID,Inventory,Sale,Purchase,Barcode,PrintToID,Image,Process,Delete,WarehouseID,Description,Photo,ManageExpire")] ItemMasterData itemMasterData)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var count = 0;
            if (string.IsNullOrWhiteSpace(itemMasterData.KhmerName))
            {
                //ViewBag.khnameerorr = "Please input khmer name !";
                count++;
            }
            if (string.IsNullOrWhiteSpace(itemMasterData.EnglishName))
            {
                //ViewBag.engnameerror = "Please input english name !";
                count++;
            }
            if (itemMasterData.PriceListID == 0)
            {
                ViewBag.Pricelist = "Please select price list !";
                count++;

            }
            if (itemMasterData.GroupUomID == 0)
            {
                ViewBag.GroupuomError = "Please select  group uom !";
                count++;

            }
            if (itemMasterData.ItemGroup1ID == 0)
            {
                ViewBag.itemgorup1 = "Please select item group ( 1 ) !";
                count++;

            }
            if (itemMasterData.Type == "0" || itemMasterData.Type == null)
            {
                ViewBag.typeError = "Please select tyle !";
                count++;
            }
            if (itemMasterData.PrintToID == 0 || itemMasterData.PrintToID == null)
            {
                ViewBag.printerError = "Please select printer name !";
                count++;
            }
            if (itemMasterData.Process == "0" || itemMasterData.Process == null)
            {
                ViewBag.processerror = "Please select process !";
                count++;

            }
            if (itemMasterData.Barcode == "0" || itemMasterData.Barcode == null)
            {
                ViewBag.barcodeerror = "Please input  barcode  !";
                count++;
            }
            else
            {
                var barcode_old = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == itemMasterData.ID);

                if (barcode_old.Barcode != itemMasterData.Barcode)
                {
                    var checkBarcode = _context.ItemMasterDatas.FirstOrDefault(w => w.Barcode == itemMasterData.Barcode);
                    if (checkBarcode != null)
                    {
                        ViewBag.barcodeerror = "This barcode have exist !";
                        count++;
                    }
                }
            }
            if (itemMasterData.ManageExpire == "0" || itemMasterData.ManageExpire == null)
            {
                ViewBag.ManageExpire = "Please select manage expire !";
                count++;
            }
            if (itemMasterData.WarehouseID == 0)
            {
                ViewBag.Warehouse = "Please select warehouse !";
                count++;
            }
            if (count > 0)
            {
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name", itemMasterData.GroupUomID);
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name");
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.Name != "None"), "ItemG2ID", "Name");
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.ItemG1ID == itemMasterData.ItemGroup1ID && d.ItemG2ID == itemMasterData.ItemGroup2ID && d.Name != "None"), "ID", "Name");
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PriceListID);
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.InventoryUoMID);
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.SaleUomID);
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PurchaseUomID);
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PrintToID);
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                return View(itemMasterData);
            }
            else
            {
                UploadImg(itemMasterData);
                var CheckItemProcess = _context.InventoryAudits.Where(x => x.ItemID == itemMasterData.ID).Count();
                var itemMaster = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == itemMasterData.ID);
               
                if (itemMasterData.GroupUomID != itemMaster.GroupUomID || itemMasterData.Process!= itemMaster.Process)
                {
                    _itemMasterData.RemoveItmeInWarehous(itemMasterData.ID);

                    var currency = _context.PriceLists.FirstOrDefault(x => x.ID == itemMasterData.PriceListID && x.Delete == false);
                    var companycur = from com in _context.Company.Where(x => x.Delete == false)
                                     join
                                     pl in _context.PriceLists on com.PriceListID equals pl.ID
                                     select new Company
                                     {
                                         PriceList = new PriceLists
                                         {
                                             CurrencyID = pl.CurrencyID
                                         }
                                     };
                    var defiendUom = _context.GroupDUoMs.Where(x => x.GroupUoMID == itemMasterData.GroupUomID).ToList();
                    var SysCurrency = 0;
                    foreach (var item in companycur)
                    {
                        SysCurrency = item.PriceList.CurrencyID;
                    }
                    //Warenouse Summary
                    if (itemMasterData.Process != "Standard")
                    {
                        WarehouseSummary warehouseSummary = new WarehouseSummary
                        {
                            WarehouseID = itemMasterData.WarehouseID,
                            ItemID = itemMasterData.ID,
                            InStock = 0,
                            ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                            SyetemDate = Convert.ToDateTime(DateTime.Today),
                            UserID = int.Parse(User.FindFirst("UserID").Value),
                            TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                            CurrencyID = SysCurrency,
                            UomID = Convert.ToInt32(itemMasterData.InventoryUoMID),
                            Cost = 0,
                            Available = 0,
                            Committed = 0,
                            Ordered = 0

                        };
                        await _itemMasterData.AddWarehouseSummary(warehouseSummary);
                    }
                    foreach (var item in defiendUom)
                    {

                        //Standard
                        if (itemMasterData.Process == "Standard")
                        {
                            if (item.AltUOM == itemMasterData.SaleUomID)
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = itemMasterData.SaleUomID,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = itemMasterData.Cost,
                                    UnitPrice = itemMasterData.UnitPrice
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }
                            else
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = item.AltUOM,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = 0,
                                    UnitPrice = 0
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }

                        }
                        //FIFO, Average
                        else
                        {

                            if (item.AltUOM == itemMasterData.SaleUomID)
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = itemMasterData.SaleUomID,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = itemMasterData.Cost,
                                    UnitPrice = itemMasterData.UnitPrice
                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);

                            }
                            else
                            {
                                PriceListDetail priceListDetail = new PriceListDetail
                                {

                                    ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                    SystemDate = Convert.ToDateTime(DateTime.Today),
                                    TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                    UserID = int.Parse(User.FindFirst("UserID").Value),
                                    UomID = item.AltUOM,
                                    CurrencyID = currency.CurrencyID,
                                    ItemID = itemMasterData.ID,
                                    PriceListID = itemMasterData.PriceListID,
                                    Cost = 0,
                                    UnitPrice = 0


                                };
                                await _itemMasterData.AddPricelistDetail(priceListDetail);
                            }
                            //Insert to warehoues detail

                            WarehouseDetail warehouseDetail = new WarehouseDetail
                            {
                                WarehouseID = itemMasterData.WarehouseID,
                                ItemID = itemMasterData.ID,
                                InStock = 0,
                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SyetemDate = Convert.ToDateTime(DateTime.Today),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                CurrencyID = SysCurrency,
                                UomID = item.AltUOM,
                                Cost = 0,
                                Available = 0,
                                Committed = 0,
                                Ordered = 0

                            };
                            await _itemMasterData.AddWarehouseDeatail(warehouseDetail);
                        }

                    }
                }
                await _itemMasterData.AddOrEdit(itemMasterData);

                return RedirectToAction(nameof(Index));
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Copy([Bind("ID,Code,KhmerName,EnglishName,StockIn,StockCommit,StockOnHand,Cost,UnitPrice,BaseUomID,PriceListID,GroupUomID,PurchaseUomID,SaleUomID,InventoryUoMID,Type,ItemGroup1ID,ItemGroup2ID,ItemGroup3ID,Inventory,Sale,Purchase,Barcode,PrintToID,Image,Process,Delete,WarehouseID,Description,ManageExpire")] ItemMasterData itemMasterData)
        {
            ViewBag.style = "fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var count = 0;
            if (itemMasterData.PriceListID == 0)
            {
                ViewBag.Pricelist = "Please select pricelist !";
                count++;

            }
            if (itemMasterData.GroupUomID == 0)
            {
                ViewBag.GroupuomError = "Please select  group uom !";
                count++;

            }
            if (itemMasterData.ItemGroup1ID == 0)
            {
                ViewBag.itemgorup1 = "Please select item group ( 1 ) !";
                count++;

            }
            if (itemMasterData.Type == "0" || itemMasterData.Type == null)
            {
                ViewBag.typeError = "Please select type !";
                count++;
            }
            if (itemMasterData.PrintToID == 0 || itemMasterData.PrintToID == null)
            {
                ViewBag.printerError = "Please select printer name !";
                count++;
            }
            if (itemMasterData.Process == "0" || itemMasterData.Process == null)
            {
                ViewBag.processerror = "Please select process !";
                count++;

            }
            if (itemMasterData.Barcode == "0" || itemMasterData.Barcode == null)
            {
                ViewBag.barcodeerror = "Please input  barcode  !";
                count++;
            }
            else
            {
                var checkBarcode = _context.ItemMasterDatas.FirstOrDefault(w => w.Barcode == itemMasterData.Barcode);
                if (checkBarcode != null)
                {
                    ViewBag.barcodeerror = "This barcode have exist !";
                    count++;
                }

            }
            if (itemMasterData.ManageExpire == "0" || itemMasterData.ManageExpire == null)
            {
                ViewBag.ManageExpire = "Please select manage expire !";
                count++;
            }
            if (itemMasterData.WarehouseID == 0)
            {
                ViewBag.Warehouse = "Please select warehouse !";
                count++;
            }
            if (!string.IsNullOrWhiteSpace(itemMasterData.Image))
            {
                ViewBag.Image = itemMasterData.Image;
            }
            
            if (count > 0)
            {
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name", itemMasterData.GroupUomID);
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name", itemMasterData.ItemGroup1ID);
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name != "None"), "ItemG2ID", "Name", itemMasterData.ItemGroup2ID);
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.Name != "None"), "ID", "Name", itemMasterData.ItemGroup3ID);
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PriceListID);
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.InventoryUoMID);
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.SaleUomID);
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PurchaseUomID);
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PrintToID);
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");
                return View(itemMasterData);
            }
            else
            {
                await _itemMasterData.AddOrEdit(itemMasterData);
                UploadImg(itemMasterData);
                var currency = _context.PriceLists.FirstOrDefault(x => x.ID == itemMasterData.PriceListID && x.Delete == false);
                var companycur = from com in _context.Company.Where(x => x.Delete == false)
                                 join
                                pl in _context.PriceLists on com.PriceListID equals pl.ID
                                 select new Company
                                 {
                                     PriceList = new PriceLists
                                     {
                                         CurrencyID = pl.CurrencyID
                                     }
                                 };
                var defiendUom = _context.GroupDUoMs.Where(x => x.GroupUoMID == itemMasterData.GroupUomID && x.Delete == false).ToList();
                var SysCurrency = 0;
                foreach (var item in companycur)
                {
                    SysCurrency = item.PriceList.CurrencyID;
                }

                //Warenouse Summary
                if (itemMasterData.Process != "Standard")
                {
                    WarehouseSummary warehouseSummary = new WarehouseSummary
                    {
                        WarehouseID = itemMasterData.WarehouseID,
                        ItemID = itemMasterData.ID,
                        InStock = 0,
                        ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                        SyetemDate = Convert.ToDateTime(DateTime.Today),
                        UserID = int.Parse(User.FindFirst("UserID").Value),
                        TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                        CurrencyID = SysCurrency,
                        UomID = Convert.ToInt32(itemMasterData.InventoryUoMID),
                        Cost = 0,
                        Available = 0,
                        Committed = 0,
                        Ordered = 0

                    };
                    await _itemMasterData.AddWarehouseSummary(warehouseSummary);
                }

                foreach (var item in defiendUom)
                {
                    //Standard
                    if (itemMasterData.Process == "Standard")
                    {
                        if (item.AltUOM == itemMasterData.SaleUomID)
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = itemMasterData.SaleUomID,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = itemMasterData.Cost,
                                UnitPrice = itemMasterData.UnitPrice
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }
                        else
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = item.AltUOM,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = 0,
                                UnitPrice = 0
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }

                    }
                    //FIFO, Average
                    else
                    {

                        if (item.AltUOM == itemMasterData.SaleUomID)
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = itemMasterData.SaleUomID,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = itemMasterData.Cost,
                                UnitPrice = itemMasterData.UnitPrice
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);

                        }
                        else
                        {
                            PriceListDetail priceListDetail = new PriceListDetail
                            {

                                ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                                SystemDate = Convert.ToDateTime(DateTime.Today),
                                TimeIn = Convert.ToDateTime(DateTime.Now.ToString("h:mm:ss")),
                                UserID = int.Parse(User.FindFirst("UserID").Value),
                                UomID = item.AltUOM,
                                CurrencyID = currency.CurrencyID,
                                ItemID = itemMasterData.ID,
                                PriceListID = itemMasterData.PriceListID,
                                Cost = 0,
                                UnitPrice = 0
                            };
                            await _itemMasterData.AddPricelistDetail(priceListDetail);
                        }
                        //Insert to warehoues detail

                        WarehouseDetail warehouseDetail = new WarehouseDetail
                        {
                            WarehouseID = itemMasterData.WarehouseID,
                            ItemID = itemMasterData.ID,
                            InStock = 0,
                            ExpireDate = Convert.ToDateTime(DateTime.Now.ToString("2019/09/09")),
                            SyetemDate = Convert.ToDateTime(DateTime.Today),
                            UserID = int.Parse(User.FindFirst("UserID").Value),
                            TimeIn = Convert.ToDateTime(DateTime.Now.ToString("hh:mm:ss tt")),
                            CurrencyID = SysCurrency,
                            UomID = item.AltUOM,
                            Cost = 0,
                            Available = 0,
                            Committed = 0,
                            Ordered = 0

                        };
                        await _itemMasterData.AddWarehouseDeatail(warehouseDetail);
                    }

                }
                ViewData["GroupUomID"] = new SelectList(_context.GroupUOMs.Where(d => d.Delete == false), "ID", "Name", itemMasterData.GroupUomID);
                ViewData["ItemGroup1ID"] = new SelectList(_context.ItemGroup1.Where(d => d.Delete == false), "ItemG1ID", "Name", itemMasterData.ItemGroup1ID);
                ViewData["ItemGroup2ID"] = new SelectList(_context.ItemGroup2.Where(d => d.Delete == false && d.Name != "None"), "ItemG2ID", "Name", itemMasterData.ItemGroup2ID);
                ViewData["ItemGroup3ID"] = new SelectList(_context.ItemGroup3.Where(d => d.Delete == false && d.Name != "None"), "ID", "Name", itemMasterData.ItemGroup3ID);
                ViewData["PriceListID"] = new SelectList(_context.PriceLists.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PriceListID);
                ViewData["InventoryUoMID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.InventoryUoMID);
                ViewData["SaleUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.SaleUomID);
                ViewData["PurchaseUomID"] = new SelectList(_context.UnitofMeasures.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PurchaseUomID);
                ViewData["PrintToID"] = new SelectList(_context.PrinterNames.Where(d => d.Delete == false), "ID", "Name", itemMasterData.PrintToID);
                ViewData["WarehouseID"] = new SelectList(_context.Warehouses.Where(x => x.Delete == false && x.BranchID == Convert.ToInt32(User.FindFirst("BranchID").Value)), "ID", "Name");

                return RedirectToAction(nameof(Index));
            }
        }

        [HttpGet]
        public IActionResult GetPrinter()
        {
            var list = _itemMasterData.GetPrinter.Where(p => p.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindeCode(string code)
        {
            var codes = _context.ItemMasterDatas.Where(c => c.Code == code  && c.Delete==false);
            return Ok(codes);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteItemMaster(int ID)
        {
           await _itemMasterData.DeleteItemMaster(ID);
            return Ok();
        }
        [HttpGet] 
        public IActionResult FindbarCode(string barcode)
        {
            var bar = _context.ItemMasterDatas.Where(c => c.Barcode == barcode && c.Delete==false);
            return Ok(bar);
        }
        [HttpPost]
        public IActionResult DeletItemViewGrid(int ID)
        {
             _itemMasterData.DeleteItemMaster(ID);
            var list = _itemMasterData.GetMasterDatas();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult CheckTransactionItem(int ItemID)
        {
            var check = _context.InventoryAudits.Where(x => x.ItemID == ItemID).Count();
            if (check > 0)
            {
                return Ok("Y");
            }
            else
            {
                return Ok("N");
            }
        }
        //Import/Export
        [HttpPost]
        public async Task<IActionResult> Upload()
        {
            ViewBag.style = "fa fa-users";
            ViewBag.Main = "Business Partners";
            ViewBag.Page = "Motocycle Import";
            ViewBag.BizPartnersMenu = "show";
            ViewBag.MotocycleImport = "highlight";

            ModelMessage message = new ModelMessage(ModelState);
            if (Request.Form.Files.Count > 0)
            {
                IFormFile file = Request.Form.Files[0];
                IWorkbook wb = null;
                ISheet sheet = null;
                int sheetIndex = int.Parse(Request.Form["SheetIndex"]);
                Stream fs = new MemoryStream();
                file.CopyTo(fs);
                fs.Position = 0;
                wb = _workbook.ReadWorkbook(fs);
                sheet = wb.GetSheetAt(sheetIndex);

                IList<ItemMasterData> ItemMasterData = new List<ItemMasterData>();
                try
                {
                    var jsons = _workbook.Serialize(sheet);
                    ItemMasterData = JsonConvert.DeserializeObject<IList<ItemMasterData>>(jsons);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("itemMasterData.Barcode", "File is invalid.");
                }

                var errors = new List<string>();
                foreach (var m in ItemMasterData)
                {
                    if (_context.ItemMasterDatas.Count() > 0 && _context.ItemMasterDatas.Any(_m => string.Compare(_m.Barcode, m.Barcode, true) == 0))
                    {
                        errors.Add(m.Barcode);
                    }
                }

                if (errors.Count() > 0)
                {
                    ModelState.AddModelError("itemMasterData.Barcode", string.Format("Items with the following barcodes are already existed: {0}", Newtonsoft.Json.JsonConvert.SerializeObject(errors)));
                }

                if (ModelState.IsValid)
                {
                    foreach (var item in ItemMasterData)
                    {
                        await Create(item);
                    }
                    message.Add("success", "Data has been uploaded.");
                    message.Approve();
                }

            }

            return Ok(message.Bind(ModelState));
        }

        public IActionResult UpdateRow(ItemMasterDataUpdateRow itemMaster)
        {
            ModelMessage msg = new ModelMessage();
            var itemMasterData = _context.ItemMasterDatas.Find(itemMaster.ID);
            if (itemMasterData.Barcode != itemMaster.Barcode)
            {
                var checkBarcode = _context.ItemMasterDatas.FirstOrDefault(w => w.Barcode == itemMaster.Barcode);
                if (checkBarcode != null)
                {
                    //ViewBag.barcodeerror = "This barcode have exist !";
                    msg.Add("barcode", "This barcode already existed.");             
                }
                
            }

            if(msg.Data.Count == 0)
            {
                itemMasterData.Code = itemMaster.Code;
                itemMasterData.KhmerName = itemMaster.KhmerName;
                itemMasterData.EnglishName = itemMaster.EnglishName;
                itemMasterData.Barcode = itemMaster.Barcode;
               
                _context.Update(itemMasterData);
                _context.SaveChanges();
                msg.Approve();
            }

            return Ok(msg);
        }

        [HttpPost]
        public IActionResult UploadImage()
        {
            int itemID = int.Parse(Request.Form["ItemID"]);           
            IFormFile imageFile = HttpContext.Request.Form.Files[0];
            string image = imageFile.FileName;
            if (imageFile != null && imageFile.Length > 0)
            {
                var itemMaster = _context.ItemMasterDatas.Find(itemID);
                if (itemMaster != null)
                {
                    var file = imageFile;
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images");
                    if (file.Length > 0)
                    {
                        //Array.ForEach(Directory.GetFiles(uploads),
                        //    delegate (string path) {
                        //        string fileName = Path.Combine(Path.GetDirectoryName(path), itemID + Path.GetExtension(path));
                        //        System.IO.File.Delete(fileName);
                        //    });
                        //string imagePath = string.Format("{0}{1}", itemID,
                        //Path.GetExtension(image));
                        using (var fileStream = new FileStream(Path.Combine(uploads, image), FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }
                        
                        itemMaster.Image = image;
                        _context.SaveChanges();

                    }
                }
                
            }
            return Ok();
        }

    }
}
