﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class PaymentMeansController : Controller
    {
        private readonly IPaymentMean _context;
        private readonly DataContext _contexts;
        public PaymentMeansController(IPaymentMean context,DataContext contexts)
        {
            _context = context;
            _contexts = contexts;
        }

        // GET: PaymentMeans
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            //Title
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Banking";
            ViewBag.Subpage = "Payment Means";
            ViewBag.Banking = "show";
            ViewBag.PaymentMeans = "highlight";
            //Filter
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            
            var permision = _contexts.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A015");
            var data = _context.PaymentMeans.OrderByDescending(o => o.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["TypeFilter"] = String.IsNullOrEmpty(sortOrder) ? "Type_desc" : "";
                ViewData["Default"] = sortOrder == "Default" ? "Default_desc" : "Default";


                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Type.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "Type_desc":
                        Cate = Cate.OrderByDescending(s => s.Type);
                        break;

                    default:
                        Cate = Cate.OrderBy(s => s.Type);
                        break;
                }
                // int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<PaymentMeans>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["TypeFilter"] = String.IsNullOrEmpty(sortOrder) ? "Type_desc" : "";
                    ViewData["Default"] = sortOrder == "Default" ? "Default_desc" : "Default";


                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Type.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "Type_desc":
                            Cate = Cate.OrderByDescending(s => s.Type);
                            break;

                        default:
                            Cate = Cate.OrderBy(s => s.Type);
                            break;
                    }
                    // int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<PaymentMeans>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
           else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }
        // GET: PaymentMeans/Create
        public IActionResult Create()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Banking";
            ViewBag.Subpage = "Payment Means";
            ViewBag.type = "Create";
            ViewBag.Menu = "show";
            ViewBag.button = "fa-plus-circle";
            var userid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var login = _contexts.LoginModals.FirstOrDefault(x => x.Status == true && x.UserID == int.Parse(User.FindFirst("UserID").Value));
                userid = login.UserID;
            }
            var permision = _contexts.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A015");
            try
            {
                if (permision.Used)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                return View();
            }  
        }
        // POST: PaymentMeans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Type,Status,Default,Delete")] PaymentMeans paymentMeans)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Banking";
            ViewBag.Subpage = "Payment Means";
            ViewBag.type = "Create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";

            if (ModelState.IsValid)
            {
                try
                {
                    await _context.AddOrEdit(paymentMeans);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {

                    ViewBag.ErrorCode = "This code already exist !";
                    return View(paymentMeans);
                }
            }
            return View(paymentMeans);
        }

        // GET: PaymentMeans/Edit/5
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Banking";
            ViewBag.Subpage = "Payment Means";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var login = _contexts.LoginModals.FirstOrDefault(x => x.Status == true && x.UserID == int.Parse(User.FindFirst("UserID").Value));
                userid = login.UserID;
            }
            var permision = _contexts.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A015");
            try
            {
                if (permision.Used)
                {
                    if (ModelState.IsValid)
                    {
                        var payment = _context.GetId(id);
                        if (payment.Delete == true)
                        {
                            return RedirectToAction(nameof(Index));
                        }
                        return View(payment);
                    }
                    return Ok();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                if (ModelState.IsValid)
                {
                    var payment = _context.GetId(id);
                    if (payment.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    return View(payment);
                }
                return Ok();
            }  
        }
        // POST: PaymentMeans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Type,Status,Default,Delete")] PaymentMeans paymentMeans)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Banking";
            ViewBag.Subpage = "Payment Means";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";

            if (id != paymentMeans.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _context.AddOrEdit(paymentMeans);
                return RedirectToAction(nameof(Index));
            }
            return View(paymentMeans);
        }

        // POST: Warehouse/Delete/5
        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            await _context.Delete(id);
            return Ok();
        }
        // POST: Warehouse/SetDefault/5
        //[HttpPost]
        //public async Task<IActionResult> SetDefault(int id)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        await _context.SetDefault(id);
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return Ok();
        //}
        
    }
}
