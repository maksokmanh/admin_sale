﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Inventory.Transaction;
using POS_WEB.Models.Services.POS;

using POS_WEB.Models.Services.ReportSale.dev;
using Rotativa.AspNetCore;

namespace POS_WEB.Controllers
{
    public class DevPrintController : Controller
    {
        private readonly DataContext _context;
        public DevPrintController(DataContext context)
        {
            _context = context;
        }
        //Print Summary Sale
        public IActionResult PrintSummarySale(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserOrderID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, UserID);
            if (Summary != null)
            {
                var Branch = "All";
                var EmpName = "All";
                var Logo = "";
                if (BranchID != 0)
                {
                    Branch = _context.Branches.FirstOrDefault(w=>w.ID==BranchID).Name;
                    Logo = _context.Branches.Include(c=>c.Company).FirstOrDefault(w => w.ID == BranchID).Company.Logo;
                }
                if (UserID != 0)
                {
                    EmpName = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == UserID).Employee.Name;
                }
                var Receipts = receiptsFilter;
                var Sale = from r in Receipts
                           join user in _context.UserAccounts on r.UserOrderID equals user.ID
                           join emp in _context.Employees on user.EmployeeID equals emp.ID
                           join curr in _context.Currency on r.LocalCurrencyID equals curr.ID
                           group new { r, emp, curr } by new { emp.ID} into datas
                           select new GroupSummarySale
                           {
                               EmpCode = datas.First().emp.Code,
                               EmpName = datas.First().emp.Name,
                               SubTotal = datas.Sum(s => s.r.GrandTotal),
                               Receipts = datas.Select(receipt=>new Receipts
                               {
                                   User = receipt.emp.Name,
                                   Receipt = receipt.r.ReceiptNo,
                                   DateIn = receipt.r.DateIn.ToString("dd-MM-yyyy"),
                                   TimeIn = receipt.r.TimeIn,
                                   DateOut = receipt.r.TimeOut,
                                   TimeOut = receipt.r.TimeOut,
                                   GrandTotal = receipt.r.GrandTotal
                               }).ToList(),
                               Header=new Header
                               {
                                   Logo= Logo,
                                   DateFrom =Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                                   DateTo= Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                                   Branch=Branch,
                                   EmpName=EmpName
                               },
                               Footer=new Footer
                               {
                                   CountReceipt = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                                   SoldAmount = string.Format("{0:n2}", Summary.FirstOrDefault().SoldAmount),
                                   DiscountItem = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                                   DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                                   TaxValue = datas.First().curr.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().TaxValue),
                                   GrandTotal = datas.First().curr.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotal)
                                   //GrandTotalSys= datas.First().curr.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotalSys)
                               }
                           };
                if (Sale.Count() > 0)
                {
                    return new ViewAsPdf(Sale.ToList());
                }
            }
            return Ok();
        }
        //Print Detail Sale
        public IActionResult PrintDetailSale(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserOrderID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, UserID);
            if (Summary != null)
            {
                var Branch = "All";
                var EmpName = "All";
                var Logo = "";
                if (BranchID != 0)
                {
                    Branch = _context.Branches.FirstOrDefault(w => w.ID == BranchID).Name;
                    Logo = _context.Branches.Include(c => c.Company).FirstOrDefault(w => w.ID == BranchID).Company.Logo;
                }
                if (UserID != 0)
                {
                    EmpName = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == UserID).Employee.Name;
                }
                var List = from r in receiptsFilter
                            join rd in _context.ReceiptDetail on r.ReceiptID equals rd.ReceiptID
                            join u in _context.UnitofMeasures on rd.UomID equals u.ID
                            join user in _context.UserAccounts on r.UserOrderID equals user.ID
                            join bra in _context.Branches on user.BranchID equals bra.ID
                            join emp in _context.Employees on user.EmployeeID equals emp.ID
                            group new { r, rd, emp, bra, u } by r.ReceiptID into g select new GroupDetailSale
                            {
                                ReceiptID = g.Key,
                                EmpName = g.First().emp.Name,
                                DateIn = g.First().r.DateIn.ToString("dd-MM-yyy"),
                                DateOut = g.First().r.DateOut.ToString("dd-MM-yyy"),
                                ReceiptNo = g.First().r.ReceiptNo,
                                DisInvoice = g.First().r.DiscountValue,
                                TotalTax = g.First().r.TaxValue,
                                Currency = g.First().rd.Currency,
                                GrandTotal = g.First().r.GrandTotal,
                                DetailItems = g
                                .Select(x => new DetailItem
                                {
                                    Code = x.rd.Code,
                                    ItemName = x.rd.KhmerName,
                                    Qty = x.rd.Qty,
                                    UoM =g.First().rd.Currency,
                                    SalePrice = x.rd.UnitPrice,
                                    DisItem = x.rd.DiscountValue,
                                    Total = x.rd.Total,
                                }).ToList(),
                                Header = new Header
                                {
                                    Logo = Logo,
                                    DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                                    DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                                    Branch = Branch,
                                    EmpName = EmpName
                                },
                                Footer = new Footer
                                {
                                    CountReceipt = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                                    SoldAmount = string.Format("{0:n2}", Summary.FirstOrDefault().SoldAmount),
                                    DiscountItem = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                                    DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                                    TaxValue = g.First().rd.Currency + " " + string.Format("{0:n2}", Summary.FirstOrDefault().TaxValue),
                                    GrandTotal = g.First().rd.Currency + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotal)
                                    //GrandTotalSys= datas.First().curr.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotalSys)
                                }
                            };

                if (List.Count() > 0)
                {
                    return new ViewAsPdf(List);
                }
            }
            return Ok();
        }
        //Print Close Shift Detail
        public IActionResult PrintCloseShiftDetail(double TranF, double TranT, int UserID,string Type="POS")
        {
            var Branch = _context.UserAccounts.Include(b => b.Branch).Include(c=>c.Company).FirstOrDefault(w => w.ID == UserID);
            var datas = GetCashoutReport(TranF, TranT, UserID);
            var list= from item in datas  group item by new { item.ItemGroup1,item.ItemGroup2,item.ItemGroup3} into data
                        select new GroupCloseShiftDetail
                        {
                            GroupName = data.First().ItemGroup1 + "/" + data.First().ItemGroup2 + "/" + data.First().ItemGroup3,
                            SubTotal=string.Format("{0:n2}",data.Sum(s=>s.Total)),
                            HeaderCloseShift = new HeaderCloseShift
                            {
                                Logo = Branch.Company.Logo,
                                Branch = Branch.Branch.Name,
                                DateIn = data.First().DateIn,
                                DateOut = data.First().DateOut,
                                EmpName = data.First().EmpName,
                                ExchangeRate = string.Format("{0:n2}", data.First().ExchangeRate)
                            },
                            DetailItems=data.
                            Select(detail=> new DetailItem {
                                Code = detail.Code,
                                ItemName = detail.KhmerName,
                                Qty = detail.Qty,
                                SalePrice = detail.Price,
                                DisItem = detail.DisItemValue,
                                Total = detail.Total,
                            }).ToList(),
                            Footer =new Footer
                            {
                                CountReceipt=string.Format("{0:n0}",datas.Count()),
                                SoldAmount=string.Format("{0:n2}",data.First().TotalSoldAmount),
                                DiscountItem= string.Format("{0:n2}", data.First().TotalDiscountItem),
                                DiscountTotal= string.Format("{0:n2}", data.First().TotalDiscountTotal),
                                TaxValue=data.First().Currency +" "+string.Format("{0:n2}", data.First().TotalVat),
                                GrandTotal= data.First().Currency + " " + string.Format("{0:n2}", data.First().GrandTotal)
                            }
                        };
            return new ViewAsPdf(list);
        }
        //Print Top Sale
        public IActionResult PrintTopSaleQuantity(string DateFrom, string DateTo, int BranchID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, 0);
            if (Summary != null)
            {
                var Branch = "All";
                var Logo = "";
                if (BranchID != 0)
                {
                    Branch = _context.Branches.FirstOrDefault(w => w.ID == BranchID).Name;
                    Logo = _context.Branches.Include(c => c.Company).FirstOrDefault(w => w.ID == BranchID).Company.Logo;
                }
                var Receipts = receiptsFilter;
                var list = from r in Receipts
                           join rd in _context.ReceiptDetail on r.ReceiptID equals rd.ReceiptID
                           join i in _context.ItemMasterDatas on rd.ItemID equals i.ID
                           join g1 in _context.ItemGroup1 on i.ItemGroup1ID equals g1.ItemG1ID
                           join g2 in _context.ItemGroup2 on i.ItemGroup2ID equals g2.ItemG2ID
                           join g3 in _context.ItemGroup3 on i.ItemGroup3ID equals g3.ID
                           join u in _context.UnitofMeasures on rd.UomID equals u.ID
                           group new { r, rd, g1, g2, g3, u, i } by new { g1.ItemG1ID, g2.ItemG2ID, g3.ID} into g
                           
                           select new GroupTopSaleQty
                           {
                               GroupName = g.First().g1.Name + "/" + g.First().g2.Name + "/" + g.First().g3.Name,
                               SubTotal=g.Sum(s=>s.rd.Qty*s.rd.UnitPrice),
                               Items=g.GroupBy(q=> new { q.rd.ItemID,q.rd.UnitPrice}).Select(item=>new Items {
                                   Barcode = item.Max(m=>m.i.Barcode),
                                   ItemCode = item.Max(m => m.rd.Code),
                                   ItemName = item.Max(m => m.rd.KhmerName),
                                   Qty = item.Sum(s => s.rd.Qty).ToString(),
                                   Uom = item.Max(m=>m.u.Name),
                                   Price = string.Format("{0:n0}", item.Max(m=>m.rd.UnitPrice)),
                                   Total = string.Format("{0:n0}",item.Sum(s=>s.rd.Qty*s.rd.UnitPrice))
                               }).ToList(),
                        
                               Header = new Header
                               {
                                   Logo = Logo,
                                   DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                                   DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                                   Branch = Branch
                               },
                               Footer = new Footer
                               {
                                   //CountReceipt = string.Format("{0:n0}",g.Count(),
                                   SoldAmount = string.Format("{0:n2}", Summary.FirstOrDefault().SoldAmount),
                                   DiscountItem = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                                   DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                                   TaxValue = g.First().rd.Currency + " " + string.Format("{0:n2}", Summary.FirstOrDefault().TaxValue),
                                   GrandTotal = g.First().rd.Currency + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotal)
                               }

                           };
                return new ViewAsPdf(list.ToList());
            }
            return Ok();
        }
        //Print Payment Means
        public IActionResult PrintPaymentMeans(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserOrderID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, UserID);
            if (Summary != null)
            {
                var Branch = "All";
                var EmpName = "All";
                var Logo = "";
                if (BranchID != 0)
                {
                    Branch = _context.Branches.FirstOrDefault(w => w.ID == BranchID).Name;
                    Logo = _context.Branches.Include(c => c.Company).FirstOrDefault(w => w.ID == BranchID).Company.Logo;
                }
                if (UserID != 0)
                {
                    EmpName = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == UserID).Employee.Name;
                }
                var Receipts = receiptsFilter;
                var Sale = from p in _context.PaymentMeans
                           join r in Receipts on p.ID equals r.PaymentMeansID
                           join u in _context.UserAccounts on r.UserOrderID equals u.ID
                           join e in _context.Employees on u.EmployeeID equals e.ID
                           join cur in _context.Currency on r.LocalCurrencyID equals cur.ID
                           group new { p, r, e, cur } by new { r.PaymentMeansID } into c
                           select new GroupPaymentMean
                           {
                               PaymentType = c.First().p.Type,
                               SubTotal = c.Sum(s => s.r.GrandTotal),
                               Receipts = c.Select(receipt => new Receipts
                               {
                                   Receipt = receipt.r.ReceiptNo,
                                   User = receipt.e.Name,
                                   DateIn = receipt.r.DateIn.ToString("dd-MM-yyyy"),
                                   TimeIn = receipt.r.TimeIn,
                                   DateOut = receipt.r.TimeOut,
                                   TimeOut = receipt.r.TimeOut,
                                   LocalCurrency = receipt.cur.Description,
                                   GrandTotal = receipt.r.GrandTotal
                               }).ToList(),
                               Header = new Header
                               {
                                   Logo = Logo,
                                   DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                                   DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                                   Branch = Branch,
                                   EmpName = EmpName
                               },
                               Footer = new Footer
                               {
                                   CountReceipt = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                                   SoldAmount = string.Format("{0:n2}", Summary.FirstOrDefault().SoldAmount),
                                   DiscountItem = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                                   DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                                   TaxValue = c.First().cur.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().TaxValue),
                                   GrandTotal = c.First().cur.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotal)
                               }
                           };
                return new ViewAsPdf(Sale.ToList());
            }
            return Ok();
        }
        //Print Sale By Customer
        public IActionResult PrintSaleByCustomer(string DateFrom, string DateTo, int BranchID, int CusID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && CusID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && CusID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && CusID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.CustomerID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && CusID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.CustomerID == CusID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, 0);
            if (Summary != null)
            {
                var Branch = "All";
                var CusName = "All";
                var Logo = "";
                if (BranchID != 0)
                {
                    Branch = _context.Branches.FirstOrDefault(w => w.ID == BranchID).Name;
                    Logo = _context.Branches.Include(c => c.Company).FirstOrDefault(w => w.ID == BranchID).Company.Logo;
                }
                if (CusID != 0)
                {
                    CusName = _context.BusinessPartners.FirstOrDefault(c => c.ID == CusID).Name;
                }
                var List = from r in receiptsFilter
                           join rd in _context.ReceiptDetail on r.ReceiptID equals rd.ReceiptID
                           join u in _context.UnitofMeasures on rd.UomID equals u.ID
                           join user in _context.UserAccounts on r.UserOrderID equals user.ID
                           join bra in _context.Branches on user.BranchID equals bra.ID
                           join emp in _context.Employees on user.EmployeeID equals emp.ID
                           join cus in _context.BusinessPartners on r.CustomerID equals cus.ID
                           group new { r, rd, emp, bra, u, cus } by new { r.CustomerID } into g
                           select new GroupSaleByCustomer
                           {
                               CusName = g.First().cus.Name,
                               SubCusTotal = g.Sum(x => x.rd.Total),
                               MasterDetails = g.GroupBy(d => d.r.ReceiptID).Select(c => new MasterDetails
                               {
                                   EmpName = c.First().emp.Name,
                                   DateIn = c.First().r.DateIn.ToString("dd-MM-yyy"),
                                   DateOut = c.First().r.DateOut.ToString("dd-MM-yyy"),
                                   ReceiptNo = c.First().r.ReceiptNo,
                                   DisInvoice = c.First().r.DiscountValue,
                                   TotalTax = c.First().r.TaxValue,
                                   Currency = c.First().rd.Currency,
                                   GrandTotal = c.First().r.GrandTotal,
                                   DetailItems = c.Select(x => new DetailItem
                                   {
                                       Code = x.rd.Code,
                                       ItemName = x.rd.KhmerName,
                                       Qty = x.rd.Qty,
                                       UoM = g.First().rd.Currency,
                                       SalePrice = x.rd.UnitPrice,
                                       DisItem = x.rd.DiscountValue,
                                       Total = x.rd.Total,
                                   }).ToList(),
                                   SBCHeader = new SBCHeader
                                   {
                                       Logo = Logo,
                                       DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                                       DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                                       Branch = Branch,
                                       CusName = CusName
                                   },
                                   Footer = new Footer
                                   {
                                       CountReceipt = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                                       SoldAmount = string.Format("{0:n2}", Summary.FirstOrDefault().SoldAmount),
                                       DiscountItem = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                                       DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                                       TaxValue = g.First().rd.Currency + " " + string.Format("{0:n2}", Summary.FirstOrDefault().TaxValue),
                                       GrandTotal = g.First().rd.Currency + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotal)
                                       //GrandTotalSys= datas.First().curr.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotalSys)
                                   }
                               }).ToList()
                           };

                if (List.Count() > 0)
                {
                    return new ViewAsPdf(List);
                }
            }
            return Ok();
        }
        //Print Tax Declaration
        public IActionResult PrintTaxDeclaration(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserOrderID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, UserID);
            if (Summary != null)
            {
                var Branch = "All";
                var EmpName = "All";
                var Logo = "";
                if (BranchID != 0)
                {
                    Branch = _context.Branches.FirstOrDefault(w => w.ID == BranchID).Name;
                    Logo = _context.Branches.Include(c => c.Company).FirstOrDefault(w => w.ID == BranchID).Company.Logo;
                }
                if (UserID != 0)
                {
                    EmpName = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == UserID).Employee.Name;
                }
                var Receipts = receiptsFilter;
                var Sale = from r in Receipts
                           join user in _context.UserAccounts on r.UserOrderID equals user.ID
                           join emp in _context.Employees on user.EmployeeID equals emp.ID
                           join curr in _context.Currency on r.LocalCurrencyID equals curr.ID
                           //group new { r, emp, curr } by new { emp.ID } into datas
                           select new GroupTaxDeclaration
                           {
                               EmpCode = emp.Code,
                               EmpName = emp.Name,
                               User = emp.Name,
                               Receipt = r.ReceiptNo,
                               DateIn = r.DateIn.ToString("dd-MM-yyyy"),
                               TimeIn = r.TimeIn,
                               DateOut = r.TimeOut,
                               TimeOut = r.TimeOut,
                               GrandTotal = r.GrandTotal,
                               Tax = r.TaxValue,
                               Header = new Header
                               {
                                   Logo = Logo,
                                   DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                                   DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                                   Branch = Branch,
                                   EmpName = EmpName
                               },
                               Footer = new Footer
                               {
                                   CountReceipt = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                                   SoldAmount = string.Format("{0:n2}", Summary.FirstOrDefault().SoldAmount),
                                   DiscountItem = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                                   DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                                   TaxValue = curr.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().TaxValue),
                                   GrandTotal = curr.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotal)
                                   //GrandTotalSys= datas.First().curr.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().GrandTotalSys)
                               }
                           };
                if (Sale.Count() > 0)
                {
                    return new ViewAsPdf(Sale.ToList());
                }
            }
            return Ok();
        }

        //Inventory
        //Inventory Print
        public IActionResult PrintTransferStock(string DateFrom, string DateTo, int FromBranchID, int ToBranchID, int FromWHID, int ToWHID, int UserID)
        {
            List<Transfer> goodsFilter = new List<Transfer>();
            if (DateFrom != null && DateTo != null && FromBranchID == 0 && ToBranchID == 0 && FromWHID == 0 && ToWHID == 0 && UserID == 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && FromBranchID != 0 && ToBranchID != 0 && FromWHID == 0 && ToWHID == 0 && UserID == 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == FromBranchID && w.BranchToID == ToBranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && FromBranchID != 0 && ToBranchID != 0 && FromWHID != 0 && ToWHID != 0 && UserID == 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == FromBranchID && w.BranchToID == ToBranchID && w.WarehouseFromID == FromWHID && w.WarehouseToID == ToWHID).ToList();
            }
            else if (DateFrom != null && DateTo != null && FromBranchID != 0 && ToBranchID != 0 && FromWHID == 0 && ToWHID == 0 && UserID != 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == FromBranchID && w.BranchToID == ToBranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && FromBranchID != 0 && ToBranchID != 0 && FromWHID != 0 && ToWHID != 0 && UserID != 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == FromBranchID && w.BranchToID == ToBranchID && w.WarehouseFromID == FromWHID && w.WarehouseToID == ToWHID && w.UserID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Transfer>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Logo = "";
            var FromBranch = "All";
            var ToBranch = "All";
            var FromWH = "All";
            var ToWH = "All";
            var EmpName = "All";

            if (FromBranchID != 0 && ToBranchID != 0)
            {
                FromBranch = _context.Branches.FirstOrDefault(w => w.ID == FromBranchID).Name;
                ToBranch = _context.Branches.FirstOrDefault(w => w.ID == ToBranchID).Name;
                Logo = _context.Branches.Include(c => c.Company).FirstOrDefault(w => w.ID == FromBranchID).Company.Logo;
            }
            if (FromWHID != 0 && ToWHID != 0)
            {
                FromWH = _context.Warehouses.FirstOrDefault(x => x.ID == FromWHID).Name;
                ToWH = _context.Warehouses.FirstOrDefault(x => x.ID == ToWHID).Name;
            }
            if (UserID != 0)
            {
                EmpName = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == UserID).Employee.Name;
            }

            var Transfers = goodsFilter;
            var list = from t in Transfers
                       join td in _context.TransferDetails on t.TarmsferID equals td.TransferID
                       group new { t, td } by t.Number into g
                       select new GroupTransferStock
                       {
                           Number = g.FirstOrDefault().t.Number,
                           PosDate = Convert.ToDateTime(g.FirstOrDefault().t.PostingDate).ToString("dd-MM-yyyy"),
                           DocDate = Convert.ToDateTime(g.FirstOrDefault().t.DocumentDate).ToString("dd-MM-yyyy"),
                           Time = Convert.ToDateTime(g.FirstOrDefault().t.Time).ToString("hh:mm tt"),
                           Subtotal = string.Format("{0:n2}", g.Sum(x => x.td.Cost * x.td.Qty)),
                           Goods = g.Select(good => new Goods
                           {
                               Barcode = good.td.Barcode,
                               Code = good.td.Code,
                               KhName = good.td.KhmerName,
                               EngName = good.td.EnglishName,
                               Qty = string.Format("{0:n0}", good.td.Qty),
                               Cost = string.Format("{0:n2}", good.td.Cost),
                               Uom = good.td.UomName,
                               ExpireDate = Convert.ToDateTime(good.td.ExpireDate).ToString("dd-MM-yyyy"),
                           }).ToList(),
                           TFHeader = new TFHeader
                           {
                               Logo = Logo,
                               BranchFrom = FromBranch,
                               BranchTo = ToBranch,
                               WHFrom = FromWH,
                               WHTo = ToWH,
                               EmpName = EmpName,
                               DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                               DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy")
                           },
                           GRFooter = new GRFooter
                           {
                               Currency = g.First().td.Currency,
                               SumGrandTotal = g.Sum(x => x.td.Cost * x.td.Qty)
                           }
                       };

            return new ViewAsPdf(list);
        }
        public IActionResult PrintGoodsReceiptStock(string DateFrom, string DateTo, int BranchID, int WHID, int UserID)
        {
            List<GoodsReceipt> goodsFilter = new List<GoodsReceipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && WHID == 0 && UserID == 0)
            {
                goodsFilter = _context.GoodsReceipts.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID == 0 && UserID == 0)
            {
                goodsFilter = _context.GoodsReceipts.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID != 0 && UserID == 0)
            {
                goodsFilter = _context.GoodsReceipts.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WHID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID != 0 && UserID != 0)
            {
                goodsFilter = _context.GoodsReceipts.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WHID && w.UserID == UserID).ToList();
            }
            else
            {
                return Ok(new List<GoodsReceipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Logo = "";
            var Branch = "All";
            var EmpName = "All";
            var WHName = "All";
            if (BranchID != 0)
            {
                Branch = _context.Branches.FirstOrDefault(w => w.ID == BranchID).Name;
                Logo = _context.Branches.Include(c => c.Company).FirstOrDefault(w => w.ID == BranchID).Company.Logo;
            }
            if (WHID != 0)
            {
                WHName = _context.Warehouses.FirstOrDefault(x => x.ID == WHID).Name;
            }
            if (UserID != 0)
            {
                EmpName = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == UserID).Employee.Name;
            }

            var GoodReceipts = goodsFilter;
            var list = from gr in GoodReceipts
                       join grd in _context.GoodReceiptDetails on gr.GoodsReceiptID equals grd.GoodsReceiptID
                       //join cur in _context.Currency on grd.CurrencyID equals cur.ID
                       group new { gr, grd } by gr.Number_No into g
                       select new GroupGoodsReceiptStock
                       {
                           NumberNo = g.FirstOrDefault().gr.Number_No,
                           PosDate = Convert.ToDateTime(g.FirstOrDefault().gr.PostingDate).ToString("dd-MM-yyyy"),
                           DocDate = Convert.ToDateTime(g.FirstOrDefault().gr.DocumentDate).ToString("dd-MM-yyyy"),
                           Subtotal = string.Format("{0:n2}", g.Sum(x => x.grd.Cost * x.grd.Quantity)),
                           Goods = g.Select(good => new Goods
                           {
                               Barcode = good.grd.BarCode,
                               Code = good.grd.Code,
                               KhName = good.grd.KhmerName,
                               EngName = good.grd.EnglishName,
                               Qty = string.Format("{0:n0}", good.grd.Quantity),
                               Cost = string.Format("{0:n2}", good.grd.Cost),
                               Uom = good.grd.UomName,
                               ExpireDate = Convert.ToDateTime(good.grd.ExpireDate).ToString("dd-MM-yyyy"),
                           }).ToList(),
                           GRHeader = new GRHeader
                           {
                               Logo = Logo,
                               Branch = Branch,
                               EmpName = EmpName,
                               WareHouse = WHName,
                               DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                               DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy")
                           },
                           GRFooter = new GRFooter
                           {
                               Currency = g.First().grd.Currency,
                               SumGrandTotal = g.Sum(x => x.grd.Cost * x.grd.Quantity)
                           }
                       };

            return new ViewAsPdf(list);
        }
        public IActionResult PrintGoodsIssueStock(string DateFrom, string DateTo, int BranchID, int WHID, int UserID)
        {
            List<GoodIssues> goodsFilter = new List<GoodIssues>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && WHID == 0 && UserID == 0)
            {
                goodsFilter = _context.GoodIssues.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID == 0 && UserID == 0)
            {
                goodsFilter = _context.GoodIssues.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID != 0 && UserID == 0)
            {
                goodsFilter = _context.GoodIssues.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WHID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID != 0 && UserID != 0)
            {
                goodsFilter = _context.GoodIssues.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WHID && w.UserID == UserID).ToList();
            }
            else
            {
                return Ok(new List<GoodIssues>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Logo = "";
            var Branch = "All";
            var EmpName = "All";
            var WHName = "All";
            if (BranchID != 0)
            {
                Branch = _context.Branches.FirstOrDefault(w => w.ID == BranchID).Name;
                Logo = _context.Branches.Include(c => c.Company).FirstOrDefault(w => w.ID == BranchID).Company.Logo;
            }
            if (WHID != 0)
            {
                WHName = _context.Warehouses.FirstOrDefault(x => x.ID == WHID).Name;
            }
            if (UserID != 0)
            {
                EmpName = _context.UserAccounts.Include(emp => emp.Employee).FirstOrDefault(w => w.ID == UserID).Employee.Name;
            }

            var GoodIssues = goodsFilter;
            var list = from gi in GoodIssues
                       join gid in _context.GoodIssuesDetails on gi.GoodIssuesID equals gid.GoodIssuesID
                       //join cur in _context.Currency on grd.CurrencyID equals cur.ID
                       group new { gi, gid } by gi.Number_No into g
                       select new GroupGoodsIssueStock
                       {
                           NumberNo = g.FirstOrDefault().gi.Number_No,
                           PosDate = Convert.ToDateTime(g.FirstOrDefault().gi.PostingDate).ToString("dd-MM-yyyy"),
                           DocDate = Convert.ToDateTime(g.FirstOrDefault().gi.DocumentDate).ToString("dd-MM-yyyy"),
                           Subtotal = string.Format("{0:n2}", g.Sum(x => x.gid.Cost * x.gid.Quantity)),
                           Goods = g.Select(good => new Goods
                           {
                               Barcode = good.gid.BarCode,
                               Code = good.gid.Code,
                               KhName = good.gid.KhmerName,
                               EngName = good.gid.EnglishName,
                               Qty = string.Format("{0:n0}", good.gid.Quantity),
                               Cost = string.Format("{0:n2}", good.gid.Cost),
                               Uom = good.gid.UomName,
                               ExpireDate = Convert.ToDateTime(good.gid.ExpireDate).ToString("dd-MM-yyyy"),
                           }).ToList(),
                           GRHeader = new GRHeader
                           {
                               Logo = Logo,
                               Branch = Branch,
                               EmpName = EmpName,
                               WareHouse = WHName,
                               DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                               DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy")
                           },
                           GRFooter = new GRFooter
                           {
                               Currency = g.First().gid.Currency,
                               SumGrandTotal = g.Sum(x => x.gid.Cost * x.gid.Quantity)
                           }
                       };

            return new ViewAsPdf(list);
        }

        public IEnumerable<SummaryTotalSale> GetSummaryTotals(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            try
            {
                var data = _context.SummaryTotalSale.FromSql("rp_GetSummarrySaleTotal @DateFrom={0},@DateTo={1}, @BranchID={2},@UserID={3}",
                parameters: new[] {
                    DateFrom.ToString(),
                    DateTo.ToString(),
                    BranchID.ToString(),
                    UserID.ToString()
                }).ToList();
                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        public IEnumerable<CashoutReport> GetCashoutReport(double Tran_F, double Tran_T, int UserID) => _context.CashoutReport.FromSql("rp_GetCashoutReport @Tran_F={0},@Tran_T={1},@UserID={2}",
          parameters: new[] {
                Tran_F.ToString(),
                Tran_T.ToString(),
                UserID.ToString()
          }).ToList();
    }
}