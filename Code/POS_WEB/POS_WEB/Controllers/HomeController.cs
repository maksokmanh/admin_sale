﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models;
using POS_WEB.Models.Services;
using POS_WEB.Models.Services.ReportSale.dev;
using POS_WEB.Models.Services.Responsitory;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly DataContext _context;
        private readonly IReport _report;
        public HomeController(DataContext context,IReport report)
        {
            _context = context;
            _report = report;
        }
        public IActionResult Test()
        {
            return View();
        }
        public IActionResult TestCss()
        {
            return View();
        }
        public IActionResult Dashboard()
        {
            ViewBag.style = "fa fa-home";
            ViewBag.Main = "Dashborad";
            ViewBag.Page = "Dashborad";
  
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
       
        [HttpPost]
        public IActionResult selectLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new Microsoft.AspNetCore.Http.CookieOptions { Expires = DateTimeOffset.UtcNow.AddDays(1) }
                );
            return LocalRedirect(returnUrl);
        }
        //DashBoard
        [HttpGet]
        public IActionResult GetStock(int BranchID)
        {
            var Date = Convert.ToDateTime(DateTime.Now);
            var Sale = from whs in _context.WarehouseSummary
                       join item in _context.ItemMasterDatas on whs.ItemID equals item.ID
                       join uom in _context.UnitofMeasures on whs.UomID equals uom.ID
                       join wh in _context.Warehouses on whs.WarehouseID equals wh.ID
                       where item.Delete == false && item.Inventory == true && item.Purchase == true && item.Type != "Standard" && wh.BranchID == BranchID
                       group new { whs, wh, item, uom } by new { wh.ID } into data
                       select new
                       {
                           Warehouse = data.First().wh.Name,
                           Instock = data.Sum(x => x.whs.InStock)
                       };
            return Ok(Sale);
        }
        public IActionResult GetPurchase(int BranchID)
        {
            var Date = Convert.ToDateTime(DateTime.Now);
            var list = from AP in _context.Purchase_APs
                       join BP in _context.BusinessPartners on AP.VendorID equals BP.ID
                       join CU in _context.Currency on AP.LocalCurrencyID equals CU.ID
                       where AP.PostingDate == Date && AP.BranchID == BranchID
                       group new { AP, BP, CU } by BP.Code into datas
                       let data = datas.FirstOrDefault()
                       select new
                       {
                           Name = data.BP.Name + " (" + data.CU.Description + ") ",
                           GrandTotal = datas.Sum(s => s.AP.Balance_Due)
                       };
            return Ok(list);
        }
        public IActionResult GetTopSale(int BranchID)
        {
            var Date = Convert.ToDateTime(DateTime.Now);
            var list = from rv in _context.RevenueItems.Where(x=>x.SystemDate == Date)
                       join i in _context.ItemMasterDatas on rv.ItemID equals i.ID
                       join u in _context.UnitofMeasures on rv.UomID equals u.ID
                       group new { rv, i, u } by new { rv.ItemID } into g

                       let data = g.FirstOrDefault()
                       select new
                       {
                           ItemName = data.i.KhmerName + " " + data.u.Name,
                           Qty = g.Where(w => w.rv.Trans_Type == "SO").Sum(c => c.rv.Qty) - g.Where(w => w.rv.Trans_Type == "SR" || w.rv.Trans_Type == "SC").Sum(c => c.rv.Qty)
                       };
            return Ok(list.OrderByDescending(o => o.Qty));
        }
        public IActionResult GetProfit(int BranchID)
        {
            var Date1 = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
            var Date2 = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
            var ChartProfit = GetSummaryTotals(Date1, Date2, BranchID, 0);
            return Ok(ChartProfit);
        }




        public IEnumerable<SummaryTotalSale> GetSummaryTotals(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            try
            {
                var data = _context.SummaryTotalSale.FromSql("rp_GetSummarrySaleTotal @DateFrom={0},@DateTo={1}, @BranchID={2},@UserID={3}",
                parameters: new[] {
                    DateFrom.ToString(),
                    DateTo.ToString(),
                    BranchID.ToString(),
                    UserID.ToString()
                }).ToList();
                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
