﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Inventory.Category;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers.Developer2
{
    [Authorize]
    public class ItemGroup1Controller : Controller
    {
        private readonly DataContext _context;
        private readonly IItemGroup _itemGroup;
        private readonly IHostingEnvironment _appEnvironment;

        public ItemGroup1Controller(DataContext context,IItemGroup itemGroup, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _itemGroup = itemGroup;
            _appEnvironment = hostingEnvironment;

        }

        // GET: ItemGroup1
        public async Task<IActionResult> Index(string minpage = "All", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(1)";
            ViewBag.InventoryMenu = "show";
            ViewBag.ItemGroup = "show";
            ViewBag.ItemGroup1 = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A018");
            var data = _itemGroup.GetItemGroup1s().OrderByDescending(x=>x.ItemG1ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
                ViewData["Color"] = sortOrder == "color" ? "color_desc" : "color";
                ViewData["backgroud"] = sortOrder == "back" ? "back_desc" : "back";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewData["CurrentFilter"] = searchString;
                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString)
                    || s.Images.Contains(searchString) || s.Background.Name.Contains(searchString) || s.Colors.Name.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "Date":
                        Cate = Cate.OrderBy(s => s.Images);
                        break;
                    case "color_desc":
                        Cate = Cate.OrderBy(s => s.Colors.Name);
                        break;
                    case "color":
                        Cate = Cate.OrderByDescending(s => s.Colors.Name);
                        break;
                    case "back_desc":
                        Cate = Cate.OrderBy(s => s.Background.Name);
                        break;
                    case "back":
                        Cate = Cate.OrderByDescending(s => s.Background.Name);
                        break;
                    case "date_desc":
                        Cate = Cate.OrderByDescending(s => s.Images);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                int.TryParse(minpage, out MaxSize);
                if (MaxSize == 0)
                {
                    int d = data.Count();
                    if (d == 0) d = 1;
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<ItemGroup1>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
                    ViewData["Color"] = sortOrder == "color" ? "color_desc" : "color";
                    ViewData["backgroud"] = sortOrder == "back" ? "back_desc" : "back";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;
                    }
                    ViewData["CurrentFilter"] = searchString;
                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString)
                        || s.Images.Contains(searchString) || s.Background.Name.Contains(searchString) || s.Colors.Name.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "Date":
                            Cate = Cate.OrderBy(s => s.Images);
                            break;
                        case "color_desc":
                            Cate = Cate.OrderBy(s => s.Colors.Name);
                            break;
                        case "color":
                            Cate = Cate.OrderByDescending(s => s.Colors.Name);
                            break;
                        case "back_desc":
                            Cate = Cate.OrderBy(s => s.Background.Name);
                            break;
                        case "back":
                            Cate = Cate.OrderByDescending(s => s.Background.Name);
                            break;
                        case "date_desc":
                            Cate = Cate.OrderByDescending(s => s.Images);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);
                    //int t = int.Parse(category);
                    //int t1 = Convert.ToInt16(category);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        if (d == 0) d = 1;
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<ItemGroup1>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }

        public IActionResult Create()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(1)";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["Name"] = new SelectList(_context.Colors, "ColorID", "Name");
                ViewData["Name"] = new SelectList(_context.Backgrounds, "BackID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A018");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["Name"] = new SelectList(_context.Colors, "ColorID", "Name");
                    ViewData["Name"] = new SelectList(_context.Backgrounds, "BackID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }          
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ItemG1ID,Name,Images,Delete,ColorID,BackID,Visible")] ItemGroup1 itemGroup1)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(1)";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath,"Images/itemgroup");
                        if (file.Length > 0)
                        {
                            //var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, itemGroup1.Images), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemGroup1.Images = itemGroup1.Images;
                            }

                        }
                    }
                }              
                await _itemGroup.AddItemGroup(itemGroup1);
                return RedirectToAction(nameof(Index));
            }
            ViewData["Name"] = new SelectList(_context.Colors, "ColorID", "Name");
            ViewData["Name"] = new SelectList(_context.Backgrounds, "BackID", "Name");
            return View(itemGroup1);
        }

    
        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(1)";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["Name"] = new SelectList(_context.Colors, "ColorID", "Name");
                ViewData["Name"] = new SelectList(_context.Backgrounds, "BackID", "Name");
                var itemGroup1 = _itemGroup.GetbyID(id);
                if (itemGroup1.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (itemGroup1 == null)
                {
                    return NotFound();
                }
                return View(itemGroup1);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A018");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["Name"] = new SelectList(_context.Colors, "ColorID", "Name");
                    ViewData["Name"] = new SelectList(_context.Backgrounds, "BackID", "Name");
                    var itemGroup1 = _itemGroup.GetbyID(id);
                    if (itemGroup1.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (itemGroup1 == null)
                    {
                        return NotFound();
                    }
                    return View(itemGroup1);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }    
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ItemG1ID,Name,Images,Delete,ColorID,BackID,Visible")] ItemGroup1 itemGroup1)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(1)";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            if (ModelState.IsValid)
            {

                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images/itemgroup");
                        if (file.Length > 0)
                        {
                            //var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, itemGroup1.Images), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemGroup1.Images = itemGroup1.Images;
                            }

                        }
                    }
                }

                await _itemGroup.AddItemGroup(itemGroup1);
               
                return RedirectToAction(nameof(Index));
            }
            ViewData["Name"] = new SelectList(_context.Colors, "ColorID", "Name");
            ViewData["Name"] = new SelectList(_context.Backgrounds, "BackID", "Name");
            return View(itemGroup1);
        }

        [HttpGet]
        public IActionResult GetColor(int id)
        {
            var list = _itemGroup.GetColors.Where(c => c.ColorID == id);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBackground(int id)
        {
            var list = _itemGroup.Backgrounds.Where(c => c.BackID == id);
            return Ok(list);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteCateory(int ID)
        {
            await _itemGroup.DeleteItemGroup(ID);
            return Ok();
        }
        [HttpGet]
        public IActionResult SelecrColor()
        {
            var list = _itemGroup.GetColors.OrderByDescending(x=>x.ColorID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult SelectBackground()
        {
            var list = _itemGroup.Backgrounds.OrderByDescending(x=>x.BackID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public async Task<IActionResult> QuickCreate(ItemGroup1 itemGroup1,IFormFile Photo)
        {  
              await _itemGroup.AddItemGroup(itemGroup1);
              return Ok();
        }
        [HttpGet]
        public IActionResult GetItemGroup()
        {
            var list = _itemGroup.GetItemGroup1s().OrderByDescending(x => x.ItemG1ID).ToList();
             return Ok(list);
        }
        [HttpPost]
        public async Task<IActionResult> Additmegroup1(ItemGroup1 itemGroup1)
        {
            var list = await _itemGroup.AddItemGroup(itemGroup1);
            return Ok(list);
        }
       [HttpPost]
       public IActionResult AddColors(Colors colors)
        {
            _context.Colors.Add(colors);
            _context.SaveChanges();
            return Ok();
        }
        [HttpPost]
        public IActionResult AddBackground(Background background)
        {
            _context.Backgrounds.Add(background);
            _context.SaveChanges();
            return Ok();
        }
    }
}
