﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NPOI.OpenXmlFormats.Spreadsheet;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Inventory;
using POS_WEB.Models.Services.Inventory.Transaction;
using POS_WEB.Models.Services.POS;
using POS_WEB.Models.Services.Purchase;
using POS_WEB.Models.Services.ReportInventory;
using POS_WEB.Models.Services.ReportPurchase.dev;
using POS_WEB.Models.Services.ReportSale;
using POS_WEB.Models.Services.ReportSale.dev;
using Rotativa.AspNetCore;

namespace POS_WEB.Controllers
{
    public class DevReportController : Controller
    {
        private readonly DataContext _context;
        public DevReportController(DataContext context)
        {
            _context = context;
        }
        //Sale View
        [HttpGet]
        public IActionResult SummarySaleView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale";
                ViewBag.Subpage = "Summary Sale Report";
                ViewBag.Report = "show";
                ViewBag.Sale = "show";
                ViewBag.SummarySale = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR001");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale";
                        ViewBag.Subpage = "Summary Sale Report";
                        ViewBag.Report = "show";
                        ViewBag.Sale = "show";
                        ViewBag.SummarySale = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult DetailSaleView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale";
                ViewBag.Subpage = "Detail Sale Report";
                ViewBag.Report = "show";
                ViewBag.Sale = "show";
                ViewBag.DetailSale = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR005");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale";
                        ViewBag.Subpage = "Detail Sale Report";
                        ViewBag.Report = "show";
                        ViewBag.Sale = "show";
                        ViewBag.DetailSale = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult CloseShiftView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale";
                ViewBag.Subpage = "Close Shift Report";
                ViewBag.Report = "show";
                ViewBag.Sale = "show";
                ViewBag.CloseShift = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR002");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale";
                        ViewBag.Subpage = "Close Shift Report";
                        ViewBag.Report = "show";
                        ViewBag.Sale = "show";
                        ViewBag.CloseShift = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult RevenueItemView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale";
                ViewBag.Subpage = "Profit & Loss Report";
                ViewBag.Report = "show";
                ViewBag.Sale = "show";
                ViewBag.RevenueItem = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR004");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale";
                        ViewBag.Subpage = "Profit & Loss Report";
                        ViewBag.Report = "show";
                        ViewBag.Sale = "show";
                        ViewBag.RevenueItem = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult TopSaleQuantityView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale";
                ViewBag.Subpage = "Top Sale Quantity";
                ViewBag.Report = "show";
                ViewBag.Sale = "show";
                ViewBag.TopSaleQuantity = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR003");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale";
                        ViewBag.Subpage = "Top Sale Quantity";
                        ViewBag.Report = "show";
                        ViewBag.Sale = "show";
                        ViewBag.TopSaleQuantity = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult PaymentMeansView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale";
                ViewBag.Subpage = "Payment Means";
                ViewBag.Report = "show";
                ViewBag.Sale = "show";
                ViewBag.PaymentMeans = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR006");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale";
                        ViewBag.Subpage = "Payment Means";
                        ViewBag.Report = "show";
                        ViewBag.Sale = "show";
                        ViewBag.PaymentMeans = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult SaleByCustomerView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale";
                ViewBag.Subpage = "Sale By Customer Report";
                ViewBag.Report = "show";
                ViewBag.Sale = "show";
                ViewBag.SaleByCustomer = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR007");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale";
                        ViewBag.Subpage = "Sale By Customer Report";
                        ViewBag.Report = "show";
                        ViewBag.Sale = "show";
                        ViewBag.SaleByCustomer = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult TaxDeclarationView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Sale";
                ViewBag.Subpage = "Tax Declaration Report";
                ViewBag.Report = "show";
                ViewBag.Sale = "show";
                ViewBag.TaxDeclaration = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR008");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Sale";
                        ViewBag.Subpage = "Tax Declaration Report";
                        ViewBag.Report = "show";
                        ViewBag.Sale = "show";
                        ViewBag.TaxDeclaration = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }

        //Purchase View
        public IActionResult SummaryPurchaseOrder()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Purchase";
                ViewBag.Subpage = "Purchaes Order Summary";
                ViewBag.Report = "show";
                ViewBag.Purchase = "show";
                ViewBag.POSummary = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "PR001");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Purchase";
                        ViewBag.Subpage = "Purchaes Order Summary";
                        ViewBag.Report = "show";
                        ViewBag.Purchase = "show";
                        ViewBag.POSummary = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }

        public IActionResult SummaryGoodsReceiptPO()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Purchase";
                ViewBag.Subpage = "Goods Receipt PO Summary";
                ViewBag.Report = "show";
                ViewBag.Purchase = "show";
                ViewBag.GRPOSummary = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "PR005");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Purchase";
                        ViewBag.Subpage = "Goods Receipt PO Summary";
                        ViewBag.Report = "show";
                        ViewBag.Purchase = "show";
                        ViewBag.GRPOSummary = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }
        public IActionResult SummaryPurchaseAP()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Purchase";
                ViewBag.Subpage = "Purchase AP Summary";
                ViewBag.Report = "show";
                ViewBag.Purchase = "show";
                ViewBag.APSummary = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "PR002");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Purchase";
                        ViewBag.Subpage = "Purchase AP Summary";
                        ViewBag.Report = "show";
                        ViewBag.Purchase = "show";
                        ViewBag.APSummary = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }
        public IActionResult SummaryPurchaseMemo()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Purchase";
                ViewBag.Subpage = "Purchase CreditMemo Summary";
                ViewBag.Report = "show";
                ViewBag.Purchase = "show";
                ViewBag.PCSummary = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "PR003");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Purchase";
                        ViewBag.Subpage = "Purchase CreditMemo Summary";
                        ViewBag.Report = "show";
                        ViewBag.Purchase = "show";
                        ViewBag.PCSummary = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }
        public IActionResult SummaryOutgoingPayment()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Purchase";
                ViewBag.Subpage = "Outgoing Payment Summary";
                ViewBag.Report = "show";
                ViewBag.Purchase = "show";
                ViewBag.OutgoingPayment = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "PR004");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Outgoing Payment Summary";
                        ViewBag.Report = "show";
                        ViewBag.Purchase = "show";
                        ViewBag.OutgoingPayment = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }

        //Inventory View
        public IActionResult StockInWarehouseView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Stock In Warehouse";
                ViewBag.Report = "show";
                ViewBag.InventoryReport = "show";
                ViewBag.StockInWarehouse = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "IR001");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Stock In Warehouse";
                        ViewBag.Report = "show";
                        ViewBag.InventoryReport = "show";
                        ViewBag.StockInWarehouse = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }
        public IActionResult InventoryAuditView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Inventory";
                ViewBag.Subpage = "Inventory Audit";
                ViewBag.Report = "show";
                ViewBag.InventoryReport = "show";
                ViewBag.InventoryAudit = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "IR005");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Inventory";
                        ViewBag.Subpage = "Inventory Audit";
                        ViewBag.Report = "show";
                        ViewBag.InventoryReport = "show";
                        ViewBag.InventoryAudit = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }
        public IActionResult StockMovingView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Inventory";
                ViewBag.Subpage = "Stock Moving";
                ViewBag.Report = "show";
                ViewBag.InventoryReport = "show";
                ViewBag.StockMoving = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "IR006");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Inventory";
                        ViewBag.Subpage = "Stock Moving";
                        ViewBag.Report = "show";
                        ViewBag.InventoryReport = "show";
                        ViewBag.StockMoving = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }
        public IActionResult StockExpiredView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Inventory";
                ViewBag.Subpage = "Stock Expired";
                ViewBag.Report = "show";
                ViewBag.InventoryReport = "show";
                ViewBag.StockExpired = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "IR007");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Inventory";
                        ViewBag.Subpage = "Stock Expired";
                        ViewBag.Report = "show";
                        ViewBag.InventoryReport = "show";
                        ViewBag.StockExpired = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }

            }
        }
        public IActionResult TransferStockView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Inventory";
                ViewBag.Subpage = "Transfer Stock";
                ViewBag.Report = "show";
                ViewBag.InventoryReport = "show";
                ViewBag.TransferStock = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR002");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Inventory";
                        ViewBag.Subpage = "Transfer Stock";
                        ViewBag.Report = "show";
                        ViewBag.InventoryReport = "show";
                        ViewBag.TransferStock = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult GoodsReceiptStockView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Inventory";
                ViewBag.Subpage = "Goods Receipt Stock";
                ViewBag.Report = "show";
                ViewBag.InventoryReport = "show";
                ViewBag.GoodsReceiptStock = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR003");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Inventory";
                        ViewBag.Subpage = "Goods Receipt Stock";
                        ViewBag.Report = "show";
                        ViewBag.InventoryReport = "show";
                        ViewBag.GoodsReceiptStock = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        public IActionResult GoodsIssueStockView()
        {
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewBag.style = "fa fa-chart-line";
                ViewBag.Main = "Report";
                ViewBag.Page = "Inventory";
                ViewBag.Subpage = "Goods Issue Stock";
                ViewBag.Report = "show";
                ViewBag.InventoryReport = "show";
                ViewBag.GoodsIssueStock = "highlight";
                return View();
            }
            else
            {
                int userid = int.Parse(User.FindFirst("UserID").Value);
                var permistion = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "SR004");
                if (permistion != null)
                {
                    if (permistion.Used == true)
                    {
                        ViewBag.style = "fa fa-chart-line";
                        ViewBag.Main = "Report";
                        ViewBag.Page = "Inventory";
                        ViewBag.Subpage = "Goods Issue Stock";
                        ViewBag.Report = "show";
                        ViewBag.InventoryReport = "show";
                        ViewBag.GoodsIssueStock = "highlight";
                        return View();
                    }
                    else
                    {
                        return RedirectToAction("AccessDenied", "Account");
                    }
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
        }
        //End View

        //Get Data
        public IActionResult SummarySaleReport(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserOrderID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, UserID);
            if (Summary != null)
            {
                var Receipts = receiptsFilter;
                var Sale = from r in Receipts
                           join user in _context.UserAccounts on r.UserOrderID equals user.ID
                           join emp in _context.Employees on user.EmployeeID equals emp.ID
                           join curr in _context.Currency on r.LocalCurrencyID equals curr.ID
                           group new { r, emp,curr } by new {r.ReceiptID} into datas
                           let data = datas.FirstOrDefault()
                           select new
                           {
                               //detail
                               EmpCode = data.emp.Code,
                               EmpName=data.emp.Name,
                               ReceiptNo=data.r.ReceiptNo,
                               DateIn =data.r.DateIn.ToString("dd-MM-yyyy"),
                               TimeIn=data.r.TimeIn,
                               DateOut=data.r.DateOut.ToString("dd-MM-yyyy"),
                               TimeOut=data.r.TimeOut,
                               GrandTotal = data.r.GrandTotal.ToString("0.000"),
                               //Summary
                               DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                               DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                               SCount = Summary.FirstOrDefault().CountReceipt.ToString("0.000"),
                               SSoldAmount = Summary.FirstOrDefault().SoldAmount.ToString("0.000"),
                               SDiscountItem = Summary.FirstOrDefault().DiscountItem.ToString("0.000"),
                               SDiscountTotal = Summary.FirstOrDefault().DiscountTotal.ToString("0.000"),
                               SVat = data.curr.Description + " " + Summary.FirstOrDefault().TaxValue.ToString("0.000"),
                               SGrandTotal = data.curr.Description + " " + Summary.FirstOrDefault().GrandTotal.ToString("0.000")

                           };
                return Ok(Sale);
            }
            return Ok(new List<Receipt>());
        }
        public IActionResult DetailSaleReport(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            
            List<Receipt> receiptsFilter = new List<Receipt>();
            if(DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if(DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID==BranchID).ToList();
            }
            else if(DateFrom != null && DateTo != null && BranchID != 0 && UserID!=0)
            {
                receiptsFilter =_context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <=Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserOrderID==UserID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
               
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom,DateTo,BranchID,UserID);
            if (Summary != null)
            {
                var Receipts = receiptsFilter;
                var Sale = from rd in _context.ReceiptDetail
                           join r in Receipts on rd.ReceiptID equals r.ReceiptID
                           join user in _context.UserAccounts on r.UserOrderID equals user.ID
                           join emp in _context.Employees on user.EmployeeID equals emp.ID
                           join Uom in _context.UnitofMeasures on rd.UomID equals Uom.ID
                           group new { r, emp, rd , Uom} by new { r.ReceiptID, rd.ID } into datas

                           let data = datas.FirstOrDefault()
                           let receipt = data.r
                           let emp = data.emp
                           let receiptDetail = data.rd
                           let Uom = data.Uom
                           select new
                           {
                               //Master
                               MReceiptID=receipt.ReceiptID,
                               MReceiptNo = receipt.ReceiptNo,
                               MUserName = emp.Name,
                               MDateOut = receipt.DateOut.ToString("dd-MM-yyyy") + " " + receipt.TimeOut,
                               MVat = string.Format("{0:n2}", receipt.TaxValue),
                               MDisTotal = string.Format("{0:n2}", receipt.DiscountValue),
                               MSubTotal = receiptDetail.Currency + " " + string.Format("{0:n2}", receipt.Sub_Total),
                               MTotal = receiptDetail.Currency + " " + string.Format("{0:n2}", receipt.GrandTotal),
                               //Detail
                               ID = receiptDetail.ID,
                               ItemCode = receiptDetail.Code,
                               KhmerName = receiptDetail.KhmerName,
                               Qty = receiptDetail.Qty,
                               Uom = Uom.Name,
                               UnitPrice = receiptDetail.UnitPrice.ToString("0.000"),
                               DisItem = receiptDetail.DiscountValue.ToString("0.000"),
                               Total = receiptDetail.Total.ToString("0.000"),
                               //Summary
                               DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                               DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                               SCount = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                               SSoldAmount = Summary.FirstOrDefault().SoldAmount.ToString("0.000"),
                               SDiscountItem = Summary.FirstOrDefault().DiscountItem.ToString("0.000"),
                               SDiscountTotal = Summary.FirstOrDefault().DiscountTotal.ToString("0.000"),
                               SVat = receiptDetail.Currency + " " + Summary.FirstOrDefault().TaxValue.ToString("0.000"),
                               SGrandTotal = receiptDetail.Currency + " " + Summary.FirstOrDefault().GrandTotal.ToString("0.000")
                           };
                return Ok(Sale);
            }
            else
            {
                return Ok(new List<Receipt>());
            }
        }
        public IActionResult CloseShiftReport(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<CloseShift> closeShiftFilter = new List<CloseShift>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                closeShiftFilter = _context.CloseShift.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                closeShiftFilter = _context.CloseShift.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                closeShiftFilter = _context.CloseShift.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else
            {
                return Ok(new List<CloseShift>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var CloseShift = from c in closeShiftFilter
                             join b in _context.Branches on c.BranchID equals b.ID
                             join u in _context.UserAccounts on c.UserID equals u.ID
                             join e in _context.Employees on u.EmployeeID equals e.ID
                             join cp in _context.Company on b.CompanyID equals cp.ID
                             join pr in _context.PriceLists on cp.PriceListID equals pr.CurrencyID
                             join cr in _context.Currency on pr.CurrencyID equals cr.ID
                             select new
                             {
                                 Trans = c.Trans_From + "/" + c.Trans_To + "/"+ c.UserID,
                                 Branch = b.Name,
                                 EmpCode = e.Code,
                                 EmpName = e.Name,
                                 DateIn = c.DateIn.ToString("dd-MM-yyyy"),
                                 TimeIn = c.TimeIn,
                                 DateOut = c.DateOut.ToString("dd-MM-yyyy"),
                                 TimeOut=c.TimeOut,
                                 CashInAmountSys = c.CashInAmount_Sys.ToString("0.000"),
                                 CashOutAmountSys = c.CashOutAmount_Sys.ToString("0.000"),
                                 SaleAmountSys =cr.Description+' '+ c.SaleAmount_Sys.ToString("0.000"),
                                 SaleAmount = c.LocalCurrency+' '+  c.SaleAmount.ToString("0.000"),
                                 TotalCashOutSys = cr.Description + ' ' + c.CashInAmount_Sys + c.SaleAmount_Sys.ToString("0.000"),
                                 //columns for sum data
                                 SumSaleAmountSys = c.SaleAmount_Sys,
                                 SumSaleAmount = c.SaleAmount,
                                 SumTotalCashOutSys=c.CashInAmount_Sys + c.SaleAmount_Sys
                             };            
            return Ok(CloseShift);
        }
        public IActionResult RevenueItemReport(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<RevenueItem> revenueItemsFilter = new List<RevenueItem>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                revenueItemsFilter = _context.RevenueItems.Where(w => w.SystemDate >= Convert.ToDateTime(DateFrom) && w.SystemDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                revenueItemsFilter = _context.RevenueItems.Where(w => w.SystemDate >= Convert.ToDateTime(DateFrom) && w.SystemDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                revenueItemsFilter = _context.RevenueItems.Where(w => w.SystemDate >= Convert.ToDateTime(DateFrom) && w.SystemDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, UserID);
            if (Summary != null)
            {
                var revenue = from rv in revenueItemsFilter
                              join uom in _context.UnitofMeasures on rv.UomID equals uom.ID
                              join cur in _context.Currency on rv.CurrencyID equals cur.ID
                              join wh in _context.Warehouses on rv.WarehouseID equals wh.ID
                              join item in _context.ItemMasterDatas on rv.ItemID equals item.ID
                              orderby rv.InvoiceNo, item.Code
                              group new { rv, uom, cur, wh, item } by new { rv.WarehouseID, rv.ItemID, rv.Cost } into datas
                              let data = datas.FirstOrDefault()
                              select new
                              {
                                  InvoiceNo = data.rv.InvoiceNo,
                                  Barcode = data.item.Barcode,
                                  ItemID = data.rv.ItemID,
                                  ItemCode = data.item.Code,
                                  ItemName = data.item.KhmerName,
                                  Qty = datas.Sum(s => s.rv.Qty),
                                  Uom = data.uom.Name,
                                  Cost = datas.Max(m => m.rv.Cost).ToString("0.000"),
                                  TotalCost = (datas.Sum(s => s.rv.Trans_Valuse).ToString("0.000")),
                                  Currency = data.cur.Description,
                                  WarehouseCode = data.wh.Code,
                                  DateOut = data.rv.SystemDate.ToString("dd-MM-yyyy") + " " + data.rv.TimeIn,
                                  //Summary
                                  DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                                  DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                                  SCount = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                                  SSoldAmount = Summary.FirstOrDefault().SoldAmount.ToString("0.000"),
                                  SDiscountItem = Summary.FirstOrDefault().DiscountItem.ToString("0.000"),
                                  SDiscountTotal = Summary.FirstOrDefault().DiscountTotal.ToString("0.000"),
                                  SVat = data.cur.Description + " " + Summary.FirstOrDefault().TaxValue.ToString("0.000"),
                                  SGrandTotal = data.cur.Description + " " + Summary.FirstOrDefault().GrandTotal.ToString("0.000"),
                                  STotalCost = data.cur.Description + " " + Summary.FirstOrDefault().TotalCost.ToString("0.000"),
                                  STotalProfit = data.cur.Description + " " + Summary.FirstOrDefault().TotalProfit.ToString("0.000")
                              };
                return Ok(revenue);
            }
            return Ok(revenueItemsFilter);
        }
        public IActionResult TopSaleQuantityReport(string DateFrom, string DateTo, int BranchID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, 0);
            if (Summary != null)
            {
                var Receipts = receiptsFilter;
                var list = from r in Receipts
                           join rd in _context.ReceiptDetail on r.ReceiptID equals rd.ReceiptID
                           join i in _context.ItemMasterDatas on rd.ItemID equals i.ID
                           join g1 in _context.ItemGroup1 on i.ItemGroup1ID equals g1.ItemG1ID
                           join g2 in _context.ItemGroup2 on i.ItemGroup2ID equals g2.ItemG2ID
                           join g3 in _context.ItemGroup3 on i.ItemGroup3ID equals g3.ID
                           join u in _context.UnitofMeasures on rd.UomID equals u.ID
                           group new { r, rd, i, g1, g2, g3, u } by new { rd.ItemID, rd.UnitPrice } into g

                           let data = g.FirstOrDefault()
                           let receiptDetail = data.rd

                           select new
                           {
                               C = g.Key,
                               Group1 = g.First().g1.Name,
                               Group2 = g.First().g2.Name,
                               Group3 = g.First().g3.Name,
                               Barcode = g.First().i.Barcode,
                               Code = receiptDetail.Code,
                               KhmerName = receiptDetail.KhmerName,
                               Qty = g.Sum(c => c.rd.Qty),
                               Uom = g.First().u.Name,
                               Price = receiptDetail.UnitPrice.ToString("0.000"),
                               Total = g.Sum(c => c.rd.Qty) * receiptDetail.UnitPrice,

                               //Summary
                               SSoldAmount = Summary.FirstOrDefault().SoldAmount.ToString("0.000"),
                               SDiscountItem = Summary.FirstOrDefault().DiscountItem.ToString("0.000"),
                               SDiscountTotal = Summary.FirstOrDefault().DiscountTotal.ToString("0.000"),
                               SVat = receiptDetail.Currency + " " + Summary.FirstOrDefault().TaxValue.ToString("0.000"),
                               SGrandTotal = receiptDetail.Currency + " " + Summary.FirstOrDefault().GrandTotal.ToString("0.000")
                           };
                return Ok(list.OrderByDescending(o => o.Qty));
            }
            else
            {
                return Ok(new List<Receipt>());
            }
        }
        public IActionResult PaymentMeansReport(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserOrderID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, UserID);
            if (Summary != null)
            {
                var receipt = receiptsFilter;
                var list = from p in _context.PaymentMeans
                           join r in receipt on p.ID equals r.PaymentMeansID
                           join u in _context.UserAccounts on r.UserOrderID equals u.ID
                           join e in _context.Employees on u.EmployeeID equals e.ID
                           join cur in _context.Currency on r.LocalCurrencyID equals cur.ID
                           group new { p, r, e, cur } by new { r.PaymentMeansID, r.ReceiptNo } into g

                           let data = g.FirstOrDefault()
                           let payment = data.p
                           let receiptt = data.r
                           let emp = data.e

                           select new
                           {
                               PaymentMeansID = g.Key,
                               PaymentMean = payment.Type,
                               ReceiptNo = receiptt.ReceiptNo,
                               UserName = emp.Name,
                               DateIn = Convert.ToDateTime(receiptt.DateIn).ToString("dd-MM-yyyy"),
                               TimeIn = receiptt.TimeIn,
                               DateOut = Convert.ToDateTime(receiptt.DateOut).ToString("dd-MM-yyyy"),
                               TimeOut = receiptt.TimeOut,
                               GrandTotal = receiptt.GrandTotal.ToString("0.000"),
                               DiscountValue = receiptt.DiscountValue,

                               //Summary
                               SSoldAmount = Summary.FirstOrDefault().SoldAmount.ToString("0.000"),
                               SDiscountItem = Summary.FirstOrDefault().DiscountItem.ToString("0.000"),
                               SDiscountTotal = Summary.FirstOrDefault().DiscountTotal.ToString("0.000"),
                               SVat = g.First().cur.Description + " " + Summary.FirstOrDefault().TaxValue.ToString("0.000"),
                               SGrandTotal = g.First().cur.Description + " " + Summary.FirstOrDefault().GrandTotal.ToString("0.000")
                           };
                return Ok(list);
            }
            else
            {
                return Ok(new List<Receipt>());
            }

        }
        public IActionResult SaleByCustomerReport(string DateFrom, string DateTo, int BranchID, int CusID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && CusID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && CusID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && CusID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.CustomerID == CusID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && CusID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.CustomerID == CusID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, 0);
            if (Summary != null)
            {
                var Receipts = receiptsFilter;
                var Sale = from rd in _context.ReceiptDetail
                           join r in Receipts on rd.ReceiptID equals r.ReceiptID
                           join user in _context.UserAccounts on r.UserOrderID equals user.ID
                           join emp in _context.Employees on user.EmployeeID equals emp.ID
                           join cus in _context.BusinessPartners on r.CustomerID equals cus.ID
                           join uom in _context.UnitofMeasures on rd.UomID equals uom.ID
                           group new { r, emp, rd, cus,uom } by new { r.CustomerID, r.ReceiptID, rd.ID } into datas

                           let data = datas.FirstOrDefault()
                           let receipt = data.r
                           let emp = data.emp
                           let receiptDetail = data.rd
                           let uom = data.uom
                           select new
                           {
                               //Master
                               MCustomer = datas.FirstOrDefault().cus.Name,
                               MCusTotal = datas.FirstOrDefault().r.Sub_Total,
                               MReceiptNo = receipt.ReceiptNo,
                               MSubTotal = receiptDetail.Total,
                               //Detail
                               ID = receiptDetail.ID,
                               ItemCode = receiptDetail.Code,
                               KhmerName = receiptDetail.KhmerName,
                               Qty = receiptDetail.Qty,
                               Uom = uom.Name,
                               UnitPrice = receiptDetail.UnitPrice.ToString("0.000"),
                               DisItem = receiptDetail.DiscountValue.ToString("0.000"),
                               Total = receiptDetail.Total.ToString("0.000"),
                               //Summary
                               DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                               DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                               SCount = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                               SSoldAmount = Summary.FirstOrDefault().SoldAmount.ToString("0.000"),
                               SDiscountItem = Summary.FirstOrDefault().DiscountItem.ToString("0.000"),
                               SDiscountTotal = Summary.FirstOrDefault().DiscountTotal.ToString("0.000"),
                               SVat = receiptDetail.Currency + " " + Summary.FirstOrDefault().TaxValue.ToString("0.000"),
                               SGrandTotal = receiptDetail.Currency + " " + Summary.FirstOrDefault().GrandTotal.ToString("0.000")
                           };
                return Ok(Sale);
            }
            else
            {
                return Ok(new List<Receipt>());
            }
        }
        public IActionResult TaxDeclarationReport(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            List<Receipt> receiptsFilter = new List<Receipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0)
            {
                receiptsFilter = _context.Receipt.Where(w => w.DateOut >= Convert.ToDateTime(DateFrom) && w.DateOut <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserOrderID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Receipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var Summary = GetSummaryTotals(DateFrom, DateTo, BranchID, UserID);
            if (Summary != null)
            {
                var Receipts = receiptsFilter;
                var Sale = from r in Receipts
                           join user in _context.UserAccounts on r.UserOrderID equals user.ID
                           join emp in _context.Employees on user.EmployeeID equals emp.ID
                           join curr in _context.Currency on r.LocalCurrencyID equals curr.ID
                           select new
                           {
                               //Detail
                               EmpCode = emp.Code,
                               EmpName = emp.Name,
                               ReceiptNo = r.ReceiptNo,
                               DateIn = r.DateIn.ToString("dd-MM-yyyy"),
                               TimeIn = r.TimeIn,
                               DateOut = r.TimeOut,
                               TimeOut = r.TimeOut,
                               GrandTotal = r.GrandTotal.ToString("0.000"),
                               Tax = r.TaxValue.ToString("0.000"),
                               //Summary
                               DateFrom = Convert.ToDateTime(DateFrom).ToString("dd-MM-yyyy"),
                               DateTo = Convert.ToDateTime(DateTo).ToString("dd-MM-yyyy"),
                               SCount = string.Format("{0:n0}", Summary.FirstOrDefault().CountReceipt),
                               SSoldAmount = Summary.FirstOrDefault().SoldAmount.ToString("0.000"),
                               SDiscountItem = Summary.FirstOrDefault().DiscountItem.ToString("0.000"),
                               SDiscountTotal = Summary.FirstOrDefault().DiscountTotal.ToString("0.000"),
                               SVat = curr.Description + " " + Summary.FirstOrDefault().TaxValue.ToString("0.000"),
                               SGrandTotal = curr.Description + " " + Summary.FirstOrDefault().GrandTotal.ToString("0.000")

                           };
                return Ok(Sale);
            }
            return Ok(new List<Receipt>());
        }
        public IEnumerable<SummaryTotalSale> GetSummaryTotals(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            try
            {
                var data = _context.SummaryTotalSale.FromSql("rp_GetSummarrySaleTotal @DateFrom={0},@DateTo={1}, @BranchID={2},@UserID={3}",
                parameters: new[] {
                    DateFrom.ToString(),
                    DateTo.ToString(),
                    BranchID.ToString(),
                    UserID.ToString()
                }).ToList();
                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        //Purchase
        public IActionResult GetPurchaseOrder(string DateFrom, string DateTo, int BranchID, int UserID, int VendorID, int WarehouseID)
        {
            List<PurchaseOrder> PurchaseOrders = new List<PurchaseOrder>();

            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && WarehouseID == 0 && VendorID == 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID == 0 && VendorID == 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID == 0 && VendorID == 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID != 0 && VendorID == 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID != 0 && VendorID == 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.WarehouseID == WarehouseID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID != 0 && VendorID != 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.WarehouseID == WarehouseID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && WarehouseID == 0 && VendorID != 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID == 0 && VendorID != 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID == 0 && VendorID != 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID != 0 && VendorID != 0)
            {
                PurchaseOrders = _context.PurchaseOrders.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID && w.VendorID == VendorID).ToList();
            }
            else
            {
                return Ok(new List<PurchaseOrder>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Summary = GetSummaryPurchaseTotals(DateFrom, DateTo, BranchID, UserID, VendorID, WarehouseID, "PO");
            if (Summary != null)
            {
                var PurchaseOrder = PurchaseOrders;
                var list = from PD in _context.PurchaseOrderDetails
                           join PO in PurchaseOrder on PD.PurchaseOrderID equals PO.PurchaseOrderID
                           join BP in _context.BusinessPartners on PO.VendorID equals BP.ID
                           join I in _context.ItemMasterDatas on PD.ItemID equals I.ID
                           join CU in _context.Currency on PO.LocalCurrencyID equals CU.ID
                           join Uom in _context.UnitofMeasures on PD.UomID equals Uom.ID
                           group new { PD, PO, BP, I, CU,Uom  } by new { PO.PurchaseOrderID, PD.PurchaseOrderDetailID } into datas
                           let data = datas.FirstOrDefault()
                           let PO = data.PO
                           let BP = data.BP
                           let I = data.I
                           let PurchaseOrderDetail = data.PD
                           let CU = data.CU
                           let Uom = data.Uom
                           select new
                           {
                               ///Master
                               PO.InvoiceNo,
                               BP.Name,
                               PostingDate = PO.PostingDate.ToString("dd-MM-yyyy"),
                               Discount = string.Format("{0:n2}", PO.DiscountValues),
                               Applied_Amount = string.Format("{0:n2}", PO.Applied_Amount),
                               Balance_Due = string.Format("{0:n2}", PO.Balance_Due),
                               Sub_Total = string.Format("{0:n2}", PO.Sub_Total),
                               //Detail
                               I.Code,
                               I.KhmerName,
                               Uom = Uom.Name,
                               PurchaseOrderDetail.Qty,
                               UnitPrice = string.Format("{0:n2}", PurchaseOrderDetail.PurchasPrice),
                               DisItem = string.Format("{0:n2}", PurchaseOrderDetail.DiscountValue),
                               Total = string.Format("{0:n2}", PurchaseOrderDetail.Total),
                               //Summary
                               CountInvoiceNo = Summary.FirstOrDefault().CountReceipt,
                               DisItems = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                               DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                               GrandTotal = CU.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().BalanceDue),
                           };
                return Ok(list);
            }
            else
            {
                return Ok(new List<PurchaseOrder>());
            }
        }
        //GetGoodsReceiptPO
        [HttpGet]
        public IActionResult GetGoodsReceiptPO(string DateFrom, string DateTo, int BranchID, int UserID, int VendorID, int WarehouseID)
        {
            List<GoodsReciptPO> GoodsReciptPOs = new List<GoodsReciptPO>();

            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && WarehouseID == 0 && VendorID == 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID == 0 && VendorID == 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID == 0 && VendorID == 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID != 0 && VendorID == 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID != 0 && VendorID == 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.WarehouseID == WarehouseID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID != 0 && VendorID != 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.WarehouseID == WarehouseID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && WarehouseID == 0 && VendorID != 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID == 0 && VendorID != 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID == 0 && VendorID != 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID != 0 && VendorID != 0)
            {
                GoodsReciptPOs = _context.GoodsReciptPOs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID && w.VendorID == VendorID).ToList();
            }
            else
            {
                return Ok(new List<GoodsReciptPO>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Summary = GetSummaryPurchaseTotals(DateFrom, DateTo, BranchID, UserID, VendorID, WarehouseID, "GRPO");
            if (Summary != null)
            {
                var GoodsReciptPO = GoodsReciptPOs;
                var list = from GRPOD in _context.GoodReciptPODetails
                           join GRPO in GoodsReciptPO on GRPOD.GoodsReciptPOID equals GRPO.ID
                           join BP in _context.BusinessPartners on GRPO.VendorID equals BP.ID
                           join I in _context.ItemMasterDatas on GRPOD.ItemID equals I.ID
                           join CU in _context.Currency on GRPO.LocalCurrencyID equals CU.ID
                           join Uom in _context.UnitofMeasures on GRPOD.UomID equals Uom.ID
                           group new { GRPOD, GRPO, BP, I, CU,Uom } by new { GRPO.ID, GRPOD.ItemID } into datas
                           let data = datas.FirstOrDefault()
                           let GRPOD = data.GRPOD
                           let GRPO = data.GRPO
                           let BP = data.BP
                           let I = data.I
                           let CU = data.CU
                           let Uom = data.Uom
                           select new
                           {
                               ///Master
                               GRPO.InvoiceNo,
                               BP.Name,
                               PostingDate = GRPO.PostingDate.ToString("dd-MM-yyyy"),
                               Discount = string.Format("{0:n2}", GRPO.DiscountValue),
                               Applied_Amount = string.Format("{0:n2}", GRPO.Applied_Amount),
                               Balance_Due = string.Format("{0:n2}", GRPO.Balance_Due),
                               Sub_Total = string.Format("{0:n2}", GRPO.Sub_Total),
                               //Detail
                               I.Code,
                               I.KhmerName,
                               Uom = Uom.Name,
                               GRPOD.Qty,
                               UnitPrice = string.Format("{0:n2}", GRPOD.PurchasPrice),
                               DisItem = string.Format("{0:n2}", GRPOD.DiscountValue),
                               Total = string.Format("{0:n2}", GRPOD.Total),
                               //Summary
                               CountInvoiceNo = Summary.FirstOrDefault().CountReceipt,
                               DisItems = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                               DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                               GrandTotal = CU.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().BalanceDue),
                           };
                return Ok(list);
            }
            else
            {
                return Ok(new List<GoodsReciptPO>());
            }
        }
        // GetPurchaseAP
        [HttpGet]
        public IActionResult GetPurchaseAP(string DateFrom, string DateTo, int BranchID, int UserID, int VendorID, int WarehouseID)
        {
            List<Purchase_AP> Purchase_APs = new List<Purchase_AP>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && WarehouseID == 0 && VendorID == 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID == 0 && VendorID == 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID == 0 && VendorID == 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID != 0 && VendorID == 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID != 0 && VendorID == 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.WarehouseID == WarehouseID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID != 0 && VendorID != 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.WarehouseID == WarehouseID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && WarehouseID == 0 && VendorID != 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID == 0 && VendorID != 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID == 0 && VendorID != 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID != 0 && VendorID != 0)
            {
                Purchase_APs = _context.Purchase_APs.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID && w.VendorID == VendorID).ToList();
            }
            else
            {
                return Ok(new List<Purchase_AP>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Summary = GetSummaryPurchaseTotals(DateFrom, DateTo, BranchID, UserID, VendorID, WarehouseID, "PU");
            if (Summary != null)
            {
                var Purchase_AP = Purchase_APs;
                var list = from APD in _context.PurchaseAPDetail
                           join AP in Purchase_AP on APD.Purchase_APID equals AP.PurchaseAPID
                           join BP in _context.BusinessPartners on AP.VendorID equals BP.ID
                           join I in _context.ItemMasterDatas on APD.ItemID equals I.ID
                           join CU in _context.Currency on AP.LocalCurrencyID equals CU.ID
                           join Uom in _context.UnitofMeasures on APD.UomID equals Uom.ID 
                           group new { APD, AP, BP, I, CU, Uom } by new { AP.PurchaseAPID, APD.PurchaseDetailAPID } into datas
                           let data = datas.FirstOrDefault()
                           let PurchaseAP = data.AP
                           let BP = data.BP
                           let I = data.I
                           let DetailPurchaseAP = data.APD
                           let CU = data.CU
                           let Uom = data.Uom
                           select new
                           {
                               ///Master
                               PurchaseAP.InvoiceNo,
                               BP.Name,
                               PostingDate = PurchaseAP.PostingDate.ToString("dd-MM-yyyy"),
                               Discount = string.Format("{0:n2}", PurchaseAP.DiscountValue),
                               Applied_Amount = string.Format("{0:n2}", PurchaseAP.Applied_Amount),
                               Balance_Due = string.Format("{0:n2}", PurchaseAP.Balance_Due),
                               Sub_Total = string.Format("{0:n2}", PurchaseAP.Sub_Total),

                               //Detail
                               I.Code,
                               I.KhmerName,
                               Uom = Uom.Name,
                               DetailPurchaseAP.Qty,
                               UnitPrice = string.Format("{0:n2}", DetailPurchaseAP.PurchasPrice),
                               DisItem = string.Format("{0:n2}", DetailPurchaseAP.DiscountValue),
                               Total = string.Format("{0:n2}", DetailPurchaseAP.Total),
                               // Summary
                               CountInvoiceNo = Summary.FirstOrDefault().CountReceipt,
                               DisItems = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                               DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                               GrandTotal = CU.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().BalanceDue),

                           };

                return Ok(list);
            }
            else
            {
                return Ok(new List<Purchase_AP>());
            }
        }
        //GetPurchaseMemo
        [HttpGet]
        public IActionResult GetPurchaseMemo(string DateFrom, string DateTo, int BranchID, int UserID, int VendorID, int WarehouseID)
        {
            List<PurchaseCreditMemo> PurchaseCreditMemos = new List<PurchaseCreditMemo>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && WarehouseID == 0 && VendorID == 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID == 0 && VendorID == 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID == 0 && VendorID == 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID != 0 && VendorID == 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID != 0 && VendorID == 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.WarehouseID == WarehouseID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID != 0 && VendorID != 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.WarehouseID == WarehouseID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && WarehouseID == 0 && VendorID != 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID == 0 && VendorID != 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && WarehouseID == 0 && VendorID != 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && WarehouseID != 0 && VendorID != 0)
            {
                PurchaseCreditMemos = _context.PurchaseCreditMemos.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID && w.VendorID == VendorID).ToList();
            }
            else
            {
                return Ok(new List<PurchaseCreditMemo>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Summary = GetSummaryPurchaseTotals(DateFrom, DateTo, BranchID, UserID, VendorID, WarehouseID, "PC");
            if (Summary != null)
            {
                var PurchaseCreditMemo = PurchaseCreditMemos;

                var list = from PCD in _context.PurchaseCreditMemoDetails
                           join PC in PurchaseCreditMemo on PCD.PurchaseCreditMemoID equals PC.PurchaseMemoID
                           join BP in _context.BusinessPartners on PC.VendorID equals BP.ID
                           join I in _context.ItemMasterDatas on PCD.ItemID equals I.ID
                           join CU in _context.Currency on PC.LocalCurrencyID equals CU.ID
                           join UOM in _context.UnitofMeasures on PC.UserID equals UOM.ID
                           group new { PCD, PC, BP, I, CU,UOM } by new { PC.PurchaseMemoID, PCD.PurchaseMemoDetailID } into datas
                           let data = datas.FirstOrDefault()
                           let PC = data.PC
                           let BP = data.BP
                           let I = data.I
                           let PurchaseCreditMemoDetail = data.PCD
                           let CU = data.CU
                           let Uom = data.UOM
                           select new
                           {
                               ///Master
                               PC.InvoiceNo,
                               BP.Name,
                               PostingDate = PC.PostingDate.ToString("dd-MM-yyyy"),
                               Discount = string.Format("{0:n2}", PC.DiscountValues),
                               Applied_Amount = string.Format("{0:n2}", PC.Applied_Amount),
                               Balance_Due = string.Format("{0:n2}", PC.Balance_Due),
                               Sub_Total = string.Format("{0:n2}", PC.Sub_Total),
                               BaseOn = PC.BaseOn,
                //Detail
                               I.Code,
                               I.KhmerName,
                               PurchaseCreditMemoDetail.Qty,
                               UnitPrice = string.Format("{0:n2}", PurchaseCreditMemoDetail.PurchasPrice),
                               DisItem = string.Format("{0:n2}", PurchaseCreditMemoDetail.DiscountValue),
                               Total = string.Format("{0:n2}", PurchaseCreditMemoDetail.Total),
                               Uom = Uom.Name,
                               // Summary
                               CountInvoiceNo = Summary.FirstOrDefault().CountReceipt,
                               DisItems = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountItem),
                               DiscountTotal = string.Format("{0:n2}", Summary.FirstOrDefault().DiscountTotal),
                               GrandTotal = CU.Description + " " + string.Format("{0:n2}", Summary.FirstOrDefault().BalanceDue),                               

                           };

                return Ok(list);
            }
            else
            {
                return Ok(new List<PurchaseCreditMemo>());
            }
        }
        [HttpGet]
        public IActionResult GetOutgoingPayment(string DateFrom, string DateTo, int BranchID, int UserID, int VendorID)
        {
            List<OutgoingPayment> OutgoingPayments = new List<OutgoingPayment>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && VendorID == 0)
            {
                OutgoingPayments = _context.OutgoingPayments.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && VendorID == 0)
            {
                OutgoingPayments = _context.OutgoingPayments.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && VendorID == 0)
            {
                OutgoingPayments = _context.OutgoingPayments.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID != 0 && VendorID != 0)
            {
                OutgoingPayments = _context.OutgoingPayments.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.UserID == UserID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && UserID == 0 && VendorID != 0)
            {
                OutgoingPayments = _context.OutgoingPayments.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.VendorID == VendorID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID == 0 && UserID == 0 && VendorID != 0)
            {
                OutgoingPayments = _context.OutgoingPayments.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.VendorID == VendorID).ToList();
            }
            else
            {
                return Ok(new List<OutgoingPayment>());
            }
            var TotalAmountDues = OutgoingPayments.Sum(s => s.TotalAmountDue);
            var countInvoiceNo = OutgoingPayments.Count();

            var list = from OPD in _context.OutgoingPaymentDetails
                       join OP in OutgoingPayments on OPD.OutgoingPaymentID equals OP.OutgoingPaymentID
                       join CU in _context.Currency on OPD.CurrencyID equals CU.ID
                       join Bus in _context.BusinessPartners on OP.VendorID equals Bus.ID
                       group new { OPD, OP, CU, Bus } by new { OP.VendorID, OPD.DocumentNo, OPD.OutgoingPaymentDetailID } into datas
                       let data = datas.FirstOrDefault()
                       let OP = data.OP
                       let OPD = data.OPD
                       let CU = data.CU
                       select new
                       {
                           //Master
                           OP.Number,
                           VendorID = datas.FirstOrDefault().Bus.Name,
                           PostingDate = OP.PostingDate.ToString("dd-MM-yyyy"),
                           DocumentDate = OP.DocumentDate.ToString("dd-MM-yyyy"),
                           TotalAmountDue = string.Format("{0:n2}", OP.TotalAmountDue),
                           //Detail
                           OPD.DocumentNo,
                           OPD.DocumentType,
                           Date = OPD.Date.ToString("dd-MM-yyyy"),
                           DiscountValue = string.Format("{0:n2}", OPD.TotalDiscount),
                           OPD.OverdueDays,
                           OPD.BalanceDue,
                           TotalPayment = string.Format("{0:n2}", OPD.Totalpayment),
                           Cash = string.Format("{0:n2}", OPD.Cash),
                           //summary
                           TotalAmountDues = CU.Description + " " + string.Format("{0:n2}", TotalAmountDues),
                           CountInvoiceNo = countInvoiceNo,

                       };
            return Ok(list);
        }


        //Inventory
        public IActionResult StockInWarehouseReport(int BranchID,int WarehouseID,bool Inactive)
        {
            if(BranchID!=0 && WarehouseID != 0)
            {
                var items = from whs in _context.WarehouseSummary
                            join item in _context.ItemMasterDatas on whs.ItemID equals item.ID
                            join uom in _context.UnitofMeasures on whs.UomID equals uom.ID
                            join wh in _context.Warehouses on whs.WarehouseID equals wh.ID
                            where whs.WarehouseID==WarehouseID && item.Delete==Inactive && item.Inventory==true && item.Purchase==true
                            orderby item.Code
                            select new
                            {
                                item.Barcode,
                                item.Code,
                                item.ID,
                                item.Image,
                                item.KhmerName,
                                item.EnglishName,
                                whs.InStock,
                                whs.Committed,
                                whs.Ordered,
                                Uom =uom.Name,
                                WhCode=wh.Code
                            };
                return Ok(items.ToList());
            }
            else if (BranchID != 0 && WarehouseID == 0)
            {
                var items = from whs in _context.WarehouseSummary
                            join item in _context.ItemMasterDatas on whs.ItemID equals item.ID
                            join uom in _context.UnitofMeasures on whs.UomID equals uom.ID
                            join wh in _context.Warehouses on whs.WarehouseID equals wh.ID
                            where item.Delete == Inactive && item.Inventory == true && item.Purchase == true && item.Type!="Standard"
                            orderby item.Code
                            select new
                            {
                                item.Barcode,
                                item.Code,
                                item.ID,
                                item.Image,
                                item.KhmerName,
                                item.EnglishName,
                                whs.InStock,
                                whs.Committed,
                                whs.Ordered,
                                Uom = uom.Name,
                                WhCode = wh.Code
                            };
                return Ok(items.ToList());
            }
            return Ok(new WarehouseSummary());
        }
        public IActionResult InventoryAuditReport(string DateFrom, string DateTo, int BranchID,int WarehouseID)
        {
            List<InventoryAudit> inventoryAudits = new List<InventoryAudit>();
            if(DateFrom!=null && DateTo!=null && BranchID==0 && WarehouseID == 0)
            {
                inventoryAudits = _context.InventoryAudits.Where(w => w.SystemDate >= Convert.ToDateTime(DateFrom) && w.SystemDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if(BranchID != 0 && WarehouseID == 0)
            {
                inventoryAudits = _context.InventoryAudits.Where(w =>w.BranchID==BranchID).ToList();
            }
            else if(BranchID != 0 && WarehouseID != 0)
            {
                inventoryAudits = _context.InventoryAudits.Where(w =>w.BranchID == BranchID && w.WarehouseID==WarehouseID).ToList();
            }

            var Inventory = from au in inventoryAudits
                            join curr in _context.Currency on au.CurrencyID equals curr.ID
                            join uom in _context.UnitofMeasures on au.UomID equals uom.ID
                            join wh in _context.Warehouses on au.WarehouseID equals wh.ID
                            join user in _context.UserAccounts on au.UserID equals user.ID
                            join emp in _context.Employees on user.EmployeeID equals emp.ID
                            join item in _context.ItemMasterDatas on au.ItemID equals item.ID
                            orderby item.Code
                            select new
                            {
                                ID = au.ID,
                                WhCode = wh.Code,
                                EmpName = emp.Name,
                                InvoiceNo = au.InvoiceNo,
                                TranType = au.Trans_Type,
                                SystemDate = au.SystemDate.ToString("dd-MM-yyyy"),
                                TimeIn = au.TimeIn,
                                ItemID = item.ID,
                                item.Code,
                                item.KhmerName,
                                item.EnglishName,
                                Qty = au.Qty,
                                Uom = uom.Name,
                                Cost = au.Cost,
                                TranValue = au.Trans_Valuse,
                                au.CumulativeQty,
                                au.CumulativeValue,
                                Currency = curr.Description,
                                au.Process,
                                item.Barcode,
                                ExpireDate = (au.ExpireDate.ToString("dd-MM-yyyy") == "01-01-0001" || au.ExpireDate.ToString("dd-MM-yyyy") == "09-09-2019") ? "None" : au.ExpireDate.ToString("dd-MM-yyyy") 

                            };
            return Ok(Inventory);
        }
        public IActionResult StockMovingReport(string DateFrom, string DateTo, int BranchID, int WarehouseID)
        {
            List<InventoryAudit> StockMovingFilter = new List<InventoryAudit>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && WarehouseID == 0)
            {
                StockMovingFilter = _context.InventoryAudits.Where(w => w.SystemDate >= Convert.ToDateTime(DateFrom) && w.SystemDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WarehouseID == 0)
            {
                StockMovingFilter = _context.InventoryAudits.Where(w => w.SystemDate >= Convert.ToDateTime(DateFrom) && w.SystemDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WarehouseID != 0)
            {
                StockMovingFilter = _context.InventoryAudits.Where(w => w.SystemDate >= Convert.ToDateTime(DateFrom) && w.SystemDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WarehouseID).ToList();
            }
           
            var StockMoving = from au in StockMovingFilter
                                  //join au in StockMovingFilter on ws.ItemID equals au.ItemID
                              join item in _context.ItemMasterDatas on au.ItemID equals item.ID 
                              join uom in _context.UnitofMeasures on au.UomID equals uom.ID
                              join cur in _context.Currency on au.CurrencyID equals cur.ID
                              join g1 in _context.ItemGroup1 on item.ItemGroup1ID equals g1.ItemG1ID
                              join g2 in _context.ItemGroup2 on item.ItemGroup2ID equals g2.ItemG2ID
                              join g3 in _context.ItemGroup3 on item.ItemGroup3ID equals g3.ID
                              join wh in _context.Warehouses on au.WarehouseID equals wh.ID
                              orderby item.Code
                              group new {uom, cur, g1, g2, g3, wh,item, au } by new { au.WarehouseID, au.ItemID } into g
                              select new
                              {
                                  WarehouseCode = g.First().wh.Name,
                                  ItemID = g.First().item.ID,
                                  Barcode = g.First().item.Barcode,
                                  Group1 = g.First().g1.Name,
                                  Group2 = g.First().g2.Name,
                                  Group3 = g.First().g3.Name,
                                  ItemCode = g.First().item.Code,
                                  g.First().item.KhmerName,
                                  g.First().item.EnglishName,

                                  OP = _context.InventoryAudits.Where(w => w.SystemDate < Convert.ToDateTime(DateFrom) && w.ItemID == g.First().item.ID && w.WarehouseID == g.First().au.WarehouseID).Sum(s => s.Qty),
                                  PU = g.Where(w => w.au.Trans_Type == "PU").Sum(s => s.au.Qty),
                                  PD = g.Where(w => w.au.Trans_Type == "PD").Sum(s => s.au.Qty),
                                  GR = g.Where(w => w.au.Trans_Type == "SI").Sum(s => s.au.Qty),
                                  PC = g.Where(w => w.au.Trans_Type == "PC").Sum(s => s.au.Qty),
                                  IS = g.Where(w => w.au.Trans_Type == "IS").Sum(s => s.au.Qty),
                                  IM = g.Where(w => w.au.Trans_Type == "IM").Sum(s => s.au.Qty),
                                  SO = g.Where(w => w.au.Trans_Type == "SO").Sum(s => s.au.Qty),
                                  SR = g.Where(w => w.au.Trans_Type == "SR").Sum(s => s.au.Qty),
                                  ED = g.Sum(s => s.au.Qty),
                                  TotalCost = g.Sum(s => s.au.Trans_Valuse),
                                  Uom = g.First().uom.Name,
                                  Currency = g.First().cur.Description
                              };

            return Ok(StockMoving);
        }
        public IActionResult StockExpiredReport(int BranchID, int WarehouseID)
        {
            List<WarehouseDetail> warehouseDetailsFilter = new List<WarehouseDetail>();
            if(BranchID!=0 && WarehouseID ==0)
            {
                warehouseDetailsFilter = _context.WarehouseDetails.ToList();
            }
            else if (BranchID != 0 && WarehouseID != 0)
            {
                warehouseDetailsFilter = _context.WarehouseDetails.Where(w => w.WarehouseID == WarehouseID).ToList();
            }
            var WareDetail = from wd in warehouseDetailsFilter.Where(w=>DateTime.Today.CompareTo(w.ExpireDate)>=0)
                             join wh in _context.Warehouses on wd.WarehouseID equals wh.ID
                             join ws in _context.WarehouseSummary on wd.ItemID equals ws.ItemID
                             join uom in _context.UnitofMeasures on ws.UomID equals uom.ID
                             join curr in _context.Currency on wd.CurrencyID equals curr.ID
                             join item in _context.ItemMasterDatas.Where(w=>w.ManageExpire!="None") on wd.ItemID equals item.ID
                             orderby item.Code
                             where item.Inventory == true && item.Purchase == true && item.Type != "Standard" && item.Delete == false
                             group new { wd, wh, item, uom,curr } by new {wd.WarehouseID,wd.ItemID,wd.ID} into datas
                             let data = datas.FirstOrDefault()
                             select new
                             {
                                 data.item.Barcode,
                                 data.item.Code,
                                 ItemID=data.item.ID,
                                 data.item.Image,
                                 data.item.KhmerName,
                                 data.item.EnglishName,
                                 InStock = datas.Sum(s => s.wd.InStock),
                                 Cost=data.wd.Cost,
                                 ExpiredDate= (data.wd.ExpireDate.ToString("dd-MM-yyyy") == "01-01-0001" || data.wd.ExpireDate.ToString("dd-MM-yyyy") == "09-09-2019") ? "None" : data.wd.ExpireDate.ToString("dd-MM-yyyy"),
                                 Uom = data.uom.Name,
                                 WhCode = data.wh.Code,
                                 Currency=data.curr.Description
                             };
                           
            return Ok(WareDetail);
        }
        public IActionResult GoodsReceiptStockReport(string DateFrom, string DateTo, int BranchID, int WHID, int UserID)
        {
            List<GoodsReceipt> goodsFilter = new List<GoodsReceipt>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && WHID == 0 && UserID == 0)
            {
                goodsFilter = _context.GoodsReceipts.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID == 0 && UserID == 0)
            {
                goodsFilter = _context.GoodsReceipts.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID != 0 && UserID == 0)
            {
                goodsFilter = _context.GoodsReceipts.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WHID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID != 0 && UserID != 0)
            {
                goodsFilter = _context.GoodsReceipts.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WHID && w.UserID == UserID).ToList();
            }
            else
            {
                return Ok(new List<GoodsReceipt>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var GoodReceipts = goodsFilter;
            var list = from gr in GoodReceipts
                       join grd in _context.GoodReceiptDetails on gr.GoodsReceiptID equals grd.GoodsReceiptID
                       //join cur in _context.Currency on grd.CurrencyID equals cur.ID
                       group new { gr, grd } by new { gr.Number_No, grd.ItemID } into g
                       select new
                       {
                           //Master
                           NumberNo = g.FirstOrDefault().gr.Number_No,
                           PostingDate = Convert.ToDateTime(g.FirstOrDefault().gr.PostingDate).ToString("dd-MM-yyyy"),
                           DocDate = Convert.ToDateTime(g.FirstOrDefault().gr.DocumentDate).ToString("dd-MM-yyyy"),
                           //Detail
                           Code = g.FirstOrDefault().grd.Code,
                           KhmerName = g.FirstOrDefault().grd.KhmerName,
                           EnglishName = g.FirstOrDefault().grd.EnglishName,
                           Qty = g.FirstOrDefault().grd.Quantity,
                           Cost = g.FirstOrDefault().grd.Cost,
                           Uom = g.FirstOrDefault().grd.UomName,
                           Barcode = g.FirstOrDefault().grd.BarCode,
                           ExpireDate = Convert.ToDateTime(g.First().grd.ExpireDate).ToString("dd-MM-yyyy"),
                           //Summary
                           Subtotal = g.Sum(x => x.grd.Cost * x.grd.Quantity),
                           Currency = g.FirstOrDefault().grd.Currency,
                           GrandTotal = g.Sum(x => x.grd.Cost * x.grd.Quantity)
                       };

            return Ok(list);
        }
        public IActionResult GoodsIssueStockReport(string DateFrom, string DateTo, int BranchID, int WHID, int UserID)
        {
            List<GoodIssues> goodsFilter = new List<GoodIssues>();
            if (DateFrom != null && DateTo != null && BranchID == 0 && WHID == 0 && UserID == 0)
            {
                goodsFilter = _context.GoodIssues.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID == 0 && UserID == 0)
            {
                goodsFilter = _context.GoodIssues.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID != 0 && UserID == 0)
            {
                goodsFilter = _context.GoodIssues.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WHID).ToList();
            }
            else if (DateFrom != null && DateTo != null && BranchID != 0 && WHID != 0 && UserID != 0)
            {
                goodsFilter = _context.GoodIssues.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == BranchID && w.WarehouseID == WHID && w.UserID == UserID).ToList();
            }
            else
            {
                return Ok(new List<GoodIssues>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var GoodIssues = goodsFilter;
            var list = from gi in GoodIssues
                       join gid in _context.GoodIssuesDetails on gi.GoodIssuesID equals gid.GoodIssuesID
                       //join cur in _context.Currency on grd.CurrencyID equals cur.ID
                       group new { gi, gid } by new { gi.Number_No, gid.ItemID } into g
                       select new
                       {
                           //Master
                           NumberNo = g.FirstOrDefault().gi.Number_No,
                           PostingDate = Convert.ToDateTime(g.FirstOrDefault().gi.PostingDate).ToString("dd-MM-yyyy"),
                           DocDate = Convert.ToDateTime(g.FirstOrDefault().gi.DocumentDate).ToString("dd-MM-yyyy"),
                           //Detail
                           Code = g.FirstOrDefault().gid.Code,
                           KhmerName = g.FirstOrDefault().gid.KhmerName,
                           EnglishName = g.FirstOrDefault().gid.EnglishName,
                           Qty = g.FirstOrDefault().gid.Quantity,
                           Cost = g.FirstOrDefault().gid.Cost,
                           Uom = g.FirstOrDefault().gid.UomName,
                           Barcode = g.FirstOrDefault().gid.BarCode,
                           ExpireDate = Convert.ToDateTime(g.First().gid.ExpireDate).ToString("dd-MM-yyyy"),
                           //Summary
                           Subtotal = g.Sum(x => x.gid.Cost * x.gid.Quantity),
                           Currency = g.FirstOrDefault().gid.Currency,
                           GrandTotal = g.Sum(x => x.gid.Cost * x.gid.Quantity)
                       };

            return Ok(list);
        }
        public IActionResult TransferStockReport(string DateFrom, string DateTo, int FromBranchID, int ToBranchID, int FromWHID, int ToWHID, int UserID)
        {
            List<Transfer> goodsFilter = new List<Transfer>();
            if (DateFrom != null && DateTo != null && FromBranchID == 0 && ToBranchID == 0 && FromWHID == 0 && ToWHID == 0 && UserID == 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo)).ToList();
            }
            else if (DateFrom != null && DateTo != null && FromBranchID != 0 && ToBranchID != 0 && FromWHID == 0 && ToWHID == 0 && UserID == 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == FromBranchID && w.BranchToID == ToBranchID).ToList();
            }
            else if (DateFrom != null && DateTo != null && FromBranchID != 0 && ToBranchID != 0 && FromWHID != 0 && ToWHID != 0 && UserID == 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == FromBranchID && w.BranchToID == ToBranchID && w.WarehouseFromID == FromWHID && w.WarehouseToID == ToWHID).ToList();
            }
            else if (DateFrom != null && DateTo != null && FromBranchID != 0 && ToBranchID != 0 && FromWHID == 0 && ToWHID == 0 && UserID != 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == FromBranchID && w.BranchToID == ToBranchID && w.UserID == UserID).ToList();
            }
            else if (DateFrom != null && DateTo != null && FromBranchID != 0 && ToBranchID != 0 && FromWHID != 0 && ToWHID != 0 && UserID != 0)
            {
                goodsFilter = _context.Transfers.Where(w => w.PostingDate >= Convert.ToDateTime(DateFrom) && w.PostingDate <= Convert.ToDateTime(DateTo) && w.BranchID == FromBranchID && w.BranchToID == ToBranchID && w.WarehouseFromID == FromWHID && w.WarehouseToID == ToWHID && w.UserID == UserID).ToList();
            }
            else
            {
                return Ok(new List<Transfer>());
            }
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var Transfer = goodsFilter;
            var list = from t in Transfer
                       join td in _context.TransferDetails on t.TarmsferID equals td.TransferID
                       group new { t, td } by new { t.Number, td.ItemID } into g
                       select new
                       {
                           //Master
                           Number = g.FirstOrDefault().t.Number,
                           PostingDate = Convert.ToDateTime(g.FirstOrDefault().t.PostingDate).ToString("dd-MM-yyyy"),
                           DocDate = Convert.ToDateTime(g.FirstOrDefault().t.DocumentDate).ToString("dd-MM-yyyy"),
                           Time = Convert.ToDateTime(g.FirstOrDefault().t.Time).ToString("hh:mm tt"),
                           //Detail
                           Code = g.FirstOrDefault().td.Code,
                           KhmerName = g.FirstOrDefault().td.KhmerName,
                           Qty = g.FirstOrDefault().td.Qty,
                           Cost = g.FirstOrDefault().td.Cost,
                           Uom = g.FirstOrDefault().td.UomName,
                           Barcode = g.FirstOrDefault().td.Barcode,
                           ExpireDate = Convert.ToDateTime(g.First().td.ExpireDate).ToString("dd-MM-yyyy"),
                           //Summary
                           Subtotal = g.Sum(x => x.td.Cost * x.td.Qty),
                           Currency = g.FirstOrDefault().td.Currency,
                           GrandTotal = g.Sum(x => x.td.Cost * x.td.Qty)
                       };

            return Ok(list);
        }

        //Paramater 
        [HttpGet]
        public IActionResult GetVendor()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Vendor").ToList();
            return Ok(list);
        }
        public IActionResult GetCustomer()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Customer").ToList();
            return Ok(list);
        }
        public IActionResult GetBranch()
        {
            var list = _context.Branches.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        public IActionResult GetEmployee(int BranchID)
        {
            var list = from user in _context.UserAccounts.Where(x => x.Delete == false)
                       join emp in _context.Employees
                       on user.EmployeeID equals emp.ID
                       where user.BranchID == BranchID
                       select new UserAccount
                       {
                           ID = user.ID,
                           Employee = new Employee
                           {
                               Name = emp.Name
                           }
                       };
            return Ok(list);
        }
        public IActionResult GetWarehouse(int BranchID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBranchFrom()
        {
            var list = _context.Branches.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBranchTo()
        {
            var list = _context.Branches.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouseFrom(int BranchID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouseTo(int BranchID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == BranchID).ToList();
            return Ok(list);
        }
        //GetSummaryTotals
        public IEnumerable<SummaryPurchaseTotal> GetSummaryPurchaseTotals(string DateFrom, string DateTo, int BranchID, int UserID, int VendorID, int WarehouseID, string Type)
        {
            try
            {

                var data = _context.SummaryPurchaseTotal.FromSql("rp_GetSummarryPurchaseTotal @DateFrom={0},@DateTo={1}, @BranchID={2},@UserID={3},@VendorID={4},@WarehouseID={5},@Type={6}",
                parameters: new[] {
                    DateFrom.ToString(),
                    DateTo.ToString(),
                    BranchID.ToString(),
                    UserID.ToString(),
                    VendorID.ToString(),
                    WarehouseID.ToString(),
                    Type.ToString()
                }).ToList();
                return data;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}