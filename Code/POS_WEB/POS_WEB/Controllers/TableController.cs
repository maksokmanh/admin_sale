﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Tables;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class TableController : Controller
    {
        private readonly DataContext _context;
        private readonly ITable _tabel;
        private readonly IHostingEnvironment _appEnvironment;
        public TableController(DataContext context,ITable table,IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _tabel = table;
            _appEnvironment = hostingEnvironment;
        }    
        public async Task<IActionResult> Index(string minpage = "All", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Table";
            ViewBag.Administrator = "show";
            ViewBag.TableMenu = "show";
            ViewBag.Table = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A009");
            var data = _tabel.GetTables().OrderBy(g => g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["TimeSortParm"] = sortOrder == "Time" ? "Time_desc" : "Time";
                ViewData["ImageSortParm"] = sortOrder == "Image" ? "Image_desc" : "Image";
                ViewData["GroupTableSortParm"] = sortOrder == "Group" ? "Group_desc" : "Group";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString) || s.GroupTable.Name.Contains(searchString) || s.Image.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "Time":
                        Cate = Cate.OrderBy(s => s.Time);
                        break;
                    case "Time_desc":
                        Cate = Cate.OrderByDescending(s => s.Time);
                        break;
                    case "Image":
                        Cate = Cate.OrderBy(s => s.Image);
                        break;
                    case "Image_desc":
                        Cate = Cate.OrderByDescending(s => s.Image);
                        break;
                    case "Group":
                        Cate = Cate.OrderBy(s => s.GroupTableID);
                        break;
                    case "Group_desc":
                        Cate = Cate.OrderByDescending(s => s.GroupTableID);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                //int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    if (d == 0) d = 1;
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<Table>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["TimeSortParm"] = sortOrder == "Time" ? "Time_desc" : "Time";
                    ViewData["ImageSortParm"] = sortOrder == "Image" ? "Image_desc" : "Image";
                    ViewData["GroupTableSortParm"] = sortOrder == "Group" ? "Group_desc" : "Group";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString) || s.GroupTable.Name.Contains(searchString) || s.Image.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "Time":
                            Cate = Cate.OrderBy(s => s.Time);
                            break;
                        case "Time_desc":
                            Cate = Cate.OrderByDescending(s => s.Time);
                            break;
                        case "Image":
                            Cate = Cate.OrderBy(s => s.Image);
                            break;
                        case "Image_desc":
                            Cate = Cate.OrderByDescending(s => s.Image);
                            break;
                        case "Group":
                            Cate = Cate.OrderBy(s => s.GroupTableID);
                            break;
                        case "Group_desc":
                            Cate = Cate.OrderByDescending(s => s.GroupTableID);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        if (d == 0) d = 1;
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    return View(await Pagination<Table>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }
    
        public IActionResult Create()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Table";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["GroupTableID"] = new SelectList(_context.GroupTables, "ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A009");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["GroupTableID"] = new SelectList(_context.GroupTables, "ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,GroupTableID,Image,Status,Time,Delete")] Table table)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Table";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (table.GroupTableID == 0)
            {
                ViewBag.required = "Please select group table !";
            }
           
            if (ModelState.IsValid)
            {
                await _tabel.AddOrEdit(table);
                UploadImg(table);
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupTableID"] = new SelectList(_context.GroupTables, "ID", "Name", table.GroupTableID);
            return View(table);
        }

        private void UploadImg(Table table)
        {
            try
            {
                var Image = HttpContext.Request.Form.Files[0];
                if (Image != null && Image.Length > 0)
                {
                    var file = Image;
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images/table");
                    if (file.Length > 0)
                    {
                        using (var fileStream = new FileStream(Path.Combine(uploads, table.Image), FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }

                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Table";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var table = _tabel.GetById(id);
                if (table.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (table == null)
                {
                    return NotFound();
                }
                ViewData["GroupTableID"] = new SelectList(_context.GroupTables, "ID", "Name", table.GroupTableID);
                return View(table);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A009");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var table = _tabel.GetById(id);
                    if (table.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (table == null)
                    {
                        return NotFound();
                    }
                    ViewData["GroupTableID"] = new SelectList(_context.GroupTables, "ID", "Name", table.GroupTableID);
                    return View(table);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,GroupTableID,Image,Status,Time,Delete")] Table table)
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Table";
            ViewBag.Subpage = "Table";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            if (table.GroupTableID == 0)
            {
                ViewBag.required = "Please select group table !";
            }
            if (ModelState.IsValid)
            {
                try
                {
                    await _tabel.AddOrEdit(table);
                    UploadImg(table);
                }
                catch (Exception)
                {
                   
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GroupTableID"] = new SelectList(_context.GroupTables, "ID", "Name", table.GroupTableID);
            return View(table);
        }

       [HttpDelete]
       public async Task<IActionResult> DeleteTable(int id)
        {
           await  _tabel.DeleteTable(id);
            return Ok();

        }
       
    }
}
