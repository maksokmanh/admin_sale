﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Inventory.Category;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers.Developer2
{
    [Authorize]
    public class ItemGroup2Controller : Controller
    {
        private readonly DataContext _context;
        private readonly IItem2Group _item2Group;
        private readonly IHostingEnvironment _appEnvironment;

        public ItemGroup2Controller(DataContext context,IItem2Group item2Group, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _item2Group = item2Group;
            _appEnvironment = hostingEnvironment;  
        }

        // GET: ItemGroup2
        public async Task<IActionResult> Index(string minpage = "All", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(2)";
            ViewBag.InventoryMenu = "show";
            ViewBag.ItemGroup = "show";
            ViewBag.ItemGroup2 = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A019");
            var data = _item2Group.ItemGroup2s().OrderByDescending(x=>x.ItemG2ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
                ViewData["Color"] = sortOrder == "color" ? "color_desc" : "color";
                ViewData["backgroud"] = sortOrder == "back" ? "back_desc" : "back";
                ViewData["NameGroup1"] = sortOrder == "item2" ? "item2_desc" : "item2";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewData["CurrentFilter"] = searchString;
                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString)
                    || s.Images.Contains(searchString) || s.Background.Name.Contains(searchString) || s.Colors.Name.Contains(searchString) || s.ItemGroup1.Name.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "Date":
                        Cate = Cate.OrderBy(s => s.Images);
                        break;
                    case "color_desc":
                        Cate = Cate.OrderBy(s => s.Colors.Name);
                        break;
                    case "color":
                        Cate = Cate.OrderByDescending(s => s.Colors.Name);
                        break;
                    case "item2_desc":
                        Cate = Cate.OrderBy(s => s.ItemGroup1.Name);
                        break;
                    case "item2":
                        Cate = Cate.OrderByDescending(s => s.ItemGroup1.Name);
                        break;
                    case "back_desc":
                        Cate = Cate.OrderBy(s => s.Background.Name);
                        break;
                    case "back":
                        Cate = Cate.OrderByDescending(s => s.Background.Name);
                        break;
                    case "date_desc":
                        Cate = Cate.OrderByDescending(s => s.Images);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }

                // int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);
                //int t = int.Parse(category);
                //int t1 = Convert.ToInt16(category);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    if (d == 0) d = 1;
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<ItemGroup2>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";
                    ViewData["Color"] = sortOrder == "color" ? "color_desc" : "color";
                    ViewData["backgroud"] = sortOrder == "back" ? "back_desc" : "back";
                    ViewData["NameGroup1"] = sortOrder == "item2" ? "item2_desc" : "item2";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;
                    }
                    ViewData["CurrentFilter"] = searchString;
                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString)
                        || s.Images.Contains(searchString) || s.Background.Name.Contains(searchString) || s.Colors.Name.Contains(searchString) || s.ItemGroup1.Name.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "Date":
                            Cate = Cate.OrderBy(s => s.Images);
                            break;
                        case "color_desc":
                            Cate = Cate.OrderBy(s => s.Colors.Name);
                            break;
                        case "color":
                            Cate = Cate.OrderByDescending(s => s.Colors.Name);
                            break;
                        case "item2_desc":
                            Cate = Cate.OrderBy(s => s.ItemGroup1.Name);
                            break;
                        case "item2":
                            Cate = Cate.OrderByDescending(s => s.ItemGroup1.Name);
                            break;
                        case "back_desc":
                            Cate = Cate.OrderBy(s => s.Background.Name);
                            break;
                        case "back":
                            Cate = Cate.OrderByDescending(s => s.Background.Name);
                            break;
                        case "date_desc":
                            Cate = Cate.OrderByDescending(s => s.Images);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    int.TryParse(minpage, out MaxSize);
                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        if (d == 0) d = 1;
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<ItemGroup2>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
           
        }

        
        public IActionResult Create()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(2)";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["BackID"] = new SelectList(_context.Backgrounds.Where(c => c.Delete == false), "BackID", "Name");
                ViewData["ColorID"] = new SelectList(_context.Colors.Where(b => b.Delete == false), "ColorID", "Name");
                ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1.Where(i => i.Delete == false), "ItemG1ID", "Name");
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A019");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["BackID"] = new SelectList(_context.Backgrounds.Where(c => c.Delete == false), "BackID", "Name");
                    ViewData["ColorID"] = new SelectList(_context.Colors.Where(b => b.Delete == false), "ColorID", "Name");
                    ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1.Where(i => i.Delete == false), "ItemG1ID", "Name");
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }  
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ItemG2ID,Name,Images,Delete,ItemG1ID,ColorID,BackID")] ItemGroup2 itemGroup2)
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(2)";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.Menu = "show";
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images/itemgroup");
                        if (file.Length > 0)
                        {
                            //var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, itemGroup2.Images), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemGroup2.Images = itemGroup2.Images;
                            }

                        }
                    }
                }
                if (itemGroup2.ItemG1ID == 0 || itemGroup2.ItemG1ID==null)
                {
                    ViewBag.Error = "Please select Item1 Name!";
                    
                }
                else
                {

                    await _item2Group.AddItemGroup2(itemGroup2);
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["BackID"] = new SelectList(_context.Backgrounds.Where(c => c.Delete == false), "BackID", "Name", itemGroup2.BackID);
            ViewData["ColorID"] = new SelectList(_context.Colors.Where(c => c.Delete == false), "ColorID", "Name", itemGroup2.ColorID);
            ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1.Where(c => c.Delete == false), "ItemG1ID", "Name", itemGroup2.ItemG1ID);
            return View(itemGroup2);
        }

        public IActionResult Edit(int id)
        {

            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(2)";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var itemGroup2 = _item2Group.getid(id);
                if (itemGroup2.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (itemGroup2 == null)
                {
                    return NotFound();
                }
                ViewData["BackID"] = new SelectList(_context.Backgrounds.Where(c => c.Delete == false), "BackID", "Name", itemGroup2.BackID);
                ViewData["ColorID"] = new SelectList(_context.Colors.Where(c => c.Delete == false), "ColorID", "Name", itemGroup2.ColorID);
                ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1.Where(c => c.Delete == false), "ItemG1ID", "Name", itemGroup2.ItemG1ID);
                return View(itemGroup2);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A019");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var itemGroup2 = _item2Group.getid(id);
                    if (itemGroup2.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (itemGroup2 == null)
                    {
                        return NotFound();
                    }
                    ViewData["BackID"] = new SelectList(_context.Backgrounds.Where(c => c.Delete == false), "BackID", "Name", itemGroup2.BackID);
                    ViewData["ColorID"] = new SelectList(_context.Colors.Where(c => c.Delete == false), "ColorID", "Name", itemGroup2.ColorID);
                    ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1.Where(c => c.Delete == false), "ItemG1ID", "Name", itemGroup2.ItemG1ID);
                    return View(itemGroup2);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ItemG2ID,Name,Images,Delete,ItemG1ID,ColorID,BackID")] ItemGroup2 itemGroup2)
        {

            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "ItemGroup";
            ViewBag.Subpage = "Item Group(2)";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.Menu = "show";
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                foreach (var Image in files)
                {
                    if (Image != null && Image.Length > 0)
                    {
                        var file = Image;
                        var uploads = Path.Combine(_appEnvironment.WebRootPath, "Images/itemgroup");
                        if (file.Length > 0)
                        {
                            //var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                            using (var fileStream = new FileStream(Path.Combine(uploads, itemGroup2.Images), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                                itemGroup2.Images = itemGroup2.Images;
                            }

                        }
                    }
                }
                if (itemGroup2.ItemG1ID == 0 || itemGroup2.ItemG1ID==null)
                {
                    ViewBag.Error = "Please select Item1 Name!";

                }
                else
                {

                    await _item2Group.AddItemGroup2(itemGroup2);
                    return RedirectToAction(nameof(Index));
                }
            }
            ViewData["BackID"] = new SelectList(_context.Backgrounds.Where(c => c.Delete == false), "BackID", "Name", itemGroup2.BackID);
            ViewData["ColorID"] = new SelectList(_context.Colors.Where(c => c.Delete == false), "ColorID", "Name", itemGroup2.ColorID);
            ViewData["ItemG1ID"] = new SelectList(_context.ItemGroup1.Where(c => c.Delete == false), "ItemG1ID", "Name", itemGroup2.ItemG1ID);
            return View(itemGroup2);
        }
       [HttpGet]
       public IActionResult GetColor(int id)
        {
            var list = _item2Group.GetColors.Where(c => c.ColorID == id);
            return Ok(list);

        }
        [HttpGet]
        public IActionResult GetBackground(int id)
        {
            var list = _item2Group.GetBackgrounds.Where(b => b.BackID == id);
            return Ok(list);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteCateory(int ID)
        {
             await _item2Group.DeleteItemGroup2(ID);
            return Ok();
        }
        [HttpGet]
        public IActionResult GetItemGroup2(int Item1ID)
        {
            var list= _item2Group.ItemGroup2s().Where(c => c.ItemG1ID == Item1ID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public async Task<IActionResult> QuickAdd(ItemGroup2 itemGroup2)
        {
            
               await  _item2Group.AddItemGroup2(itemGroup2);
               return Ok();
        }
        [HttpGet]
        public IActionResult GetData(int ID)
        {

            var list = _item2Group.ItemGroup2s().OrderByDescending(x=>x.ItemG2ID).Where(c=>c.ItemG1ID==ID);
            return Ok(list);
        }
        [HttpPost]
        public async Task<IActionResult> Additmegroup2(ItemGroup2 itemGroup2)
        {
            var list = await _item2Group.AddItemGroup2(itemGroup2);
            return Ok(list);
        }
    }
}
