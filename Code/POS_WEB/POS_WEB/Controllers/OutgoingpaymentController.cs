﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Responsitory;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    [Authorize]
    public class OutgoingpaymentController : Controller
    {
        private readonly DataContext _context;
        private readonly IOutgoingPayment _outgoing;
        public OutgoingpaymentController(DataContext context,IOutgoingPayment outgoing)
        {
            _context = context;
            _outgoing = outgoing;
        }
        public IActionResult Outgoingpayment()
        {
            ViewBag.style = "fa-cogs";
            ViewBag.Main = "Banking";
            ViewBag.Page = "Outgoing Payment";
            ViewBag.Subpage = "";
            ViewBag.Banking = "show";
            ViewBag.OutgoingPaymentMenu = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A042");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
           else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
        }
        [HttpGet]
        public IActionResult GetNumberNo()
        {
            var count = _context.OutgoingPayments.Count() + 1;
            var list = "OP-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetVendor()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Vendor").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseAP(int VendorID)
        {
            var list = _outgoing.GetOutgoingPaymentVendor(VendorID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult SaveOutgoingPayment(OutgoingPayment outgoing)
        {
            _context.OutgoingPayments.Add(outgoing);
            _context.SaveChanges();
            foreach (var item in outgoing.OutgoingPaymentDetails.ToList())
            {
                if (item.DocumentType == "PU")
                {
                    var balance = item.BalanceDue - item.Cash - item.TotalDiscount;
                    if (balance == 0)
                    {
                        var outvender = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == item.DocumentNo);
                        outvender.Status = "close";
                        _context.OutgoingPaymentVendors.Update(outvender);
                        var purchaseAP = _context.Purchase_APs.FirstOrDefault(x => x.InvoiceNo == item.DocumentNo);
                        purchaseAP.Status = "close";
                        purchaseAP.Applied_Amount = purchaseAP.Applied_Amount + item.BalanceDue;
                        _context.Purchase_APs.Update(purchaseAP);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var goingvender = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == item.DocumentNo);
                        goingvender.Applied_Amount = item.Applied_Amount + item.Cash + item.TotalDiscount;
                        goingvender.BalanceDue = item.BalanceDue - (item.Cash + item.TotalDiscount);
                        _context.OutgoingPaymentVendors.Update(goingvender);
                        var purchaseAP = _context.Purchase_APs.FirstOrDefault(x => x.InvoiceNo == item.DocumentNo);
                        purchaseAP.Applied_Amount = purchaseAP.Applied_Amount + item.Cash + item.TotalDiscount;
                        _context.Purchase_APs.Update(purchaseAP);
                        _context.SaveChanges();
                    }
                }
                else
                {
                    var balance = item.BalanceDue - item.Cash - item.TotalDiscount;
                    if (balance == 0)
                    {
                        var outvender = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == item.DocumentNo);
                        outvender.Status = "close";
                        _context.OutgoingPaymentVendors.Update(outvender);

                        var purchaseMemo = _context.PurchaseCreditMemos.FirstOrDefault(x => x.InvoiceNo == item.DocumentNo);
                        purchaseMemo.Status = "close";
                        purchaseMemo.Applied_Amount = purchaseMemo.Applied_Amount - item.BalanceDue;
                        _context.PurchaseCreditMemos.Update(purchaseMemo);
                        _context.SaveChanges();

                    }
                    else
                    {
                        var goingvender = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == item.DocumentNo);
                        goingvender.Applied_Amount = item.Applied_Amount + item.Cash + item.TotalDiscount;
                        goingvender.BalanceDue = item.BalanceDue - (item.Cash + item.TotalDiscount);
                        _context.OutgoingPaymentVendors.Update(goingvender);
                        var purchaseMemo = _context.PurchaseCreditMemos.FirstOrDefault(x => x.InvoiceNo == item.DocumentNo);
                        purchaseMemo.Applied_Amount = purchaseMemo.Applied_Amount - (item.Cash + item.TotalDiscount);
                        _context.SaveChanges();
                    }
                }

            }
            return Ok();
        }
    }
}
