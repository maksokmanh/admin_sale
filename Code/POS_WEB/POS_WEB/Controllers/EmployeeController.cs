﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class EmployeeController : Controller
    {
        private readonly IEmployee _emp;
        private readonly DataContext _context;
        private readonly IHostingEnvironment _environment;

        public EmployeeController(IEmployee emp, DataContext context, IHostingEnvironment environment)
        {
            _emp = emp;
            _context = context;
            _environment = environment;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string minpage = "5", string sortOrder = null, string searchString = null, int? page = null, string currentFilter = null)
        {
            ViewBag.style = "fa-user";
            ViewBag.Main = "Human Resources";
            ViewBag.Page = "Employee Master Data";
            ViewBag.HR = "show";
            ViewBag.EmpMasterData = "highlight";

            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A011");
            var emp = _emp.Emp().Where(x => x.Delete == false).OrderByDescending(x => x.ID);
            var prod = from e in emp select e;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["IDSortParm"] = String.IsNullOrEmpty(sortOrder);
                ViewData["NameSortParm"] = sortOrder == "name" ? "name-descending" : "name";
                ViewData["CodeSortParm"] = sortOrder == "code" ? "code-descending" : "code";
                ViewData["BirthdateSortParm"] = sortOrder == "birthdate" ? "birthdate-descending" : "birthdate";
                ViewData["HiredateSortParm"] = sortOrder == "hiredate" ? "hiredate-descending" : "hiredate";
                ViewData["AddressSortParm"] = sortOrder == "Address" ? "Address-descending" : "Address";
                ViewData["EmailSortParm"] = sortOrder == "Email" ? "Email-descending" : "Email";
                ViewData["GenderSortParm"] = sortOrder == "Gender" ? "Gender-descending" : "Gender";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewData["CurrentFilter"] = searchString;
                //var prod = from e in emp  select e;
                if (!String.IsNullOrEmpty(searchString))
                {
                    prod = prod.Where(predicate: p => p.Name.Contains(searchString) || p.Address.Contains(searchString) ||
                    p.Code.Contains(searchString) || p.Email.Contains(searchString) || p.Phone.Contains(searchString)

                    );
                }
                switch (sortOrder)
                {
                    case "name-descending":
                        prod = prod.OrderByDescending(x => x.Name);
                        break;
                    case "name":
                        prod = prod.OrderBy(x => x.Name);
                        break;
                    case "Address":
                        prod = prod.OrderBy(x => x.Address);
                        break;
                    case "Address-descending":
                        prod = prod.OrderByDescending(x => x.Address);
                        break;
                    case "Email":
                        prod = prod.OrderBy(x => x.Email);
                        break;
                    case "Email-descending":
                        prod = prod.OrderByDescending(x => x.Email);
                        break;
                    case "Gender":
                        prod = prod.OrderBy(x => x.Gender);
                        break;
                    case "Gender-descending":
                        prod = prod.OrderByDescending(x => x.Gender);
                        break;
                    case "code-descending":
                        prod = prod.OrderByDescending(x => x.Code).Take(10);
                        break;
                    case "code":
                        prod = prod.OrderBy(x => x.Code);
                        break;
                    case "birthdate-descending":
                        prod = prod.OrderByDescending(x => x.Birthdate);
                        break;
                    case "birthdate":
                        prod = prod.OrderBy(x => x.Birthdate);
                        break;
                    case "hiredate-descending":
                        prod = prod.OrderByDescending(x => x.Birthdate);
                        break;
                    case "hiredate":
                        prod = prod.OrderBy(x => x.Birthdate);
                        break;
                    default:
                        prod = prod.OrderBy(x => x.ID);
                        break;
                }
                // int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);

                if (MaxSize == 0)
                {
                    int d = prod.Count();
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;
                }
                var em = await Pagination<Employee>.CreateAsync(prod, page ?? 1, pageSize);
                return View(em);
            }
            if (permision != null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["IDSortParm"] = String.IsNullOrEmpty(sortOrder);
                    ViewData["NameSortParm"] = sortOrder == "name" ? "name-descending" : "name";
                    ViewData["CodeSortParm"] = sortOrder == "code" ? "code-descending" : "code";
                    ViewData["BirthdateSortParm"] = sortOrder == "birthdate" ? "birthdate-descending" : "birthdate";
                    ViewData["HiredateSortParm"] = sortOrder == "hiredate" ? "hiredate-descending" : "hiredate";
                    ViewData["AddressSortParm"] = sortOrder == "Address" ? "Address-descending" : "Address";
                    ViewData["EmailSortParm"] = sortOrder == "Email" ? "Email-descending" : "Email";
                    ViewData["GenderSortParm"] = sortOrder == "Gender" ? "Gender-descending" : "Gender";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;
                    }
                    ViewData["CurrentFilter"] = searchString;
                    //var prod = from e in emp  select e;
                    if (!String.IsNullOrEmpty(searchString))
                    {
                        prod = prod.Where(predicate: p => p.Name.Contains(searchString) || p.Address.Contains(searchString) ||
                        p.Code.Contains(searchString) || p.Email.Contains(searchString) || p.Phone.Contains(searchString)

                        );
                    }
                    switch (sortOrder)
                    {
                        case "name-descending":
                            prod = prod.OrderByDescending(x => x.Name);
                            break;
                        case "name":
                            prod = prod.OrderBy(x => x.Name);
                            break;
                        case "Address":
                            prod = prod.OrderBy(x => x.Address);
                            break;
                        case "Address-descending":
                            prod = prod.OrderByDescending(x => x.Address);
                            break;
                        case "Email":
                            prod = prod.OrderBy(x => x.Email);
                            break;
                        case "Email-descending":
                            prod = prod.OrderByDescending(x => x.Email);
                            break;
                        case "Gender":
                            prod = prod.OrderBy(x => x.Gender);
                            break;
                        case "Gender-descending":
                            prod = prod.OrderByDescending(x => x.Gender);
                            break;
                        case "code-descending":
                            prod = prod.OrderByDescending(x => x.Code).Take(10);
                            break;
                        case "code":
                            prod = prod.OrderBy(x => x.Code);
                            break;
                        case "birthdate-descending":
                            prod = prod.OrderByDescending(x => x.Birthdate);
                            break;
                        case "birthdate":
                            prod = prod.OrderBy(x => x.Birthdate);
                            break;
                        case "hiredate-descending":
                            prod = prod.OrderByDescending(x => x.Birthdate);
                            break;
                        case "hiredate":
                            prod = prod.OrderBy(x => x.Birthdate);
                            break;
                        default:
                            prod = prod.OrderBy(x => x.ID);
                            break;
                    }
                    // int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);

                    if (MaxSize == 0)
                    {
                        int d = prod.Count();
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;
                    }
                    var em = await Pagination<Employee>.CreateAsync(prod, page ?? 1, pageSize);
                    return View(em);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        public IActionResult Create()
        {
            ViewBag.style = "fa-user";
            ViewBag.Main = "Human Resources";
            ViewBag.Page = "Employee Master Data";
            ViewBag.button = "fa-plus-circle";
            ViewBag.type = "Create";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A011");
            if(permision!=null)
            {
                if (permision.Used)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }
        [HttpPost]
        //[AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Code,Name,Gender,Birthdate,Hiredate,Address,Phone,Email,Photo,Image,IsUser,Stopwork,IsDelete,Position")] Employee employee)
        {
            ViewBag.style = "fa-user";
            ViewBag.Main = "Human Resources";
            ViewBag.Page = "Employee Master Data";
            ViewBag.button = "fa-plus-circle";
            ViewBag.type = "Create";
            ViewBag.Menu = "show";
            employee.Birthdate.ToString("yyyy/MM/dd");
            employee.Hiredate.ToString("yyyy/MM/dd");
            if (ModelState.IsValid)
            {
                try
                {
                    await _emp.Add(employee);
                    UploadImg(employee);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {
                    ViewBag.Error = "This code already exist !";
                    return View(employee);
                }
                
            }
            else
            {
                var error = ModelState.Values.SelectMany(v => v.Errors);


            }
            return View(employee);

        }
        public void UploadImg(Employee employee)
        {
            try
            {
                var Image = HttpContext.Request.Form.Files[0];
                if (Image != null && Image.Length > 0)
                {
                    var file = Image;
                    var uploads = Path.Combine(_environment.WebRootPath, "images/employee");
                    if (file.Length > 0)
                    {
                        using (var fileStream = new FileStream(Path.Combine(uploads, employee.Photo), FileMode.Create))
                        {
                            file.CopyTo(fileStream);
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        public async Task<IActionResult> Update(int? id)
        {
            ViewBag.style = "fa-user";
            ViewBag.Main = "Human Resources";
            ViewBag.Page = "Employee Master Data";
            ViewBag.button = "fa-edit";
            ViewBag.type = "Edit";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (id == null)
            {
                return NotFound();
            }                     
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var employee = await _context.Employees.FindAsync(id);
                if (employee.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (employee == null)
                {
                    return NotFound();
                }
                return View(employee);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A011");
            if(permision!=null)
            {
                if (permision.Used)
                {
                    var employee = await _context.Employees.FindAsync(id);
                    if (employee.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (employee == null)
                    {
                        return NotFound();
                    }
                    return View(employee);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public async Task<IActionResult> Update(int? id, [Bind("ID,Code,Name,Gender,Birthdate,Hiredate,Address,Phone,Email,Photo,Image,IsUser,Stopwork,IsDelete,Position")] Employee employee)
        {
            if (id != employee.ID)
            {
                return NotFound();
            }
            ViewBag.style = "fa-user";
            ViewBag.Main = "Human Resources";
            ViewBag.Page = "Employee Master Data";
            ViewBag.button = "fa-edit";
            ViewBag.type = "Edit";
            ViewBag.Menu = "show";
            if (ModelState.IsValid)
            {
                try
                {
                    await _emp.Update(id, employee);
                    UploadImg(employee);
                }
                catch (Exception)
                {
                    ViewBag.Error = "This code already exist !";
                    return View(employee);
                }
                return RedirectToAction(nameof(Index));
            }
            return View(employee);
        }
        public async Task<IActionResult> Delete(int? id)
        {
            await _emp.Delete(id);
            return RedirectToAction(nameof(Index));
        }

        private bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.ID == id);
        }
    }
}