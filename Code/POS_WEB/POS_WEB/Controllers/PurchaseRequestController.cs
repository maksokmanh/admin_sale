﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Purchase;
using POS_WEB.Models.Services.Responsitory;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    public class PurchaseRequestController : Controller
    {
        private readonly DataContext _context;
        private readonly IPurchaseRequest _request;
        public PurchaseRequestController(DataContext context ,IPurchaseRequest request)
        {
            _context = context;
            _request = request;
        }
        // GET: /<controller>/
        public IActionResult PurchaseRequest()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "Purchase Request";
            ViewBag.Menu = "show";
            ViewBag.PurchaseMenu = "show";
            ViewBag.PurchasePR = "highlight";
            return View();
        }
        [HttpGet]
        public IActionResult GetInvoicenomber()
        {
            var count = _context.PurchaseRequests.Count() + 1;
            var invoice = "PR-" + count.ToString().PadLeft(7, '0');
            return Ok(invoice);
        }
        [HttpGet]
        public IActionResult GetEmployee()
        {
            var list = _context.Employees.Where(x => x.Delete == false && x.IsUser == true).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBranch()
        {
            var list = _context.Branches.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetCurrencyDefualt()
        {
            var list = _request.GetCurrencyDefualt().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouses(int ID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetGroupDefind()
        {
            var list = _request.GetAllgroupDUoMs().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouse_Request(int ID)
        {
            var list = _request.ServiceMapItemPurchaseRequests(ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindBarcode(int WarehouseID,string Barcode)
        {
            var list = _request.FindBarcode(WarehouseID, Barcode).ToList();
            return Ok(list);

            //try
            //{

            //}
            //catch (Exception)
            //{

            //    return Ok();
            //}
        }
        [HttpPost]
        public IActionResult SavePurchseRequest(PurchaseRequest purchase)
        {
            if (purchase.PurchaseRequestID == 0)
            {
                _context.PurchaseRequests.Add(purchase);
                _context.SaveChanges();
                foreach (var check in purchase.PurchaseRequestDetails.ToList())
                {
                    if (check.Qty == 0)
                    {
                        _context.PurchaseRequestDetails.Remove(check);
                        _context.SaveChanges();
                    }
                }
                
            }
            else
            {
                _context.PurchaseRequests.Update(purchase);
                _context.SaveChanges();
                foreach (var check in purchase.PurchaseRequestDetails.ToList())
                {
                    if (check.Qty == 0)
                    {
                        _context.PurchaseRequestDetails.Remove(check);
                        _context.SaveChanges();
                    }
                }
            }
            return Ok();
        }
        [HttpGet]
        public IActionResult GetVendor()
        {
            var list = _context.BusinessPartners.Where(x => x.Type == "Vendor" && x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindPurchaseRequest(string number)
        {
            var list = _context.PurchaseRequests.Include(x => x.PurchaseRequestDetails).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            else
            {
                return Ok();
            }
        }
        [HttpGet]
        public IActionResult GetItemByWarehouseRequest_Detail(int warehouseid,string invoice) {
            var list = _request.GetDetailPurchaseRequest(warehouseid, invoice).ToList();
            return Ok(list);
        }
    }
}
