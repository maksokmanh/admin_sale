﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using POS_WEB.AppContext;
using POS_WEB.Models.Sale;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Inventory;
using POS_WEB.Models.Services.POS.service;
using POS_WEB.Models.Services.Responsitory;
using System.Dynamic;
using POS_WEB.Models.Services.HumanResources;
using System.Reflection;
using POS_WEB.Models.Services.Banking;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    public class SaleController : Controller
    {
        private readonly DataContext _context;
        private readonly ISale _isale;
        private readonly IPOS _pos;
        public SaleController(DataContext context, ISale sale, IPOS pos)
        {
            _context = context;
            _isale = sale;
            _pos = pos;
        }

        private void ValidateSummary(dynamic master, IEnumerable<dynamic> details)
        {
            if (master.WarehouseID == 0)
            {
                ModelState.AddModelError("WarehouseID", "Warehouse need to be selected.");
            }

            if (master.BranchID == 0)
            {
                ModelState.AddModelError("BranchID", "Branch not matched with warehouse.");
            }

            if (master.CusID == 0)
            {
                ModelState.AddModelError("CusID", "Please choose any customer.");
            }

            if (details.Count() == 0)
            {
                ModelState.AddModelError("Details", "Please choose at least one detail item.");
            }

            double subtotal = 0;
            foreach (var dt in details)
            {
                subtotal += dt.Total;
                if (dt.Qty <= 0)
                {
                    ModelState.AddModelError("Details", "Required item detail quantity greater than 0.");
                }

                switch (dt.TypeDis.ToLower())
                {
                    case "percent":
                        if((double)dt.DisRate > 100)
                        {
                            ModelState.AddModelError("Details", "Item detail discount exceeded its maximum value.");
                        }
                        break;
                    case "cash":
                        if ((double)dt.DisRate > (double)dt.Qty * (double)dt.UnitPrice)
                        {
                            ModelState.AddModelError("Details", "Item detail discount exceeded its maximum value.");
                        }
                        break;
                }

            }


            master.SubTotal = subtotal;
            master.SubTotal_Sys = subtotal;
            switch (master.TypeDis.ToLower())
            {
                case "percent":
                    master.DisValue = master.SubTotal * (master.DisRate / 100);
                    master.TotalAmount = master.SubTotal * (1 - master.DisRate / 100);
                    if(master.DisRate > 100)
                    {
                        ModelState.AddModelError("master", "Sale item  discount exceeded its maximum value.");
                    }
                    break;
                case "cash":
                    master.DisValue = master.DisRate;
                    master.TotalAmount = master.SubTotal - master.DisRate;
                    if ((double)master.DisRate > master.SubTotal)
                    {
                        ModelState.AddModelError("master", "Sale item discount exceeded its maximum value.");
                    }
                    break;
            }
            
            master.TotalAmount_Sys = master.TotalAmount;

        }

        private void UpdateSourceCopy(dynamic master, IEnumerable<dynamic> details, dynamic copyMaster, IEnumerable<dynamic> copyDedails, SaleCopyType copyType)
        {          
            bool canClose = true;
            foreach (var cd in copyDedails)
            {
                foreach (var d in details)
                {
                    switch (copyType)
                    {
                        case SaleCopyType.Quotation:
                            if (d.SQDID == cd.SQDID)
                            {
                                if (cd.OpenQty > 0)
                                {
                                    cd.OpenQty -= d.Qty;
                                } 

                                if(cd.OpenQty <= 0)
                                {
                                    cd.Delete = true;
                                }

                            }
                            break;
                        case SaleCopyType.Order:
                            if (d.SODID == cd.SODID)
                            {
                                if (cd.OpenQty > 0)
                                {
                                    cd.OpenQty -= d.OpenQty;
                                }

                                if (cd.OpenQty <= 0)
                                {
                                    cd.Delete = true;
       
                                }
                            }
                            break;
                        case SaleCopyType.Delivery:
                            if (d.SDDID == cd.SDDID)
                            {
                                if (cd.OpenQty > 0)
                                {
                                    cd.OpenQty -= d.OpenQty;
                                }

                                if (cd.OpenQty <= 0)
                                {
                                    cd.Delete = true;
                                }
                            }
                            break;
                        case SaleCopyType.AR:
                            if (d.SARDID == cd.SARDID)
                            {
                                if (cd.OpenQty > 0)
                                {
                                    cd.OpenQty -= d.OpenQty;
                                }

                                if (cd.OpenQty <= 0)
                                {
                                    cd.Delete = true;
                                }
                            }
                            break;
                    }

                }
            }

            foreach(var cd in copyDedails)
            {
                canClose = canClose && cd.OpenQty <= 0;
            }

            if (canClose)
            {
                copyMaster.Status = "closed";
            }
        }

           
        //--------------------------------------------// start Sale SQ //------------------------------------//
        public IActionResult SaleQuote()
        {
            ViewBag.style = "fa-dollar-sign";
            ViewBag.Main = "Sale";
            ViewBag.Page = "Quotation";
            ViewBag.Sale = "show";
            ViewBag.SaleQuotation = "highlight";
            return View();
        }
        [HttpGet]
        public IActionResult GetCustomer(int? id)
        {
            if(id != null)
            {
                return Ok(_context.BusinessPartners.FirstOrDefault(c => c.ID == id));
            } else
            {
                var cus = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Customer").ToList();
                return Ok(cus);
            }                           
        }
        [HttpGet]
        public IActionResult GetWarehouse() {
            var ware = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == int.Parse(User.FindFirst("BranchID").Value)).ToList();
            return Ok(ware);
        }

        [HttpGet]
        public IActionResult GetDefaultCurrency()
        {
            var currency = GetSystemCurrencies().First();
            return Ok(currency);
        }

        private IEnumerable<SystemCurrency> GetSystemCurrencies() {
            IEnumerable<SystemCurrency> currencies = (from com in _context.Company.Where(x => x.Delete == false)
                                        join p in _context.PriceLists.Where(x => x.Delete == false) on com.PriceListID equals p.ID
                                        join c in _context.Currency.Where(x => x.Delete == false) on p.CurrencyID equals c.ID
                                        select new SystemCurrency
                                        {
                                            ID = c.ID,
                                            Description = c.Description
                                        });
            return currencies;                            
        }

        [HttpGet]
        public IActionResult GetCurrency() {
            var cur = _context.Currency.Where(x => x.Delete == false).ToList();
            return Ok(cur);
        }

        [HttpGet]
        public IActionResult FindCustomer(string name = "")
        {
            var cus = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Customer").ToList();
            if (!string.IsNullOrEmpty(name))
            {
                name = name.Replace(" ", string.Empty).ToLower();
                cus = cus.Where(ap =>
                    ap.Code.Replace(" ", string.Empty).ToLower().Contains(name)
                   | ap.Name.Replace(" ", string.Empty).ToLower().Contains(name)
                   | ap.Phone.Replace(" ", string.Empty).ToLower().Contains(name)
                ).ToList();
            }
            return Ok(cus);
        }
        [HttpGet]
        public IActionResult GetSaleQuoteInvoice(bool IncludeVat)
        {
            var count = _context.SaleQuotes.Where(x => x.IncludeVat == false).Count() + 1;
            var list = "SQ-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetSaleQuoteInvoiceVAT()
        {
            var count = _context.SaleQuotes.Where(x => x.IncludeVat == true).Count() + 1;
            var list = "SQ-VAT-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetExchange()
        {
             var ex=from e in _context.ExchangeRates.Where(x=>x.Delete==false) join 
                    c in _context.Currency.Where(x=>x.Delete==false) on e.CurrencyID equals c.ID
                    select new TpExchange
                    {
                        ID=e.ID,
                        CurID=e.CurrencyID,
                        CurName=c.Description,
                        Rate=e.Rate,
                        SetRate=e.SetRate
                    };
            return Ok(ex);
        }
        [HttpGet]
        public IActionResult GetItem(int CusID) {
            var cus = _context.BusinessPartners.FirstOrDefault(x => x.Delete == false && x.Type == "Customer" && x.ID == CusID);
            if (cus != null)
            {
                var ls_item = _isale.GetItemMaster(cus.PriceListID).ToList();                   
                return Ok(new { status="T", ls_item });
            }
            else
            {
                return Ok(new { status = "F"});
            }
        }

        public IActionResult GetItemByBarcode(string barcode)
        {
            return Ok();
        }


        [HttpGet]
        public IActionResult GetVat()
        {
            var vat = _context.Tax.Where(x => x.Delete == false);
            return Ok(vat);
        }
        [HttpGet]
        public IActionResult GetGDUom()
        {
            var g = _isale.GetAllGroupDefind().ToList();
            return Ok(g);
        }

        [HttpPost]
        public IActionResult UpdateSaleQuote(string data)
        {
            SaleQuote saleQuote = JsonConvert.DeserializeObject<SaleQuote>(data);
            saleQuote.ChangeLog = DateTime.Now;
            ModelMessage msg = new ModelMessage();
            saleQuote.UserID = int.Parse(User.FindFirst("UserID").Value);
            saleQuote.BranchID = int.Parse(User.FindFirst("BranchID").Value);

            if (!ValidDate(saleQuote.ValidUntilDate))
            {
                ModelState.AddModelError("UntilDate", "Item has invalid until date.");
            }

            ValidateSummary(saleQuote, saleQuote.SaleQuoteDetails);

            foreach (var d in saleQuote.SaleQuoteDetails)
            {
                if (saleQuote.SQID == 0)
                {
                    d.OpenQty = d.Qty;
                } 
               
            }
              
            using (var t = _context.Database.BeginTransaction())
            {
                if (ModelState.IsValid)
                {      
                    _context.SaleQuotes.Update(saleQuote);
                    _context.SaveChanges();
                                   
                    t.Commit();
                    ModelState.AddModelError("success", "Item save successfully.");
                    msg.Approve();
                }
            }
            return Ok(msg.Bind(ModelState));
        }

        [HttpGet]
        public IActionResult FindSaleQuote(string number)
        {
            var list = _context.SaleQuotes.Include(x => x.SaleQuoteDetails).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            return Ok();
        }

       
        //--------------------------------------------// End Sale SQ // -------------------------
        public IActionResult SaleOrder()
        {
            ViewBag.style = "fa-dollar-sign";
            ViewBag.Main = "Sale";
            ViewBag.Page = "Order";
            ViewBag.Sale = "show";
            ViewBag.SaleOrder = "highlight";
            return View();
        }

        public bool ValidDate(DateTime until)
        {
            return until.CompareTo(DateTime.Today) >= 0;
        }

        public IActionResult GetSaleQuotes()
        {
            var items = _context.SaleQuotes.Include(d => d.SaleQuoteDetails).Where(item => item.Status == "open");
            return Ok(items);
        }

        public IActionResult GetSaleOrders()
        {
            var items = _context.SaleOrders.Include(od =>od.SaleOrderDetails).Where(i => i.Status == "open");
            return Ok(items);
        }

        public IActionResult GetSaleDeliveries()
        {
            var items = _context.SaleDeliveries.Include(d => d.SaleDeliveryDetails).Where(item => item.Status == "open");
            return Ok(items);
        }

        public IActionResult GetSaleARs()
        {
            var items = _context.SaleARs.Include(d => d.SaleARDetails).Where(item => item.Status == "open");
            return Ok(items);
        }

        private IEnumerable<ItemsReturn> CheckStockForOrder(SaleOrder data)
        {
            List<ItemsReturn> list = new List<ItemsReturn>();
            List<ItemsReturn> list_group = new List<ItemsReturn>();
            foreach (var item in data.SaleOrderDetails.ToList())
            {
                var check = list_group.Find(w => w.ItemID == item.ItemID);
                var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom => uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                if (check == null)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == data.WarehouseID && w.ItemID == item.ItemID);
                    if (item_warehouse != null)
                    {
                        ItemsReturn item_group = new ItemsReturn
                        {
                            Code = item.ItemCode,
                            ItemID = item.ItemID,
                            KhmerName = item_group_uom.KhmerName + ' ' + item_group_uom.UnitofMeasureInv.Name,
                            InStock = (decimal)item_warehouse.InStock - (decimal)item_warehouse.Committed,
                            OrderQty = (decimal)item.PrintQty * (decimal)uom_defined.Factor,
                            Committed = (decimal)item_warehouse.Committed
                        };
                        list_group.Add(item_group);
                    }

                }
                else
                {
                    check.OrderQty = (decimal)check.OrderQty + (decimal)item.PrintQty * (decimal)uom_defined.Factor;
                }
            }
            foreach (var item in list_group)
            {
                if (item.OrderQty > item.InStock)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == data.WarehouseID && w.ItemID == item.ItemID);
                    ItemsReturn item_return = new ItemsReturn
                    {
                        Line_ID = item.Line_ID,
                        Code = item.Code,
                        ItemID = item.ItemID,
                        KhmerName = item.KhmerName,
                        InStock = item.InStock,
                        OrderQty = item.OrderQty,
                        Committed = (decimal)item_warehouse.Committed
                    };
                    list.Add(item_return);
                }

            }
            return list;
        }

        public IActionResult RemoveQuoteDetail(int detailID)
        {
            ModelMessage msg = new ModelMessage();
            var details = _context.SaleQuoteDetails.Where(d => d.SQDID == detailID);
            //ValidateOnEdit(details);
            if (ModelState.IsValid)
            {
                _context.SaleQuoteDetails.Find(detailID).Delete = true;
                _context.SaveChanges();
                msg.Approve();
            }
           
            return Ok(msg.Bind(ModelState));
        }

        [HttpPost]
        public IActionResult UpdateSaleOrder(string data)
        {
            SaleOrder saleOrder = JsonConvert.DeserializeObject<SaleOrder>(data);
            saleOrder.ChangeLog = DateTime.Now;
            ModelMessage msg = new ModelMessage();
            saleOrder.UserID = int.Parse(User.FindFirst("UserID").Value);
            saleOrder.BranchID = int.Parse(User.FindFirst("BranchID").Value);
            if(!ValidDate(saleOrder.DeliveryDate))
            {
                ModelState.AddModelError("DeliveryDate", "Item has invalid delivery date.");
            }

            ValidateSummary(saleOrder, saleOrder.SaleOrderDetails);
            foreach(var dt in saleOrder.SaleOrderDetails)
            {
                if (dt.SODID <= 0)
                {
                    dt.PrintQty = dt.Qty;
                    if(saleOrder.CopyType > 0)
                    {
                        dt.Qty = dt.OpenQty;
                    } else
                    {
                        dt.OpenQty = dt.Qty;
                    }
                }
                else
                {
                    dt.PrintQty = dt.Qty - dt.PrintQty;           
                }
               
            }

            var _itemReturneds = CheckStockForOrder(saleOrder);
            if (_itemReturneds.Count() > 0)
            {
                int count = 1;
                foreach (var ir in _itemReturneds)
                {
                    ModelState.AddModelError("itemReturn" + count.ToString(), "The item &lt;&lt;" + ir.KhmerName + "&gt;&gt; has stock left " + ir.InStock + ".");
                    count++;
                }
            }

            using (var t = _context.Database.BeginTransaction())
            {             
                if (ModelState.IsValid)
                {                 
                    if(saleOrder.CopyType == SaleCopyType.Quotation)
                    {
                        var quoteMaster = _context.SaleQuotes.Include(q => q.SaleQuoteDetails).
                            FirstOrDefault(m => string.Compare(m.InvoiceNo, saleOrder.CopyKey) == 0);
                        if(quoteMaster != null)
                        {
                            UpdateSourceCopy(saleOrder, saleOrder.SaleOrderDetails, quoteMaster, quoteMaster.SaleQuoteDetails, SaleCopyType.Quotation);
                        }                       
                    }
                   
                    _context.SaleOrders.Update(saleOrder);
                    _context.SaveChanges();
                    _context.Database.ExecuteSqlCommand("admin_SaleOrder @OrderID={0}",
                       parameters: new[] {
                           saleOrder.SOID.ToString()
                       });
                   
                    t.Commit();
                    ModelState.AddModelError("success", "Item save successfully.");
                    msg.Approve();
                }
            }
            
            return Ok(new { Model = msg.Bind(ModelState), ItemReturn = new List<ItemsReturn>() });
        }

        [HttpGet]
        public IActionResult FindSaleOrder(string number)
        {
            var list = _context.SaleOrders.Include(x => x.SaleOrderDetails).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            return Ok();
        }

        [HttpGet]
        public IActionResult GetSaleOrderInvoice(bool IncludeVat)
        {
            var count = _context.SaleOrders.Where(x => x.IncludeVat == false).Count() + 1;
            var list = "SO-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }

        [HttpGet]
        public IActionResult GetSaleOrderInvoiceVAT()
        {
            var count = _context.SaleOrders.Where(x => x.IncludeVat == true).Count() + 1;
            var list = "SO-VAT-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        //-----------------------------------End Sale Order-------------------------------//
        public IActionResult SaleDelivery()
        {
            ViewBag.style = "fa-dollar-sign";
            ViewBag.Main = "Sale";
            ViewBag.Page = "Sale Delivery";
            ViewBag.Sale = "show";
            ViewBag.SaleDelivery = "highlight";
            return View();
        }

        private IEnumerable<ItemsReturn> CheckStockForDelivery(SaleDelivery data)
        {
            List<ItemsReturn> list = new List<ItemsReturn>();
            List<ItemsReturn> list_group = new List<ItemsReturn>();
            foreach (var item in data.SaleDeliveryDetails.ToList())
            {
                var check = list_group.Find(w => w.ItemID == item.ItemID);
                var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom => uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                if (check == null)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == data.WarehouseID && w.ItemID == item.ItemID);
                    if (item_warehouse != null)
                    {
                        ItemsReturn item_group = new ItemsReturn
                        {
                            Code = item.ItemCode,
                            ItemID = item.ItemID,
                            KhmerName = item_group_uom.KhmerName + ' ' + item_group_uom.UnitofMeasureInv.Name,
                            InStock = (decimal)item_warehouse.InStock - (decimal)item_warehouse.Committed,
                            OrderQty = (decimal)item.PrintQty * (decimal)uom_defined.Factor,
                            Committed = (decimal)item_warehouse.Committed
                        };
                        list_group.Add(item_group);
                    }

                }
                else
                {
                    check.OrderQty = (decimal)check.OrderQty + (decimal)item.PrintQty * (decimal)uom_defined.Factor;
                }
            }

            foreach (var item in list_group)
            {
                if (item.OrderQty > item.InStock)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == data.WarehouseID && w.ItemID == item.ItemID);
                    ItemsReturn item_return = new ItemsReturn
                    {
                        Line_ID = item.Line_ID,
                        Code = item.Code,
                        ItemID = item.ItemID,
                        KhmerName = item.KhmerName,
                        InStock = item.InStock,
                        OrderQty = item.OrderQty,
                        Committed = (decimal)item_warehouse.Committed
                    };
                    list.Add(item_return);
                }

            }
            return list;
        }

        public void RemoveOrderDetail(int id)
        {
            _context.SaleOrderDetails.Find(id).Delete = true;
            _context.SaveChanges();
        }

        [HttpPost]
        public IActionResult CreateSaleDelivery(string data)
        {
            SaleDelivery saleDelivery = JsonConvert.DeserializeObject<SaleDelivery>(data);
            saleDelivery.ChangeLog = DateTime.Now;
            ModelMessage msg = new ModelMessage();
            saleDelivery.UserID = int.Parse(User.FindFirst("UserID").Value);
            saleDelivery.BranchID = int.Parse(User.FindFirst("BranchID").Value);
            if (!ValidDate(saleDelivery.DueDate))
            {
                ModelState.AddModelError("DueDate", "Item has invalid due date.");
            }

            ValidateSummary(saleDelivery, saleDelivery.SaleDeliveryDetails);
            foreach (var dt in saleDelivery.SaleDeliveryDetails)
            {
                if (dt.SODID == 0)
                {
                    dt.PrintQty = dt.Qty;
                    if (saleDelivery.CopyType > 0)
                    {
                        dt.Qty = dt.OpenQty;
                    }
                    else
                    {
                        dt.OpenQty = dt.Qty;
                    }
                }
                else
                {
                    dt.PrintQty = dt.Qty - dt.PrintQty;
                }
            }

            List<ItemsReturn> returns = CheckStockForDelivery(saleDelivery).ToList();
            int count = 1;
            foreach (var ir in returns)
            {
                ModelState.AddModelError("itemReturn" + count.ToString(), "The item &lt;&lt;" + ir.KhmerName + "&gt;&gt; has stock left " + ir.InStock + ".");
                count++;
            }
            using (var t = _context.Database.BeginTransaction())
            {
                switch (saleDelivery.CopyType)
                {
                    case SaleCopyType.Quotation:
                        var quoteMaster = _context.SaleQuotes.Include(q => q.SaleQuoteDetails)
                            .FirstOrDefault(m => string.Compare(m.InvoiceNo, saleDelivery.CopyKey) == 0);
                        if (quoteMaster != null)
                        {
                            UpdateSourceCopy(saleDelivery, saleDelivery.SaleDeliveryDetails, quoteMaster, quoteMaster.SaleQuoteDetails, SaleCopyType.Quotation);
                        }
                        break;
                    case SaleCopyType.Order:
                        var orderMaster = _context.SaleOrders.Include(o => o.SaleOrderDetails)
                            .FirstOrDefault(m => string.Compare(m.InvoiceNo, saleDelivery.CopyKey) == 0);
                        if (orderMaster != null)
                        {
                            UpdateSourceCopy(saleDelivery, saleDelivery.SaleDeliveryDetails, orderMaster, orderMaster.SaleOrderDetails, SaleCopyType.Order);                          
                        }
                        break;
                }

                if (ModelState.IsValid)
                {                                
                    _context.SaleDeliveries.Update(saleDelivery);
                    _context.SaveChanges();
                    _context.Database.ExecuteSqlCommand("admin_SaleDelivery @OrderID={0}",
                       parameters: new[] {
                           saleDelivery.SDID.ToString()
                       });

                    t.Commit();
                    ModelState.AddModelError("success", "Item save successfully.");
                    msg.Approve();
                }
            }

            return Ok(new { Model = msg.Bind(ModelState), ItemReturn = new List<ItemsReturn>() });
        }

        [HttpGet]
        public IActionResult FindSaleDelivery(string number)
        {
            var list = _context.SaleDeliveries.Include(x => x.SaleDeliveryDetails).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            return Ok();
        }

        [HttpGet]
        public IActionResult GetSaleDeliveryInvoice(bool IncludeVat)
        {
            var count = _context.SaleDeliveries.Where(x => x.IncludeVat == false).Count() + 1;
            var list = "SD-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }

        [HttpGet]
        public IActionResult GetSaleDeliveryInvoiceVAT()
        {
            var count = _context.SaleDeliveries.Where(x => x.IncludeVat == true).Count() + 1;
            var list = "SD-VAT-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }

        //-------------End Sale Delivery----------------//

        public IActionResult SaleAR()
        {
            ViewBag.style = "fa-dollar-sign";
            ViewBag.Main = "Sale";
            ViewBag.Page = "Sale AR";
            ViewBag.Sale = "show";
            ViewBag.SaleAR = "highlight";
            return View();
        }     

       

        private IEnumerable<ItemsReturn> CheckStockForAR(SaleAR data)
        {
            List<ItemsReturn> list = new List<ItemsReturn>();
            List<ItemsReturn> list_group = new List<ItemsReturn>();
            foreach (var item in data.SaleARDetails.ToList())
            {
                var check = list_group.Find(w => w.ItemID == item.ItemID);
                var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom => uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                if (check == null)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == data.WarehouseID && w.ItemID == item.ItemID);
                    if (item_warehouse != null)
                    {
                        ItemsReturn item_group = new ItemsReturn
                        {
                            Code = item.ItemCode,
                            ItemID = item.ItemID,
                            KhmerName = item_group_uom.KhmerName + ' ' + item_group_uom.UnitofMeasureInv.Name,
                            InStock = (decimal)item_warehouse.InStock - (decimal)item_warehouse.Committed,
                            OrderQty = (decimal)item.PrintQty * (decimal)uom_defined.Factor,
                            Committed = (decimal)item_warehouse.Committed
                        };
                        list_group.Add(item_group);
                    }
                }
                else
                {
                    check.OrderQty = (decimal)check.OrderQty + (decimal)item.PrintQty * (decimal)uom_defined.Factor;
                }
            }

            foreach (var item in list_group)
            {
                if (item.OrderQty > item.InStock)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == data.WarehouseID && w.ItemID == item.ItemID);
                    ItemsReturn item_return = new ItemsReturn
                    {
                        Line_ID = item.Line_ID,
                        Code = item.Code,
                        ItemID = item.ItemID,
                        KhmerName = item.KhmerName,
                        InStock = item.InStock,
                        OrderQty = item.OrderQty,
                        Committed = (decimal)item_warehouse.Committed
                    };
                    list.Add(item_return);
                }

            }
            return list;
        }

        public void RemoveARDetail(int id)
        {
            _context.SaleARDetails.Find(id).Delete = true;
            _context.SaveChanges();
        }

        [HttpPost]
        public bool CreateIncomingPaymentCustomer<T>(T saleType)
        {
            string currencyName = _context.Currency.Find(GetValue(saleType, "LocalCurrencyID")).Description;
            SystemCurrency syCurrency = GetSystemCurrencies().FirstOrDefault(c => c.ID == (int)GetValue(saleType, "LocalCurrencyID"));
            string[] docTypes = GetValue(saleType, "InvoiceNo").ToString().Split("-");
            string docType = docTypes[0];
            if((bool)GetValue(saleType, "IncludeVat"))
            {
                docType = string.Format("{0}-{1}", docTypes[0], docTypes[1]);
            }

            IncomingPaymentCustomer ipcustomer = new IncomingPaymentCustomer
            {
                CustomerID = (int)GetValue(saleType, "CusID"),
                BranchID = (int)GetValue(saleType, "BranchID"),
                WarehouseID = (int)GetValue(saleType, "WarehouseID"),
                CurrencyID = (int)GetValue(saleType, "LocalCurrencyID"),
                DocumentNo = GetValue(saleType, "InvoiceNo").ToString(),
                DocumentType = docType,
                Applied_Amount = (double)GetValue(saleType, "AppliedAmount"),
                CurrencyName = currencyName,
                ExchangeRate = (double)GetValue(saleType, "ExchangeRate"),
                Cash = (double)GetValue(saleType, "TotalAmount"),
                CashDiscount = (double)GetValue(saleType, "DisRate"),
                Total = (double)GetValue(saleType, "SubTotal"),
                TotalDiscount = (double)GetValue(saleType, "DisValue"),
                BalanceDue = (double)GetValue(saleType, "TotalAmount"),
                TotalPayment = (double)GetValue(saleType, "TotalAmount"),
                Status = GetValue(saleType, "Status").ToString(),
                Date = (DateTime)GetValue(saleType, "DueDate"),
                SysCurrency = syCurrency.ID,
                SysName = syCurrency.Description
            };
            _context.IncomingPaymentCustomers.Update(ipcustomer);
            _context.SaveChanges();
            return true;
        }


        [HttpPost]
        public IActionResult CreateSaleAR(string data)
        {
            SaleAR saleAR = JsonConvert.DeserializeObject<SaleAR>(data);
            saleAR.ChangeLog = DateTime.Now;
            ModelMessage msg = new ModelMessage();
            saleAR.UserID = int.Parse(User.FindFirst("UserID").Value);
            saleAR.BranchID = int.Parse(User.FindFirst("BranchID").Value);
            ValidateSummary(saleAR, saleAR.SaleARDetails);
            if (saleAR.SARID > 0)
            {
                ModelState.AddModelError("saleAR", "Item cannot be chanage.");
            }

            if (saleAR.AppliedAmount > saleAR.TotalAmount)
            {
                ModelState.AddModelError("saleAppliedAmount", "Applied amount cannot exceeds total amount.");
            }
            
            foreach (var dt in saleAR.SaleARDetails)
            {
                if (dt.SARDID == 0)
                {
                    dt.PrintQty = dt.Qty;
                    if (saleAR.CopyType > 0)
                    {
                        dt.Qty = dt.OpenQty;
                    }
                    else
                    {
                        dt.OpenQty = dt.Qty;
                    }
                }             
            }
            var _itemReturneds = CheckStockForAR(saleAR); 
            if(_itemReturneds.Count() > 0)
            {
                int count = 1;
                foreach(var ir in _itemReturneds)
                {
                    ModelState.AddModelError("itemReturn" + count.ToString(), "The item &lt;&lt;" + ir.KhmerName + "&gt;&gt; has stock left "+ ir.InStock + ".");
                    count++;
                }
            }
            using (var t = _context.Database.BeginTransaction())
            {        
                if (ModelState.IsValid)
                {                   
                    _context.SaleARs.Update(saleAR);
                    _context.SaveChanges();
                    _context.Database.ExecuteSqlCommand("admin_SaleAR @OrderID={0}",
                            parameters: new[] {
                                    saleAR.SARID.ToString()
                            });
                    CreateIncomingPaymentCustomer(saleAR);
                    switch (saleAR.CopyType)
                    {
                        case SaleCopyType.Quotation:
                            var quoteMaster = _context.SaleQuotes.Include(q => q.SaleQuoteDetails)
                                .FirstOrDefault(m => string.Compare(m.InvoiceNo, saleAR.CopyKey) == 0);
                            UpdateSourceCopy(saleAR, saleAR.SaleARDetails, quoteMaster, quoteMaster.SaleQuoteDetails, SaleCopyType.Quotation);
                            break;
                        case SaleCopyType.Order:
                            var orderMaster = _context.SaleOrders.Include(o => o.SaleOrderDetails)
                                .FirstOrDefault(m => string.Compare(m.InvoiceNo, saleAR.CopyKey) == 0);
                            UpdateSourceCopy(saleAR, saleAR.SaleARDetails, orderMaster, orderMaster.SaleOrderDetails, SaleCopyType.Order);
                            break;
                        case SaleCopyType.Delivery:
                            var deliveryMaster = _context.SaleDeliveries.Include(o => o.SaleDeliveryDetails)
                                .FirstOrDefault(m => string.Compare(m.InvoiceNo, saleAR.CopyKey) == 0);
                            UpdateSourceCopy(saleAR, saleAR.SaleARDetails, deliveryMaster, deliveryMaster.SaleDeliveryDetails, SaleCopyType.Delivery);
                            
                            break;
                    }
                    t.Commit();
                    ModelState.AddModelError("success", "Item save successfully.");
                    msg.Approve();
                }
            }
          
            return Ok(new { Model = msg.Bind(ModelState), ItemReturn = _itemReturneds });
        }

        [HttpGet]
        public IActionResult FindSaleAR(string number)
        {
            var list = _context.SaleARs.Include(x => x.SaleARDetails).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            return Ok();
        }

        [HttpGet]
        public IActionResult GetSaleARInvoice(bool IncludeVat)
        {
            var count = _context.SaleARs.Where(x => x.IncludeVat == false).Count() + 1;
            var list = "SAR-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }

        [HttpGet]
        public IActionResult GetSaleARInvoiceVAT()
        {
            var count = _context.SaleARs.Where(x => x.IncludeVat == true).Count() + 1;
            var list = "SAR-VAT-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }

        //------------------------End Sale AR------------------------//
        public IActionResult SaleCreditMemo()
        {
            ViewBag.style = "fa-dollar-sign";
            ViewBag.Main = "Sale";
            ViewBag.Page = "Sale Credit Memo";
            ViewBag.Sale = "show";
            ViewBag.SaleCreditMemo = "highlight";
            return View();
        }

        public void RemoveCreditMemoDetail(int id)
        {
            _context.SaleCreditMemoDetails.Find(id).Delete = true;
            _context.SaveChanges();
        }

        [HttpPost]
        public IActionResult CreateSaleCreditMemo(string data)
        {
            SaleCreditMemo creditMemo = JsonConvert.DeserializeObject<SaleCreditMemo>(data);
            creditMemo.ChangeLog = DateTime.Now;
            ModelMessage msg = new ModelMessage();
            creditMemo.UserID = int.Parse(User.FindFirst("UserID").Value);
            creditMemo.BranchID = int.Parse(User.FindFirst("BranchID").Value);
            ValidateSummary(creditMemo, creditMemo.SaleCreditMemoDetails);

            foreach (var dt in creditMemo.SaleCreditMemoDetails)
            {
                if (dt.SCMOID == 0)
                {
                    dt.PrintQty = dt.Qty;
                    if (creditMemo.CopyType > 0)
                    {
                        dt.Qty = dt.OpenQty;
                    }
                    else
                    {
                        dt.OpenQty = dt.Qty;
                    }
                }
                else
                {
                    dt.PrintQty = dt.Qty - dt.PrintQty;
                }
                
            }

            using (var t = _context.Database.BeginTransaction())
            {
                if (ModelState.IsValid)
                {                  
                    if(SaleCopyType.AR == creditMemo.CopyType)
                    {
                        var ARMaster = _context.SaleARs.Include(o => o.SaleARDetails)
                               .FirstOrDefault(m => string.Compare(m.InvoiceNo, creditMemo.CopyKey) == 0);
                        UpdateSourceCopy(creditMemo, creditMemo.SaleCreditMemoDetails, ARMaster, ARMaster.SaleARDetails, SaleCopyType.AR);
                    }
                    _context.SaleCreditMemos.Update(creditMemo);
                    _context.SaveChanges();
                    _context.Database.ExecuteSqlCommand("admin_SaleCreditMemo @OrderID={0}",
                       parameters: new[] {
                           creditMemo.SCMOID.ToString()
                       });
                    CreateIncomingPaymentCustomer(creditMemo);
                    t.Commit();
                    ModelState.AddModelError("success", "Item save successfully.");
                    msg.Approve();
                }
            }

            return Ok(new { Model = msg.Bind(ModelState), ItemReturn = new List<ItemsReturn>() });
        }

        [HttpGet]
        public IActionResult FindSaleCreditMemo(string number)
        {
            var list = _context.SaleCreditMemos.Include(x => x.SaleCreditMemoDetails).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            return Ok();
        }

        [HttpGet]
        public IActionResult GetSaleCreditMemoInvoice(bool IncludeVat)
        {
            var count = _context.SaleCreditMemos.Where(x => x.IncludeVat == false).Count() + 1;
            var list = "SCMO-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }

        [HttpGet]
        public IActionResult GetSaleCreditMemoInvoiceVAT()
        {
            var count = _context.SaleCreditMemos.Where(x => x.IncludeVat == true).Count() + 1;
            var list = "SCMO-VAT-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }


        //----Sale Credit Memo History-----//
        public IActionResult SaleDeliveryHistory()
        {
            ViewBag.style = "fa-dollar-sign";
            ViewBag.Main = "Sale";
            ViewBag.Page = "Sale Delivery History";
            ViewBag.Sale = "show";
            ViewBag.SaleDelivery = "highlight";
            List<BusinessPartner> customers = _context.BusinessPartners.Where(c => !c.Delete && c.Type.ToLower() == "customer").ToList();
            List<Warehouse> warehouses = _context.Warehouses.Where(c => !c.Delete).ToList();

            var saleHistories = GetSaleHistoriesByType(HistoryOfSaleType.Delivery).Where(h => DateTime.Parse(h.PostingDate).CompareTo(DateTime.Today) >= 0 && DateTime.Parse(h.PostingDate).CompareTo(DateTime.Today) <= 0);
            HistoryOfSaleViewModel history = new HistoryOfSaleViewModel
            {
                Customers = customers ?? new List<BusinessPartner>(),
                Warhouses = warehouses ?? new List<Warehouse>(),
                SaleHistories = saleHistories.ToList() ?? new List<HistoryOfSale>()
            };
            return View(history);
        }

        //----Sale AR History-----//
        public IActionResult SaleARHistory()
        {
            ViewBag.style = "fa-dollar-sign";
            ViewBag.Main = "Sale";
            ViewBag.Page = "Sale AR History";
            ViewBag.Sale = "show";
            ViewBag.SaleAR = "highlight";
            List<BusinessPartner> customers = _context.BusinessPartners.Where(c => !c.Delete && c.Type.ToLower() == "customer").ToList();
            List<Warehouse> warehouses = _context.Warehouses.Where(c => !c.Delete).ToList();

            var saleHistories = GetSaleHistoriesByType(HistoryOfSaleType.AR).Where(h => DateTime.Parse(h.PostingDate).CompareTo(DateTime.Today) >= 0 && DateTime.Parse(h.PostingDate).CompareTo(DateTime.Today) <= 0);
            HistoryOfSaleViewModel history = new HistoryOfSaleViewModel
            {
                Customers = customers ?? new List<BusinessPartner>(),
                Warhouses = warehouses ?? new List<Warehouse>(),
                SaleHistories = saleHistories.ToList() ?? new List<HistoryOfSale>()
            };
            return View(history);
        }

        //----Sale Credit Memo History-----//
        public IActionResult SaleCreditMemoHistory()
        {
            ViewBag.style = "fa-dollar-sign";
            ViewBag.Main = "Sale";
            ViewBag.Page = "Sale Credit Memo History";
            ViewBag.Sale = "show";
            ViewBag.SaleCreditMemo = "highlight";
            List<BusinessPartner> customers = _context.BusinessPartners.Where(c => !c.Delete && c.Type.ToLower() == "customer").ToList();
            List<Warehouse> warehouses = _context.Warehouses.Where(c => !c.Delete).ToList();

            var saleHistories = GetSaleHistoriesByType(HistoryOfSaleType.CreditMemo).Where(h => DateTime.Parse(h.PostingDate)
                .CompareTo(DateTime.Today) >= 0 && DateTime.Parse(h.PostingDate).CompareTo(DateTime.Today) <= 0);
            HistoryOfSaleViewModel history = new HistoryOfSaleViewModel
            {
                Customers = customers ?? new List<BusinessPartner>(),
                Warhouses = warehouses ?? new List<Warehouse>(),
                SaleHistories = saleHistories.ToList() ?? new List<HistoryOfSale>()
            };
            return View(history);
        }

        public IActionResult SearchSaleHistory(HistoryOfSaleType saleType, string word = "")
        {
            if (word != null)
            {
                string _word = word.Replace(" ", "").ToLowerInvariant();
                return Ok(GetSaleHistoriesByType(saleType).Where(h =>
                                    string.Compare(h.InvoiceNo.Trim().ToLowerInvariant(), _word, true) == 0
                                    || h.CustomerName.Replace(" ", "").ToLowerInvariant().Contains(_word)
                       ));
            }
            return Ok();
        }

        public IActionResult FilterSaleHistory(HistoryOfSaleFilter filter)
        {
            var saleHistories = GetSaleHistoriesByType(filter.SaleType);
            switch (filter.DateType)
            {
                case SaleDateType.PosingDate:
                    saleHistories = saleHistories.Where(h => DateTime.Parse(h.PostingDate)
                        .CompareTo(filter.DateFrom) >= 0 && DateTime.Parse(h.PostingDate).CompareTo(filter.DateTo) <= 0);
                    break;
                case SaleDateType.DocumentDate:
                    saleHistories = saleHistories.Where(h => DateTime.Parse(h.DocumentDate)
                        .CompareTo(filter.DateFrom) >= 0 && DateTime.Parse(h.DocumentDate).CompareTo(filter.DateTo) <= 0);
                    break;
                case SaleDateType.DueDate:
                    saleHistories = saleHistories.Where(h => DateTime.Parse(h.DueDate)
                        .CompareTo(filter.DateFrom) >= 0 && DateTime.Parse(h.DueDate).CompareTo(filter.DateTo) <= 0);
                    break;
            }

            if (filter.Customer > 0)
            {
                saleHistories = saleHistories.Where(h => h.CustomerID == filter.Customer);
            }

            if (filter.Warehouse > 0)
            {
                saleHistories = saleHistories.Where(h => h.WarehouseID == filter.Warehouse);
            }

            return Ok(saleHistories);
        }

        private IEnumerable<HistoryOfSale> GetSaleHistoriesByType(HistoryOfSaleType saleType)
        {
            switch (saleType)
            {
                case HistoryOfSaleType.Delivery:
                    return GetSaleHistories(_context.SaleDeliveries, "SDID");
                case HistoryOfSaleType.AR:
                    return GetSaleHistories(_context.SaleARs, "SARID");
                case HistoryOfSaleType.CreditMemo:
                    return GetSaleHistories(_context.SaleCreditMemos, "SCMOID");
            }
            return new List<HistoryOfSale>();
        }

        private IEnumerable<HistoryOfSale> GetSaleHistories<T>(IEnumerable<T> saleTypes, string key)
        {
            var saleHistories = from sa in saleTypes
                                join c in _context.BusinessPartners.Where(bp => bp.Type.ToLower() == "customer" && !bp.Delete)
                                on typeof(T).GetProperty("CusID").GetValue(sa) equals c.ID
                                join w in _context.Warehouses.Where(wh => !wh.Delete) on typeof(T).GetProperty("WarehouseID").GetValue(sa) equals w.ID
                                join cu in _context.Currency on typeof(T).GetProperty("LocalCurrencyID").GetValue(sa) equals cu.ID
                                select new HistoryOfSale
                                {
                                    ID = int.Parse(GetValue(sa, key).ToString()),
                                    InvoiceNo = GetValue(sa, "InvoiceNo").ToString(),
                                    CustomerName = c.Name,
                                    UserName = HttpContext.User.Identity.Name,
                                    BalanceDueLC = string.Format("{0} {1:N3}", cu.Description, GetValue(sa, "TotalAmount")),
                                    BalanceDueSC = string.Format("{0} {1:N3}", cu.Description, GetValue(sa, "TotalAmount_Sys")),
                                    ExchangeRate = string.Format("{0} {1:N2}", cu.Description, GetValue(sa, "ExchangeRate")),
                                    Status = typeof(T).GetProperty("Status").GetValue(sa).ToString(),
                                    PostingDate = ((DateTime)GetValue(sa, "PostingDate")).ToLongDateString(),
                                    DocumentDate = ((DateTime)GetValue(sa, "DocumentDate")).ToLongDateString(),                                
                                    DueDate = ((DateTime)GetValue(sa, "DueDate")).ToLongDateString(),
                                    CustomerID = c.ID,
                                    WarehouseID = c.ID
                                };
            return saleHistories;
        }//----End Sale History-----//

        private object GetValue<T>(T obj, string prop)
        {
            return typeof(T).GetProperty(prop).GetValue(obj);
        }

    }
}
