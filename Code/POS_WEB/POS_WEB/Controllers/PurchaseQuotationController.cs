﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.ReportClass;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Purchase;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    [Authorize]
    public class PurchaseQuotationController : Controller
    {
        public readonly IPurchaseQuotation _iquotation;
        public readonly IGUOM _gUOM;
        public readonly DataContext _context;
        public PurchaseQuotationController(IPurchaseQuotation quotation,DataContext context,IGUOM gUOM)
        {
            _iquotation = quotation;
            _context = context;
            _gUOM = gUOM;
        }
        public IActionResult Purchasequotation()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "Quotation";
            ViewBag.Menu = "show";
            ViewBag.PurchaseMenu = "show";
            ViewBag.PurchaseQT = "highlight";
            var userid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var login = _context.LoginModals.FirstOrDefault(x => x.Status == true && x.UserID == int.Parse(User.FindFirst("UserID").Value));
                userid = login.UserID;
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A022");
            try
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpGet]
        public IActionResult GetItemByWarehouse_Quotation(int ID)
        {
            var list = _iquotation.ServiceMapItemMasterDatas(ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouses(int ID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID==ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBusinessPartners()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type== "Vendor").ToList();
            return Ok(list);
        }
        [HttpGet] 
        public IActionResult GetInvoicenomber()
        {
            var count = _context.PurchaseQuotations.Count()+1;
            var invoice ="PQ-"+count.ToString().PadLeft(7, '0');
            return Ok(invoice);
        }
        [HttpGet]
        public IActionResult GetGroupDefind()
        {
            var list = _gUOM.GetAllgroupDUoMs().ToList();
            return Ok(list);
                
        }
        [HttpGet]
        public IActionResult Getcurrency()
        {
            var cur = _context.Currency.Where(x => x.Delete == false).ToList();
            return Ok(cur);
        }
        [HttpGet]
        public IActionResult GetFilterLocaCurrency(int CurrencyID)
        {
            var list = _iquotation.GetExchangeRates(CurrencyID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetCurrencyDefualt()
        {
            var list = _iquotation.GetCurrencyDefualt().ToList();
            return Ok(list);
        }
       

        [HttpPost]
        public IActionResult GetUserAccout(int UserID)
        {
            var list=from user in _context.UserAccounts.Where(x=>x.Delete==false) join 
                     emp in _context.Employees.Where(x=>x.Delete==false) on user.EmployeeID equals emp.ID
                     where user.ID==UserID
                     select new UserAccount
                     {
                         Employee=new Employee {
                             Name=emp.Name
                         }
                     };
            return Ok(list);
        }
        [HttpPost]
        public IActionResult GetQuotationDetail(int quotationID)
        {
            var list = _iquotation.ServiceQuotationDetails(quotationID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult PurchaseStory()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "Quotation";
            ViewBag.Subpage = "Purchase Story";
            ViewBag.Menu = "show";
            var userid = 0;
            if (User.FindFirst("Password").Value != "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value != "Kernel")
            {
                var login = _context.LoginModals.FirstOrDefault(x => x.Status == true && x.UserID == int.Parse(User.FindFirst("UserID").Value));
                userid = login.UserID;
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A022");
            try
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            catch (Exception)
            {
                return View();
            }
        }
        [HttpPost]
        public IActionResult SavePurchaseQuotation(PurchaseQuotation purchase,string Type)
        {
            if (Type == "Add")
            {
                if (purchase.PurchaseQuotationID == 0)
                {
                    _context.PurchaseQuotations.Add(purchase);
                    _context.SaveChanges();

                    foreach (var check in purchase.PurchaseQuotationDetail.ToList())
                    {
                        if (check.Qty <= 0)
                        {
                            _context.PurchaseQuotationDetails.Remove(check);
                            _context.SaveChanges();
                        }
                    }

                }
                else
                {
                    _context.PurchaseQuotations.Update(purchase);
                    _context.SaveChanges();
                    foreach (var item in purchase.PurchaseQuotationDetail.ToList())
                    {

                        if (item.Qty <= 0)
                        {
                            _context.PurchaseQuotationDetails.Remove(item);
                            _context.SaveChanges();
                        }
                    }
                }
            }
            else if (Type == "PR")
            {
                var remark = purchase.Remark;
                string InvoiceOrder = "";
                double openqty = 0;
                InvoiceOrder = (remark.Split("/"))[1];
                List<PurchaseQuotationDetail> Comfirn = new List<PurchaseQuotationDetail>();
                foreach (var items in purchase.PurchaseQuotationDetail.ToList())
                {
                    if (items.Qty > 0)
                    {
                        Comfirn.Add(items);
                    }
                }
                if (purchase.PurchaseQuotationID == 0)
                {
                    _context.PurchaseQuotations.Add(purchase);
                    _context.SaveChanges();
                    foreach (var check in purchase.PurchaseQuotationDetail.ToList())
                    {
                        if (check.Qty <= 0)
                        {
                            _context.Remove(check);
                            _context.SaveChanges();
                        }
                    }
                    

                    var purchaseRequest = _context.PurchaseRequests.FirstOrDefault(x => x.InvoiceNo == InvoiceOrder);

                    int ReqestID = purchaseRequest.PurchaseRequestID;
                    var detail = _context.PurchaseRequestDetails.Where(x => x.PurchaseRequestID == ReqestID && x.Delete == false);
                    if (Comfirn.Count() > 0)
                    {
                        foreach (var item in detail.ToList())
                        {
                            foreach (var items in Comfirn.ToList())
                            {
                                if (item.RequiredDetailID == items.requestID)
                                {
                                    if (items.Qty > item.OpenQty)
                                    {
                                        openqty = item.OpenQty - item.OpenQty;
                                        var purchaseDetail_1 = _context.PurchaseRequestDetails.FirstOrDefault(x => x.RequiredDetailID == item.RequiredDetailID);
                                        purchaseDetail_1.OpenQty = openqty;
                                        _context.Update(purchaseDetail_1);
                                        _context.SaveChanges();
                                        if (openqty == 0)
                                        {
                                            purchaseDetail_1.Delete = true;
                                            _context.Update(purchaseDetail_1);
                                            _context.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        openqty = item.OpenQty - items.Qty;
                                        var purchaseDetail = _context.PurchaseRequestDetails.FirstOrDefault(x => x.RequiredDetailID == item.RequiredDetailID);
                                        purchaseDetail.OpenQty = openqty;
                                        _context.Update(purchaseDetail);
                                        _context.SaveChanges();
                                        if (openqty == 0)
                                        {
                                            purchaseDetail.Delete = true;
                                            _context.Update(purchaseDetail);
                                            _context.SaveChanges();
                                        }
                                    }

                                }
                            }
                        }
                    }
                    if (checkStatus(detail))
                    {
                        var purchaseAP = _context.PurchaseRequests.FirstOrDefault(x => x.InvoiceNo == InvoiceOrder);
                        purchaseAP.Status = "close";
                        _context.Update(purchaseAP);
                        _context.SaveChanges();
                    }
                }
            }
            
            return Ok();
        }
        public bool checkStatus(IEnumerable<PurchaseRequestDetail> invoices)
        {
            bool result = true;
            foreach (var inv in invoices)
            {
                if (inv.Delete == false)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
        [HttpGet]
        public IActionResult FindPurchaseQutation(string number)
        {
            var list = _context.PurchaseQuotations.Include(x => x.PurchaseQuotationDetail).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            else
            {
                return Ok();
            }
        }
        [HttpGet]
        public IActionResult GetItemByWarehouseQuotation_Detail(int warehouseid,string invoice)
        {
            var list = _iquotation.GetQuotationDetail(warehouseid, invoice).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBusinessPartner_Quotation()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type == "Vendor").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetUserAccout_Quotation(int UserID)
        {
            var list = from user in _context.UserAccounts.Where(x => x.Delete == false)
                       join
                       emp in _context.Employees.Where(x => x.Delete == false) on user.EmployeeID equals emp.ID
                       where user.ID == UserID
                       select new UserAccount
                       {
                           Employee = new Employee
                           {
                               Name = emp.Name
                           }
                       };
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouse_Quotation(int ID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseQuotationReport(int BranchID,int WarehouseID,string PostingDate,string DocumentDate,string RequierdDate,string Check)
        {
            var list = _iquotation.GetPurchaseQuotations(BranchID, WarehouseID, PostingDate, DocumentDate, RequierdDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseQuotationByWarehouse(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string RequierdDate, string Check)
        {
            var list = _iquotation.GetPurchaseQuotations(BranchID, WarehouseID, PostingDate, DocumentDate, RequierdDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseQuotationByPostingDate(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string RequierdDate, string Check)
        {
            var list = _iquotation.GetPurchaseQuotations(BranchID, WarehouseID, PostingDate, DocumentDate, RequierdDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseQuotationDocumentDate(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string RequierdDate, string Check)
        {
            var list = _iquotation.GetPurchaseQuotations(BranchID, WarehouseID, PostingDate, DocumentDate, RequierdDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseQuotationRequierdDatedDate(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string RequierdDate, string Check)
        {
            var list = _iquotation.GetPurchaseQuotations(BranchID, WarehouseID, PostingDate, DocumentDate, RequierdDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseQuotationAllItem(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string RequierdDate, string Check)
        {
            var list = _iquotation.GetPurchaseQuotations(BranchID, WarehouseID, PostingDate, DocumentDate, RequierdDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindPurchaseRequest(int ID,string Invoice)
        {

            return Ok();
        }
    }
}
