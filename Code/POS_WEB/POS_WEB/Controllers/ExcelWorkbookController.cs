﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using CentricKernel;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.ExcelFile;
using POS_WEB.Models.Services.ImportMotocycle;
using POS_WEB.Models.Services.Inventory;

namespace POS_WEB.Controllers
{
 
    public class ExcelWorkbookController : Controller
    {
        private readonly DataContext _context;
        private readonly WorkbookContext _workbook;
        private readonly IHostingEnvironment _rootDir;
        public ExcelWorkbookController(DataContext context, IHostingEnvironment env) {
            _context = context;
            _rootDir = env;
            _workbook = new WorkbookContext();
        }
        public IActionResult Import()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Item Master Data ";
            ViewBag.Subpage = "Import";
            ViewBag.InventoryMenu = "show";
            ViewBag.ItemMasterData = "highlight";
            return View();
        }

        public IActionResult PreviewComponent() {
            var group_defined_uom = from duom in _context.GroupDUoMs.OrderBy(g=>g.GroupUoMID)
                                    join uom in _context.UnitofMeasures on duom.AltUOM equals uom.ID
                                    join guom in _context.GroupUOMs.Where(w=>w.Delete==false) on duom.GroupUoMID equals guom.ID
                                    select new
                                    {
                                        duom.ID,
                                        duom.GroupUoMID,
                                        duom.AltUOM,                                       
                                        uom.Name,
                                        duom.Factor
                                    };
            var itemMaster = new {
                PriceLists = _context.PriceLists.Where(w => w.Delete == false),
                GroupUOMs = _context.GroupUOMs.Where(w => w.Delete == false),
                GroupDefinedUoM = group_defined_uom,
                PrinterNames = _context.PrinterNames.Where(w => w.Delete == false),
                Warehouses = _context.Warehouses.Where(w=>w.Delete==false),
                ItemGroup1 = _context.ItemGroup1.Where(ig1 => ig1.Delete==false && string.Compare(ig1.Name, "None", true) != 0).OrderBy(g1 => g1.ItemG1ID),
                ItemGroup2 = _context.ItemGroup2.Where(ig2 =>ig2.Delete==false && string.Compare(ig2.Name, "None", true) != 0).OrderBy(g2 => g2.ItemG1ID),
                ItemGroup3 = _context.ItemGroup3.Where(ig3 =>ig3.Delete==false && string.Compare(ig3.Name, "None", true) != 0).OrderBy(g3 => g3.ItemG1ID).OrderBy(g3 => g3.ItemG2ID)
            };
            return Ok(itemMaster);
        }


        [HttpPost]
        public IActionResult GetFileNames()
        {
            ModelMessage message = new ModelMessage(ModelState);
            IList<string> sheetNames = new List<string>();
            if (Request.Form.Files.Count > 0)
            {
                IFormFile file = Request.Form.Files[0];
                if (ModelState.IsValid)
                {
                    using (Stream fs = new MemoryStream())
                    {
                        file.CopyTo(fs);
                        fs.Position = 0;                       
                        IWorkbook wb = WorkbookFactory.Create(fs);
                        for (int i = 0; i < wb.NumberOfSheets; i++)
                        {
                            sheetNames.Add(wb.GetSheetAt(i).SheetName);
                        }
                    }
                }
            }

            return Ok(sheetNames);
        }

        public async Task<IActionResult> Download()
        {
            string fullPath = Path.GetFullPath(string.Format("{0}{1}", _rootDir.WebRootPath, "/FileTemplate/template.xlsx"));

            var memory = new MemoryStream();
            using (Stream fs = System.IO.File.OpenRead(fullPath))
            {
                await fs.CopyToAsync(memory);
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", Path.GetFileName(fullPath));
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ExportItemMasters(FileDirectory directory)
        {
            ModelMessage message = new ModelMessage(ModelState);
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] randomBytes = new byte[12];
            rng.GetBytes(randomBytes);
            string ext = ".xlsx, .xls";
            string fileName = directory.FileName ?? Convert.ToBase64String(randomBytes);
            if (!string.IsNullOrEmpty(fileName))
            {
                fileName = fileName.Contains(ext.Split(".")[1]) ? fileName : fileName + directory.FileType;
            }

            //if (!Directory.Exists(directory.FilePath))
            //{
            //    ModelState.AddModelError("directory.FilePath", "File Path is not valid.");
            //}
            string fullPath = string.Format(@"{0}\{1}", _rootDir.WebRootPath, fileName);

            //if (ModelState.IsValid)
            //{
            //    var appointments = GetMotocycleTimelyAppointments();
            //    _workbook.SaveAsExcel(appointments, fullPath);
            //    message.Add("valiation.Success", "File has been save successfully.");
            //    message.Action = ModelAction.CONFIRM;

            //}

            return Ok(message.Bind(ModelState));
        }

        [HttpPost]
        public IActionResult PreviewItemMasterData()
        {
            IFormFile file = Request.Form.Files[0];
            using (Stream fs = new MemoryStream())
            {
                int sheetIndex = int.Parse(Request.Form["SheetIndex"]);
                file.CopyToAsync(fs);
                fs.Position = 0;
                IWorkbook wb = _workbook.ReadWorkbook(fs);
                return Ok(_workbook.ParseKeyValue(wb.GetSheetAt(sheetIndex)));
            }
        }

    }
}