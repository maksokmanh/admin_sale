﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models;
using POS_WEB.Models.Services.Account;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class FunctionsController : Controller
    {
        private readonly DataContext _context;

        public FunctionsController(DataContext context)
        {
            _context = context;
        }      
        public async Task<IActionResult> Index()
        {
            ViewBag.style = "fas fa-cogs";
            ViewBag.Main = "Setting";
            ViewBag.Page = "Functions";
            ViewBag.Menu = "show";
            return View(await _context.Functions.OrderBy(o=>o.Type).ToListAsync());
        }
        public IActionResult Create()
        {
            ViewBag.style = "fas fa-cogs";
            ViewBag.Main = "Functions";
            ViewBag.Page = "Create";
            ViewBag.Menu = "show";
            return View();
        }       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Code,Name,Type")] Function function)
        {
            if (ModelState.IsValid)
            {
                _context.Add(function);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(function);
        }      
        public async Task<IActionResult> Update(int? id)
        {
            ViewBag.style = "fas fa-cogs";
            ViewBag.Main = "Functions";
            ViewBag.Page = "Update";
            ViewBag.Menu = "show";
            if (id == null)
            {
                return NotFound();
            }

            var function = await _context.Functions.FindAsync(id);
            if (function == null)
            {
                return NotFound();
            }
            return View(function);
        }       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, [Bind("ID,Code,Name,Type")] Function function)
        {
            if (id != function.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(function);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FunctionExists(function.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(function);
        }  
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var fun = await _context.Functions.FindAsync(id);
            _context.Functions.Remove(fun);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }    

        private bool FunctionExists(int id)
        {
            return _context.Functions.Any(e => e.ID == id);
        }
    }
}
