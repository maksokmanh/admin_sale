﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Inventory.Transaction;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers
{
    [Authorize]
    public class TransferController : Controller
    {
        private readonly DataContext _context;
        public readonly ITransfer _transfer;
        public TransferController(DataContext context,ITransfer transfer)
        {
            _transfer = transfer;
            _context = context;
        }
        public IActionResult Transfer()
        {
            ViewBag.style = "fa fa-cubes";
            ViewBag.Main = "Inventory";
            ViewBag.Page = "Transaction";
            ViewBag.Subpage = "Transfer";
            ViewBag.InventoryMenu = "show";
            ViewBag.Transaction = "show";
            ViewBag.Transfer = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A041");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccesssDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccesssDenied", "Account");
            }
        }
        [HttpGet]
        public IActionResult GetInvoicenomber()
        {
            var count = _context.Transfers.Count() + 1;
            var list = "IM-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public  IActionResult GetWarehousesFrom(int BranchID)
        {
            var list = _transfer.GetFromWarehouse(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehousesTo(int BranchID)
        {
            var list = _transfer.GetToWarehouse.Where(x=>x.BranchID==BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBranch()
        {
            var list = _transfer.GetBranches.ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouse_By_filterBranch(int BranchID)
        {
            var list = _transfer.GetWarehouse_filter_Branch(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouseFrom(int warehouesId)
        {
            var list = _transfer.GetItemMasterBy_Warehouse(warehouesId).ToList(); 
            return Ok(list);
        }
        public IActionResult SaveTransfer(Transfer transfer)
        {
            List<ItemsReturnPC> list_group = new List<ItemsReturnPC>();
            if (transfer.BranchID == transfer.BranchToID && transfer.WarehouseFromID == transfer.WarehouseToID)
            {
                var detail = transfer.TransferDetails.First();
                detail.Check = "same";//condition return for the same warehouse
                return Ok(detail);
            }
            else
            {
                foreach (var item in transfer.TransferDetails.ToList())
                {
    
                    if (item.Qty < 0)
                    {
                        _context.Remove(item);
                        _context.SaveChanges();
                    }
                    else
                    {
                        var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom => uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                        var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                        var wh_detail_out = _context.WarehouseDetails.FirstOrDefault(w => w.ItemID == item.ItemID
                                                                                    && w.UomID == item.UomID
                                                                                    && w.Cost == item.Cost / uom_defined.Factor
                                                                                    && w.ExpireDate==item.ExpireDate
                                                                                    && w.WarehouseID == transfer.WarehouseFromID);

                        if(wh_detail_out!=null)
                        {
                            if (wh_detail_out.InStock < item.Qty * uom_defined.Factor)
                            {
                                var item_master = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == item.ItemID);
                                ItemsReturnPC item_group = new ItemsReturnPC
                                {
                                    Line_ID = item.LineID,
                                    Code = item_master.Code,
                                    ItemID = item.ItemID,
                                    KhmerName = item_group_uom.KhmerName + ' ' + item.UomName,
                                    InStock = ((wh_detail_out.InStock - wh_detail_out.Committed)/uom_defined.Factor).ToString(),
                                    OrderQty = (item.Qty).ToString(),
                                    Committed = wh_detail_out.Committed.ToString()
                                };
                                list_group.Add(item_group);
                            }
                        }
                        else
                        {
                            var item_master = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == item.ItemID);
                            ItemsReturnPC item_group = new ItemsReturnPC
                            {
                                Line_ID = item.LineID,
                                Code = item_master.Code,
                                ItemID = item.ItemID,
                                KhmerName = item_group_uom.KhmerName + ' ' + item.UomName,
                                InStock ="0", 
                                OrderQty ="Have not exist.",
                                Committed ="0"
                            };
                            list_group.Add(item_group);
                        }
                       
                    }
                }
                if (list_group.Count > 0)
                {
                    return Ok(list_group);
                }
                else
                {
                    if (transfer.UserRequestID == 0)
                    {
                        transfer.UserRequestID = transfer.UserID;
                    }
                    if (transfer.Time == null)
                    {
                        transfer.Time = DateTime.Now.ToLongTimeString();
                    }
                    _context.Transfers.Add(transfer);
                    _context.SaveChanges();
                    var TransferID = transfer.TarmsferID;
                    _transfer.SaveTrasfers(TransferID);
                    return Ok();
                }
            }
           
        }
        [HttpPost]
        public IActionResult SaveTransfer1(Transfer transfer)
        {   

            if(transfer.BranchID==transfer.BranchToID && transfer.WarehouseFromID == transfer.WarehouseToID)
            {
                var detail = transfer.TransferDetails.First();
                detail.Check = "Hello";
                return Ok(detail);
            }
            else
            {
                List<TransferDetail> list = new List<TransferDetail>();

                foreach (var item in transfer.TransferDetails.ToList())
                {
                    var checkStock_ount = _context.WarehouseDetails.FirstOrDefault(x => x.Cost == item.Cost &&
                                                                             x.UomID == item.UomID &&
                                                                             x.ExpireDate == item.ExpireDate &&
                                                                             x.ItemID == item.ItemID &&
                                                                             x.WarehouseID == transfer.WarehouseFromID);
                    var checkStock_in = _context.WarehouseDetails.FirstOrDefault(x => x.Cost == item.Cost &&
                                                                               x.UomID == item.UomID &&
                                                                               x.ExpireDate == item.ExpireDate &&
                                                                               x.ItemID == item.ItemID &&
                                                                               x.WarehouseID == transfer.WarehouseToID);
                    if (checkStock_ount != null)
                    {
                        if (checkStock_in != null)
                        {
                            if (item.Qty > checkStock_ount.InStock)
                            {
                                item.Check = "Over stock";
                                list.Add(item);
                            }
                        }
                        else
                        {
                            if (item.Qty > checkStock_ount.InStock)
                            {
                                item.Check = "Over stock";
                                list.Add(item);
                            }
                        }
                    }
                    else
                    {
                        item.Check = "Item not found in warehouse from";
                        list.Add(item);
                    }
                }
                if (list.Count() > 0)
                {
                    return Ok(list);
                }
                else
                {
                    if (transfer.UserRequestID == 0)
                    {
                        transfer.UserRequestID = transfer.UserID;
                    }
                    if (transfer.Time == null)
                    {
                        transfer.Time = DateTime.Now.ToLongTimeString();
                    }
                    _context.Transfers.Add(transfer);
                    _context.SaveChanges();
                    var TransferID = transfer.TarmsferID;
                    _transfer.SaveTrasfers(TransferID);
                    return Ok();
                }
            }
        }
        [HttpGet]
        public IActionResult FindBarcode(int WarehouseID, string Barcode)
        {
            try
            {
                var list = _transfer.GetItemFindBarcode(WarehouseID, Barcode).ToList();
                return Ok(list);
            }
            catch (Exception)
            {

                return Ok();
            }
        }
    }
}
