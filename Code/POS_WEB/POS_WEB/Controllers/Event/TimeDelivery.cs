﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using POS_WEB.Models;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.POS;
using POS_WEB.Models.Services.POS.service;
using POS_WEB.Models.Services.Purchase.Print;
using POS_WEB.Models.Services.ReportSale;
using POS_WEB.Models.Services.ReportSale.dev;
using POS_WEB.Models.ServicesClass;
using POS_WEB.Models.SignalR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace POS_WEB.Controllers.Event
{
    public class TimeDelivery
    {
        private static TimeDelivery instance;
        private static List<TableTimerTask> _tableTimerTasks = new List<TableTimerTask>();
      
        private static bool isInterval = true;
        private static Thread thread1;

        private CancellationTokenSource tokenSource = new CancellationTokenSource();
        public static IHubContext<SignalRClient> _hubcontext;
       
        public static TimeDelivery GetInstance(IHubContext<SignalRClient> hubcontext)
        {
            if (instance == null)
            {
                instance = new TimeDelivery();
                _hubcontext = hubcontext;
            }
            
            return instance;
        }
        public async Task PushTimer(int tableId, string time, char status, CancellationTokenSource cancellationToken)
        {
            var data = StartTimeTable(tableId, time, status);
            await _hubcontext.Clients.All.SendAsync("TimeWalker", data, cancellationToken.Token);
        }

        public void InitTableTime(List<TableTimerTask> tableTimerTasks)
        {
            _tableTimerTasks = tableTimerTasks;
        }

        public TableTimerTask StartTimeTable(int tableId, string time, char status)
        {
            TableTimerTask tableTimerTask = _tableTimerTasks.Find(t => t.TableTime.ID == tableId);
            if (tableTimerTask == null)
            {
                tableTimerTask = new TableTimerTask
                {
                    TableTime = new TableTime { Index = _tableTimerTasks.Count, ID = tableId, Time = time, Status = status },
                    CancellationToken = new CancellationTokenSource()
                };
                _tableTimerTasks.Add(tableTimerTask);

            }
            else
            {
                tableTimerTask.TableTime.Time = time;//tableTimerTask.TableTime.Time;
                tableTimerTask.TableTime.Status = status;
            }

            return tableTimerTask;
        }
        public void ResetTimeTable(int tableId,char status,string time)
        {
            TableTimerTask tableTimerTask = _tableTimerTasks.Find(t => t.TableTime.ID == tableId);
            if (tableTimerTask != null)
            {
                _tableTimerTasks[tableTimerTask.TableTime.Index].TableTime.Status =status;
                _tableTimerTasks[tableTimerTask.TableTime.Index].TableTime.Time = time;
                //_tableTimerTasks.Remove(tableTimerTask);
            }
        }
        public string StopTimeTable(int tableId, char status)
        {
            TableTimerTask tableTimerTask = _tableTimerTasks.Find(t => t.TableTime.ID == tableId);
            if (tableTimerTask != null)
            {
                _tableTimerTasks[tableTimerTask.TableTime.Index].TableTime.Status = status;
                return tableTimerTask.TableTime.Time;
            }
            return "00:00:00";

        }
        public string GetTimeTable(int tableId)
        {
            TableTimerTask tableTimerTask = _tableTimerTasks.Find(t => t.TableTime.ID == tableId);
            if (tableTimerTask != null)
            {
                return tableTimerTask.TableTime.Time;
            }
            return "00:00:00";

        }
        public void StartTimer()
        {
            try
            {
                if (thread1 == null)
                {
                    thread1 = new Thread(() => { IncreaseTime(); });
                    thread1.Start();
                    isInterval = true;
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }

        public void StopTimer()
        {
            if (thread1 != null && thread1.IsAlive)
            {
                try
                {
                    tokenSource.Cancel();
                }
                catch (Exception e)
                {
                    Console.Write(e);
                }
                isInterval = false;
                thread1.Abort();
            }
        }

        public void Canceltask(int tableid)
        {
            try
            {
                CancellationTokenSource tokenSource = _tableTimerTasks.Find(t => t.TableTime.ID == tableid).CancellationToken;
                tokenSource.Cancel();
                CancellationToken ct = tokenSource.Token;
                ct.ThrowIfCancellationRequested();
            }
            catch (Exception e)
            {
                Console.Write(e);
            }
        }
        public async void PushOrderByTable(List<Order> list, int tableid, string user)
        {
           await _hubcontext.Clients.All.SendAsync("PushOrder", list, tableid, user);
           // await _hubcontext.Clients.All.SendAsync("PushDataToSecondScreen", list);
        }
        public async void ClearUserOrder(int tableid)
        {
            await _hubcontext.Clients.All.SendAsync("ClearUserOrder", tableid);
        }
        public async void PushOrderByTableToAvailable(int tableid)
        {
            await _hubcontext.Clients.All.SendAsync("PushOrderToAvailable", tableid);
        }
       
        public async void PrintOrder(List<PrintOrder> items, List<PrinterName> printers,List<GeneralSetting> setting)
        {
           await _hubcontext.Clients.All.SendAsync("PrintItemOrder", items, printers,setting);
        }
        public async void PrintBill(List<PrintBill> list,List<GeneralSetting> setting,string ip)
        {
            if (list.Count > 0)
            {
                await _hubcontext.Clients.All.SendAsync("PrintItemBill", list,setting,ip);
                await _hubcontext.Clients.All.SendAsync("PushStatusBill", list[0].OrderID);
            }
        }
        public async void GetTableAvailable(string move)
        {
            await _hubcontext.Clients.All.SendAsync("GetTableAvailable", move);
        }
        public async static void IncreaseTime()
        {
            var interval = 0;
            while (isInterval)
            {
                if (_tableTimerTasks.Count == 0)
                {
                    continue;
                }

                foreach (TableTimerTask tableTimerTask in _tableTimerTasks.ToList())
                {
                    if (tableTimerTask.TableTime.Status == TableTime.LOCK)
                    {
                        DateTime dateTime = DateTime.ParseExact(tableTimerTask.TableTime.Time, "HH:mm:ss", CultureInfo.InvariantCulture);
                        DateTime dateTime1 = dateTime.AddSeconds(1);
                        _tableTimerTasks[tableTimerTask.TableTime.Index].TableTime.Time = dateTime1.ToString("HH:mm:ss", DateTimeFormatInfo.InvariantInfo);
                    }
                }

                await _hubcontext.Clients.All.SendAsync("TimeWalker", _tableTimerTasks.ToArray());
               
                await Task.Delay(1000);
                interval++;
            }
        }
       
        //POS report
        public async void PrintCashout(List<CashoutReport> cashouts,string ip)
        {
            await _hubcontext.Clients.All.SendAsync("PrintCashout", cashouts,ip);
        }
     
    }
}
