﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Purchase;
using POS_WEB.Models.Services.Responsitory;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    public class PurchasePOController : Controller
    {
        private readonly DataContext _context;
        private readonly IGoodRecepitPo _recepitPo;
        public PurchasePOController(DataContext context,IGoodRecepitPo recepitPo)
        {
            _context = context;
            _recepitPo = recepitPo;
        }
        // GET: /<controller>/
        public IActionResult PurchasePO()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "Goods Recepit PO";
            ViewBag.Menu = "show";
            ViewBag.PurchaseMenu = "show";
            ViewBag.PurchaseGRPO = "highlight";
            return View();
        }
        [HttpGet]
        public IActionResult GetInvoicenomber() {
           var cout= _context.GoodsReciptPOs.Count() + 1;
           var list = "PD-" + cout.ToString().PadLeft(7, '0');
            return Ok(list); 
        }
        [HttpGet]
        public IActionResult GetBusinessPartners()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type=="Vendor").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetCurrencyDefualt()
        {
            var list = _recepitPo.GetCurrencyDefualt().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouses(int ID)
        {
            var list = _context.Warehouses.Where(x => x.BranchID == ID && x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetGroupDefind()
        {
            var list = _recepitPo.GetAllGroupDefind().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult Getcurrency()
        {
            var list = _context.Currency.Where(x => x.Delete == false);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouse_PO(int ID)
        {
            var list = _recepitPo.serviceMapItemMasterDataPurchasPO(ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindBarcode(int WarehouseID, string Barcode)
        {
            try
            {
                var list = _recepitPo.FindItemBarcode(WarehouseID, Barcode).ToList();
                return Ok(list);
            }
            catch (Exception)
            {
                return Ok();
            }
        }
        [HttpGet]
        public IActionResult GetFilterLocaCurrency(int CurrencyID)
        {
            var list = _recepitPo.GetExchangeRates(CurrencyID).ToList();
            return Ok(list);
        }
        [HttpPost] 
        public IActionResult SavePurchaseGoodPO(GoodsReciptPO purchase,string Type)
        {
            if (Type == "Add")
            {
                _context.GoodsReciptPOs.Add(purchase);
                _context.SaveChanges();
                foreach (var check in purchase.GoodReciptPODetails.ToList())
                {
                    if (check.Qty <= 0)
                    {
                        _context.Remove(check);
                        _context.SaveChanges();
                    }
                }
                _recepitPo.GoodReceiptStockPO(purchase.ID,Type);

            }else if (Type == "PO")
             {
                string InvoiceOrder = "";
                double openqty = 0;
                var remark = purchase.Remark;
                InvoiceOrder = (remark.Split("/"))[1];

                List<GoodReciptPODetail> Comfirn = new List<GoodReciptPODetail>();
                List<GoodReciptPODetail> List = new List<GoodReciptPODetail>();

                foreach (var items in purchase.GoodReciptPODetails.ToList())
                {
                    var checkOrdered = _context.WarehouseDetails.FirstOrDefault(w => w.ItemID == items.ItemID && w.UomID == items.UomID && w.WarehouseID == purchase.WarehouseID);
                    if (checkOrdered == null)
                    {
                        List.Add(items);
                    }
                    else
                    {
                        if (items.Qty > 0)
                        {
                            Comfirn.Add(items);
                        }
                        
                    }
                }
                if (List.Count() > 0)
                {
                    return Ok(List);
                }
                else
                {
                    if (purchase.ID == 0)
                    {
                        _context.GoodsReciptPOs.Add(purchase);
                        _context.SaveChanges();
                        _recepitPo.GoodReceiptStockPO(purchase.ID,Type);

                        var purchaseOrder = _context.PurchaseOrders.FirstOrDefault(x => x.InvoiceNo == InvoiceOrder);
                        var subAppliedAmount = _context.PurchaseOrders.Where(x => x.InvoiceNo == InvoiceOrder);
                        purchaseOrder.Applied_Amount = subAppliedAmount.Sum(x => x.Applied_Amount) + purchase.Balance_Due;
                        _context.PurchaseOrders.Update(purchaseOrder);
                        _context.SaveChanges();

                        int OrderID = purchaseOrder.PurchaseOrderID;
                        var detail = _context.PurchaseOrderDetails.Where(x => x.PurchaseOrderID == OrderID && x.Delete == false);
                        if (Comfirn.Count() > 0)
                        {
                            foreach (var item in detail.ToList())
                            {
                                foreach (var items in Comfirn.ToList())
                                {
                                    if (item.PurchaseOrderDetailID == items.OrderID)
                                    {
                                        if (items.Qty > item.Qty)
                                        {
                                            openqty = item.OpenQty - item.OpenQty;
                                            var purchaseDetail = _context.PurchaseOrderDetails.FirstOrDefault(x => x.PurchaseOrderDetailID == item.PurchaseOrderDetailID);
                                            purchaseDetail.OpenQty = openqty;
                                            _context.Update(purchaseDetail);
                                            _context.SaveChanges();
                                            if (openqty == 0)
                                            {
                                                purchaseDetail.Delete = true;
                                                _context.Update(purchaseDetail);
                                                _context.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            openqty = item.OpenQty - items.Qty;
                                            var purchaseDetail = _context.PurchaseOrderDetails.FirstOrDefault(x => x.PurchaseOrderDetailID == item.PurchaseOrderDetailID);
                                            purchaseDetail.OpenQty = openqty;
                                            _context.Update(purchaseDetail);
                                            _context.SaveChanges();
                                            if (openqty == 0)
                                            {
                                                purchaseDetail.Delete = true;
                                                _context.Update(purchaseDetail);
                                                _context.SaveChanges();
                                            }
                                        }
                                       
                                    }
                                }
                            }
                        }
                        if (checkStatus(detail))
                        {
                            var purchaseAP = _context.PurchaseOrders.FirstOrDefault(x => x.InvoiceNo == InvoiceOrder);
                            purchaseAP.Status = "close";
                            _context.Update(purchaseAP);
                            _context.SaveChanges();
                        }
                    }
                }
            }
            return Ok();
        }
        public bool checkStatus(IEnumerable<PurchaseOrderDetail> invoices)
        {
            bool result = true;
            foreach (var inv in invoices)
            {
                if (inv.Delete == false)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
        [HttpGet]
        public IActionResult FindPurchaseGoodReceiptPO(string number)
        {
            var list = _context.GoodsReciptPOs.Include(w => w.GoodReciptPODetails).FirstOrDefault(x => x.InvoiceNo == number);
            if (list != null)
            {
                return Ok(list);
            }
            else
            {
                return Ok();
            }
        }
        [HttpGet]
        public IActionResult GetItemByWarehouse_PO_Detail(int warehouseid,string invoice)
        {
            var list = _recepitPo.ServiceMapItemMasterDataGoodReceiptPOs_Detail(warehouseid, invoice).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBusinessPartner_PO()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type=="Vendor").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetUserAccout_PO(int UserID)
        {
            var list = from user in _context.UserAccounts.Where(x => x.Delete == false)
                       join
                       emp in _context.Employees.Where(x => x.Delete == false) on user.EmployeeID equals emp.ID
                       where user.ID == UserID
                       select new UserAccount
                       {
                           Employee = new Employee
                           {
                               Name = emp.Name
                           }
                       };
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouse_PO(int ID)
        {
            var list = _context.Warehouses.Where(x => x.BranchID == ID && x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseorder(int BranchID)
        {
            var list = _recepitPo.GetAllPruchaseOrder(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindPurhcaseOrder(int ID,string Invoice)
        {
            var list = _recepitPo.GetPurchaseAP_From_PurchaseOrder(ID, Invoice).ToList();
            return Ok(list);
        }
        public IActionResult GoodReceptPOHistory()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "Goods Receipt PO";
            ViewBag.Subpage = "History";
            ViewBag.Menu = "show";
            return View();
        }
        [HttpGet]
        public IActionResult GetPurchaseGoodPoReport(int BarbchID,int WarehouseID,string PostingDate,string DocumentDate,string DuDate,string Check)
        {
            var list = _recepitPo.GetReportGoodReceiptPO(BarbchID, WarehouseID, PostingDate, DocumentDate, DuDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseGoodPOByWarehouse(int BarbchID, int WarehouseID, string PostingDate, string DocumentDate, string DuDate, string Check)
        {
            var list = _recepitPo.GetReportGoodReceiptPO(BarbchID, WarehouseID, PostingDate, DocumentDate, DuDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseGoodReceiptByPostingDate(int BarbchID, int WarehouseID, string PostingDate, string DocumentDate, string DuDate, string Check)
        {
            var list = _recepitPo.GetReportGoodReceiptPO(BarbchID, WarehouseID, PostingDate, DocumentDate, DuDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseGoodP0ByDocumentDate(int BarbchID, int WarehouseID, string PostingDate, string DocumentDate, string DuDate, string Check)
        {
            var list = _recepitPo.GetReportGoodReceiptPO(BarbchID, WarehouseID, PostingDate, DocumentDate, DuDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseGoodPOByDeliveryDatedDate(int BarbchID, int WarehouseID, string PostingDate, string DocumentDate, string DuDate, string Check)
        {
            var list = _recepitPo.GetReportGoodReceiptPO(BarbchID, WarehouseID, PostingDate, DocumentDate, DuDate, Check).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseGoodPOAllItem(int BarbchID, int WarehouseID, string PostingDate, string DocumentDate, string DuDate, string Check)
        {
            var list = _recepitPo.GetReportGoodReceiptPO(BarbchID, WarehouseID, PostingDate, DocumentDate, DuDate, Check).ToList();
            return Ok(list);
        }
    }
}
