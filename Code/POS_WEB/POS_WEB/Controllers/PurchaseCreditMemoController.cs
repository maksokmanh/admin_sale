﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.POS.service;
using POS_WEB.Models.Services.Purchase;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    [Authorize]
    public class PurchaseCreditMemoController : Controller
    {
        private readonly DataContext _context;
        private readonly IPurchaseCreditMemo _PurchasMemo;
        private readonly IGUOM _gUOM;
        public PurchaseCreditMemoController(DataContext context,IPurchaseCreditMemo purchaseCredit,IGUOM gUOM)
        {
            _context = context;
            _PurchasMemo = purchaseCredit;
            _gUOM = gUOM;
        }
        [HttpGet]
        public IActionResult PurchaseCreditMemo()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "A/P Credit Memo";
            ViewBag.PurchaseMenu = "show";
            ViewBag.PurchaseCreditMemo = "highlight";

            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A025");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpPost]
        public IActionResult PurchaseCreditMemo(string data)
        {
            ViewBag.Invoice = data;
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "A/P Credit Memo";
            ViewBag.Menu = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A025");
           if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        //[HttpGet]
        //public IActionResult GetPurchaseCreditMemo_From_PurchaseAP(string Invoice)
        //{
        //    var list = _PurchasMemo.GetPurchaseCreditMemo_From_PurchaseAP(Invoice).ToList();
        //    return Ok(list);
        //}  
        [HttpGet]
        public IActionResult GetInvoicenomber()
        {
            var count = _context.PurchaseCreditMemos.Count() +1;
            var list = "PC-" + count.ToString().PadLeft(7, '0');
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBusinessPartners()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type== "Vendor").ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetCurrencyDefualt()
        {
            var list = _PurchasMemo.GetCurrencies().ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouses(int ID)
        {
            var list = _context.Warehouses.Where(x => x.Delete == false && x.BranchID == ID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetGroupDefind()
        {
            var list = _gUOM.GetAllgroupDUoMs();
            return Ok(list);
        }
        [HttpGet]
        public  IActionResult Getcurrency()
        {
            var list = _context.Currency.Where(x => x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouse_Memo(int ID)
        {
            var list = _PurchasMemo.ServiceMapItemMasterDataPurchaseCreditMemos(ID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult CancelPayment(string Invoice)
        {
            var checkpay = _context.OutgoingPaymentDetails.Where(x => x.DocumentNo == Invoice && x.Delete==false);
            var _Ap = _context.Purchase_APs.FirstOrDefault(x => x.InvoiceNo == Invoice);
            var vendor = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == Invoice);
            foreach (var item in checkpay.ToList())
            {
                _Ap.Applied_Amount = _Ap.Applied_Amount - (item.Cash + item.TotalDiscount);
                _Ap.Status = "open";
                vendor.BalanceDue = vendor.BalanceDue + (item.Cash + item.TotalDiscount);
                vendor.Applied_Amount = vendor.Applied_Amount - (item.Cash + item.TotalDiscount);
                vendor.Status = "open";
                item.Delete = true;
                _context.OutgoingPaymentDetails.Update(item);
                _context.Purchase_APs.Update(_Ap);
                _context.OutgoingPaymentVendors.Update(vendor);
                _context.SaveChanges();
            }
            return Ok();
        }
        [HttpPost]
        public IActionResult SavePurchaseCreditMemo(PurchaseCreditMemo purchase, string Type)
        {           
            List<ItemsReturnPC> list = new List<ItemsReturnPC>();
            List<ItemsReturnPC> list_group = new List<ItemsReturnPC>();
            foreach (var item in purchase.PurchaseCreditMemoDetails.ToList())
            {
                var check = list_group.Find(w => w.ItemID == item.ItemID);
                var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom => uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                if (check == null)
                {
                    var item_warehouse = _context.WarehouseSummary.FirstOrDefault(w => w.WarehouseID == purchase.WarehouseID && w.ItemID == item.ItemID);
                    var item_master = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == item.ItemID);
                    if (item_warehouse != null)
                    {
                        ItemsReturnPC item_group = new ItemsReturnPC
                        {
                            Line_ID = item.LineID,
                            Code = item_master.Code,
                            ItemID = item.ItemID,
                            KhmerName = item_group_uom.KhmerName + ' ' + item_group_uom.UnitofMeasureInv.Name,
                            InStock = (item_warehouse.InStock - item_warehouse.Committed).ToString(),
                            OrderQty = (item.Qty * uom_defined.Factor).ToString(),
                            Committed = item_warehouse.Committed.ToString()
                        };
                        list_group.Add(item_group);
                    }

                }
                else
                {
                    check.OrderQty = (double.Parse(check.OrderQty) + item.Qty*uom_defined.Factor).ToString();
                }
            }
            foreach (var item in list_group)
            {
                if (double.Parse(item.OrderQty) > double.Parse(item.InStock))
                {
                    ItemsReturnPC item_return = new ItemsReturnPC
                    {
                        Line_ID = item.Line_ID,
                        Code = item.Code,
                        ItemID = item.ItemID,
                        KhmerName = item.KhmerName,
                        InStock = item.InStock.ToString(),
                        OrderQty = item.OrderQty.ToString()
                    };
                    list.Add(item_return);
                }

            }
            //check instock
            if (list.Count > 0)
            {
                return Ok(list);//Count >0
            }
            else
            {
                if (Type == "Add")
                {
                    foreach (var item in purchase.PurchaseCreditMemoDetails.ToList())
                    {
                        if (item.Qty > 0)
                        {
                            object item_return;
                            var item_group_uom = _context.ItemMasterDatas.Include(gu => gu.GroupUOM).Include(uom => uom.UnitofMeasureInv).FirstOrDefault(w => w.ID == item.ItemID);
                            var uom_defined = _context.GroupDUoMs.FirstOrDefault(w => w.GroupUoMID == item_group_uom.GroupUomID && w.AltUOM == item.UomID);
                            var item_master = _context.ItemMasterDatas.FirstOrDefault(w => w.ID == item.ItemID);
                            if (item_master.ManageExpire == "None")
                            {
                                item_return = _context.WarehouseDetails.FirstOrDefault(w => w.ItemID == item.ItemID && w.UomID == item.UomID && w.Cost == item.PurchasPrice / uom_defined.Factor && w.InStock >= item.Qty * uom_defined.Factor);

                            }
                            else
                            {
                                item_return = _context.WarehouseDetails.FirstOrDefault(w => w.ItemID == item.ItemID && w.UomID == item.UomID && w.Cost == item.PurchasPrice / uom_defined.Factor && w.ExpireDate == item.ExpireDate && w.InStock >= item.Qty * uom_defined.Factor);

                            }

                            if (item_return == null)
                            {
                                var uom = _context.UnitofMeasures.FirstOrDefault(w => w.ID == item.UomID);
                                ItemsReturnPC item_add = new ItemsReturnPC
                                {
                                    Line_ID = item.LineID,
                                    Code = item_master.Code,
                                    ItemID = item.ItemID,
                                    KhmerName = item_master.KhmerName + ' ' + uom.Name,
                                    InStock ="0",
                                    OrderQty = "Have not exist."
                                };
                                list.Add(item_add);
                            }
                        }
                    }
                    if (list.Count() > 0)
                    {
                        return Ok(list);
                    }
                    else
                    {
                        _context.PurchaseCreditMemos.Add(purchase);
                        _context.SaveChanges();
                        foreach (var check in purchase.PurchaseCreditMemoDetails.ToList())
                        {
                            if (check.Qty <= 0)
                            {
                                _context.Remove(check);
                                _context.SaveChanges();
                            }
                        }
                        _PurchasMemo.GoodIssuesStock(purchase.PurchaseMemoID, "AP");
                    }
                }
                else if (Type == "PU")
                {
                    string APInvoice = "";
                    if (purchase.Remark.Contains("/"))
                    {
                        APInvoice = purchase.Remark.Split("/")[1];
                        purchase.BaseOn = APInvoice;
                    }
                    // check payment
                    var checkpay = _context.OutgoingPaymentDetails.FirstOrDefault(x => x.DocumentNo == APInvoice && x.Delete == false);
                    if (checkpay != null)
                    {
                        CheckPayment check = new CheckPayment()
                        {
                            Check = "T",
                            Invoice = APInvoice
                        };
                        return Ok(check);
                    }
                    else
                    {
                        //do credit memo
                        _context.PurchaseCreditMemos.Add(purchase);
                        _context.SaveChanges();
                        foreach (var check in purchase.PurchaseCreditMemoDetails.ToList())
                        {
                            if (check.Qty <= 0)
                            {
                                _context.Remove(check);
                                _context.SaveChanges();
                            }
                            else
                            {
                                //check close AP
                                var purchase_ap = _context.Purchase_APs.FirstOrDefault(w => w.InvoiceNo == APInvoice);
                                var purchase_detail = _context.PurchaseDetails.FirstOrDefault(w => w.Purchase_APID == purchase_ap.PurchaseAPID && w.PurchaseDetailAPID == check.APID);
                                double open_qty = purchase_detail.OpenQty - check.Qty;
                                if (open_qty <= 0)
                                {
                                    purchase_detail.Delete = true;
                                    purchase_detail.OpenQty = open_qty;
                                }
                                else
                                {
                                    purchase_detail.OpenQty = open_qty;
                                }
                                _context.Update(purchase_detail);
                                _context.SaveChanges();
                            }
                        }
                        _PurchasMemo.GoodIssuesStock(purchase.PurchaseMemoID, "AP");
                        _PurchasMemo.UpdatePurchaseAP(APInvoice,purchase);
                    }
                }
               
            }
            return Ok();
        }
        public IActionResult SavePurchaseCreditMemo1(PurchaseCreditMemo purchase,string Type)
        {
            var remark = purchase.Remark;
            string InvoiceAp = "";
            double openqty = 0;
            if (Type == "Add")
            {
                List<PurchaseCreditMemoDetail> list = new List<PurchaseCreditMemoDetail>();
                List<PurchaseCreditMemoDetail> Comfirn = new List<PurchaseCreditMemoDetail>();
                foreach (var items in purchase.PurchaseCreditMemoDetails.ToList())
                {
                    var checkstock = _context.WarehouseDetails.FirstOrDefault(w => w.ItemID == items.ItemID && w.Cost == items.PurchasPrice * purchase.ExchangeRate && w.UomID == items.UomID && w.ExpireDate == items.ExpireDate && w.WarehouseID == purchase.WarehouseID);
                    if (checkstock == null)
                    {
                        items.check = "Not​​​​ Transaction";
                        list.Add(items);
                    }
                    else
                    {
                        if (items.Qty > checkstock.InStock)
                        {
                            items.check = "Excessive Stock";
                            list.Add(items);
                        }
                        else
                        {
                            if (items.Qty > 0)
                            {
                                Comfirn.Add(items);
                            }
                        }
                    }
                }
                if (list.Count() > 0)
                {
                    return Ok(list);
                }
                else
                {
                    if (purchase.PurchaseMemoID == 0)
                    {
                        _context.PurchaseCreditMemos.Add(purchase);
                        _context.SaveChanges();
                        foreach (var check in purchase.PurchaseCreditMemoDetails.ToList())
                        {
                            if (check.Qty <= 0)
                            {
                                _context.Remove(check);
                                _context.SaveChanges();
                            }
                        }
                        _PurchasMemo.GoodIssuesStock(purchase.PurchaseMemoID, "AP");
                        
                    }
                }
            }
            else if (Type == "PU")
            {
                InvoiceAp = (remark.Split("/"))[1];
                List<PurchaseCreditMemoDetail> list = new List<PurchaseCreditMemoDetail>();
                List<PurchaseCreditMemoDetail> Comfirn = new List<PurchaseCreditMemoDetail>();
                // check payment
                var checkpay = _context.OutgoingPaymentDetails.FirstOrDefault(x => x.DocumentNo == InvoiceAp && x.Delete == false);
                if (checkpay != null)
                {
                    CheckPayment check = new CheckPayment()
                    {
                        Check = "T",
                        Invoice = InvoiceAp
                    };
                    return Ok(check);
                }
                else
                {
                    foreach (var items in purchase.PurchaseCreditMemoDetails.ToList())
                    {
                        var checkstock = _context.WarehouseDetails.FirstOrDefault(w => w.ItemID == items.ItemID && w.Cost == items.PurchasPrice * purchase.ExchangeRate && w.UomID == items.UomID && w.ExpireDate == items.ExpireDate && w.WarehouseID == purchase.WarehouseID);

                        if (checkstock == null)
                        {
                            items.check = "Not​​​​ Transaction";
                            list.Add(items);
                        }
                        else
                        {
                            if (items.Qty> checkstock.InStock)
                            {
                                items.check = "Over Stock";
                                list.Add(items);
                            }
                            else
                            {
                                if (items.Qty > 0)
                                {
                                    Comfirn.Add(items);
                                }

                            }
                        }
                    }
                    if (list.Count() > 0)
                    {
                        return Ok(list);
                    }
                    else
                    {
                        if (purchase.PurchaseMemoID == 0)
                        {
                            _context.PurchaseCreditMemos.Add(purchase);
                            _context.SaveChanges();
                            foreach (var check in purchase.PurchaseCreditMemoDetails.ToList())
                            {
                                if (check.Qty <= 0)
                                {
                                    _context.Remove(check);
                                    _context.SaveChanges();
                                }
                            }
                            _PurchasMemo.GoodIssuesStock(purchase.PurchaseMemoID, "AP");

                            var purchaseAp = _context.Purchase_APs.FirstOrDefault(x => x.InvoiceNo == InvoiceAp);
                            var outgoing = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == InvoiceAp);
                            var subApplied_Amount = _context.Purchase_APs.Where(x => x.InvoiceNo == InvoiceAp);

                            purchaseAp.Applied_Amount = subApplied_Amount.Sum(x => x.Applied_Amount) + purchase.Balance_Due;
                            outgoing.Applied_Amount = outgoing.Applied_Amount + purchase.Balance_Due_Sys;
                            _context.OutgoingPaymentVendors.Update(outgoing);
                            _context.Purchase_APs.Update(purchaseAp);
                            _context.SaveChanges();
                            int ApID = purchaseAp.PurchaseAPID;
                            var detail = _context.PurchaseDetails.Where(x => x.Purchase_APID == ApID && x.Delete == false);
                            if (Comfirn.Count() > 0)
                            {
                                foreach (var item in detail.ToList())
                                {
                                    foreach (var items in Comfirn.ToList())
                                    {
                                        if (item.PurchaseDetailAPID == items.APID)
                                        {
                                            openqty = item.OpenQty - items.Qty;

                                            var purchaseDetail = _context.PurchaseDetails.FirstOrDefault(x => x.PurchaseDetailAPID == item.PurchaseDetailAPID);
                                            purchaseDetail.OpenQty = openqty;
                                            _context.Update(purchaseDetail);
                                            _context.SaveChanges();

                                            if (openqty == 0)
                                            {
                                                purchaseDetail = _context.PurchaseDetails.FirstOrDefault(x => x.PurchaseDetailAPID == item.PurchaseDetailAPID);
                                                purchaseDetail.Delete = true;
                                                _context.Update(purchaseDetail);
                                                _context.SaveChanges();
                                            }

                                        }
                                    }
                                }
                            }
                            if (checkStatus(detail))
                            {
                                var purchaseAP = _context.Purchase_APs.FirstOrDefault(x => x.InvoiceNo == InvoiceAp);
                                purchaseAP.Status = "close";
                                _context.Update(purchaseAP);
                                var outgo = _context.OutgoingPaymentVendors.FirstOrDefault(x => x.DocumentNo == InvoiceAp);
                                outgo.Status = "close";
                                _context.OutgoingPaymentVendors.Update(outgo);
                                _context.SaveChanges();
                            }
                        }
                    }
                }
            }
            return Ok();
        }
        public bool checkStatus(IEnumerable<Purchase_APDetail> invoices)
        {
            bool result = true;
            foreach (var inv in invoices)
            {
                if (inv.Delete == false)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
       
        [HttpPost]
        public IActionResult FindPurchaseCreditMemo(string number)
        {
            var list = _context.PurchaseCreditMemos.Include(x=>x.PurchaseCreditMemoDetails).FirstOrDefault(x => x.InvoiceNo == number);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetItemByWarehouse_Memo_Detail(int warehouseid,string invoice)
        {
            var list = _PurchasMemo.ServiceMapItemMasterDataPurchaseCreditMemosDetail(warehouseid, invoice).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetBusinessPartner_Memo()
        {
            var list = _context.BusinessPartners.Where(x => x.Delete == false && x.Type=="Vendor").ToList();
            return Ok(list);
        }
        [HttpPost]
        public IActionResult GetUserAccout_Memo(int UserID)
        {
            var list = from user in _context.UserAccounts.Where(x => x.Delete == false)
                       join
                       emp in _context.Employees.Where(x => x.Delete == false) on user.EmployeeID equals emp.ID
                       where user.ID == UserID
                       select new UserAccount
                       {
                           Employee = new Employee
                           {
                               Name = emp.Name
                           }
                       }; 
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouse_Memo(int ID)
        {
            var list = _context.Warehouses.Where(x => x.BranchID == ID && x.Delete == false).ToList();
            return Ok(list);
        }
        public IActionResult PurchaseCreditMemoHistory()
        {
            ViewBag.style = "fa fa-shopping-cart";
            ViewBag.Main = "Purchase";
            ViewBag.Page = "A/P Credit Memo";
            ViewBag.Subpage = "A/P Credit Memo History";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A025");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
        }
        [HttpGet]
        public IActionResult GetPurchaseMemoReport(int BranchID,int WarehouseID,string PostingDate,string DocumenteDate,string DeliveryDate,string Check)
        {
            var list = _PurchasMemo.ReportPurchaseCreditMemo(BranchID, WarehouseID, PostingDate, DocumenteDate, DeliveryDate, Check);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetWarehouses_Memo(int ID)
        {
            var list = _context.Warehouses.Where(x => x.BranchID == ID && x.Delete == false).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseCreditMemoByWarehouse(int BranchID, int WarehouseID, string PostingDate, string DocumenteDate, string DeliveryDate, string Check)
        {
            var list = _PurchasMemo.ReportPurchaseCreditMemo(BranchID, WarehouseID, PostingDate, DocumenteDate, DeliveryDate, Check);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseCreditMemoByPostingDate(int BranchID, int WarehouseID, string PostingDate, string DocumenteDate, string DeliveryDate, string Check)
        {
            var list = _PurchasMemo.ReportPurchaseCreditMemo(BranchID, WarehouseID, PostingDate, DocumenteDate, DeliveryDate, Check);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseCreditMemoByDocumentDate(int BranchID, int WarehouseID, string PostingDate, string DocumenteDate, string DeliveryDate, string Check)
        {
            var list = _PurchasMemo.ReportPurchaseCreditMemo(BranchID, WarehouseID, PostingDate, DocumenteDate, DeliveryDate, Check);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseCreditMemoByDeliveryDatedDate(int BranchID, int WarehouseID, string PostingDate, string DocumenteDate, string DeliveryDate, string Check)
        {
            var list = _PurchasMemo.ReportPurchaseCreditMemo(BranchID, WarehouseID, PostingDate, DocumenteDate, DeliveryDate, Check);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseCreditMemoAllItem(int BranchID, int WarehouseID, string PostingDate, string DocumenteDate, string DeliveryDate, string Check)
        {
            var list = _PurchasMemo.ReportPurchaseCreditMemo(BranchID, WarehouseID, PostingDate, DocumenteDate, DeliveryDate, Check);
            return Ok(list);
        }
        [HttpGet]
        public IActionResult GetPurchaseAP(int BranchID)
        {
            var list = _PurchasMemo.GetAllPurchaseAP(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindPurchaseAP(int ApID, string Invoice)
        {
            var list = _PurchasMemo.GetPurchaseCreditMemo_From_PurchaseAP(ApID, Invoice).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindBarcode(int WarehouseID,string Barcode)
        {
            try
            {
                var list = _PurchasMemo.GetItemFromBarcode(WarehouseID, Barcode).ToList();
                return Ok(list);
            }
            catch (Exception)
            {

                return Ok();
            }
            
        }
        [HttpGet]
        public IActionResult GetGoodReceiptPO(int BranchID)
        {
            var list = _PurchasMemo.GetAllGoodsReceiptPO(BranchID).ToList();
            return Ok(list);
        }
        [HttpGet]
        public IActionResult FindGoodReceiptPO(int GoodPOID,string Invoice)
        {
            var list = _PurchasMemo.GetPurchaseMemo_From_PurchaseGoodReceipt(GoodPOID, Invoice).ToList();
            return Ok(list);
        }
    }
}
