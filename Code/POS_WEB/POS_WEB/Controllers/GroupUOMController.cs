﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Pagination;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.ServicesClass;

namespace POS_WEB.Controllers.Developer2
{
    [Authorize]
    public class GroupUOMController : Controller
    {
        private readonly DataContext _context;
        private readonly IGUOM _guom;

        public GroupUOMController(DataContext context,IGUOM gUOM)
        {
            _context = context;
            _guom = gUOM;
        }

   
        public async Task<IActionResult> Index(string minpage = "All", string sortOrder = null, string currentFilter = null, string searchString = null, int? page = null)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure Group";
            ViewBag.Administrator = "show";
            ViewBag.Inventory = "show";
            ViewBag.UnitofMeasureGroup = "highlight";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);

           
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A007");
            var data = _guom.GetGroupUOMs().OrderBy(g=>g.ID);
            var Cate = from s in data select s;
            int pageSize = 0, MaxSize = 0;
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                ViewData["CurrentSort"] = sortOrder;
                ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewData["CodeSortParm"] = sortOrder == "Code" ? "Code_desc" : "Code";
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;

                }
                ViewData["CurrentFilter"] = searchString;

                //var Cate = from s in data select s;

                if (!String.IsNullOrEmpty(searchString))
                {
                    Cate = Cate.Where(s => s.Name.Contains(searchString)
                    || s.Code.Contains(searchString));

                }
                switch (sortOrder)
                {
                    case "name_desc":
                        Cate = Cate.OrderByDescending(s => s.Name);
                        break;
                    case "Code":
                        Cate = Cate.OrderBy(s => s.Code);
                        break;
                    case "Code_desc":
                        Cate = Cate.OrderByDescending(s => s.Code);
                        break;
                    default:
                        Cate = Cate.OrderBy(s => s.Name);
                        break;
                }
                //int pageSize = 0, MaxSize = 0;
                int.TryParse(minpage, out MaxSize);
                //int t = int.Parse(category);
                //int t1 = Convert.ToInt16(category);

                if (MaxSize == 0)
                {
                    int d = data.Count();
                    if (d == 0) d = 1;
                    pageSize = d;
                    ViewBag.sizepage5 = "All";
                }
                else
                {
                    if (MaxSize == 5)
                    {
                        ViewBag.sizepage1 = minpage;
                    }
                    else if (MaxSize == 10)
                    {
                        ViewBag.sizepage2 = minpage;
                    }
                    else if (MaxSize == 15)
                    {
                        ViewBag.sizepage3 = minpage;
                    }
                    else if (MaxSize == 20)
                    {
                        ViewBag.sizepage4 = minpage;
                    }
                    else
                    {
                        ViewBag.sizepage5 = minpage;
                    }
                    pageSize = MaxSize;


                }
                return View(await Pagination<GroupUOM>.CreateAsync(Cate, page ?? 1, pageSize));
            }
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    ViewData["CurrentSort"] = sortOrder;
                    ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                    ViewData["CodeSortParm"] = sortOrder == "Code" ? "Code_desc" : "Code";
                    if (searchString != null)
                    {
                        page = 1;
                    }
                    else
                    {
                        searchString = currentFilter;

                    }
                    ViewData["CurrentFilter"] = searchString;

                    //var Cate = from s in data select s;

                    if (!String.IsNullOrEmpty(searchString))
                    {
                        Cate = Cate.Where(s => s.Name.Contains(searchString)
                        || s.Code.Contains(searchString));

                    }
                    switch (sortOrder)
                    {
                        case "name_desc":
                            Cate = Cate.OrderByDescending(s => s.Name);
                            break;
                        case "Code":
                            Cate = Cate.OrderBy(s => s.Code);
                            break;
                        case "Code_desc":
                            Cate = Cate.OrderByDescending(s => s.Code);
                            break;
                        default:
                            Cate = Cate.OrderBy(s => s.Name);
                            break;
                    }
                    //int pageSize = 0, MaxSize = 0;
                    int.TryParse(minpage, out MaxSize);
                    //int t = int.Parse(category);
                    //int t1 = Convert.ToInt16(category);

                    if (MaxSize == 0)
                    {
                        int d = data.Count();
                        if (d == 0) d = 1;
                        pageSize = d;
                        ViewBag.sizepage5 = "All";
                    }
                    else
                    {
                        if (MaxSize == 5)
                        {
                            ViewBag.sizepage1 = minpage;
                        }
                        else if (MaxSize == 10)
                        {
                            ViewBag.sizepage2 = minpage;
                        }
                        else if (MaxSize == 15)
                        {
                            ViewBag.sizepage3 = minpage;
                        }
                        else if (MaxSize == 20)
                        {
                            ViewBag.sizepage4 = minpage;
                        }
                        else
                        {
                            ViewBag.sizepage5 = minpage;
                        }
                        pageSize = MaxSize;


                    }
                    return View(await Pagination<GroupUOM>.CreateAsync(Cate, page ?? 1, pageSize));
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }
            
        }
        public IActionResult Create()
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure Group";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.A_Inventory = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                return View();
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A007");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            }       
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Code,Name,Delete")] GroupUOM groupUOM)
        {

            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure Group";
            ViewBag.type = "create";
            ViewBag.button = "fa-plus-circle";
            ViewBag.A_Inventory = "show";

            if (ModelState.IsValid)
            {
                try
                {
                    await _guom.AddorEditGUOM(groupUOM);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {
                    ViewBag.Error = "This code already exist !";
                    return View(groupUOM);
                }
                
            }
           
            return View(groupUOM);
        }

        public IActionResult Edit(int id)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure Group";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.A_Inventory = "show";
            var userid = 0;
            int.TryParse(User.FindFirst("UserID").Value, out userid);
            if (User.FindFirst("Password").Value == "YdQusX4G7SJ+txRJ2IZYDmx/L+s6SnnI4hQ+PqwCoDl09gtTubaDQiCfqhfDNYVn" && User.FindFirst("Username").Value == "Kernel")
            {
                var groupUOM = _guom.getid(id);
                if (groupUOM.Delete == true)
                {
                    return RedirectToAction(nameof(Index));
                }
                if (groupUOM == null)
                {
                    return NotFound();
                }
                return View(groupUOM);
            }
            var permision = _context.UserPrivilleges.FirstOrDefault(x => x.UserID == userid && x.Code == "A007");
            if(permision!=null)
            {
                if (permision.Used == true)
                {
                    var groupUOM = _guom.getid(id);
                    if (groupUOM.Delete == true)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    if (groupUOM == null)
                    {
                        return NotFound();
                    }
                    return View(groupUOM);
                }
                else
                {
                    return RedirectToAction("AccessDenied", "Account");
                }
            }
            else
            {
                return RedirectToAction("AccessDenied", "Account");
            } 
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Code,Name,Delete")] GroupUOM groupUOM)
        {
            ViewBag.style = "fa fa-cogs";
            ViewBag.Main = "Administrator";
            ViewBag.Page = "Inventory";
            ViewBag.Subpage = "Unit of Measure Group";
            ViewBag.type = "Edit";
            ViewBag.button = "fa-edit";
            ViewBag.A_Inventory = "show";
            if (groupUOM.Code == null)
            {
                ViewBag.required = "Please input code !";
                return View(groupUOM);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    await _guom.AddorEditGUOM(groupUOM);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {
                    ViewBag.Error = "This code already exist !";
                    return View(groupUOM);
                }
            }
            return View(groupUOM);

        }
        [HttpDelete]
        public async Task<IActionResult> deleteGroupUoM(int ID)
        {
           await _guom.DleteGUoM(ID);
            return Ok();
        }
        [HttpGet]
        public IActionResult GetDataDGroup(int ID)
        {
            var list = _guom.GetgroupDUoMs(ID).Where(g => g.GroupUoMID ==ID).ToList();
            return Ok(list);
        }
        [HttpPost]
        public async Task<IActionResult> InsertDGroupUOM(GroupDUoM groupDUoM,int ID)
        {
            await _guom.InsertDGroupUOM(groupDUoM);
            var list = _guom.GetgroupDUoMs(ID).Where(g => g.GroupUoMID == ID).ToList();
            return Ok(list);
        }
        [HttpDelete]
        public async Task<IActionResult> DeleteDefined(int ID,int IDGroup)
        {
              await _guom.DeleteDefinedGroup(ID);

              var list = _guom.GetgroupDUoMs(IDGroup).Where(g => g.GroupUoMID == IDGroup).ToList();
              return Ok(list);
        }
        
        [HttpGet]
        public IActionResult listGroupUom(int id)
        {
            var list = _guom.GetgroupDUoMs(id).Where(x => x.GroupUoMID == id).ToList();
            return Ok(list);
        }
    }
}
