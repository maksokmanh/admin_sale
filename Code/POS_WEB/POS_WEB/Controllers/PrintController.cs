﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Sale.Print;
using POS_WEB.Models.Services.POS;
using POS_WEB.Models.Services.POS.service;
using POS_WEB.Models.Services.Purchase.Print;
using POS_WEB.Models.Services.Responsitory;
using Rotativa.AspNetCore;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POS_WEB.Controllers
{
    public class PrintController : Controller
    {
        // GET: /<controller>/
        private readonly DataContext _context;
        private readonly IReport _report;
        public PrintController(DataContext context, IReport report)
        {
            _context = context;
            _report = report;
        }
        //Summary Sale
        public IActionResult PrintSummarySale(string DateFrom, string DateTo, int BranchID, int UserID)
        {
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var list = _report.GetSummarySales(DateFrom, DateTo, BranchID, UserID).ToList();
            if (list.Count > 0)
            {
                list.First().DateFrom = DateFrom;
                list.First().DateTo = DateTo;
            }
            return new ViewAsPdf(list);
        }

        //Top Sale Quatity
        public IActionResult PrintTopSaleQuantity(string DateFrom, string DateTo, int BranchID)
        {
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            var list = _report.GetTopSaleQuantities(DateFrom, DateTo, BranchID).ToList();
            if (list.Count > 0)
            {
                list.First().DateFrom = DateFrom;
                list.First().DateTo = DateTo;
            }
            return new ViewAsPdf(list);
        }

        //Revenues Item
        public IActionResult PrintRevenuesItem(string DateFrom, string DateTo, int BranchID, int ItemID, string Process)
        {
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";

            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }
            if (BranchID == 0)
            {
                BranchID = Convert.ToInt32(User.FindFirst("BranchID").Value);
            }
            var brand = _context.Branches.FirstOrDefault(x => x.ID == BranchID);

            var list = _report.GetSummaryRevenuesItems(DateFrom, DateTo, BranchID, ItemID, Process).ToList();
            if (list.Count > 0)
            {
                list.First().DateTo = DateTo;
                list.First().DateFrom = DateFrom;
                list.First().Branch = brand.Name;
            }

            return new ViewAsPdf(list);
        }

        //Print cashout
        public IActionResult PrintCashout(int Tran_F, int Tran_T, int UserID, string DateFrom, string DateTo)
        {
            if (DateFrom == null)
            {
                DateFrom = "1900-01-01";
            }
            if (DateTo == null)
            {
                DateTo = "1900-01-01";
            }

            var cashoutreport = _report.GetDetailCloseShif(UserID, Tran_F, Tran_T).ToList();
            if (cashoutreport.Count > 0)
            {
                cashoutreport.First().DateFrom = DateFrom;
                cashoutreport.First().DateTo = DateTo;
            }
            return new ViewAsPdf(cashoutreport);
        }

        //Print PO
        public IActionResult PrintPurchaseOrder(int PurchaseID)
        {
            var list = GetPrintPurchaseOrders(PurchaseID).ToList();
            if (list.Count > 0)
            {
                list.First().PurchasID = PurchaseID;
            }
            return new ViewAsPdf(list);
        }
        private IEnumerable<PrintPurchaseAP> GetPrintPurchaseOrders(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintPurchaseOrder @PurchaseID={0}",
           parameters: new[] {
                 PurchaseID.ToString()
           });

        //Print PrintGoodReceiptPO
        public IActionResult PrintGoodsReceiptPO(int PurchaseID)
        {
            var list = GetPrintGoodReciptPO(PurchaseID).ToList();
            if (list.Count > 0)
            {
                list.First().PurchasID = PurchaseID;
            }
            return new ViewAsPdf(list);
        }
        private IEnumerable<PrintPurchaseAP> GetPrintGoodReciptPO(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintGoodReceiptPO @PurchaseID={0}",
          parameters: new[]{
              PurchaseID.ToString()
          });

        //PrintPurchaseAP
        public IActionResult PrintPurchaseAP(int PurchaseID) 
        {
            var list = GetPrintPurchaseAP(PurchaseID).ToList();
            if(list.Count > 0)
            {
                list.First().PurchasID = PurchaseID;
            }
            return new ViewAsPdf(list);
        }
        private IEnumerable<PrintPurchaseAP> GetPrintPurchaseAP(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintPurchaseAP @PurchaseID={0}",
        parameters: new[]{
            PurchaseID.ToString()
        });
         
        public IActionResult PrintPurchaseCreditMemo(int PurchaseID)
        {
            var list = GitPurchaseCreditMemo(PurchaseID).ToList();
            if (list.Count > 0)
            {
                list.First().PurchasID = PurchaseID;
            }
            return new ViewAsPdf(list);
        }
        private IEnumerable<PrintPurchaseAP> GitPurchaseCreditMemo(int PurchaseID) => _context.PrintPurchaseAPs.FromSql("wd_PrintPurchaseCreditMemo @PurchaseID={0}",
        parameters: new[]{
            PurchaseID.ToString()
        });
        //Print histroy CreditMemo
        public IActionResult SaleCreditMemoHistory(int ID)
        {
            var list = from SC in _context.SaleCreditMemos.Where(m => m.SCMOID == ID)
                       join SCD in _context.SaleCreditMemoDetails on SC.SCMOID equals SCD.SCMOID
                       join BP in _context.BusinessPartners on SC.CusID equals BP.ID
                       join I in _context.ItemMasterDatas on SCD.ItemID equals I.ID
                       join CUR in _context.Currency on SC.LocalCurrencyID equals CUR.ID
                       join B in _context.Branches on SC.BranchID equals B.ID
                       join U in _context.UserAccounts on SC.BranchID equals U.BranchID
                       group new { SC, SCD, BP, I, CUR, B, U } by SCD.SCMODID into g
                       let data = g.FirstOrDefault()
                       let master = data.SC
                       let detail = data.SCD
                       let cu = data.CUR
                       let BP = data.BP
                       let B = data.B
                       let U = data.U
                       select new PrintSaleHistory
                       {
                           //Master
                           ID = g.FirstOrDefault().SC.SCMOID,
                           Invoice = master.InvoiceNo,
                           PostingDate = master.PostingDate.ToString("dd-MM-yyyy"),
                           DocumentDate = master.DocumentDate.ToString("dd-MM-yyyy"),
                           DueDate = master.DueDate.ToString("dd-MM-yyyy"),
                           CusName = BP.Type,
                           Phone = BP.Phone,
                           Address = BP.Address,
                           UserName = U.Username,
                           CusNo = BP.Code,
                           RefNo = master.RefNo,
                           Branch = g.First().B.Name,
                           //Detail
                           ItemCode = detail.ItemCode,
                           ItemNameKh = detail.ItemNameKH,
                           Qty = detail.Qty,
                           Price = detail.UnitPrice,
                           DiscountValue_Detail = detail.DisValue,
                           UomName = detail.UomName,
                           Amount = detail.Qty * detail.UnitPrice,
                           LocalCurrency = cu.Description,
                           //Summary
                           Sub_Total = g.Sum(s => s.SCD.Qty * s.SCD.UnitPrice),
                           DiscountValue = g.Sum(s => s.SCD.DisValue),
                           VatValue = g.Sum(s => s.SCD.VatValue),
                           //  Applied_Amount=g.Sum()
                           TotalAmount = g.Sum(s => s.SC.TotalAmount)
                       };
            //list = null;
            if (list == null) return NotFound();
            //var _list = list.Where(d => d.ID == ID);
            return new ViewAsPdf(list);
        }
        //Print history SaleAR
        public IActionResult SaleARHistory(int ID)
        {
            var list = from SAR in _context.SaleARs.Where(m => m.SARID == ID)
                       join SARD in _context.SaleARDetails on SAR.SARID equals SARD.SARID
                       join BP in _context.BusinessPartners on SAR.CusID equals BP.ID
                       join I in _context.ItemMasterDatas on SARD.ItemID equals I.ID
                       join CUR in _context.Currency on SAR.LocalCurrencyID equals CUR.ID
                       join B in _context.Branches on SAR.BranchID equals B.ID
                       join U in _context.UserAccounts on SAR.BranchID equals U.BranchID
                       group new { SAR, SARD, BP, I, CUR, B, U } by SARD.SARDID into g
                       let data = g.FirstOrDefault()
                       let master = data.SAR
                       let detail = data.SARD
                       let cu = data.CUR
                       let BP = data.BP
                       let B = data.B
                       let U = data.U
                       select new PrintSaleHistory
                       {
                           //Master
                           ID = g.Key,
                           Invoice = master.InvoiceNo,
                           PostingDate = master.PostingDate.ToString("dd-MM-yyyy"),
                           DocumentDate = master.DocumentDate.ToString("dd-MM-yyyy"),
                           DueDate = master.DueDate.ToString("dd-MM-yyyy"),
                           CusName = BP.Type,
                           Phone = BP.Phone,
                           Address = BP.Address,
                           UserName = U.Username,
                           CusNo = BP.Code,
                           RefNo = master.RefNo,
                           Branch = B.Name,
                           //Detail
                           ItemCode = detail.ItemCode,
                           ItemNameKh = detail.ItemNameKH,
                           Qty = detail.Qty,
                           Price = detail.UnitPrice,
                           DiscountValue_Detail = detail.DisValue,
                           UomName = detail.UomName,
                           Amount = detail.Qty * detail.UnitPrice,
                           LocalCurrency = cu.Description,
                           //Summary
                           Sub_Total = g.Sum(s => s.SARD.Qty * s.SARD.UnitPrice),
                           DiscountValue = g.Sum(s => s.SARD.DisValue),
                           VatValue = g.Sum(s => s.SARD.VatValue),
                           //Applied_Amount=g.Sum(s)
                           TotalAmount = g.Sum(s => s.SAR.TotalAmount)
                       };
            if (list == null) return NotFound();
            return new ViewAsPdf(list);
        }
    }       
}
