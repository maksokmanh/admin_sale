﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Responsitory;
using POS_WEB.Models.SignalR;
using Rotativa.AspNetCore;

namespace POS_WEB
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAntiforgery(options => options.HeaderName = "MY-XSRF-TOKEN");
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(24);
                options.Cookie.HttpOnly = false;
                options.IOTimeout = TimeSpan.FromHours(1);
            });
            services.AddSession(options =>
            {
                options.Cookie.Name = ".AdventureWorks.Session";
                options.IdleTimeout = TimeSpan.FromHours(24);
                options.Cookie.IsEssential = true;
            });
            services.AddAuthorization(options =>
            {
                options.AddPolicy("UserOnly", policy => policy.RequireClaim("User"));
                options.AddPolicy("UserOnly", policy => policy.RequireUserName("manager"));
            });
            services.AddSignalR();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            .AddSessionStateTempDataProvider()
            .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix, options => { options.ResourcesPath = "Resources"; })
            .AddDataAnnotationsLocalization()
            .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
            services.AddDbContext<DataContext>(option => option.UseSqlServer(Configuration["UsersConnection:ConnectionString"]));
            services.AddLocalization(option => option.ResourcesPath = "Resources");
            services.AddTransient<IItemGroup, ItemGorup1Responsitory>();
            services.AddTransient<IItem2Group, ItemGroup2Responsitory>();
            services.AddTransient<IItemGroup3, ItemGroup3Responsitory>();
            services.AddTransient<IUOM, UnitofMeasureResponsitory>();
            services.AddTransient<IGUOM, GroupUoMResponsitory>();
            services.AddTransient<IBranch, BranchRepository>();
            services.AddTransient<IBusinessPartner, BusinessPartnerRepository>();
            services.AddTransient<ICompany, CompanyRepository>();
            services.AddTransient<ICurrency, CurrencyRepository>();
            services.AddTransient<IEmployee,EmployeeRepository>();
            services.AddTransient<IExchangeRate, ExcahageRateRepository>();
            services.AddTransient<IGroupTable, GroupTableRepository>();
            services.AddTransient<IItemMasterData,ItemMasterDataRepository>();
            services.AddTransient<IPaymentMean,PaymentMeanRepository>();
            services.AddTransient<IPriceList, PriceListRepository>();
            services.AddTransient<IPriceListDetail,PriceListDetailRepository>();
            services.AddTransient<IPrinterName,PrinterNameRepository>();
            services.AddTransient<IPromotion,PromotionRepository>();
            services.AddTransient<IReceiptInformation,ReceiptInformationRepository>();
            services.AddTransient<ITable,TableRepository>();
            services.AddTransient<ITax,TaxRepository>();
            services.AddTransient<IUserAccount, UserAccountRepositoy>();
            services.AddTransient<IUserPrivillege,UserPrivillegeRepository>();
            services.AddTransient<IWarehouse,WarehouseRepository>();
            services.AddTransient<IWarehouseDetail,WarehouseDetailRepository>();
            services.AddTransient<ICardPoint, CardPointResponsitory>();
            services.AddTransient<ICardType, CardTypeResponsitory>();
            services.AddTransient<IMemberCard, MemberResponsitory>();
            services.AddTransient<IPromitionPrice, PromotionPriceRepository>();
            services.AddTransient<IPurchaseQuotation, PurchaseQuotationResponsitory>();
            services.AddTransient<IPurchaseAP, PurchaseAPResponsitory>();
            services.AddTransient<IPurchaseCreditMemo, PurchaseCreditMemoResponsitory>();
            services.AddTransient<IPurchaseOrder, PurchaseOrderResponsitory>();
            services.AddTransient<IPOS, POSRepository>();
            services.AddTransient<IGoodIssuse, GoodIssuseResponsitory>();
            services.AddTransient<IGoodsReceipt, GoodsReceiptResponsitory>();
            services.AddTransient<IGoodsReceipts, GoodsReceiptsResponsitory>();
            services.AddTransient<ITransfer, TransferResponsitory>();
            services.AddTransient<IOutgoingPayment, OutgoingPaymentResponsitory>();
           
            services.AddTransient<IReport, ReportResponsitory>();
            services.AddTransient<IGoodRecepitPo, GoodReceiptPoResponsitory>();
            services.AddTransient<IGoodsReceiptPoReturn, GoodsReceiptPoReturnResponsitory>();
            services.AddTransient<IPurchaseRequest, PurchaseRquestResponsitory>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddTransient<ISale, SaleRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            var supportCultures = new List<CultureInfo>
            {
                new CultureInfo("zh-CN"),
                new CultureInfo("en-US"),
                new CultureInfo("km-KH")

            };
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                SupportedCultures = supportCultures,
                SupportedUICultures = supportCultures
            });
            app.UseDefaultFiles();
         
            app.UseStaticFiles();
            
            app.UseAuthentication();
            app.UseSession();
          
            app.UseSignalR(routes =>
            {
                routes.MapHub<SignalRClient>("/realbox");
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });
            RotativaConfiguration.Setup(env);
        }
    }
}
