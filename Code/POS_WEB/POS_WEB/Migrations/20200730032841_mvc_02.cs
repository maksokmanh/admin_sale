﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace POS_WEB.Migrations
{
    public partial class mvc_02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "TotalCost",
                schema: "dbo",
                table: "rp_SummaryTotalSale",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalProfit",
                schema: "dbo",
                table: "rp_SummaryTotalSale",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalCost",
                schema: "dbo",
                table: "rp_SummaryTotalSale");

            migrationBuilder.DropColumn(
                name: "TotalProfit",
                schema: "dbo",
                table: "rp_SummaryTotalSale");
        }
    }
}
