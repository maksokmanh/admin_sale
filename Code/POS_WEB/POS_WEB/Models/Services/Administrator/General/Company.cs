﻿using POS_WEB.Models.Services.Inventory.PriceList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.General
{
    [Table("tbCompany" ,Schema ="dbo")]
    public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }        
        [StringLength(50), Required(ErrorMessage = "Please input name !")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Select PriceList !")]
        public int PriceListID { get; set; }
        [StringLength(50), Required(ErrorMessage = "Please input location !")]
        public string Location { get; set; }
        [StringLength(220)]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }    
        [StringLength(25),Required]
        public string Process { get; set; } //FIFO,Average,Standard
        public bool Delete { get; set; }
        //Foreign Key
        public string Logo { get; set; }
        [ForeignKey("PriceListID")]
        public PriceLists PriceList { get; set; }
        
    }
}
