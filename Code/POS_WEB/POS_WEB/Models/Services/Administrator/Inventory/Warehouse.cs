﻿using POS_WEB.Models.Services.Administrator.General;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.Inventory
{
    [Table("tbWarhouse",Schema ="dbo")]
    public class Warehouse
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int BranchID { get; set; }
        [StringLength(50), Required(ErrorMessage = "Please input code")]
        public string Code { get; set; }
        [StringLength(50),Required(ErrorMessage ="Please input name !")]
        public string Name { get; set; }
        public double StockIn { get; set; } = 0;
        [StringLength(50),Required(ErrorMessage = "Please input location !")]
        public string Location { get; set; }
        [StringLength(50), Required(ErrorMessage = "Please input address !")]
        public string Address { get; set; }
        public bool Delete { get; set; }
        [ForeignKey("BranchID")]
        public Branch Branch { get; set; }
    }
}
