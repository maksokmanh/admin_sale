﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Administrator.Inventory
{
    [Table("tbUnitofMeasure",Schema ="dbo")]
    public class UnitofMeasure
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage ="Please input code !")]
        public string Code { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string Name { get; set; }
        public string AltUomName { get; set; }
        public bool Delete { get; set; }
     
    }
}
