﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Banking
{
    [Table("tbPaymentMeans",Schema ="dbo")]
    public class PaymentMeans
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string Type { get; set; }
        public bool Status { get; set; }
        public bool Delete { get; set; }
    }
}
