﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Banking
{
    [Table("tbCurrency",Schema ="dbo")]
    public class Currency
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Please input symbol !")]
        public string Symbol { get; set; }
        public string Description { get; set; }
        public bool Delete { get; set; }
    }
}
