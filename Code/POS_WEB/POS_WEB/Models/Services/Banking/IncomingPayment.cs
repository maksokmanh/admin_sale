﻿using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.HumanResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Banking
{
    [Table("tbIncomingPayment", Schema = "dbo")]
    public class IncomingPayment
    {
        [Key]
        public int IncomingPaymentID { get; set; }
        public int UserID { get; set; }
        public int CustomerID { get; set; }
        public int BranchID { get; set; }
        public string Ref_No { get; set; }
        [Column(TypeName = "Date")]
        public DateTime PostingDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DocumentDate { get; set; }
        public double TotalAmountDue { get; set; }
        public string Number { get; set; }
        public string Remark { get; set; }
        public List<IncomingPaymentDetail> IncomingPaymentDetails { get; set; }

        [ForeignKey("UserID")]
        public UserAccount UserAccount { get; set; }
        [ForeignKey("CustomerID ")]
        public BusinessPartner BusinessPartner { get; set; }
        [ForeignKey("BranchID")]
        public Branch Branch { get; set; }
    }
}
