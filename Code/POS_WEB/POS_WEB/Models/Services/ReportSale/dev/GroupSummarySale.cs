﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.ReportSale.dev
{
    public class GroupSummarySale
    {
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public double SubTotal { get; set; }
        public List<Receipts> Receipts { get; set; }
        public Header Header { get; set; }
        public Footer Footer { get; set; }
    }
    public class Receipts
    {
        public string User { get; set; }
        public string Receipt { get; set; }
        public string DateIn { get; set; }
        public string TimeIn { get; set; }
        public string DateOut { get; set; }
        public string TimeOut { get; set; }
        public double GrandTotal { get; set; }
        public string LocalCurrency { get; set; }
    }
    public class Header
    {
        public string Logo { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Branch { get; set; }
        public string EmpName { get; set; }
        
    }
    public class Footer
    {
        public string CountReceipt { get; set; }
        public string SoldAmount { get; set; }
        public string DiscountItem { get; set; }
        public string DiscountTotal { get; set; }
        public string TaxValue { get; set; }
        public string GrandTotal { get; set; }
        public string GrandTotalSys { get; set; }
    }
}
