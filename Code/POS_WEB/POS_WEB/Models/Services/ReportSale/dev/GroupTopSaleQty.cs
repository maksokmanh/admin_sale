﻿using POS_WEB.Models.Services.Inventory.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.ReportSale.dev
{
    public class GroupTopSaleQty
    {
        //Group
        public string GroupName { get; set; }
        public double SubTotal { get; set; }
        public Header Header { get; set; }
        public List<Items> Items { get; set; } 
        public Footer Footer { get; set; }

    }
    public class Items
    {
        public string ItemID { get; set; }
        public string Barcode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Qty { get; set; }
        public string Uom { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
    }
}
