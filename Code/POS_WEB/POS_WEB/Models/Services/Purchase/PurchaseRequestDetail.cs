﻿using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.HumanResources;
using POS_WEB.Models.Services.Inventory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Purchase
{
    [Table("tbPurchaseRequiredDetail",Schema ="dbo")]
    public class PurchaseRequestDetail
    {
        [Key]
        public int RequiredDetailID { get; set; }
        public int PurchaseRequestID { get; set; }
        public int LineID { get; set; }
        public int ItemID { get; set; }
        public int UomID { get; set; }
        public int VendorID { get; set; }
        public int SystemCurrencyID { get; set; }
        public double OpenQty { get; set; }
        public double Total { get; set; }
        [Column(TypeName = "Date")]
        [DataType(DataType.Date)]
        public DateTime RequiredDate { get; set; }

        public double Qty { get; set; }
        public double UnitPrice { get; set; }
        public double ExchangRate { get; set; }
        public string Remark { get; set; }
        public bool Delete { get; set; }

        [ForeignKey("ItemID")]
        public ItemMasterData ItemMasterData { get; set; }
        [ForeignKey("UomID")]
        public UnitofMeasure UnitofMeasure { get; set; }
        [ForeignKey("SystemCurrencyID")]
        public Currency Currency { get; set; }


    }
}
