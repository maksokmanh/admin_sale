﻿using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Purchase
{
    [Table("tbPurchaseRequest",Schema ="dbo")]
    public class PurchaseRequest
    {
        [Key]
        public int PurchaseRequestID { get; set; }
        public int BranchID { get; set; }
        public int UserID { get; set; }
        public int SystemCurrencyID { get; set; }
        public int WarehoueseID { get; set; }
        public string InvoiceNo { get; set; }
        public List<PurchaseRequestDetail> PurchaseRequestDetails { get; set; }
        [Column(TypeName = "Date")]
        public DateTime PostingDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DocumentDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime RequiredDate { get; set; }   
        [Column(TypeName = "Date")]
        public DateTime ValidUntil { get; set; }
        public double Balance_Due { get; set; }
        public double ExchangeRate { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }

        [ForeignKey("WarehoueseID")]
        public Warehouse Warehouse { get; set; }
        [ForeignKey("BranchID")]
        public Branch Branch { get; set; }
        [ForeignKey("SystemCurrencyID")]
        public Currency Currency { get; set; }
        [ForeignKey("UserID")]
        public UserAccount UserAccount { get; set; }

    }
}
