﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory
{
    public class ItemMasterDataUpdateRow
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string KhmerName { get; set; }
        public string EnglishName { get; set; }
        public string Barcode { get; set; }
        public string Image { get; set; }
        public IFormFileCollection Files { get; set; }
    }
}
