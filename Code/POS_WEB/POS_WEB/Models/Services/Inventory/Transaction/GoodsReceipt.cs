﻿using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory.Transaction
{
    [Table("tbGoodsReceitp",Schema ="dbo")]
    public class GoodsReceipt
    {
        [Key]
        public int GoodsReceiptID { get; set; }
        public int BranchID { get; set; }
        public int UserID { get; set; }
        public int WarehouseID { get; set; }   
        [Column(TypeName = "Date")]
        public DateTime PostingDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DocumentDate { get; set; }
        public string Ref_No { get; set; }
        public string Number_No { get; set; }
        public string Remark { get; set; }
        public List<GoodReceiptDetail> GoodReceiptDetails  { get; set; }
        [ForeignKey("BranchID")]
        public Branch Branch { get; set; }
        [ForeignKey("UserID")]
        public UserAccount UserAcount { get; set; }
        [ForeignKey("WarehouseID")]
        public Warehouse Warehouse { get; set; }
    }
}
