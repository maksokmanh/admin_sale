﻿using POS_WEB.Models.Services.Banking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory.PriceList
{
    [Table("tbPriceList", Schema = "dbo")]
    public class PriceLists
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Please input name !")]
        public string Name { get; set; }
        public bool Delete { get; set; }
        //Foreign Key
        public int CurrencyID { get; set; }
        [ForeignKey("CurrencyID")]
        public Currency Currency { get; set; }       
       
    }
}
