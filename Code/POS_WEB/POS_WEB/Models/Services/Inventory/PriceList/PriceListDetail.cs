﻿
using POS_WEB.Models.Services.Account;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Inventory.PriceList
{
    [Table("tbPriceListDetail", Schema = "dbo")]
    public class PriceListDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int PriceListID { get; set; }
        public int ItemID { get; set; }
        public int UserID { get; set; }
        public int? UomID { get; set; }
        public int CurrencyID { get; set; }
        public double Quantity { get; set; } = 1;
        public float Discount { get; set; } = 0;
        public string TypeDis { get; set; } = "Percent";
        public int PromotionID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",ApplyFormatInEditMode =true)]
        public DateTime ExpireDate { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd-MM-yyyy}",ApplyFormatInEditMode =true)]
        public DateTime SystemDate { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(DataFormatString ="{0:hh:mm-ss}",ApplyFormatInEditMode =true)]
        public DateTime TimeIn { get; set; }
     
        [MinLength(0)]
        public double Cost { get; set; }
        [MinLength(0)]
        public double UnitPrice { get; set; }
        [ForeignKey("PriceListID")]
        public PriceLists PriceLists { get; set; }
    }
}
