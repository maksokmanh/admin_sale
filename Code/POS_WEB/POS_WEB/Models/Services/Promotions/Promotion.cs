﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Promotions
{
    [Table("tbPromotion",Schema ="dbo")]
    public class Promotion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage ="Please input name !")]
        public string Name { get; set; }  
        public string Type { get; set; }
        [Column(TypeName = "Date")]
        [Required(ErrorMessage ="Please input start date !")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "Date")]
        [Required(ErrorMessage ="Please input stop date !")]
        public DateTime StopDate { get; set; }

        public TimeSpan StartTime { get; set; }
        public TimeSpan StopTime { get; set; }
        public bool Active { get; set; }
    }
}
