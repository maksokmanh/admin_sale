﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Promotions
{
    [Table("CardType",Schema ="dbo")]
    public class CardType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage ="Please input name ! ")]
        public string Name { get; set; }
        public double Discount { get; set; }
        public string TypeDis { get; set; }//Percent ,Cash
        public bool Delete { get; set; }
    }
}
