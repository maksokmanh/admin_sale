﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS.service
{
    public class CombineReceipt
    {
        public int TableID { get; set; }
        public int OrderID { get; set; }
        public List<Receipt> Receipts { get; set; }
    }
}
