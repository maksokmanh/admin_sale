﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS.service
{
    public class PrintBill
    {
        public int? OrderID { get; set; }
        public string Logo { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public string Tel1 { get; set; }
        public string Tel2 { get; set; }
        public string Table { get; set; }
        public string OrderNo { get; set; }
        public string ReceiptNo { get; set; }
        public string Cashier { get; set; }
        public string DateTimeIn { get; set; }
        public string DateTimeOut { get; set; }
        //Detail
        public string Item { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string DisItem { get; set; }
        public string Amount { get; set; }
        //Summary
        public string SubTotal { get; set; }
        public string DisRate { get; set; }
        public string DisValue { get; set; }
        public string TypeDis { get; set; }
        public string GrandTotal { get; set; }
        public string GrandTotalSys { get; set; }
        public string VatRate { get; set; }
        public string VatValue { get; set; }
        //Receive
        public string Received { get; set; }
        public string Change { get; set; }
        public string ChangeSys { get; set; }
       
        //Fother
        public string DescKh { get; set; }
        public string DescEn { get; set; }
       
        public string ExchangeRate { get; set; }
      
        public string Printer { get; set; }
        public string Print { get; set; }
        public string ItemDesc { get; set; }
        public string CustomerInfo { get; set; }
        public string Team { get; set; }
        public string ItemType { get; set; }
        public string PaymentMeans { get; set; }

    }
}
