﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS.service
{
    public class ReturnItem
    {
        public int id { get; set; }
        public int item_id { get; set; }
        public int receipt_id { get; set; }
        public string code { get; set; }
        public string kh { get; set; }
        public double openQty { get; set; }
        public double returnQty { get; set; }
        public int user_id { get; set; }
    }
}
