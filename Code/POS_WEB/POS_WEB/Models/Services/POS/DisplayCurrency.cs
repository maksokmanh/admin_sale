﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS
{
    [Table("tbDisplayCurrency",Schema ="dbo")]
    public class DisplayCurrency
    {
        [Key]
        public int ID { get; set; }
        public string BaseCurr { get; set; }//USD
        public string AltCurr { get; set; }//KHR
        public double Rate { get; set; }//4000
    }
}
