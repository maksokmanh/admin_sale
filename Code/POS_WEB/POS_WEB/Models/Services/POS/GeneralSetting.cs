﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS { 
    [Table("tbGeneralSetting", Schema = "dbo")]
    public class GeneralSetting
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int BranchID { get; set; }
        public string Receiptsize { get; set; } 
        public string ReceiptTemplate { get; set; }
        public bool DaulScreen { get; set; }
        public bool PrintReceiptOrder { get; set; }
        public bool PrintReceiptTender { get; set; }
        public int PrintCountReceipt { get; set; }
        public int QueueCount { get; set; }
        public int SysCurrencyID { get; set; }
        public int LocalCurrencyID { get; set; }
        public double RateIn { get; set; }
        public double RateOut { get; set; }
        public string Printer { get; set; }
        public int PaymentMeansID { get; set; }
        public int CompanyID { get; set; }
        public int WarehouseID { get; set; }
        public int CustomerID { get; set; }
        public int PriceListID { get; set; }
        public bool VatAble { get; set; }
        public string VatNum { get; set; }
        public string Wifi { get; set; }
        public string CurrencyDisplay { get; set; }
        public double DisplayRate { get; set; }
        public string MacAddress { get; set; }
        public bool AutoQueue { get; set; } = true;
        public bool PrintLabel { get; set; } = false;

       
    }
}
