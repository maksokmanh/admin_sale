﻿using POS_WEB.Models.Services.Inventory.Category;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS
{
    [Table("tbItemComment",Schema ="dbo")]
    public class ItemComment
    {
        [Key]
        public int ID { get; set; }
        public string Description { get; set; }
       
    }
}
