﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.POS
{
    [Table("tbCloseShift",Schema ="dbo")]
    public class CloseShift
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DateIn { get; set; }
        public string TimeIn { get; set; }
        [Column(TypeName = "Date")]
        public DateTime DateOut { get; set; }
        public string TimeOut { get; set; }
        public int? BranchID { get; set; }
        public int UserID { get; set; }
        public double CashInAmount_Sys { get; set; }
        public double SaleAmount { get; set; }
        public double SaleAmount_Sys { get; set; }
        public string LocalCurrency { get; set; }
        public double CashOutAmount_Sys { get; set; }//input from user cash
        public double ExchangeRate { get; set; }
        public double Trans_From { get; set; }
        public double Trans_To { get; set; }
        public bool Close { get; set; }
    }
}
