﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Inventory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IGoodsReceipt
    {
        IEnumerable<Warehouse> GetWarehouse_From(int ID);
        IEnumerable<GoodReceiptDetail> GetItemByWarehouse_From(int warehouseID);
        IEnumerable<GoodReceiptDetail> GetItemFindeBarcode(int warehouseID,string Barcode);
        void SaveGoodIssues(GoodsReceipt receipt);
    }
    public class GoodsReceiptResponsitory : IGoodsReceipt
    {
        private readonly DataContext _context;
        public GoodsReceiptResponsitory(DataContext context) 
        {
            _context = context;
        }

        public IEnumerable<Warehouse> GetWarehouse_From(int ID) => _context.Warehouses.Where(x => x.Delete == false && x.BranchID==ID).ToList(); 
        public IEnumerable<GoodReceiptDetail> GetItemByWarehouse_From(int warehouseID) => _context.GoodReceiptDetails.FromSql("sp_GetItembyWarehouseFrom_GoodsReceipt @WarehouseID={0}",
            parameters:new[] {
                warehouseID.ToString()
            });

        public void SaveGoodIssues(GoodsReceipt receipt)
        {
            _context.GoodsReceipts.Add(receipt);
            _context.SaveChanges();
            var IssuesID = receipt.GoodsReceiptID;
            IssuesStock(IssuesID);
        }
        public void IssuesStock(int IssuesID)
        {
            _context.Database.ExecuteSqlCommand("sp_GoodsReceiptStock @GoodReceiptID={0}",
                parameters:new[] {
                    IssuesID.ToString()
                });
        }

        public IEnumerable<GoodReceiptDetail> GetItemFindeBarcode(int warehouseID, string Barcode) => _context.GoodReceiptDetails.FromSql("sp_FindBarcodeGoodReceipt @WarehouseID={0},@Barcode={1}",
            parameters:new[] {
                warehouseID.ToString(),
                Barcode.ToString()
            });
       
    }
}
