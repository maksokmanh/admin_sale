﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.InventoryAuditReport;
using POS_WEB.Models.Services.ReportDashboard;
using POS_WEB.Models.Services.ReportInventory;
using POS_WEB.Models.Services.ReportPurchase;
using POS_WEB.Models.Services.ReportSale;
using POS_WEB.Models.Services.ReportSale.dev;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IReport
    {
        IEnumerable<SummarySale> GetSummarySales(string DateFrom, string DateTo, int BranchID, int UserID);
        IEnumerable<DetailSale> GetDetailSales(int OrderID);
        IEnumerable<SummaryPurchaseAP> GetSummaryPurchaseAPs(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID, int VendorID);
        IEnumerable<DetailPurchaseAp> GetDetailPurchaseAps(int PurchaseID);
        IEnumerable<ReportCloseShft> GetReportCloseShfts(string DateFrom, string DateTo, int BranchID, int UserID);
        IEnumerable<SummarySale> GetDetailCloseShif(int UserID, int Tran_From, int Tran_To);
        IEnumerable<TopSaleQuantity> GetTopSaleQuantities(string DateFrom, string DateTo, int BranchID);
        IEnumerable<DetailTopSaleQty> DetailTopSaleQties(int ItemID,int UomID,string DateFrom,string DateTo,int BranchID);
        IEnumerable<DetailPurchaseAp> GetDetailPurchaseMemo(int PurchaseID);
        IEnumerable<SummaryPurchaseAP> GetPruchaseMemoSummary(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID, int VendorID);
        IEnumerable<DashboardSaleSummary> GetDashboardSaleSummary(int BranchID);
        IEnumerable<DashboardSaleSummary> GetDashboardPurchaseSummary(int BranchID);
        IEnumerable<SummaryPurchaseAP> GetPruchaseOrderSummary(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID, int VendorID);
        IEnumerable<DetailPurchaseAp> GetDetailPurchaseOrder(int PurchaseID);
        IEnumerable<StockInWarehouse> GetStockInWarehouses(int WarehouseID, string Process,int ItemID);
        IEnumerable<StockInWarehouse> GetStockAudit();
        IEnumerable<StockInWarehouse_Detail> GetStockInWarehouse_Details(int ItemID, string Process,int WarehouseID);
        IEnumerable<ServiceInventoryAudit> GetServiceInventoryAudits(int ItemID, int WarehouseID);
        IEnumerable<SummaryOutgoingPayment> GetSummaryOutgoings(string DateFrom, string DateTo, int BranchID, int UserID,int VendorID);
        IEnumerable<SummaryDetailOutgoingPayment> GetDetailOutgoingPayments(int OutID);
        IEnumerable<SummaryDetailOutgoingPayment> GetDetailInvoice(string Invoice);
        IEnumerable<SummaryTransferStock> GetSummaryTransferStocks(string DateFrom, string DateTo, int FromBranchID, int ToBranchID, int FromWarehouseID, int ToWarehouseID, int UserID);
        IEnumerable<SummaryDetaitTransferStock> GetSummaryDetaitTransferStocks(int TranID);
        IEnumerable<SummaryTransferStock> GetSummaryGoodsReceiptStock(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID);
        IEnumerable<SummaryDetaitTransferStock> GetSummaryDetailGoodsReceipt(int ReceiptID);
        IEnumerable<SummaryTransferStock> GetSummaryGoodsIssuseStock(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID);
        IEnumerable<SummaryDetaitTransferStock> GetSummaryDetailGoodsIssuse(int IssuseID);
        IEnumerable<SummaryRevenuesItem> GetSummaryRevenuesItems(String DateFrom, string DateTo, int BranchID, int ItemID, string Process);
        IEnumerable<SummaryRevenuesItem> GetSummaryRevenuesItemsDetail(String DateFrom, string DateTo, int BranchID, int ItemID, string Process);
        IEnumerable<DashboardTopSale> GetDashboardTopSale { get; }
        IEnumerable<DashboardTopSale> GetDashboardStockInwaerhose { get; }

        IEnumerable<CashoutReport> GetCashoutReport(double Tran_F, double Tran_To, int UserID);
    }
    public class ReportResponsitory:IReport
    {
        private DataContext _context;

        public IEnumerable<DashboardTopSale> GetDashboardTopSale => _context.DashboardTopsale.FromSql("db_TopSale");

        public IEnumerable<DashboardTopSale> GetDashboardStockInwaerhose => _context.DashboardTopsale.FromSql("db_GetStockInwarehouse");

        public ReportResponsitory(DataContext context)
        {
            _context = context;
        }
        public IEnumerable<DetailSale> GetDetailSales(int OrderID) => _context.DetailSales.FromSql("rp_GetDetailSale @OrderID={0}",
          parameters: new[] {
                OrderID.ToString()
          });


        public IEnumerable<SummarySale> GetSummarySales(string DateFrom, string DateTo, int BranchID, int UserID) => _context.SummarySales.FromSql("rp_GetSummarrySale @BranchId={0},@DateT={1},@DateF={2},@UserId={3}",
            parameters: new[] {

                BranchID.ToString(),
                DateTo.ToString(),
                DateFrom.ToString(),
                UserID.ToString()
            });
        public IEnumerable<DetailPurchaseAp> GetDetailPurchaseAps(int PurchaseID) => _context.DetailPurchaseAps.FromSql("rp_DatailPruchaseAP @PurchaseID={0}",
          parameters: new[] {
                PurchaseID.ToString()
          });


        public IEnumerable<SummaryPurchaseAP> GetSummaryPurchaseAPs(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID, int VendorID) => _context.SummaryPurchaseAPs.FromSql("rp_SummaryPruchaseAP @DateF={0},@DateT={1},@BranchID={2},@WarehousID={3},@UserID={4},@VendorID={5}",
           parameters: new[] {
               DateFrom.ToString(),
               DateTo.ToString(),
               BranchID.ToString(),
               WarehouseID.ToString(),
               UserID.ToString(),
               VendorID.ToString()
           });
        public IEnumerable<ReportCloseShft> GetReportCloseShfts(string DateFrom, string DateTo, int BranchID, int UserID) => _context.ReportCloseShfts.FromSql("rp_GetCloseShift @BranchId={0},@DateF={1},@DateT={2},@UserId={3}",
          parameters: new[] {
                BranchID.ToString(),
                DateFrom.ToString(),
                DateTo.ToString(),
                UserID.ToString()
          });

        public IEnumerable<SummarySale> GetDetailCloseShif(int UserID, int Tran_From, int Tran_To) => _context.SummarySales.FromSql("rp_GetDetailCloseShift @UserID={0},@Tran_From={1},@Tran_To={2}",
                parameters: new[] {
                    UserID.ToString(),
                    Tran_From.ToString(),
                    Tran_To.ToString()
                });

        public IEnumerable<TopSaleQuantity> GetTopSaleQuantities(string DateFrom, string DateTo, int BranchID) => _context.TopSaleQuantities.FromSql("rp_GetTopSaleQauntity @BranchId={0},@DateF={1},@DateT={2}",
            parameters:new[] {
                BranchID.ToString(),
                DateFrom.ToString(),
                DateTo.ToString()
            });

        public IEnumerable<DetailTopSaleQty> DetailTopSaleQties(int ItemID, int UomID, string DateFrom, string DateTo, int BranchID) => _context.DetailTopSaleQties.FromSql("rp_GetTopSaleQauntityDetail @BranchId={0},@DateF={1},@DateT={2},@ItemID={3},@UomID={4}",
            parameters:new[] {
                BranchID.ToString(),
                DateFrom.ToString(),
                DateTo.ToString(),
                ItemID.ToString(),
                UomID.ToString()
            });

        public IEnumerable<SummaryPurchaseAP> GetPruchaseMemoSummary(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID, int VendorID) => _context.SummaryPurchaseAPs.FromSql("rp_SummaryPruchaseMemo @DateF={0},@DateT={1},@BranchID={2},@WarehousID={3},@UserID={4},@VendorID={5}",
            parameters:new[] {
                DateFrom.ToString(),
                DateTo.ToString(),
                BranchID.ToString(),
                WarehouseID.ToString(),
                UserID.ToString(),
                VendorID.ToString()
                });

        public IEnumerable<DetailPurchaseAp> GetDetailPurchaseMemo(int PurchaseID) => _context.DetailPurchaseAps.FromSql("rp_DatailPruchasMemo @PurchaseID={0}",
            parameters:new[] {
                PurchaseID.ToString()
            });

        public IEnumerable<DashboardSaleSummary> GetDashboardSaleSummary(int BranchID) => _context.DashboardSaleSummary.FromSql("db_GetSaleSummary @BranchID={0}",
            parameters:new[] {
                BranchID.ToString()
            });

        public IEnumerable<DashboardSaleSummary> GetDashboardPurchaseSummary(int BranchID) => _context.DashboardSaleSummary.FromSql("db_GetPurchaseSummary @BranchID={0}",
            parameters:new[] {
                BranchID.ToString()
            });


        public IEnumerable<SummaryPurchaseAP> GetPruchaseOrderSummary(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID, int VendorID) => _context.SummaryPurchaseAPs.FromSql("rp_SummaryPruchaseOrder @DateF={0},@DateT={1},@BranchID={2},@WarehousID={3},@UserID={4},@VendorID={5}",
            parameters:new[] {

                DateFrom.ToString(),
                DateTo.ToString(),
                BranchID.ToString(),
                WarehouseID.ToString(),
                UserID.ToString(),
                VendorID.ToString()
            });

        public IEnumerable<DetailPurchaseAp> GetDetailPurchaseOrder(int PurchaseID) => _context.DetailPurchaseAps.FromSql("rp_DatailPruchaseOrder @PurchaseID={0}",
            parameters:new[] {
                PurchaseID.ToString()
            });

        public IEnumerable<StockInWarehouse> GetStockAudit() => _context.StockInWarehouses.FromSql("rp_GetItemStockAudit");
           
        public IEnumerable<StockInWarehouse> GetStockInWarehouses(int WarehouseID, string Process, int ItemID) => _context.StockInWarehouses.FromSql("rp_GetItemInWarehouse @Process={0},@WarehouseID={1},@ItemID={2}",
           parameters: new[] {
                Process.ToString(),
                WarehouseID.ToString(),
                ItemID.ToString()
           });
        public IEnumerable<StockInWarehouse_Detail> GetStockInWarehouse_Details(int ItemID, string Process,int WarehouseID) => _context.StockInWarehouse_Details.FromSql("rp_GetItemInWarehouse @Process={0},@WarehouseID={1},@ItemID={2}",
            parameters:new[] {
                Process.ToString(),
                WarehouseID.ToString(),
                ItemID.ToString()
            });

        public IEnumerable<ServiceInventoryAudit> GetServiceInventoryAudits(int ItemID, int WarehouseID) => _context.ServiceInventoryAudits.FromSql("sp_GetInventoryAudit @ItemID={0},@WarehouseID={1}",
            parameters:new[] {
                ItemID.ToString(),
                WarehouseID.ToString()
            });

        public IEnumerable<SummaryOutgoingPayment> GetSummaryOutgoings(string DateFrom, string DateTo, int BranchID, int UserID, int VendorID) => _context.SummaryOutgoingPayments.FromSql("rp_GetOutgoingPayment @DateFrom={0},@DateTo={1},@BranchID={2},@UserID={3},@VendorID={4}",
            parameters:new[] {
                DateFrom.ToString(),
                DateTo.ToString(),
                BranchID.ToString(),
                UserID.ToString(), 
                VendorID.ToString()
            });

        public IEnumerable<SummaryDetailOutgoingPayment> GetDetailOutgoingPayments(int OutID) => _context.SummaryDetailOutgoingPayments.FromSql("rp_GetDetailOutgoingPayment @OutID={0}",
            parameters:new[] {
                OutID.ToString()
            });

        public IEnumerable<SummaryDetailOutgoingPayment> GetDetailInvoice(string Invoice) => _context.SummaryDetailOutgoingPayments.FromSql("rp_GetDetail_Invoice_outgoingpayment @Invoice={0}",
            parameters:new[] {
                Invoice.ToString()
            });

        public IEnumerable<SummaryTransferStock> GetSummaryTransferStocks(string DateFrom, string DateTo, int FromBranchID, int ToBranchID, int FromWarehouseID, int ToWarehouseID, int UserID) => _context.SummaryTransferStocks.FromSql("rp_GetSummaryTransferStock @DateFrom={0},@DateTo={1},@FromBranchID={2},@ToBranchID={3},@FromWarehouseID={4},@ToWarehouseID={5},@UserID={6}",
            parameters:new[] {
                DateFrom.ToString(),
                DateTo.ToString(),
                FromBranchID.ToString(),
                ToBranchID.ToString(),
                FromWarehouseID.ToString(),
                ToWarehouseID.ToString(),
                UserID.ToString()
            });


        public IEnumerable<SummaryDetaitTransferStock> GetSummaryDetaitTransferStocks(int TranID) => _context.SummaryDetaitTransferStocks.FromSql("rp_GetSummaryDetailTransferStock @TranID={0}",
            parameters:new[] {
                TranID.ToString()
            });

        public IEnumerable<SummaryTransferStock> GetSummaryGoodsReceiptStock(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID) => _context.SummaryTransferStocks.FromSql("rp_GetSummaryGoodsRecepitStock @DateFrom={0},@DateTo={1},@BranchID={2},@WarehouseID={3},@UserID={4}",
            parameters:new[] {
                DateFrom.ToString(),
                DateTo.ToString(),
                BranchID.ToString(),
                WarehouseID.ToString(),
                UserID.ToString()
            });


        public IEnumerable<SummaryDetaitTransferStock> GetSummaryDetailGoodsReceipt(int ReceiptID) => _context.SummaryDetaitTransferStocks.FromSql("rp_GetDetailGoodsRecipt @ReceiptID={0}",
          parameters:new[] {
              ReceiptID.ToString()
          });

        public IEnumerable<SummaryTransferStock> GetSummaryGoodsIssuseStock(string DateFrom, string DateTo, int BranchID, int WarehouseID, int UserID)=> _context.SummaryTransferStocks.FromSql("rp_GetSummaryGoodsIssuse @DateFrom={0},@DateTo={1},@BranchID={2},@WarehouseID={3},@UserID={4}",
            parameters:new[] {
                DateFrom.ToString(),
                DateTo.ToString(),
                BranchID.ToString(),
                WarehouseID.ToString(),
                UserID.ToString()
       });

        public IEnumerable<SummaryDetaitTransferStock> GetSummaryDetailGoodsIssuse(int IssuseID) => _context.SummaryDetaitTransferStocks.FromSql("rp_GetDetailGoodsIssuse @IssuseID={0}",
            parameters:new[] {
                  IssuseID.ToString()
            });

        public IEnumerable<SummaryRevenuesItem> GetSummaryRevenuesItems(string DateFrom, string DateTo, int BranchID, int ItemID, string Process) => _context.SummaryRevenuesItems.FromSql("rp_GetRevenuesItems @Process={0},@Branch={1},@DateF={2},@DateT={3},@ItemID={4}",
            parameters:new[] {
                Process.ToString(),
                BranchID.ToString(),
                DateFrom.ToString(),
                DateTo.ToString(),
                ItemID.ToString()
            });
        

        public IEnumerable<SummaryRevenuesItem> GetSummaryRevenuesItemsDetail(string DateFrom, string DateTo, int BranchID, int ItemID, string Process) => _context.SummaryRevenuesItems.FromSql("rp_GetRevenuesItems @Process={0},@Branch={1},@DateF={2},@DateT={3},@ItemID={4}",
            parameters: new[] {
                Process.ToString(),
                BranchID.ToString(),
                DateFrom.ToString(),
                DateTo.ToString(),
                ItemID.ToString()
            });
        public IEnumerable<CashoutReport> GetCashoutReport(double Tran_F, double Tran_T, int UserID) => _context.CashoutReport.FromSql("rp_GetCashoutReport @Tran_F={0},@Tran_T={1},@UserID={2}",
          parameters: new[] {
                Tran_F.ToString(),
                Tran_T.ToString(),
                UserID.ToString()
            });
    }
}
