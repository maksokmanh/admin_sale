﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.ClassCopy;
using POS_WEB.Models.ReportClass;
using POS_WEB.Models.Services.Administrator.General;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.Banking;
using POS_WEB.Models.Services.Inventory.PriceList;
using POS_WEB.Models.ServicesClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    public interface IGoodRecepitPo
    {
        IEnumerable<Company> GetCurrencyDefualt();
        IEnumerable<GroupDUoM> GetAllGroupDefind();
        IEnumerable<ServiceMapItemMasterDataPurchasAP> serviceMapItemMasterDataPurchasPO(int ID);
        IEnumerable<ServiceMapItemMasterDataPurchasAP> FindItemBarcode(int WarehouseID, string Barcode);
        IEnumerable<ExchangeRate> GetExchangeRates(int ID);
        void GoodReceiptStockPO(int purchaseID,string Type);
        IEnumerable<ServiceMapItemMasterDataPurchasAP> ServiceMapItemMasterDataGoodReceiptPOs_Detail(int warehouseid, string invoice);
        IEnumerable<ReportPurchaseOrder> GetAllPruchaseOrder(int BranchID);
        IEnumerable<PurchaseAP_To_PurchaseMemo> GetPurchaseAP_From_PurchaseOrder(int ID, string Invoice);
        IEnumerable<ReportPurchaseAP> GetReportGoodReceiptPO(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeDate, string Check);
    }
    public class GoodReceiptPoResponsitory:IGoodRecepitPo
    {
        private readonly DataContext _context;
        public GoodReceiptPoResponsitory(DataContext context)
        {
            _context = context;
        }
        public IEnumerable<Company> GetCurrencyDefualt()
        {
            IEnumerable<Company> list = (
                from pd in _context.PriceLists.Where(x => x.Delete == false)
                join com in _context.Company.Where(x => x.Delete == false) on
                pd.ID equals com.PriceListID
                join cur in _context.Currency.Where(x => x.Delete == false)
                on pd.CurrencyID equals cur.ID
                select new Company
                {
                    PriceList = new PriceLists
                    {
                        Currency = new Currency
                        {
                            Description = cur.Description
                        }
                    }
                }

                );
            return list;
        }
        public IEnumerable<GroupDUoM> GetAllGroupDefind()
        {
            var uom = _context.UnitofMeasures.Where(u => u.Delete == false);
            var guom = _context.GroupUOMs.Where(g => g.Delete == false);
            var duom = _context.GroupDUoMs.Where(o => o.Delete == false);
            var list = (from du in duom
                        join g_uom in guom on du.GroupUoMID equals g_uom.ID
                        join buo in uom on du.BaseUOM equals buo.ID
                        join auo in uom on du.AltUOM equals auo.ID
                        where g_uom.Delete == false
                        select new GroupDUoM
                        {
                            ID = du.ID,
                            GroupUoMID = du.GroupUoMID,
                            UoMID = du.UoMID,
                            AltQty = du.AltQty,
                            BaseUOM = du.BaseUOM,
                            AltUOM = du.AltUOM,
                            BaseQty = du.BaseQty,
                            Factor = du.Factor,

                            UnitofMeasure = new UnitofMeasure
                            {
                                ID = auo.ID,
                                Name = buo.Name,
                                AltUomName = auo.Name
                            },

                        }
                );
            return list;
        }

        public IEnumerable<ServiceMapItemMasterDataPurchasAP> serviceMapItemMasterDataPurchasPO(int ID) => _context.ServiceMapItemMasterDataPurchasAPs.FromSql("sp_GetListItemMasterDataGoodReceiptPO @WarehouseID={0}",
            parameters:new[] {
                ID.ToString()
            });
        public IEnumerable<ServiceMapItemMasterDataPurchasAP> FindItemBarcode(int WarehouseID, string Barcode) => _context.ServiceMapItemMasterDataPurchasAPs.FromSql("sp_FindBarcodePurchaseAP @WarehouseID={0},@Barcode={1}",
        parameters: new[] {
               WarehouseID.ToString(),
               Barcode.ToString()
        });

        public IEnumerable<ExchangeRate> GetExchangeRates(int ID)
        {
            IEnumerable<ExchangeRate> list = from ex in _context.ExchangeRates.Where(x => x.Delete == false)
                                             join
                                            cur in _context.Currency.Where(x => x.Delete == false) on
                                            ex.CurrencyID equals cur.ID
                                             where cur.ID == ID
                                             select new ExchangeRate
                                             {
                                                 Rate = ex.Rate,
                                                 ID = ex.ID,
                                                 CurrencyID = ex.CurrencyID,
                                                 Currency = new Currency
                                                 {
                                                     Description = cur.Description
                                                 }
                                             };
            return list;
        }
        public void GoodReceiptStockPO(int purchaseID,string Type)
        {
            _context.Database.ExecuteSqlCommand("sp_GoodReceiptStockGoodReceiptPO @purchaseID={0},@Type={1}",
                parameters: new[] {
                   purchaseID.ToString(),
                   Type
                });
        }
        public IEnumerable<ServiceMapItemMasterDataPurchasAP> ServiceMapItemMasterDataGoodReceiptPOs_Detail(int warehouseid, string invoice) => _context.ServiceMapItemMasterDataPurchasAPs.FromSql("sp_GetListItemMasterDataGoodReceiptPO_Detail @WarehouseID={0},@InvoiceNo={1}",
          parameters: new[] {
                warehouseid.ToString(),
                invoice.ToString()
          });
        public IEnumerable<ReportPurchaseOrder> GetAllPruchaseOrder(int BranchID) => _context.ReportPurchaseOrders.FromSql("sp_GetAllPurchaeOrder @BranchID={0}",
           parameters: new[] {
                BranchID.ToString()
           });
        public IEnumerable<PurchaseAP_To_PurchaseMemo> GetPurchaseAP_From_PurchaseOrder(int ID, string Invoice) => _context.PurchaseAP_To_PurchaseMemos.FromSql("sp_GetPurchaseAP_form_PurchaseOrder @PurchaseOrderID={0},@Invoice={1}",
           parameters: new[] {
                ID.ToString(),
                Invoice.ToString()
           });
        public IEnumerable<ReportPurchaseAP> GetReportGoodReceiptPO(int BranchID, int WarehouseID, string PostingDate, string DocumentDate, string DeDate, string Check) => _context.ReportPurchaseAPs.FromSql("sp_ReportGoodReceiptPO @BranchID={0},@warehouseID={1},@PostingDate={2},@DueDate={3},@DocumentDate={4},@Check={5}",
         parameters: new[] {
                BranchID.ToString(),
                WarehouseID.ToString(),
                PostingDate.ToString(),
                DocumentDate.ToString(),
                DeDate.ToString(),
                Check.ToString()
         });
    }
}
