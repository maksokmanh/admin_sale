﻿using Microsoft.EntityFrameworkCore;
using POS_WEB.AppContext;
using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.POS.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.Responsitory
{
    //------------------------------------------------// start Interface Sale //--------------  
    public interface ISale
    {
        IEnumerable<ServiceItemSales> GetItemMaster(int PLID);
        IEnumerable<GroupDUoM> GetAllGroupDefind();
    }
    //----------------------------------------------// end Interface sale // ----------------

    //--------------------------------------------- // Start Responsitory // ---------------
    public class SaleRepository : ISale
    {
        private readonly DataContext _context;
        public SaleRepository(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<ServiceItemSales> GetItemMaster(int PLID) => _context.ServiceItemSales.FromSql("sale_GetItem @PricelistID={0}",
            parameters: new[] {
                PLID.ToString()
            });

        public IEnumerable<GroupDUoM> GetAllGroupDefind()
        {
            var uom = _context.UnitofMeasures.Where(u => u.Delete == false);
            var guom = _context.GroupUOMs.Where(g => g.Delete == false);
            var duom = _context.GroupDUoMs.Where(o => o.Delete == false);
            var list = (from du in duom
                        join g_uom in guom on du.GroupUoMID equals g_uom.ID
                        join buo in uom on du.BaseUOM equals buo.ID
                        join auo in uom on du.AltUOM equals auo.ID
                        where g_uom.Delete == false
                        select new GroupDUoM
                        {
                            ID = du.ID,
                            GroupUoMID = du.GroupUoMID,
                            UoMID = du.UoMID,
                            AltQty = du.AltQty,
                            BaseUOM = du.BaseUOM,
                            AltUOM = du.AltUOM,
                            BaseQty = du.BaseQty,
                            Factor = du.Factor,

                            UnitofMeasure = new UnitofMeasure
                            {
                                ID = auo.ID,
                                Name = buo.Name,
                                AltUomName = auo.Name
                            },

                        }
                );
            return list;
        }

    }
    //----------------------------------------------// End Responsitory // ------------------
}
