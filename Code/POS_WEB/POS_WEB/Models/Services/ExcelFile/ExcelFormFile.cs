﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Services.ExcelFile
{
    public class ExcelFormFile
    {
        public IFormFile FormFile { get; set; }
    }
}
