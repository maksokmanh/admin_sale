﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Sale
{
    [Table("tbSaleDeliveryDetail")]
    public class SaleDeliveryDetail
    {
        [Key]
        public int SDDID { get; set; }
        public int SDID { get; set; }
        public int SQDID { get; set; }
        public int SODID { get; set; }
        public int ItemID { get; set; }
        public string ItemCode { get; set; }
        public string ItemNameKH { get; set; }
        public string ItemNameEN { get; set; }
        public double Qty { get; set; }

        public double OpenQty { get; set; }
        public double PrintQty { get; set; }
        public int GUomID { get; set; }
        public int UomID { get; set; }
        public string UomName { get; set; }
        public double Cost { get; set; } = 0;
        public double UnitPrice { get; set; }
        public double DisRate { get; set; }
        public double DisValue { get; set; }
        public string TypeDis { get; set; }
        public double VatRate { get; set; }
        public double VatValue { get; set; }
        public double Total { get; set; }
        public double Total_Sys { get; set; }
        public int CurrencyID { get; set; }

        public DateTime ExpireDate { get; set; }
        public string ItemType { get; set; }
        public string Remarks { get; set; }
        public bool Delete { get; set; }
    }
}
