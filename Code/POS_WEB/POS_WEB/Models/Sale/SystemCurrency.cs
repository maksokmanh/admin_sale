﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Sale
{
    public class SystemCurrency
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }
}
