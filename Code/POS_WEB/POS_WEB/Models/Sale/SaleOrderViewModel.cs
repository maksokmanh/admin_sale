﻿using POS_WEB.Models.Services.Administrator.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Sale
{
    public class SaleOrderViewModel
    {
        public SaleOrder SaleOrder { get; set; }
        public Warehouse Warehouse { get; set; }
    }
}
