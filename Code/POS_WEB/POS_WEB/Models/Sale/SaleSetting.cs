﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Sale
{
    public enum SaleCopyType { None = 0, Quotation = 1, Order = 2, Delivery = 3, AR = 4 };
}
