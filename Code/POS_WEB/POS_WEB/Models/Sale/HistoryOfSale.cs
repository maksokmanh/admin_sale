﻿using POS_WEB.Models.Services.Administrator.Inventory;
using POS_WEB.Models.Services.HumanResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POS_WEB.Models.Sale
{
    public enum HistoryOfSaleType { Quotation = 1, Order = 2, Delivery = 3, AR = 4, CreditMemo = 5 }
    public enum SaleDateType { PosingDate = 1, DocumentDate = 2, DueDate = 3 }
    
    public class HistoryOfSale
    {
        public int ID { get; set; }
        public string InvoiceNo { get; set; }
        public string CustomerName { get; set; }
        public string UserName { get; set; }
        public string BalanceDueLC { get; set; }
        public string BalanceDueSC { get; set; }
        public string ExchangeRate { get; set; }
        public string PostingDate { get; set; }
        public string DocumentDate { get; set; }     
        public string DueDate { get; set; }
        public string Status { get; set; }   
        public int CustomerID { get; set; }
        public int WarehouseID { get; set; }
    }

    public class HistoryOfSaleViewModel
    {
        public List<BusinessPartner> Customers { get; set; }
        public List<Warehouse> Warhouses { get; set; }
        public List<HistoryOfSale> SaleHistories { get; set; }
    }

    public class HistoryOfSaleFilter
    {
        public int Customer { get; set; }
        public int Warehouse { get; set; }
        public DateTime DateFrom { get; set; }     
        public DateTime DateTo { get; set; }
        public SaleDateType DateType { get; set; }
        public HistoryOfSaleType SaleType { get; set; }
    }
}
